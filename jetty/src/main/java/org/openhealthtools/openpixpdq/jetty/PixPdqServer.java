/**
 *  Copyright (c) 2009-2011 Misys Open Source Solutions (MOSS) and others
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *  implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 *  Contributors:
 *    Misys Open Source Solutions - initial API and implementation
 *    -
 */

package org.openhealthtools.openpixpdq.jetty;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.mortbay.jetty.Connector;
import org.mortbay.jetty.Server;
import org.mortbay.jetty.bio.SocketConnector;
import org.mortbay.jetty.security.SslSocketConnector;
import org.mortbay.jetty.webapp.WebAppContext;
import org.openhealthtools.openexchange.config.BootStrapProperties;
import org.openhealthtools.openexchange.config.ConfigurationException;
import org.openhealthtools.openexchange.config.PropertyFacade;
import org.openhealthtools.openexchange.utils.CipherSuitesUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * This class manages the stand alone PIXPDQ server startup.
 *
 * @author <a href="mailto:wenzhi.li@misys.com">Wenzhi Li</a>
 */
public class PixPdqServer {

    private static Log log = LogFactory.getLog(PixPdqServer.class);
    private Server server = null;

    public PixPdqServer() {
    }

    /**
     * The main method to start up the PIXPDQ server.
     */
    public static void main(String[] args) {
        try {
            String[] propertyFiles = BootStrapProperties.getPropertyFiles(new String[]{"jetty.properties"});
            PropertyFacade.loadProperties(propertyFiles);
        } catch (ConfigurationException e) {
            log.error("Failed to load OpenPIXPDQ jetty properties", e);
        }

        PixPdqServer server = new PixPdqServer();
        try {
            server.startContainer();
        } catch (Exception e) {
            log.error("Failed to start jetty container", e);
            System.exit(1);
        }
        Runtime.getRuntime().addShutdownHook(new ShutdownHook(server));
    }


    protected void startContainer() throws Exception {
        // Create and Start Jetty Container
        server = new Server();

        List<Connector> connectors = new ArrayList<Connector>();
        Connector connector = createNonSecureConnector();
        connectors.add(connector);

        Connector secureConnector = createSecureConnector();
        connectors.add(secureConnector);

        server.setConnectors(connectors.toArray(new Connector[connectors.size()]));

        String context = PropertyFacade.getString("context.path");
        String root = PropertyFacade.getString("webapp.root");

        WebAppContext webappContext = new WebAppContext(root, "/" + context);
        server.setHandler(webappContext);

        server.start();

    }

    private Connector createNonSecureConnector() {
        Connector connector = new SocketConnector();
        String hostname = PropertyFacade.getString("host", "localhost");

        int port = PropertyFacade.getInteger("port");

        connector.setHost(hostname);
        connector.setPort(port);

        return connector;
    }

    private Connector createSecureConnector() {
        String hostname = PropertyFacade.getString("host", "localhost");
        int tlsPort = PropertyFacade.getInteger("tls.port");
        String keyStore = PropertyFacade.getString("key.store");
        String keyPassword = PropertyFacade.getString("key.password");
        String trustStore = PropertyFacade.getString("trust.store");
        String trustPassword = PropertyFacade.getString("trust.password");

        SslSocketConnector secureconnector = new SslSocketConnector();
        secureconnector.setHost(hostname);
        secureconnector.setPort(tlsPort);
        secureconnector.setKeystore(keyStore);
        secureconnector.setKeyPassword(keyPassword);
        secureconnector.setTruststore(trustStore);
        secureconnector.setPassword(trustPassword);
        secureconnector.setExcludeCipherSuites(getExcludeCipherSuites());
        return secureconnector;
    }

    public void stop() throws Exception {
        if (server != null && server.isStarted()) {
            try {
                server.stop();
            } catch (Exception e) {
                log.error("Failed to stop jetty container.", e);
            }
        }
    }

    private static class ShutdownHook extends Thread {
        private PixPdqServer server = null;

        ShutdownHook(PixPdqServer server) {
            this.server = server;
        }

        public void run() {
            try {
                this.server.stop();
            } catch (Exception e) {
                log.error("Failed to stop PIXPDQ Server.", e);
            }
        }
    }

    private String[] getExcludeCipherSuites() {
        String[] supportedCiperSuites = PropertyFacade.getStringArray("https.cipher.suites");
        List<String> includes = Arrays.asList(supportedCiperSuites);

        List<String> excludes = new ArrayList<String>();
        List<String> allCipherSuites = Arrays.asList(CipherSuitesUtil.allCipherSuites);
        for (String cipherSuite : allCipherSuites) {
            if (!includes.contains(cipherSuite)) {
                excludes.add(cipherSuite);
            }
        }

        return excludes.toArray(new String[excludes.size()]);
    }
}
