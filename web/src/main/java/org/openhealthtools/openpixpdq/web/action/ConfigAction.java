/**
 *  Copyright (c) 2009-2011 Misys Open Source Solutions (MOSS) and others
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *  implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 *  Contributors:
 *    Misys Open Source Solutions - initial API and implementation
 *    -
 */

package org.openhealthtools.openpixpdq.web.action;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.openhealthtools.openexchange.actorconfig.IActorDescription;
import org.openhealthtools.openpixpdq.web.servlet.PixPdqConfigurationLoader;
import org.openhealthtools.openpixpdq.web.vo.ConfigBean;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

/**
 * A Struts Tiles action which implements the header of each page,
 * checking for the
 */
public class ConfigAction extends Action {

    /**
     * TODO document
     *
     * @param mapping  The ActionMapping used to select this instance
     * @param form     The optional ActionForm bean for this request
     * @param request  The servlet request we are processing
     * @param response The servlet response we are creating
     * @throws Exception if business logic throws an exception
     */
    public ActionForward execute(ActionMapping mapping, ActionForm form,
                                 HttpServletRequest request, HttpServletResponse response) {
        try {
            ConfigBean cb = (ConfigBean) form;
            if (cb == null || cb.getAction() == null || cb.getAction().equals("")) {
                if (cb == null) {
                    cb = new ConfigBean();
                }
                List<IActorDescription> l = (ArrayList<IActorDescription>) PixPdqConfigurationLoader.getInstance().getActorDescriptions();

                Collections.sort(l, new compareTypes());
                request.setAttribute("ActorList", l);
                List aList = new LinkedList();
                String[] sList = new String[l.size()];
                int x = 0;
                for (IActorDescription ida : l) {
                    if (ida.isInstalled()) {
                        sList[x++] = ida.getName();
                    }
                }
                cb.setActors(sList);
                request.setAttribute("ConfiBean", cb);
                return mapping.findForward("success");
            }
            if (cb.getAction().equalsIgnoreCase("load")) {
                //First reset the config settings before loading
                PixPdqConfigurationLoader.getInstance().resetConfiguration(null, null);
                PixPdqConfigurationLoader.getInstance().loadConfiguration(cb.getConfigFile(), false);
                List<IActorDescription> l = (ArrayList<IActorDescription>) PixPdqConfigurationLoader.getInstance().getActorDescriptions();
                Collections.sort(l, new compareTypes());
                request.setAttribute("ActorList", l);
                List aList = new LinkedList();
                String[] sList = new String[l.size()];
                int x = 0;
                for (IActorDescription ida : l) {
                    if (ida.isInstalled()) {
                        sList[x++] = ida.getName();
                        //aList.add(ida.getId());
                    }
                }
                cb.setActors(sList);
                request.setAttribute("ConfiBean", cb);
                return mapping.findForward("success");
            } else if (cb.getAction().equalsIgnoreCase("save")) {
                List<Object> lString = new LinkedList<Object>();
                StringBuffer selectedActors = new StringBuffer();
                for (String s : cb.getActors()) {
                    if (selectedActors.length() > 0)
                        selectedActors.append(",");
                    selectedActors.append(s);
                    lString.add(s);
                }
                String sLogFile = cb.getLogfile();
                if (sLogFile != null && !sLogFile.equals("")) {
                    PixPdqConfigurationLoader.getInstance().resetConfiguration(lString, sLogFile);
                } else {
                    PixPdqConfigurationLoader.getInstance().resetConfiguration(lString, null);
                }
                List<IActorDescription> l = (ArrayList<IActorDescription>) PixPdqConfigurationLoader.getInstance().getActorDescriptions();
                Collections.sort(l, new compareTypes());
                request.setAttribute("ActorList", l);
                request.setAttribute("ConfiBean", cb);
                return mapping.findForward("success");
            } else if (cb.getAction().equalsIgnoreCase("stop all")) {
                PixPdqConfigurationLoader.getInstance().resetConfiguration(null, null);
                List<IActorDescription> l = (ArrayList<IActorDescription>) PixPdqConfigurationLoader.getInstance().getActorDescriptions();
                Collections.sort(l, new compareTypes());
                request.setAttribute("ActorList", l);
                cb.setActors(null);
                request.setAttribute("ConfiBean", cb);
                return mapping.findForward("success");
            }

        } catch (Exception e) {
            return null;
        }
        return (mapping.findForward("success"));
    }

    private class compareTypes implements Comparator {

        public int compare(Object first, Object second) {
            try {
                IActorDescription f = (IActorDescription) first;
                IActorDescription s = (IActorDescription) second;
                return f.getType().compareToIgnoreCase(s.getType());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return 0;
        }
    }

}
