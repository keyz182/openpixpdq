/**
 *  Copyright (c) 2009-2011 Misys Open Source Solutions (MOSS) and others
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *  implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 *  Contributors:
 *    Misys Open Source Solutions - initial API and implementation
 *    -
 */

package org.openhealthtools.openpixpdq.web.grid;

import org.openhealthtools.openexchange.datamodel.Identifier;
import org.openhealthtools.openexchange.datamodel.PatientIdentifier;

import java.util.List;

public class Tooltip {


    public static final String getPatientIDTooltipDiv(List<PatientIdentifier> pids, String divId) {
        StringBuffer buffer = new StringBuffer();
        buffer.append("<div style=\"display:none\" id=\"tooltips\">");
        buffer.append("<div id=\"");
        buffer.append(divId);
        buffer.append("\">");

        buffer.append("<table class=\"pidTooltip\" border=\"1\" cellspacing=\"0\">");
        //buffer.append("<thead><th align=\"center\"> List of Matched Patient IDs </th></thead>");
        buffer.append("<tr><td>");
        buffer.append("<table class=\"demographicTooltip\" colspan=\"2\" border=\"1\" cellspacing=\"0\" style=\" font-family: Verdana, Helvetica; font-size: 12px;\">");
        buffer.append("<tr style=\" font-weight: bold;\"><td align=\"center\">Patient ID </td><td align=\"center\">Assigning Authority</td></tr>");

        if (pids != null) {
            for (PatientIdentifier pid : pids) {
                Identifier id = pid.getAssigningAuthority();
                String assigningAuth = id.getNamespaceId() + id.getUniversalId() + id.getUniversalIdType();
                buffer.append("<tr><td>" + pid.getId() + "</td><td>" + assigningAuth + "</td></tr>");

            }
        }
        buffer.append("</table>");
        buffer.append("</td></tr>");

        buffer.append("</table></div></div>");
        return buffer.toString();
    }
}