/**
 *  Copyright (c) 2009-2011 Misys Open Source Solutions (MOSS) and others
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *  implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 *  Contributors:
 *    Misys Open Source Solutions - initial API and implementation
 *    -
 */

package org.openhealthtools.openpixpdq.web.vo;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

import javax.servlet.http.HttpServletRequest;


/**
 * @author Anil kumar
 * @date Jan 23, 2009
 */

public class ConfigBean extends ActionForm {

    private String action;
    private String configFile;
    private String logfile;
    private String[] actors;

    /**
     * @return Returns the action.
     */
    public String getAction() {
        return action;
    }

    /**
     * @param action The action to set.
     */
    public void setAction(String action) {
        this.action = action;
    }

    /**
     * @return Returns the actors.
     */
    public String[] getActors() {
        return actors;
    }

    /**
     * @param actors The actors to set.
     */
    public void setActors(String[] actors) {
        this.actors = actors;
    }

    /**
     * @return Returns the configFile.
     */
    public String getConfigFile() {
        return configFile;
    }

    /**
     * @param configFile The configFile to set.
     */
    public void setConfigFile(String configFile) {
        this.configFile = configFile;
    }

    /**
     * @return Returns the logfile.
     */
    public String getLogfile() {
        return logfile;
    }

    /**
     * @param logfile The logfile to set.
     */
    public void setLogfile(String logfile) {
        this.logfile = logfile;
    }

    /**
     * Reset all properties to their default values.
     */
    public void reset(ActionMapping mapping, HttpServletRequest request) {
        this.action = null;
        this.configFile = null;
        this.logfile = null;
        this.actors = null;
    }
}
