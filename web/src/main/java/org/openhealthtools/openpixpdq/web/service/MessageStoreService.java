/**
 *  Copyright (c) 2009-2011 Misys Open Source Solutions (MOSS) and others
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *  implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 *  Contributors:
 *    Misys Open Source Solutions - initial API and implementation
 *    -
 */

package org.openhealthtools.openpixpdq.web.service;

import org.apache.log4j.Logger;
import org.openhealthtools.openpixpdq.api.IMessageStoreLogger;
import org.openhealthtools.openpixpdq.api.MessageStore;
import org.openhealthtools.openpixpdq.web.dao.MessageStoreDAO;
import org.openhealthtools.openpixpdq.web.dao.MessageStoreDAOImpl;

import java.util.List;

/**
 * @author Anil kumar
 * @date Nov 25, 2008
 */

public class MessageStoreService implements IMessageStoreLogger {

    private static Logger log = Logger.getLogger(MessageStoreService.class);

    private MessageStoreDAO messagelogdao = new MessageStoreDAOImpl();

    public void saveLog(MessageStore messageLog) {
        if (messageLog.getInMessage() != null) {
            String messagein = messageLog.getInMessage().replace("\r", "<br>");
            ;
            messageLog.setInMessage(messagein);
        }
        if (messageLog.getOutMessage() != null) {
            String messageout = messageLog.getOutMessage().replace("\r", "<br>");
            messageLog.setOutMessage(messageout);
        }
        messagelogdao.saveLog(messageLog);
    }

    public List<MessageStore> searchLog(MessageStore messageLog) {
        return messagelogdao.searchLog(messageLog);

    }
}
