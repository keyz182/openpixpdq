<%--

     Copyright (c) 2009-2011 Misys Open Source Solutions (MOSS) and others

     Licensed under the Apache License, Version 2.0 (the "License");
     you may not use this file except in compliance with the License.
     You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

     Unless required by applicable law or agreed to in writing, software
     distributed under the License is distributed on an "AS IS" BASIS,
     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
     implied. See the License for the specific language governing
     permissions and limitations under the License.

     Contributors:
       Misys Open Source Solutions - initial API and implementation
       -

--%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>

<%@ taglib uri="/WEB-INF/lib/core.tld" prefix="c" %>
<%@ taglib uri="/WEB-INF/lib/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/lib/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/lib/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/lib/jmesa.tld" prefix="jmesa" %>
<link rel="stylesheet" href="<c:url value='/css/jmesa.css'/>" type="text/css"/>
<link rel="stylesheet" href="<c:url value='/css/table.css'/>" type="text/css"/>
<link rel="stylesheet" href="<c:url value='/css/domTT.css'/>" type="text/css"/>
<script type="text/javascript" src="<c:url value='/scripts/jmesa.js'/>"></script>
<script type="text/javascript" language="javascript" src="<c:url value="/scripts/domLib.js"/>"></script>
<script type="text/javascript" language="javascript" src="<c:url value="/scripts/domTT.js"/>"></script>
<script type="text/javascript" src="<c:url value='/scripts/jquery-1.2.2.pack.js'/>"></script>
<script type="text/javascript" src="<c:url value='/scripts/messagestore.js'/>"></script>
<!--script type="text/javascript" src="<c:url value='/scripts/grid.js'/>"></script> -->


<input type="hidden" id="contextRoot" name="contextRoot" value="<%=request.getContextPath() %>">
<table class="TableTS" cellpadding="0" cellspacing="0">
    <thead class="TableTS" align="left">
    <th class="TableTS">Message Log - SearchCriteria:</th>
    </thead>
    <tr>
        <td>
            <div class="Table">
                <html:form action="MessageStore.do" method="post">
                    <table class="TableMS">

                        <tr>
                            <td align="center">IP</td>
                            <td align="left"><html:text property="ip" size="18" maxlength="80"/></td>
                            <td align="center">MessageId</td>
                            <td align="left"><html:text property="messageId" size="18" maxlength="80"/></td>
                            <td align="center">ErrorMessage</td>
                            <td align="left"><html:text property="errorMessage" size="18" maxlength="255"/></td>
                            <td align="center">SendingFacility</td>
                            <td align="left"><html:text property="sendingFacility" size="18" maxlength="80"/></td>

                        </tr>
                        <tr>
                            <td align="center">SendingAppl</td>
                            <td align="left"><html:text property="sendingApplication" size="18" maxlength="80"/></td>
                            <td align="center">ReceivingFacility</td>
                            <td align="left"><html:text property="receivingFacility" size="18" maxlength="80"/></td>
                            <td align="center">ReceivingAppl</td>
                            <td align="left"><html:text property="receivingApplication" size="18" maxlength="80"/></td>
                            <td align="center">Message Date</td>
                            <td align="left"><html:text property="messageDate" value="MM/DD/YYYY" size="18"/></td>

                        </tr>
                        <tr width="100%">
                            <td align="right"><html:submit property="action" value="Search"></html:submit></td>
                        </tr>
                    </table>
                    <table class="TableJMESA" cellpadding="0" cellspacing="0">
                        <tr>
                            <td>
                                <div id="tabletag">

                                    <jmesa:tableFacade id="tag"
                                                       items="${personlist}"
                                                       maxRows="10"
                                                       editable="false"
                                                       stateAttr="restore" var="bean" rowFilter="">
                                        <jmesa:htmlTable width="100%">
                                            <jmesa:htmlRow uniqueProperty="messageId">
                                                <jmesa:htmlColumn title="IP" property="ip" width="40px"
                                                                  cellRenderer="org.openhealthtools.openpixpdq.web.grid.OverflowCell"/>
                                                <jmesa:htmlColumn title="MessageId" property="messageId" width="100px"
                                                                  cellRenderer="org.openhealthtools.openpixpdq.web.grid.OverflowCell"/>
                                                <jmesa:htmlColumn title="Error<br/>Message" property="errorMessage"
                                                                  width="110px"
                                                                  cellRenderer="org.openhealthtools.openpixpdq.web.grid.OverflowCell"/>
                                                <jmesa:htmlColumn title="Sending<br/>Facility"
                                                                  property="sendingFacility" width="150px"
                                                                  cellRenderer="org.openhealthtools.openpixpdq.web.grid.OverflowCell"/>
                                                <jmesa:htmlColumn title="Sending<br/>Application"
                                                                  property="sendingApplication" width="100px"
                                                                  cellRenderer="org.openhealthtools.openpixpdq.web.grid.OverflowCell"/>
                                                <jmesa:htmlColumn title="Receiving<br/>Facility"
                                                                  property="receivingFacility" width="150px"
                                                                  cellRenderer="org.openhealthtools.openpixpdq.web.grid.OverflowCell"/>
                                                <jmesa:htmlColumn title="Receiving<br/>Application"
                                                                  property="receivingApplication" width="100px"
                                                                  cellRenderer="org.openhealthtools.openpixpdq.web.grid.OverflowCell"/>
                                                <jmesa:htmlColumn title="MessageDate" property="messageDate"
                                                                  pattern="yyyy-MM-ddHH:mm:SS.sssz"
                                                                  cellRenderer="org.openhealthtools.openpixpdq.web.grid.OverflowCell"/>
                                                <jmesa:htmlColumn title="Event" property="triggerEvent" width="60px"
                                                                  cellRenderer="org.openhealthtools.openpixpdq.web.grid.OverflowCell"/>
                                                <jmesa:htmlColumn title="Input<br/>Message" property="inMessage"
                                                                  width="100px"
                                                                  cellRenderer="org.openhealthtools.openpixpdq.web.grid.OverflowCell"/>
                                                <jmesa:htmlColumn title="Output<br/>Message" property="outMessage"
                                                                  width="100px"
                                                                  cellRenderer="org.openhealthtools.openpixpdq.web.grid.OverflowCell"/>

                                            </jmesa:htmlRow>
                                        </jmesa:htmlTable>
                                    </jmesa:tableFacade>
                                </div>
                            </td>
                        </tr>
                    </table>
                </html:form></div>
        </td>
    </tr>
</table>
<script>
    dispLogDetails();
</script>