ALTER TABLE PUBLIC.person
	ADD COLUMN account VARCHAR(255);

ALTER TABLE PUBLIC.person
	ADD COLUMN account_identifier_domain_id INTEGER;

ALTER TABLE PUBLIC.person
	ADD CONSTRAINT fk_account_identifier_domain_id
	FOREIGN KEY (account_identifier_domain_id)
		REFERENCES PUBLIC.identifier_domain(identifier_domain_id)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION;

ALTER TABLE PUBLIC.person
	ADD COLUMN custom6 VARCHAR(255),
	ADD COLUMN custom7 VARCHAR(255),
	ADD COLUMN custom8 VARCHAR(255),
	ADD COLUMN custom9 VARCHAR(255),
	ADD COLUMN custom10 VARCHAR(255),
	ADD COLUMN custom11 VARCHAR(255),
	ADD COLUMN custom12 VARCHAR(255),
	ADD COLUMN custom13 VARCHAR(255),
	ADD COLUMN custom14 VARCHAR(255),
	ADD COLUMN custom15 VARCHAR(255),
	ADD COLUMN custom16 VARCHAR(255),
	ADD COLUMN custom17 VARCHAR(255),
	ADD COLUMN custom18 VARCHAR(255),
	ADD COLUMN custom19 VARCHAR(255),
	ADD COLUMN custom20 VARCHAR(255),
	ADD COLUMN group_number VARCHAR(64);

-- Structure for table phone_type:
CREATE TABLE phone_type (
    phone_type_cd INTEGER NOT NULL,
    phone_type_name VARCHAR(64) NOT NULL,
    phone_type_description VARCHAR(255),
    phone_type_code VARCHAR(64) NOT NULL
) WITHOUT OIDS;

INSERT INTO phone_type (phone_type_cd, phone_type_name, phone_type_description, phone_type_code) VALUES (1, 'Beeper Number', 'Beeper Number or paging device suitable to solicit or to leave a very short message', 'B');
INSERT INTO phone_type (phone_type_cd, phone_type_name, phone_type_description, phone_type_code) VALUES (2, 'Cellular Phone Number', 'Cellular Phone Number', 'C');
INSERT INTO phone_type (phone_type_cd, phone_type_name, phone_type_description, phone_type_code) VALUES (3, 'E-mail Address', 'E-mail Address', 'E');
INSERT INTO phone_type (phone_type_cd, phone_type_name, phone_type_description, phone_type_code) VALUES (4, 'Fax Number', 'Fax Number', 'F');
INSERT INTO phone_type (phone_type_cd, phone_type_name, phone_type_description, phone_type_code) VALUES (5, 'Home Phone Number', 'Home Phone Number', 'H');
INSERT INTO phone_type (phone_type_cd, phone_type_name, phone_type_description, phone_type_code) VALUES (6, 'Office Phone Number', 'Office Phone Number', 'O');
INSERT INTO phone_type (phone_type_cd, phone_type_name, phone_type_description, phone_type_code) VALUES (7, 'Primary Home Number', 'The primary home to reach a person after business hours', 'HP');
INSERT INTO phone_type (phone_type_cd, phone_type_name, phone_type_description, phone_type_code) VALUES (8, 'Vacation Home Number', 'A vacation home to reach a person while on vacation', 'HV');
INSERT INTO phone_type (phone_type_cd, phone_type_name, phone_type_description, phone_type_code) VALUES (9, 'Answering Service', 'An automated answering machine used for less urgent cases', 'AS');
INSERT INTO phone_type (phone_type_cd, phone_type_name, phone_type_description, phone_type_code) VALUES (10, 'Emergency Number', 'A contact specifically designated to be used for emergencies. This is the first choice in emergencies, independent of any other use codes.', 'EC');
INSERT INTO phone_type (phone_type_cd, phone_type_name, phone_type_description, phone_type_code) VALUES (11, 'Unknown', 'Phone type is unknown.', 'UN');

ALTER TABLE ONLY phone_type
    ADD CONSTRAINT phone_type_pkey PRIMARY KEY (phone_type_cd);

ALTER TABLE PUBLIC.person
	ADD COLUMN phone_type_cd INTEGER;

ALTER TABLE ONLY person
    ADD CONSTRAINT fk_phone_type FOREIGN KEY (phone_type_cd) REFERENCES phone_type(phone_type_cd);

