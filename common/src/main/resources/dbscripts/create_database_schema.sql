------------------------------------------
--Host     : localhost
--Database : openempi



SET SESSION AUTHORIZATION 'openempi';
SET search_path = PUBLIC, pg_catalog;

-- Structure for table address_type (OID = 34474):
CREATE TABLE address_type (
    address_type_cd INTEGER NOT NULL,
    address_type_name VARCHAR(64) NOT NULL,
    address_type_description VARCHAR(255),
    address_type_code VARCHAR(64) NOT NULL
) WITHOUT OIDS;

INSERT INTO address_type (address_type_cd, address_type_name, address_type_description, address_type_code) VALUES (1, 'Bad Address', 'Bad Address', 'BA');
INSERT INTO address_type (address_type_cd, address_type_name, address_type_description, address_type_code) VALUES (2, 'Billing Address', 'Billing Address', 'BI');
INSERT INTO address_type (address_type_cd, address_type_name, address_type_description, address_type_code) VALUES (3, 'Birth Address', 'Birth Address', 'N');
INSERT INTO address_type (address_type_cd, address_type_name, address_type_description, address_type_code) VALUES (4, 'Birth Delivery Location', 'Birth Delivery Address', 'BDL');
INSERT INTO address_type (address_type_cd, address_type_name, address_type_description, address_type_code) VALUES (5, 'Country of Origin', 'Country of Origin', 'F');
INSERT INTO address_type (address_type_cd, address_type_name, address_type_description, address_type_code) VALUES (6, 'Current or Temporary', 'Current or Temporary', 'C');
INSERT INTO address_type (address_type_cd, address_type_name, address_type_description, address_type_code) VALUES (7, 'Firm/Business', 'Firm/Business', 'B');
INSERT INTO address_type (address_type_cd, address_type_name, address_type_description, address_type_code) VALUES (8, 'Home', 'Home', 'H');
INSERT INTO address_type (address_type_cd, address_type_name, address_type_description, address_type_code) VALUES (9, 'Legal Address', 'Legal Address', 'L');
INSERT INTO address_type (address_type_cd, address_type_name, address_type_description, address_type_code) VALUES (10, 'Mailing', 'Mailing', 'M');
INSERT INTO address_type (address_type_cd, address_type_name, address_type_description, address_type_code) VALUES (11, 'Office/Business', 'Office/Business', 'O');
INSERT INTO address_type (address_type_cd, address_type_name, address_type_description, address_type_code) VALUES (12, 'Permanent', 'Permanent', 'P');
INSERT INTO address_type (address_type_cd, address_type_name, address_type_description, address_type_code) VALUES (13, 'Registry Home', 'Registry home refers to the information system, typically managed by a public health agency, that stores patient information', 'RH');
INSERT INTO address_type (address_type_cd, address_type_name, address_type_description, address_type_code) VALUES (14, 'Residence at birth', 'Residence at birth (home address at time of birth)', 'BR');
INSERT INTO address_type (address_type_cd, address_type_name, address_type_description, address_type_code) VALUES (15, 'Service Location', 'Refers to the location in which service is rendered', 'S');
INSERT INTO address_type (address_type_cd, address_type_name, address_type_description, address_type_code) VALUES (16, 'Shipping Address', 'Shipping Address', 'SH');
INSERT INTO address_type (address_type_cd, address_type_name, address_type_description, address_type_code) VALUES (17, 'Vacation', 'Vacation', 'V');

-- Structure for table app_user (OID = 34479):
CREATE TABLE app_user (
    id bigint NOT NULL,
    username VARCHAR(50) NOT NULL,
    email VARCHAR(255) NOT NULL,
    phone_number VARCHAR(255),
    password_hint VARCHAR(255),
    first_name VARCHAR(50) NOT NULL,
    last_name VARCHAR(50) NOT NULL,
    website VARCHAR(255),
    account_expired boolean NOT NULL,
    account_locked boolean NOT NULL,
    credentials_expired boolean NOT NULL,
    city VARCHAR(50) NOT NULL,
    province VARCHAR(100),
    postal_code VARCHAR(15) NOT NULL,
    address VARCHAR(150),
    country VARCHAR(100),
    account_enabled boolean,
    "version" INTEGER,
    "password" VARCHAR(255) NOT NULL
) WITHOUT OIDS;

INSERT INTO app_user (id, username, email, phone_number, password_hint, first_name, last_name, website, account_expired, account_locked, credentials_expired, city, province, postal_code, address, country, account_enabled, "version", "password") VALUES (2, 'user', 'support@sysnetint.com', '', 'A male kitty.', 'Open', 'Empi', 'http://www.sysnetint.com', FALSE, FALSE, FALSE, 'Reston', 'VA', '20191', '', 'US', TRUE, 1, '12dea96fec20593566ab75692c9949596833adc9');
INSERT INTO app_user (id, username, email, phone_number, password_hint, first_name, last_name, website, account_expired, account_locked, credentials_expired, city, province, postal_code, address, country, account_enabled, "version", "password") VALUES (1, 'admin', 'odysseas@sysnetint.com', '', 'A male kitty.', 'Admin', 'User', 'http://www.sysnetint.com', FALSE, FALSE, FALSE, 'Herndon', 'VA', '20171', '', 'US', TRUE, 1, 'd033e22ae348aeb5660fc2140aec35850c4da997');
COMMIT;

-- Structure for table role (OID = 34546):
CREATE TABLE role (
    id bigint NOT NULL,
    NAME VARCHAR(20),
    description VARCHAR(64)
) WITHOUT OIDS;

INSERT INTO "role" (id, "name", description) VALUES
  (-1, 'ROLE_ADMIN', 'Administrator role (can edit Users)'),
  (-2, 'ROLE_USER', 'Default role for all users');

-- Structure for table user_role (OID = 34551):
CREATE TABLE user_role (
    user_id bigint NOT NULL,
    role_id bigint NOT NULL
) WITHOUT OIDS;

INSERT INTO user_role (user_id, role_id) VALUES
 (1, -1),
 (1, -2),
 (2, -2),
 (2, -1);

-- Structure for table ethnic_group (OID = 34488):
CREATE TABLE ethnic_group (
    ethnic_group_cd INTEGER NOT NULL,
    ethnic_group_name VARCHAR(64) NOT NULL,
    ethnic_group_description VARCHAR(255),
    ethnic_group_code VARCHAR(64) NOT NULL
) WITHOUT OIDS;

INSERT INTO ethnic_group (ethnic_group_cd, ethnic_group_name, ethnic_group_description, ethnic_group_code) VALUES (1,'Hispanic or Latino','Hispanic or Latino','2135-2');
INSERT INTO ethnic_group (ethnic_group_cd, ethnic_group_name, ethnic_group_description, ethnic_group_code) VALUES (2,'Spaniard','Spaniard','2137-8');
INSERT INTO ethnic_group (ethnic_group_cd, ethnic_group_name, ethnic_group_description, ethnic_group_code) VALUES (3,'Andalusian','Andalusian','2138-6');
INSERT INTO ethnic_group (ethnic_group_cd, ethnic_group_name, ethnic_group_description, ethnic_group_code) VALUES (4,'Asturian','Asturian','2139-4');
INSERT INTO ethnic_group (ethnic_group_cd, ethnic_group_name, ethnic_group_description, ethnic_group_code) VALUES (5,'Castillian','Castillian','2140-2');
INSERT INTO ethnic_group (ethnic_group_cd, ethnic_group_name, ethnic_group_description, ethnic_group_code) VALUES (6,'Catalonian','Catalonian','2141-0');
INSERT INTO ethnic_group (ethnic_group_cd, ethnic_group_name, ethnic_group_description, ethnic_group_code) VALUES (7,'Belearic Islander','Belearic Islander','2142-8');
INSERT INTO ethnic_group (ethnic_group_cd, ethnic_group_name, ethnic_group_description, ethnic_group_code) VALUES (8,'Gallego','Gallego','2143-6');
INSERT INTO ethnic_group (ethnic_group_cd, ethnic_group_name, ethnic_group_description, ethnic_group_code) VALUES (9,'Valencian','Valencian','2144-4');
INSERT INTO ethnic_group (ethnic_group_cd, ethnic_group_name, ethnic_group_description, ethnic_group_code) VALUES (10,'Canarian','Canarian','2145-1');
INSERT INTO ethnic_group (ethnic_group_cd, ethnic_group_name, ethnic_group_description, ethnic_group_code) VALUES (11,'Spanish Basque','Spanish Basque','2146-9');
INSERT INTO ethnic_group (ethnic_group_cd, ethnic_group_name, ethnic_group_description, ethnic_group_code) VALUES (12,'Mexican','Mexican','2148-5');
INSERT INTO ethnic_group (ethnic_group_cd, ethnic_group_name, ethnic_group_description, ethnic_group_code) VALUES (13,'Mexican American','Mexican American','2149-3');
INSERT INTO ethnic_group (ethnic_group_cd, ethnic_group_name, ethnic_group_description, ethnic_group_code) VALUES (14,'Mexicano','Mexicano','2150-1');
INSERT INTO ethnic_group (ethnic_group_cd, ethnic_group_name, ethnic_group_description, ethnic_group_code) VALUES (15,'Chicano','Chicano','2151-9');
INSERT INTO ethnic_group (ethnic_group_cd, ethnic_group_name, ethnic_group_description, ethnic_group_code) VALUES (16,'La Raza','La Raza','2152-7');
INSERT INTO ethnic_group (ethnic_group_cd, ethnic_group_name, ethnic_group_description, ethnic_group_code) VALUES (17,'Mexican American Indian','Mexican American Indian','2153-5');
INSERT INTO ethnic_group (ethnic_group_cd, ethnic_group_name, ethnic_group_description, ethnic_group_code) VALUES (18,'Central American','Central American','2155-0');
INSERT INTO ethnic_group (ethnic_group_cd, ethnic_group_name, ethnic_group_description, ethnic_group_code) VALUES (19,'Costa Rican','Costa Rican','2156-8');
INSERT INTO ethnic_group (ethnic_group_cd, ethnic_group_name, ethnic_group_description, ethnic_group_code) VALUES (20,'Guatemalan','Guatemalan','2157-6');
INSERT INTO ethnic_group (ethnic_group_cd, ethnic_group_name, ethnic_group_description, ethnic_group_code) VALUES (21,'Honduran','Honduran','2158-4');
INSERT INTO ethnic_group (ethnic_group_cd, ethnic_group_name, ethnic_group_description, ethnic_group_code) VALUES (22,'Nicaraguan','Nicaraguan','2159-2');
INSERT INTO ethnic_group (ethnic_group_cd, ethnic_group_name, ethnic_group_description, ethnic_group_code) VALUES (23,'Panamanian','Panamanian','2160-0');
INSERT INTO ethnic_group (ethnic_group_cd, ethnic_group_name, ethnic_group_description, ethnic_group_code) VALUES (24,'Salvadoran','Salvadoran','2161-8');
INSERT INTO ethnic_group (ethnic_group_cd, ethnic_group_name, ethnic_group_description, ethnic_group_code) VALUES (25,'Central American Indian','Central American Indian','2162-6');
INSERT INTO ethnic_group (ethnic_group_cd, ethnic_group_name, ethnic_group_description, ethnic_group_code) VALUES (26,'Canal Zone','Canal Zone','2163-4');
INSERT INTO ethnic_group (ethnic_group_cd, ethnic_group_name, ethnic_group_description, ethnic_group_code) VALUES (27,'South American','South American','2165-9');
INSERT INTO ethnic_group (ethnic_group_cd, ethnic_group_name, ethnic_group_description, ethnic_group_code) VALUES (28,'Argentinean','Argentinean','2166-7');
INSERT INTO ethnic_group (ethnic_group_cd, ethnic_group_name, ethnic_group_description, ethnic_group_code) VALUES (29,'Bolivian','Bolivian','2167-5');
INSERT INTO ethnic_group (ethnic_group_cd, ethnic_group_name, ethnic_group_description, ethnic_group_code) VALUES (30,'Chilean','Chilean','2168-3');
INSERT INTO ethnic_group (ethnic_group_cd, ethnic_group_name, ethnic_group_description, ethnic_group_code) VALUES (31,'Colombian','Colombian','2169-1');
INSERT INTO ethnic_group (ethnic_group_cd, ethnic_group_name, ethnic_group_description, ethnic_group_code) VALUES (32,'Ecuadorian','Ecuadorian','2170-9');
INSERT INTO ethnic_group (ethnic_group_cd, ethnic_group_name, ethnic_group_description, ethnic_group_code) VALUES (33,'Paraguayan','Paraguayan','2171-7');
INSERT INTO ethnic_group (ethnic_group_cd, ethnic_group_name, ethnic_group_description, ethnic_group_code) VALUES (34,'Peruvian','Peruvian','2172-5');
INSERT INTO ethnic_group (ethnic_group_cd, ethnic_group_name, ethnic_group_description, ethnic_group_code) VALUES (35,'Uruguayan','Uruguayan','2173-3');
INSERT INTO ethnic_group (ethnic_group_cd, ethnic_group_name, ethnic_group_description, ethnic_group_code) VALUES (36,'Venezuelan','Venezuelan','2174-1');
INSERT INTO ethnic_group (ethnic_group_cd, ethnic_group_name, ethnic_group_description, ethnic_group_code) VALUES (37,'South American Indian','South American Indian','2175-8');
INSERT INTO ethnic_group (ethnic_group_cd, ethnic_group_name, ethnic_group_description, ethnic_group_code) VALUES (38,'Criollo','Criollo','2176-6');
INSERT INTO ethnic_group (ethnic_group_cd, ethnic_group_name, ethnic_group_description, ethnic_group_code) VALUES (39,'Latin American','Latin American','2178-2');
INSERT INTO ethnic_group (ethnic_group_cd, ethnic_group_name, ethnic_group_description, ethnic_group_code) VALUES (40,'Puerto Rican','Puerto Rican','2180-8');
INSERT INTO ethnic_group (ethnic_group_cd, ethnic_group_name, ethnic_group_description, ethnic_group_code) VALUES (41,'Cuban','Cuban','2182-4');
INSERT INTO ethnic_group (ethnic_group_cd, ethnic_group_name, ethnic_group_description, ethnic_group_code) VALUES (42,'Dominican','Dominican','2184-0');
INSERT INTO ethnic_group (ethnic_group_cd, ethnic_group_name, ethnic_group_description, ethnic_group_code) VALUES (43,'Not Hispanic or Latino','Not Hispanic or Latino','2186-5');

-- Structure for table gender (OID = 34493):
CREATE TABLE gender (
    gender_cd INTEGER NOT NULL,
    gender_name VARCHAR(64) NOT NULL,
    gender_description VARCHAR(255),
    gender_code VARCHAR(64) NOT NULL
) WITHOUT OIDS;

INSERT INTO gender (gender_cd, gender_name, gender_description, gender_code) VALUES (1, 'Female', 'Female', 'F');
INSERT INTO gender (gender_cd, gender_name, gender_description, gender_code) VALUES (2, 'Male', 'Male', 'M');
INSERT INTO gender (gender_cd, gender_name, gender_description, gender_code) VALUES (3, 'Other', 'Other', 'O');
INSERT INTO gender (gender_cd, gender_name, gender_description, gender_code) VALUES (4, 'Unknown', 'Unknown', 'U');

-- Structure for table identifier_domain (OID = 34498):
CREATE TABLE identifier_domain (
    identifier_domain_id INTEGER NOT NULL,
    universal_identifier VARCHAR(255),
    universal_identifier_type_code VARCHAR(255),
    namespace_identifier VARCHAR(255),
    date_created TIMESTAMP without TIME ZONE NOT NULL,
    creator_id bigint NOT NULL
) WITHOUT OIDS;

CREATE TABLE PUBLIC.identifier_domain_attribute (
    identifier_domain_attribute_id  INTEGER NOT NULL,
    identifier_domain_id            INTEGER NOT NULL,
    attribute_name                  VARCHAR(255) NOT NULL,
    attribute_value                 VARCHAR(255) NOT NULL
) WITHOUT OIDS;

-- Structure for table language (OID = 34503):
CREATE TABLE "language" (
    language_cd INTEGER NOT NULL,
    language_name VARCHAR(64) NOT NULL,
    language_description VARCHAR(255),
    language_code VARCHAR(64) NOT NULL
) WITHOUT OIDS;

INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (1,'Afar','Afar','aa');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (2,'Abkhazian','Abkhazian','ab');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (3,'Avestan','Avestan','ae');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (4,'Afrikaans','Afrikaans','af');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (5,'Akan','Akan','ak');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (6,'Amharic','Amharic','am');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (7,'Aragonese','Aragonese','an');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (8,'Arabic','Arabic','ar');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (9,'Assamese','Assamese','as');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (10,'Avaric','Avaric','av');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (11,'Aymara','Aymara','ay');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (12,'Azerbaijani','Azerbaijani','az');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (13,'Bashkir','Bashkir','ba');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (14,'Belarusian','Belarusian','be');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (15,'Bulgarian','Bulgarian','bg');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (16,'Bihari','Bihari','bh');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (17,'Bislama','Bislama','bi');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (18,'Bambara','Bambara','bm');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (19,'Bengali','Bengali','bn');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (20,'Tibetan','Tibetan','bo');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (21,'Breton','Breton','br');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (22,'Bosnian','Bosnian','bs');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (23,'Catalan;','Catalan;','ca');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (24,'Chechen','Chechen','ce');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (25,'Chamorro','Chamorro','ch');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (26,'Corsican','Corsican','co');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (27,'Cree','Cree','cr');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (28,'Czech','Czech','cs');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (29,'Church','Church','cu');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (30,'Chuvash','Chuvash','cv');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (31,'Welsh','Welsh','cy');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (32,'Danish','Danish','da');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (33,'German','German','de');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (34,'Divehi','Divehi','dv');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (35,'Dzongkha','Dzongkha','dz');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (36,'Ewe','Ewe','ee');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (37,'Greek,','Greek,','el');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (38,'English','English','en');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (39,'Esperanto','Esperanto','eo');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (40,'Spanish;','Spanish;','es');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (41,'Estonian','Estonian','et');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (42,'Basque','Basque','eu');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (43,'Persian','Persian','fa');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (44,'Fulah','Fulah','ff');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (45,'Finnish','Finnish','fi');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (46,'Fijian','Fijian','fj');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (47,'Faroese','Faroese','fo');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (48,'French','French','fr');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (49,'Frisian','Frisian','fy');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (50,'Irish','Irish','ga');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (51,'Gaelic;','Gaelic;','gd');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (52,'Galician','Galician','gl');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (53,'Guarani','Guarani','gn');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (54,'Gujarati','Gujarati','gu');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (55,'Manx','Manx','gv');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (56,'Hausa','Hausa','ha');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (57,'Hebrew','Hebrew','he');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (58,'Hindi','Hindi','hi');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (59,'Hiri','Hiri','ho');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (60,'Croatian','Croatian','hr');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (61,'Haitian;','Haitian;','ht');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (62,'Hungarian','Hungarian','hu');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (63,'Armenian','Armenian','hy');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (64,'Herero','Herero','hz');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (65,'Auxiliary','Auxiliary','ia');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (66,'Indonesian','Indonesian','id');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (67,'Interlingue','Interlingue','ie');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (68,'Igbo','Igbo','ig');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (69,'Sichuan','Sichuan','ii');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (70,'Inupiaq','Inupiaq','ik');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (71,'Ido','Ido','io');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (72,'Icelandic','Icelandic','is');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (73,'Italian','Italian','it');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (74,'Inuktitut','Inuktitut','iu');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (75,'Japanese','Japanese','ja');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (76,'Javanese','Javanese','jv');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (77,'Georgian','Georgian','ka');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (78,'Kongo','Kongo','kg');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (79,'Gikuyu;','Gikuyu;','ki');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (80,'Kuanyama;','Kuanyama;','kj');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (81,'Kazakh','Kazakh','kk');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (82,'Greenlandic;','Greenlandic;','kl');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (83,'Khmer','Khmer','km');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (84,'Kannada','Kannada','kn');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (85,'Korean','Korean','ko');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (86,'Kanuri','Kanuri','kr');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (87,'Kashmiri','Kashmiri','ks');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (88,'Kurdish','Kurdish','ku');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (89,'Komi','Komi','kv');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (90,'Cornish','Cornish','kw');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (91,'Kirghiz','Kirghiz','ky');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (92,'Latin','Latin','la');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (93,'Letzeburgesch;','Letzeburgesch;','lb');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (94,'Ganda','Ganda','lg');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (95,'Limburgan;','Limburgan;','li');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (96,'Lingala','Lingala','ln');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (97,'Lao','Lao','lo');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (98,'Lithuanian','Lithuanian','lt');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (99,'Luba-Katanga','Luba-Katanga','lu');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (100,'Latvian','Latvian','lv');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (101,'Malagasy','Malagasy','mg');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (102,'Marshallese','Marshallese','mh');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (103,'Maori','Maori','mi');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (104,'Macedonian','Macedonian','mk');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (105,'Malayalam','Malayalam','ml');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (106,'Mongolian','Mongolian','mn');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (107,'Moldavian','Moldavian','mo');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (108,'Marathi','Marathi','mr');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (109,'Malay','Malay','ms');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (110,'Maltese','Maltese','mt');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (111,'Burmese','Burmese','my');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (112,'Nauru','Nauru','na');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (113,'Norwegian','Norwegian','nb');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (114,'Ndebele,','Ndebele,','nd');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (115,'Nepali','Nepali','ne');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (116,'Ndonga','Ndonga','ng');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (117,'Dutch;','Dutch;','nl');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (118,'Norwegian','Norwegian','nn');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (119,'Norwegian','Norwegian','no');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (120,'Ndebele,','Ndebele,','nr');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (121,'Navajo;','Navajo;','nv');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (122,'Chewa;','Chewa;','ny');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (123,'Occitan','Occitan','oc');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (124,'Ojibwa','Ojibwa','oj');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (125,'Oromo','Oromo','om');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (126,'Oriya','Oriya','or');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (127,'Ossetian;','Ossetian;','os');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (128,'Panjabi;','Panjabi;','pa');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (129,'Pali','Pali','pi');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (130,'Polish','Polish','pl');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (131,'Pushto','Pushto','ps');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (132,'Portuguese','Portuguese','pt');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (133,'Quechua','Quechua','qu');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (134,'Raeto-Romance','Raeto-Romance','rm');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (135,'Rundi','Rundi','rn');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (136,'Romanian','Romanian','ro');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (137,'Russian','Russian','ru');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (138,'Kinyarwanda','Kinyarwanda','rw');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (139,'Sanskrit','Sanskrit','sa');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (140,'Sardinian','Sardinian','sc');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (141,'Sindhi','Sindhi','sd');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (142,'Northern','Northern','se');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (143,'Sango','Sango','sg');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (144,'Sinhala;','Sinhala;','si');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (145,'Slovak','Slovak','sk');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (146,'Slovenian','Slovenian','sl');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (147,'Samoan','Samoan','sm');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (148,'Shona','Shona','sn');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (149,'Somali','Somali','so');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (150,'Albanian','Albanian','sq');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (151,'Serbian','Serbian','sr');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (152,'Swati','Swati','ss');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (153,'Sotho,','Sotho,','st');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (154,'Sundanese','Sundanese','su');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (155,'Swedish','Swedish','sv');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (156,'Swahili','Swahili','sw');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (157,'Tamil','Tamil','ta');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (158,'Telugu','Telugu','te');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (159,'Tajik','Tajik','tg');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (160,'Thai','Thai','th');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (161,'Tigrinya','Tigrinya','ti');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (162,'Turkmen','Turkmen','tk');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (163,'Tagalog','Tagalog','tl');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (164,'Tswana','Tswana','tn');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (165,'Tonga','Tonga','to');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (166,'Turkish','Turkish','tr');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (167,'Tsonga','Tsonga','ts');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (168,'Tatar','Tatar','tt');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (169,'Twi','Twi','tw');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (170,'Tahitian','Tahitian','ty');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (171,'Uighur;','Uighur;','ug');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (172,'Ukrainian','Ukrainian','uk');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (173,'Urdu','Urdu','ur');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (174,'Uzbek','Uzbek','uz');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (175,'Venda','Venda','ve');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (176,'Vietnamese','Vietnamese','vi');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (177,'Volap�k','Volap�k','vo');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (178,'Walloon','Walloon','wa');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (179,'Wolof','Wolof','wo');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (180,'Xhosa','Xhosa','xh');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (181,'Yiddish','Yiddish','yi');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (182,'Yoruba','Yoruba','yo');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (183,'Zhuang;','Zhuang;','za');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (184,'Chinese','Chinese','zh');
INSERT INTO LANGUAGE (language_cd, language_name, language_description, language_code) VALUES (185,'Zulu','Zulu','zu');

-- Structure for table name_type (OID = 34508):
CREATE TABLE name_type (
    name_type_cd INTEGER NOT NULL,
    name_type_name VARCHAR(64) NOT NULL,
    name_type_description VARCHAR(255),
    name_type_code VARCHAR(64) NOT NULL
) WITHOUT OIDS;

INSERT INTO name_type (name_type_cd, name_type_name, name_type_description, name_type_code) VALUES (1, 'Alias Name', 'Alias Name', 'A');
INSERT INTO name_type (name_type_cd, name_type_name, name_type_description, name_type_code) VALUES (2, 'Legal Name', 'Legal Name', 'L');
INSERT INTO name_type (name_type_cd, name_type_name, name_type_description, name_type_code) VALUES (3, 'Display Name', 'Display Name', 'D');
INSERT INTO name_type (name_type_cd, name_type_name, name_type_description, name_type_code) VALUES (4, 'Maiden Name', 'Maiden Name', 'M');
INSERT INTO name_type (name_type_cd, name_type_name, name_type_description, name_type_code) VALUES (5, 'Adopted Name', 'Adopted Name', 'C');
INSERT INTO name_type (name_type_cd, name_type_name, name_type_description, name_type_code) VALUES (6, 'Name at Birth', 'Name at Birth', 'B');
INSERT INTO name_type (name_type_cd, name_type_name, name_type_description, name_type_code) VALUES (7, 'Partner Name', 'Name of Partner/Spouse', 'P');
INSERT INTO name_type (name_type_cd, name_type_name, name_type_description, name_type_code) VALUES (8, 'Coded Name', 'Coded Pseudo-Name to ensure anonymity', 'S');
INSERT INTO name_type (name_type_cd, name_type_name, name_type_description, name_type_code) VALUES (9, 'Tribal Name', 'Tribal/Community Name', 'T');
INSERT INTO name_type (name_type_cd, name_type_name, name_type_description, name_type_code) VALUES (10, 'Unspecified', 'Unspecified', 'U');

-- Structure for table nationality (OID = 34513):
CREATE TABLE nationality (
    nationality_cd INTEGER NOT NULL,
    nationality_name VARCHAR(64) NOT NULL,
    nationality_description VARCHAR(255),
    nationality_code VARCHAR(64) NOT NULL
) WITHOUT OIDS;

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (1,'AFGHANISTAN','AFGHANISTAN','AF');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (2,'AFGHANISTAN','AFGHANISTAN','AFG');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (3,'AFGHANISTAN','AFGHANISTAN','004');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (4,'ALAND ISLANDS','ALAND ISLANDS','AX');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (5,'ALAND ISLANDS','ALAND ISLANDS','ALA');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (6,'ALAND ISLANDS','ALAND ISLANDS','248');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (7,'ALBANIA','ALBANIA','AL');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (8,'ALBANIA','ALBANIA','ALB');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (9,'ALBANIA','ALBANIA','008');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (10,'ALGERIA','ALGERIA','DZ');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (11,'ALGERIA','ALGERIA','DZA');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (12,'ALGERIA','ALGERIA','012');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (13,'AMERICAN SAMOA','AMERICAN SAMOA','AS');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (14,'AMERICAN SAMOA','AMERICAN SAMOA','ASM');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (15,'AMERICAN SAMOA','AMERICAN SAMOA','016');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (16,'ANDORRA','ANDORRA','AD');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (17,'ANDORRA','ANDORRA','AND');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (18,'ANDORRA','ANDORRA','020');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (19,'ANGOLA','ANGOLA','AO');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (20,'ANGOLA','ANGOLA','AGO');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (21,'ANGOLA','ANGOLA','024');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (22,'ANGUILLA','ANGUILLA','AI');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (23,'ANGUILLA','ANGUILLA','AIA');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (24,'ANGUILLA','ANGUILLA','660');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (25,'ANTARCTICA','ANTARCTICA','AQ');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (26,'ANTARCTICA','ANTARCTICA','ATA');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (27,'ANTARCTICA','ANTARCTICA','010');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (28,'ANTIGUA AND BARBUDA','ANTIGUA AND BARBUDA','AG');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (29,'ANTIGUA AND BARBUDA','ANTIGUA AND BARBUDA','ATG');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (30,'ANTIGUA AND BARBUDA','ANTIGUA AND BARBUDA','028');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (31,'ARGENTINA','ARGENTINA','AR');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (32,'ARGENTINA','ARGENTINA','ARG');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (33,'ARGENTINA','ARGENTINA','032');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (34,'ARMENIA','ARMENIA','AM');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (35,'ARMENIA','ARMENIA','ARM');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (36,'ARMENIA','ARMENIA','051');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (37,'ARUBA','ARUBA','AW');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (38,'ARUBA','ARUBA','ABW');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (39,'ARUBA','ARUBA','533');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (40,'AUSTRALIA','AUSTRALIA','AU');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (41,'AUSTRALIA','AUSTRALIA','AUS');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (42,'AUSTRALIA','AUSTRALIA','036');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (43,'AUSTRIA','AUSTRIA','AT');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (44,'AUSTRIA','AUSTRIA','AUT');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (45,'AUSTRIA','AUSTRIA','040');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (46,'AZERBAIJAN','AZERBAIJAN','AZ');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (47,'AZERBAIJAN','AZERBAIJAN','AZE');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (48,'AZERBAIJAN','AZERBAIJAN','031');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (49,'BAHAMAS','BAHAMAS','BS');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (50,'BAHAMAS','BAHAMAS','BHS');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (51,'BAHAMAS','BAHAMAS','044');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (52,'BAHRAIN','BAHRAIN','BH');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (53,'BAHRAIN','BAHRAIN','BHR');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (54,'BAHRAIN','BAHRAIN','048');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (55,'BANGLADESH','BANGLADESH','BD');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (56,'BANGLADESH','BANGLADESH','BGD');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (57,'BANGLADESH','BANGLADESH','050');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (58,'BARBADOS','BARBADOS','BB');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (59,'BARBADOS','BARBADOS','BRB');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (60,'BARBADOS','BARBADOS','052');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (61,'BELARUS','BELARUS','BY');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (62,'BELARUS','BELARUS','BLR');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (63,'BELARUS','BELARUS','112');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (64,'BELGIUM','BELGIUM','BE');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (65,'BELGIUM','BELGIUM','BEL');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (66,'BELGIUM','BELGIUM','056');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (67,'BELIZE','BELIZE','BZ');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (68,'BELIZE','BELIZE','BLZ');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (69,'BELIZE','BELIZE','084');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (70,'BENIN','BENIN','BJ');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (71,'BENIN','BENIN','BEN');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (72,'BENIN','BENIN','204');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (73,'BERMUDA','BERMUDA','BM');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (74,'BERMUDA','BERMUDA','BMU');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (75,'BERMUDA','BERMUDA','060');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (76,'BHUTAN','BHUTAN','BT');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (77,'BHUTAN','BHUTAN','BTN');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (78,'BHUTAN','BHUTAN','064');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (79,'BOLIVIA, PLURINATIONAL STATE OF','BOLIVIA, PLURINATIONAL STATE OF','BO');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (80,'BOLIVIA, PLURINATIONAL STATE OF','BOLIVIA, PLURINATIONAL STATE OF','BOL');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (81,'BOLIVIA, PLURINATIONAL STATE OF','BOLIVIA, PLURINATIONAL STATE OF','068');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (82,'BOSNIA AND HERZEGOVINA','BOSNIA AND HERZEGOVINA','BA');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (83,'BOSNIA AND HERZEGOVINA','BOSNIA AND HERZEGOVINA','BIH');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (84,'BOSNIA AND HERZEGOVINA','BOSNIA AND HERZEGOVINA','070');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (85,'BOTSWANA','BOTSWANA','BW');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (86,'BOTSWANA','BOTSWANA','BWA');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (87,'BOTSWANA','BOTSWANA','072');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (88,'BOUVET ISLAND','BOUVET ISLAND','BV');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (89,'BOUVET ISLAND','BOUVET ISLAND','BVT');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (90,'BOUVET ISLAND','BOUVET ISLAND','074');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (91,'BRAZIL','BRAZIL','BR');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (92,'BRAZIL','BRAZIL','BRA');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (93,'BRAZIL','BRAZIL','076');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (94,'BRITISH INDIAN OCEAN TERRITORY','BRITISH INDIAN OCEAN TERRITORY','IO');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (95,'BRITISH INDIAN OCEAN TERRITORY','BRITISH INDIAN OCEAN TERRITORY','IOT');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (96,'BRITISH INDIAN OCEAN TERRITORY','BRITISH INDIAN OCEAN TERRITORY','086');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (97,'BRUNEI DARUSSALAM','BRUNEI DARUSSALAM','BN');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (98,'BRUNEI DARUSSALAM','BRUNEI DARUSSALAM','BRN');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (99,'BRUNEI DARUSSALAM','BRUNEI DARUSSALAM','096');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (100,'BULGARIA','BULGARIA','BG');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (101,'BULGARIA','BULGARIA','BGR');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (102,'BULGARIA','BULGARIA','100');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (103,'BURKINA FASO','BURKINA FASO','BF');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (104,'BURKINA FASO','BURKINA FASO','BFA');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (105,'BURKINA FASO','BURKINA FASO','854');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (106,'BURUNDI','BURUNDI','BI');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (107,'BURUNDI','BURUNDI','BDI');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (108,'BURUNDI','BURUNDI','108');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (109,'CAMBODIA','CAMBODIA','KH');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (110,'CAMBODIA','CAMBODIA','KHM');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (111,'CAMBODIA','CAMBODIA','116');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (112,'CAMEROON','CAMEROON','CM');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (113,'CAMEROON','CAMEROON','CMR');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (114,'CAMEROON','CAMEROON','120');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (115,'CANADA','CANADA','CA');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (116,'CANADA','CANADA','CAN');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (117,'CANADA','CANADA','124');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (118,'CAPE VERDE','CAPE VERDE','CV');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (119,'CAPE VERDE','CAPE VERDE','CPV');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (120,'CAPE VERDE','CAPE VERDE','132');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (121,'CAYMAN ISLANDS','CAYMAN ISLANDS','KY');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (122,'CAYMAN ISLANDS','CAYMAN ISLANDS','CYM');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (123,'CAYMAN ISLANDS','CAYMAN ISLANDS','136');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (124,'CENTRAL AFRICAN REPUBLIC','CENTRAL AFRICAN REPUBLIC','CF');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (125,'CENTRAL AFRICAN REPUBLIC','CENTRAL AFRICAN REPUBLIC','CAF');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (126,'CENTRAL AFRICAN REPUBLIC','CENTRAL AFRICAN REPUBLIC','140');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (127,'CHAD','CHAD','TD');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (128,'CHAD','CHAD','TCD');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (129,'CHAD','CHAD','148');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (130,'CHILE','CHILE','CL');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (131,'CHILE','CHILE','CHL');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (132,'CHILE','CHILE','152');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (133,'CHINA','CHINA','CN');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (134,'CHINA','CHINA','CHN');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (135,'CHINA','CHINA','156');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (136,'CHRISTMAS ISLAND','CHRISTMAS ISLAND','CX');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (137,'CHRISTMAS ISLAND','CHRISTMAS ISLAND','CXR');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (138,'CHRISTMAS ISLAND','CHRISTMAS ISLAND','162');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (139,'COCOS (KEELING) ISLANDS','COCOS (KEELING) ISLANDS','CC');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (140,'COCOS (KEELING) ISLANDS','COCOS (KEELING) ISLANDS','CCK');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (141,'COCOS (KEELING) ISLANDS','COCOS (KEELING) ISLANDS','166');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (142,'COLOMBIA','COLOMBIA','CO');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (143,'COLOMBIA','COLOMBIA','COL');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (144,'COLOMBIA','COLOMBIA','170');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (145,'COMOROS','COMOROS','KM');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (146,'COMOROS','COMOROS','COM');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (147,'COMOROS','COMOROS','174');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (148,'CONGO','CONGO','CG');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (149,'CONGO','CONGO','COG');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (150,'CONGO','CONGO','178');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (151,'CONGO, THE DEMOCRATIC REPUBLIC OF THE','CONGO, THE DEMOCRATIC REPUBLIC OF THE','CD');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (152,'CONGO, THE DEMOCRATIC REPUBLIC OF THE','CONGO, THE DEMOCRATIC REPUBLIC OF THE','COD');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (153,'CONGO, THE DEMOCRATIC REPUBLIC OF THE','CONGO, THE DEMOCRATIC REPUBLIC OF THE','180');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (154,'COOK ISLANDS','COOK ISLANDS','CK');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (155,'COOK ISLANDS','COOK ISLANDS','COK');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (156,'COOK ISLANDS','COOK ISLANDS','184');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (157,'COSTA RICA','COSTA RICA','CR');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (158,'COSTA RICA','COSTA RICA','CRI');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (159,'COSTA RICA','COSTA RICA','188');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (160,'COTE D''IVOIRE','COTE D''IVOIRE','CI');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (161,'COTE D''IVOIRE','COTE D''IVOIRE','CIV');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (162,'COTE D''IVOIRE','COTE D''IVOIRE','384');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (163,'CROATIA','CROATIA','HR');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (164,'CROATIA','CROATIA','HRV');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (165,'CROATIA','CROATIA','191');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (166,'CUBA','CUBA','CU');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (167,'CUBA','CUBA','CUB');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (168,'CUBA','CUBA','192');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (169,'CYPRUS','CYPRUS','CY');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (170,'CYPRUS','CYPRUS','CYP');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (171,'CYPRUS','CYPRUS','196');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (172,'CZECH REPUBLIC','CZECH REPUBLIC','CZ');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (173,'CZECH REPUBLIC','CZECH REPUBLIC','CZE');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (174,'CZECH REPUBLIC','CZECH REPUBLIC','203');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (175,'DENMARK','DENMARK','DK');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (176,'DENMARK','DENMARK','DNK');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (177,'DENMARK','DENMARK','208');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (178,'DJIBOUTI','DJIBOUTI','DJ');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (179,'DJIBOUTI','DJIBOUTI','DJI');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (180,'DJIBOUTI','DJIBOUTI','262');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (181,'DOMINICA','DOMINICA','DM');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (182,'DOMINICA','DOMINICA','DMA');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (183,'DOMINICA','DOMINICA','212');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (184,'DOMINICAN REPUBLIC','DOMINICAN REPUBLIC','DO');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (185,'DOMINICAN REPUBLIC','DOMINICAN REPUBLIC','DOM');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (186,'DOMINICAN REPUBLIC','DOMINICAN REPUBLIC','214');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (187,'ECUADOR','ECUADOR','EC');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (188,'ECUADOR','ECUADOR','ECU');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (189,'ECUADOR','ECUADOR','218');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (190,'EGYPT','EGYPT','EG');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (191,'EGYPT','EGYPT','EGY');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (192,'EGYPT','EGYPT','818');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (193,'EL SALVADOR','EL SALVADOR','SV');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (194,'EL SALVADOR','EL SALVADOR','SLV');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (195,'EL SALVADOR','EL SALVADOR','222');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (196,'EQUATORIAL GUINEA','EQUATORIAL GUINEA','GQ');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (197,'EQUATORIAL GUINEA','EQUATORIAL GUINEA','GNQ');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (198,'EQUATORIAL GUINEA','EQUATORIAL GUINEA','226');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (199,'ERITREA','ERITREA','ER');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (200,'ERITREA','ERITREA','ERI');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (201,'ERITREA','ERITREA','232');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (202,'ESTONIA','ESTONIA','EE');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (203,'ESTONIA','ESTONIA','EST');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (204,'ESTONIA','ESTONIA','233');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (205,'ETHIOPIA','ETHIOPIA','ET');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (206,'ETHIOPIA','ETHIOPIA','ETH');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (207,'ETHIOPIA','ETHIOPIA','231');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (208,'FALKLAND ISLANDS (MALVINAS)','FALKLAND ISLANDS (MALVINAS)','FK');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (209,'FALKLAND ISLANDS (MALVINAS)','FALKLAND ISLANDS (MALVINAS)','FLK');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (210,'FALKLAND ISLANDS (MALVINAS)','FALKLAND ISLANDS (MALVINAS)','238');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (211,'FAROE ISLANDS','FAROE ISLANDS','FO');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (212,'FAROE ISLANDS','FAROE ISLANDS','FRO');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (213,'FAROE ISLANDS','FAROE ISLANDS','234');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (214,'FIJI','FIJI','FJ');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (215,'FIJI','FIJI','FJI');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (216,'FIJI','FIJI','242');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (217,'FINLAND','FINLAND','FI');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (218,'FINLAND','FINLAND','FIN');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (219,'FINLAND','FINLAND','246');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (220,'FRANCE','FRANCE','FR');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (221,'FRANCE','FRANCE','FRA');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (222,'FRANCE','FRANCE','250');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (223,'FRENCH GUIANA','FRENCH GUIANA','GF');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (224,'FRENCH GUIANA','FRENCH GUIANA','GUF');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (225,'FRENCH GUIANA','FRENCH GUIANA','254');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (226,'FRENCH POLYNESIA','FRENCH POLYNESIA','PF');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (227,'FRENCH POLYNESIA','FRENCH POLYNESIA','PYF');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (228,'FRENCH POLYNESIA','FRENCH POLYNESIA','258');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (229,'FRENCH SOUTHERN TERRITORIES','FRENCH SOUTHERN TERRITORIES','TF');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (230,'FRENCH SOUTHERN TERRITORIES','FRENCH SOUTHERN TERRITORIES','ATF');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (231,'FRENCH SOUTHERN TERRITORIES','FRENCH SOUTHERN TERRITORIES','260');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (232,'GABON','GABON','GA');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (233,'GABON','GABON','GAB');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (234,'GABON','GABON','266');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (235,'GAMBIA','GAMBIA','GM');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (236,'GAMBIA','GAMBIA','GMB');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (237,'GAMBIA','GAMBIA','270');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (238,'GEORGIA','GEORGIA','GE');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (239,'GEORGIA','GEORGIA','GEO');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (240,'GEORGIA','GEORGIA','268');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (241,'GERMANY','GERMANY','DE');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (242,'GERMANY','GERMANY','DEU');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (243,'GERMANY','GERMANY','276');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (244,'GHANA','GHANA','GH');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (245,'GHANA','GHANA','GHA');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (246,'GHANA','GHANA','288');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (247,'GIBRALTAR','GIBRALTAR','GI');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (248,'GIBRALTAR','GIBRALTAR','GIB');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (249,'GIBRALTAR','GIBRALTAR','292');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (250,'GREECE','GREECE','GR');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (251,'GREECE','GREECE','GRC');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (252,'GREECE','GREECE','300');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (253,'GREENLAND','GREENLAND','GL');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (254,'GREENLAND','GREENLAND','GRL');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (255,'GREENLAND','GREENLAND','304');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (256,'GRENADA','GRENADA','GD');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (257,'GRENADA','GRENADA','GRD');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (258,'GRENADA','GRENADA','308');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (259,'GUADELOUPE','GUADELOUPE','GP');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (260,'GUADELOUPE','GUADELOUPE','GLP');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (261,'GUADELOUPE','GUADELOUPE','312');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (262,'GUAM','GUAM','GU');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (263,'GUAM','GUAM','GUM');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (264,'GUAM','GUAM','316');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (265,'GUATEMALA','GUATEMALA','GT');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (266,'GUATEMALA','GUATEMALA','GTM');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (267,'GUATEMALA','GUATEMALA','320');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (268,'GUERNSEY','GUERNSEY','GG');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (269,'GUERNSEY','GUERNSEY','GGY');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (270,'GUERNSEY','GUERNSEY','831');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (271,'GUINEA','GUINEA','GN');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (272,'GUINEA','GUINEA','GIN');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (273,'GUINEA','GUINEA','324');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (274,'GUINEA-BISSAU','GUINEA-BISSAU','GW');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (275,'GUINEA-BISSAU','GUINEA-BISSAU','GNB');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (276,'GUINEA-BISSAU','GUINEA-BISSAU','624');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (277,'GUYANA','GUYANA','GY');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (278,'GUYANA','GUYANA','GUY');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (279,'GUYANA','GUYANA','328');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (280,'HAITI','HAITI','HT');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (281,'HAITI','HAITI','HTI');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (282,'HAITI','HAITI','332');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (283,'HEARD ISLAND AND MCDONALD ISLANDS','HEARD ISLAND AND MCDONALD ISLANDS','HM');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (284,'HEARD ISLAND AND MCDONALD ISLANDS','HEARD ISLAND AND MCDONALD ISLANDS','HMD');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (285,'HEARD ISLAND AND MCDONALD ISLANDS','HEARD ISLAND AND MCDONALD ISLANDS','334');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (286,'HOLY SEE (VATICAN CITY STATE)','HOLY SEE (VATICAN CITY STATE)','VA');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (287,'HOLY SEE (VATICAN CITY STATE)','HOLY SEE (VATICAN CITY STATE)','VAT');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (288,'HOLY SEE (VATICAN CITY STATE)','HOLY SEE (VATICAN CITY STATE)','336');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (289,'HONDURAS','HONDURAS','HN');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (290,'HONDURAS','HONDURAS','HND');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (291,'HONDURAS','HONDURAS','340');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (292,'HONG KONG','HONG KONG','HK');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (293,'HONG KONG','HONG KONG','HKG');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (294,'HONG KONG','HONG KONG','344');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (295,'HUNGARY','HUNGARY','HU');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (296,'HUNGARY','HUNGARY','HUN');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (297,'HUNGARY','HUNGARY','348');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (298,'ICELAND','ICELAND','IS');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (299,'ICELAND','ICELAND','ISL');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (300,'ICELAND','ICELAND','352');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (301,'INDIA','INDIA','IN');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (302,'INDIA','INDIA','IND');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (303,'INDIA','INDIA','356');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (304,'INDONESIA','INDONESIA','ID');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (305,'INDONESIA','INDONESIA','IDN');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (306,'INDONESIA','INDONESIA','360');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (307,'IRAN (ISLAMIC REPUBLIC OF)','IRAN (ISLAMIC REPUBLIC OF)','IR');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (308,'IRAN (ISLAMIC REPUBLIC OF)','IRAN (ISLAMIC REPUBLIC OF)','IRN');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (309,'IRAN (ISLAMIC REPUBLIC OF)','IRAN (ISLAMIC REPUBLIC OF)','364');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (310,'IRAQ','IRAQ','IQ');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (311,'IRAQ','IRAQ','IRQ');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (312,'IRAQ','IRAQ','368');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (313,'IRELAND','IRELAND','IE');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (314,'IRELAND','IRELAND','IRL');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (315,'IRELAND','IRELAND','372');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (316,'ISLE OF MAN','ISLE OF MAN','IM');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (317,'ISLE OF MAN','ISLE OF MAN','IMM');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (318,'ISLE OF MAN','ISLE OF MAN','833');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (319,'ISRAEL','ISRAEL','IL');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (320,'ISRAEL','ISRAEL','ISR');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (321,'ISRAEL','ISRAEL','376');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (322,'ITALY','ITALY','IT');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (323,'ITALY','ITALY','ITA');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (324,'ITALY','ITALY','380');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (325,'JAMAICA','JAMAICA','JM');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (326,'JAMAICA','JAMAICA','JAM');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (327,'JAMAICA','JAMAICA','388');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (328,'JAPAN','JAPAN','JP');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (329,'JAPAN','JAPAN','JPN');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (330,'JAPAN','JAPAN','392');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (331,'JERSEY','JERSEY','JE');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (332,'JERSEY','JERSEY','JEY');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (333,'JERSEY','JERSEY','832');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (334,'JORDAN','JORDAN','JO');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (335,'JORDAN','JORDAN','JOR');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (336,'JORDAN','JORDAN','400');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (337,'KAZAKHSTAN','KAZAKHSTAN','KZ');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (338,'KAZAKHSTAN','KAZAKHSTAN','KAZ');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (339,'KAZAKHSTAN','KAZAKHSTAN','398');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (340,'KENYA','KENYA','KE');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (341,'KENYA','KENYA','KEN');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (342,'KENYA','KENYA','404');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (343,'KIRIBATI','KIRIBATI','KI');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (344,'KIRIBATI','KIRIBATI','KIR');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (345,'KIRIBATI','KIRIBATI','296');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (346,'KOREA, DEMOCRATIC PEOPLE''S REPUBLIC OF','KOREA, DEMOCRATIC PEOPLE''S REPUBLIC OF','KP');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (347,'KOREA, DEMOCRATIC PEOPLE''S REPUBLIC OF','KOREA, DEMOCRATIC PEOPLE''S REPUBLIC OF','PRK');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (348,'KOREA, DEMOCRATIC PEOPLE''S REPUBLIC OF','KOREA, DEMOCRATIC PEOPLE''S REPUBLIC OF','408');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (349,'KOREA, REPUBLIC OF','KOREA, REPUBLIC OF','KR');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (350,'KOREA, REPUBLIC OF','KOREA, REPUBLIC OF','KOR');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (351,'KOREA, REPUBLIC OF','KOREA, REPUBLIC OF','410');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (352,'KUWAIT','KUWAIT','KW');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (353,'KUWAIT','KUWAIT','KWT');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (354,'KUWAIT','KUWAIT','414');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (355,'KYRGYZSTAN','KYRGYZSTAN','KG');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (356,'KYRGYZSTAN','KYRGYZSTAN','KGZ');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (357,'KYRGYZSTAN','KYRGYZSTAN','417');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (358,'LAO PEOPLE''S DEMOCRATIC REPUBLIC','LAO PEOPLE''S DEMOCRATIC REPUBLIC','LA');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (359,'LAO PEOPLE''S DEMOCRATIC REPUBLIC','LAO PEOPLE''S DEMOCRATIC REPUBLIC','LAO');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (360,'LAO PEOPLE''S DEMOCRATIC REPUBLIC','LAO PEOPLE''S DEMOCRATIC REPUBLIC','418');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (361,'LATVIA','LATVIA','LV');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (362,'LATVIA','LATVIA','LVA');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (363,'LATVIA','LATVIA','428');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (364,'LEBANON','LEBANON','LB');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (365,'LEBANON','LEBANON','LBN');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (366,'LEBANON','LEBANON','422');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (367,'LESOTHO','LESOTHO','LS');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (368,'LESOTHO','LESOTHO','LSO');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (369,'LESOTHO','LESOTHO','426');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (370,'LIBERIA','LIBERIA','LR');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (371,'LIBERIA','LIBERIA','LBR');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (372,'LIBERIA','LIBERIA','430');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (373,'LIBYAN ARAB JAMAHIRIYA','LIBYAN ARAB JAMAHIRIYA','LY');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (374,'LIBYAN ARAB JAMAHIRIYA','LIBYAN ARAB JAMAHIRIYA','LBY');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (375,'LIBYAN ARAB JAMAHIRIYA','LIBYAN ARAB JAMAHIRIYA','434');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (376,'LIECHTENSTEIN','LIECHTENSTEIN','LI');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (377,'LIECHTENSTEIN','LIECHTENSTEIN','LIE');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (378,'LIECHTENSTEIN','LIECHTENSTEIN','438');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (379,'LITHUANIA','LITHUANIA','LT');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (380,'LITHUANIA','LITHUANIA','LTU');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (381,'LITHUANIA','LITHUANIA','440');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (382,'LUXEMBOURG','LUXEMBOURG','LU');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (383,'LUXEMBOURG','LUXEMBOURG','LUX');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (384,'LUXEMBOURG','LUXEMBOURG','442');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (385,'MACAO','MACAO','MO');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (386,'MACAO','MACAO','MAC');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (387,'MACAO','MACAO','446');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (388,'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF','MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF','MK');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (389,'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF','MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF','MKD');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (390,'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF','MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF','807');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (391,'MADAGASCAR','MADAGASCAR','MG');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (392,'MADAGASCAR','MADAGASCAR','MDG');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (393,'MADAGASCAR','MADAGASCAR','450');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (394,'MALAWI','MALAWI','MW');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (395,'MALAWI','MALAWI','MWI');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (396,'MALAWI','MALAWI','454');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (397,'MALAYSIA','MALAYSIA','MY');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (398,'MALAYSIA','MALAYSIA','MYS');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (399,'MALAYSIA','MALAYSIA','458');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (400,'MALDIVES','MALDIVES','MV');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (401,'MALDIVES','MALDIVES','MDV');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (402,'MALDIVES','MALDIVES','462');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (403,'MALI','MALI','ML');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (404,'MALI','MALI','MLI');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (405,'MALI','MALI','466');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (406,'MALTA','MALTA','MT');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (407,'MALTA','MALTA','MLT');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (408,'MALTA','MALTA','470');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (409,'MARSHALL ISLANDS','MARSHALL ISLANDS','MH');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (410,'MARSHALL ISLANDS','MARSHALL ISLANDS','MHL');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (411,'MARSHALL ISLANDS','MARSHALL ISLANDS','584');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (412,'MARTINIQUE','MARTINIQUE','MQ');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (413,'MARTINIQUE','MARTINIQUE','MTQ');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (414,'MARTINIQUE','MARTINIQUE','474');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (415,'MAURITANIA','MAURITANIA','MR');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (416,'MAURITANIA','MAURITANIA','MRT');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (417,'MAURITANIA','MAURITANIA','478');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (418,'MAURITIUS','MAURITIUS','MU');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (419,'MAURITIUS','MAURITIUS','MUS');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (420,'MAURITIUS','MAURITIUS','480');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (421,'MAYOTTE','MAYOTTE','YT');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (422,'MAYOTTE','MAYOTTE','MYT');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (423,'MAYOTTE','MAYOTTE','175');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (424,'MEXICO','MEXICO','MX');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (425,'MEXICO','MEXICO','MEX');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (426,'MEXICO','MEXICO','484');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (427,'MICRONESIA, FEDERATED STATES OF','MICRONESIA, FEDERATED STATES OF','FM');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (428,'MICRONESIA, FEDERATED STATES OF','MICRONESIA, FEDERATED STATES OF','FSM');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (429,'MICRONESIA, FEDERATED STATES OF','MICRONESIA, FEDERATED STATES OF','583');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (430,'MOLDOVA, REPUBLIC OF','MOLDOVA, REPUBLIC OF','MD');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (431,'MOLDOVA, REPUBLIC OF','MOLDOVA, REPUBLIC OF','MDA');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (432,'MOLDOVA, REPUBLIC OF','MOLDOVA, REPUBLIC OF','498');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (433,'MONACO','MONACO','MC');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (434,'MONACO','MONACO','MCO');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (435,'MONACO','MONACO','492');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (436,'MONGOLIA','MONGOLIA','MN');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (437,'MONGOLIA','MONGOLIA','MNG');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (438,'MONGOLIA','MONGOLIA','496');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (439,'MONTENEGRO','MONTENEGRO','ME');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (440,'MONTENEGRO','MONTENEGRO','MNE');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (441,'MONTENEGRO','MONTENEGRO','499');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (442,'MONTSERRAT','MONTSERRAT','MS');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (443,'MONTSERRAT','MONTSERRAT','MSR');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (444,'MONTSERRAT','MONTSERRAT','500');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (445,'MOROCCO','MOROCCO','MA');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (446,'MOROCCO','MOROCCO','MAR');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (447,'MOROCCO','MOROCCO','504');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (448,'MOZAMBIQUE','MOZAMBIQUE','MZ');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (449,'MOZAMBIQUE','MOZAMBIQUE','MOZ');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (450,'MOZAMBIQUE','MOZAMBIQUE','508');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (451,'MYANMAR','MYANMAR','MM');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (452,'MYANMAR','MYANMAR','MMR');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (453,'MYANMAR','MYANMAR','104');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (454,'NAMIBIA','NAMIBIA','NA');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (455,'NAMIBIA','NAMIBIA','NAM');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (456,'NAMIBIA','NAMIBIA','516');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (457,'NAURU','NAURU','NR');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (458,'NAURU','NAURU','NRU');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (459,'NAURU','NAURU','520');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (460,'NEPAL','NEPAL','NP');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (461,'NEPAL','NEPAL','NPL');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (462,'NEPAL','NEPAL','524');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (463,'NETHERLANDS','NETHERLANDS','NL');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (464,'NETHERLANDS','NETHERLANDS','NLD');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (465,'NETHERLANDS','NETHERLANDS','528');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (466,'NETHERLANDS ANTILLES','NETHERLANDS ANTILLES','AN');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (467,'NETHERLANDS ANTILLES','NETHERLANDS ANTILLES','ANT');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (468,'NETHERLANDS ANTILLES','NETHERLANDS ANTILLES','530');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (469,'NEW CALEDONIA','NEW CALEDONIA','NC');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (470,'NEW CALEDONIA','NEW CALEDONIA','NCL');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (471,'NEW CALEDONIA','NEW CALEDONIA','540');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (472,'NEW ZEALAND','NEW ZEALAND','NZ');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (473,'NEW ZEALAND','NEW ZEALAND','NZL');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (474,'NEW ZEALAND','NEW ZEALAND','554');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (475,'NICARAGUA','NICARAGUA','NI');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (476,'NICARAGUA','NICARAGUA','NIC');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (477,'NICARAGUA','NICARAGUA','558');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (478,'NIGER','NIGER','NE');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (479,'NIGER','NIGER','NER');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (480,'NIGER','NIGER','562');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (481,'NIGERIA','NIGERIA','NG');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (482,'NIGERIA','NIGERIA','NGA');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (483,'NIGERIA','NIGERIA','566');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (484,'NIUE','NIUE','NU');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (485,'NIUE','NIUE','NIU');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (486,'NIUE','NIUE','570');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (487,'NORFOLK ISLAND','NORFOLK ISLAND','NF');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (488,'NORFOLK ISLAND','NORFOLK ISLAND','NFK');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (489,'NORFOLK ISLAND','NORFOLK ISLAND','574');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (490,'NORTHERN MARIANA ISLANDS','NORTHERN MARIANA ISLANDS','MP');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (491,'NORTHERN MARIANA ISLANDS','NORTHERN MARIANA ISLANDS','MNP');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (492,'NORTHERN MARIANA ISLANDS','NORTHERN MARIANA ISLANDS','580');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (493,'NORWAY','NORWAY','NO');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (494,'NORWAY','NORWAY','NOR');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (495,'NORWAY','NORWAY','578');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (496,'OMAN','OMAN','OM');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (497,'OMAN','OMAN','OMN');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (498,'OMAN','OMAN','512');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (499,'PAKISTAN','PAKISTAN','PK');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (500,'PAKISTAN','PAKISTAN','PAK');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (501,'PAKISTAN','PAKISTAN','586');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (502,'PALAU','PALAU','PW');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (503,'PALAU','PALAU','PLW');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (504,'PALAU','PALAU','585');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (505,'PALESTINIAN TERRITORY, OCCUPIED','PALESTINIAN TERRITORY, OCCUPIED','PS');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (506,'PALESTINIAN TERRITORY, OCCUPIED','PALESTINIAN TERRITORY, OCCUPIED','PSE');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (507,'PALESTINIAN TERRITORY, OCCUPIED','PALESTINIAN TERRITORY, OCCUPIED','275');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (508,'PANAMA','PANAMA','PA');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (509,'PANAMA','PANAMA','PAN');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (510,'PANAMA','PANAMA','591');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (511,'PAPUA NEW GUINEA','PAPUA NEW GUINEA','PG');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (512,'PAPUA NEW GUINEA','PAPUA NEW GUINEA','PNG');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (513,'PAPUA NEW GUINEA','PAPUA NEW GUINEA','598');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (514,'PARAGUAY','PARAGUAY','PY');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (515,'PARAGUAY','PARAGUAY','PRY');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (516,'PARAGUAY','PARAGUAY','600');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (517,'PERU','PERU','PE');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (518,'PERU','PERU','PER');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (519,'PERU','PERU','604');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (520,'PHILIPPINES','PHILIPPINES','PH');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (521,'PHILIPPINES','PHILIPPINES','PHL');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (522,'PHILIPPINES','PHILIPPINES','608');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (523,'PITCAIRN','PITCAIRN','PN');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (524,'PITCAIRN','PITCAIRN','PCN');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (525,'PITCAIRN','PITCAIRN','612');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (526,'POLAND','POLAND','PL');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (527,'POLAND','POLAND','POL');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (528,'POLAND','POLAND','616');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (529,'PORTUGAL','PORTUGAL','PT');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (530,'PORTUGAL','PORTUGAL','PRT');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (531,'PORTUGAL','PORTUGAL','620');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (532,'PUERTO RICO','PUERTO RICO','PR');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (533,'PUERTO RICO','PUERTO RICO','PRI');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (534,'PUERTO RICO','PUERTO RICO','630');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (535,'QATAR','QATAR','QA');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (536,'QATAR','QATAR','QAT');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (537,'QATAR','QATAR','634');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (538,'REUNION','REUNION','RE');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (539,'REUNION','REUNION','REU');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (540,'REUNION','REUNION','638');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (541,'ROMANIA','ROMANIA','RO');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (542,'ROMANIA','ROMANIA','ROU');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (543,'ROMANIA','ROMANIA','642');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (544,'RUSSIAN FEDERATION','RUSSIAN FEDERATION','RU');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (545,'RUSSIAN FEDERATION','RUSSIAN FEDERATION','RUS');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (546,'RUSSIAN FEDERATION','RUSSIAN FEDERATION','643');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (547,'RWANDA','RWANDA','RW');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (548,'RWANDA','RWANDA','RWA');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (549,'RWANDA','RWANDA','646');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (550,'SAINT BARTHELEMY','SAINT BARTHELEMY','BL');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (551,'SAINT BARTHELEMY','SAINT BARTHELEMY','BLM');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (552,'SAINT BARTHELEMY','SAINT BARTHELEMY','652');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (553,'SAINT HELENA','SAINT HELENA','SH');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (554,'SAINT HELENA','SAINT HELENA','SHN');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (555,'SAINT HELENA','SAINT HELENA','654');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (556,'SAINT KITTS AND NEVIS','SAINT KITTS AND NEVIS','KN');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (557,'SAINT KITTS AND NEVIS','SAINT KITTS AND NEVIS','KNA');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (558,'SAINT KITTS AND NEVIS','SAINT KITTS AND NEVIS','659');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (559,'SAINT LUCIA','SAINT LUCIA','LC');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (560,'SAINT LUCIA','SAINT LUCIA','LCA');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (561,'SAINT LUCIA','SAINT LUCIA','662');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (562,'SAINT MARTIN (FRENCH PART)','SAINT MARTIN (FRENCH PART)','MT');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (563,'SAINT MARTIN (FRENCH PART)','SAINT MARTIN (FRENCH PART)','MAF');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (564,'SAINT MARTIN (FRENCH PART)','SAINT MARTIN (FRENCH PART)','663');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (565,'SAINT PIERRE AND MIQUELON','SAINT PIERRE AND MIQUELON','PM');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (566,'SAINT PIERRE AND MIQUELON','SAINT PIERRE AND MIQUELON','SPM');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (567,'SAINT PIERRE AND MIQUELON','SAINT PIERRE AND MIQUELON','666');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (568,'SAINT VINCENT AND THE GRENADINES','SAINT VINCENT AND THE GRENADINES','VC');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (569,'SAINT VINCENT AND THE GRENADINES','SAINT VINCENT AND THE GRENADINES','VCT');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (570,'SAINT VINCENT AND THE GRENADINES','SAINT VINCENT AND THE GRENADINES','670');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (571,'SAMOA','SAMOA','WS');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (572,'SAMOA','SAMOA','WSM');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (573,'SAMOA','SAMOA','882');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (574,'SAN MARINO','SAN MARINO','SM');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (575,'SAN MARINO','SAN MARINO','SMR');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (576,'SAN MARINO','SAN MARINO','674');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (577,'SAO TOME AND PRINCIPE','SAO TOME AND PRINCIPE','ST');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (578,'SAO TOME AND PRINCIPE','SAO TOME AND PRINCIPE','STP');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (579,'SAO TOME AND PRINCIPE','SAO TOME AND PRINCIPE','678');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (580,'SAUDI ARABIA','SAUDI ARABIA','SA');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (581,'SAUDI ARABIA','SAUDI ARABIA','SAU');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (582,'SAUDI ARABIA','SAUDI ARABIA','682');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (583,'SENEGAL','SENEGAL','SN');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (584,'SENEGAL','SENEGAL','SEN');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (585,'SENEGAL','SENEGAL','686');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (586,'SERBIA','SERBIA','RS');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (587,'SERBIA','SERBIA','SRB');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (588,'SERBIA','SERBIA','688');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (589,'SEYCHELLES','SEYCHELLES','SC');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (590,'SEYCHELLES','SEYCHELLES','SYC');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (591,'SEYCHELLES','SEYCHELLES','690');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (592,'SIERRA LEONE','SIERRA LEONE','SL');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (593,'SIERRA LEONE','SIERRA LEONE','SLE');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (594,'SIERRA LEONE','SIERRA LEONE','694');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (595,'SINGAPORE','SINGAPORE','SG');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (596,'SINGAPORE','SINGAPORE','SGP');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (597,'SINGAPORE','SINGAPORE','702');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (598,'SLOVAKIA','SLOVAKIA','SK');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (599,'SLOVAKIA','SLOVAKIA','SVK');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (600,'SLOVAKIA','SLOVAKIA','703');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (601,'SLOVENIA','SLOVENIA','SI');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (602,'SLOVENIA','SLOVENIA','SVN');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (603,'SLOVENIA','SLOVENIA','705');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (604,'SOLOMON ISLANDS','SOLOMON ISLANDS','SB');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (605,'SOLOMON ISLANDS','SOLOMON ISLANDS','SLB');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (606,'SOLOMON ISLANDS','SOLOMON ISLANDS','090');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (607,'SOMALIA','SOMALIA','SO');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (608,'SOMALIA','SOMALIA','SOM');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (609,'SOMALIA','SOMALIA','706');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (610,'SOUTH AFRICA','SOUTH AFRICA','ZA');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (611,'SOUTH AFRICA','SOUTH AFRICA','ZAF');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (612,'SOUTH AFRICA','SOUTH AFRICA','710');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (613,'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS','SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS','GS');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (614,'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS','SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS','SGS');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (615,'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS','SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS','239');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (616,'SPAIN','SPAIN','ES');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (617,'SPAIN','SPAIN','ESP');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (618,'SPAIN','SPAIN','724');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (619,'SRI LANKA','SRI LANKA','LK');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (620,'SRI LANKA','SRI LANKA','LKA');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (621,'SRI LANKA','SRI LANKA','144');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (622,'SUDAN','SUDAN','SD');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (623,'SUDAN','SUDAN','SDN');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (624,'SUDAN','SUDAN','736');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (625,'SURINAME','SURINAME','SR');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (626,'SURINAME','SURINAME','SUR');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (627,'SURINAME','SURINAME','740');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (628,'SVALBARD AND JAN MAYEN','SVALBARD AND JAN MAYEN','SJ');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (629,'SVALBARD AND JAN MAYEN','SVALBARD AND JAN MAYEN','SJM');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (630,'SVALBARD AND JAN MAYEN','SVALBARD AND JAN MAYEN','744');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (631,'SWAZILAND','SWAZILAND','SZ');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (632,'SWAZILAND','SWAZILAND','SWZ');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (633,'SWAZILAND','SWAZILAND','748');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (634,'SWEDEN','SWEDEN','SE');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (635,'SWEDEN','SWEDEN','SWE');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (636,'SWEDEN','SWEDEN','752');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (637,'SWITZERLAND','SWITZERLAND','CH');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (638,'SWITZERLAND','SWITZERLAND','CHE');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (639,'SWITZERLAND','SWITZERLAND','756');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (640,'SYRIAN ARAB REPUBLIC','SYRIAN ARAB REPUBLIC','SY');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (641,'SYRIAN ARAB REPUBLIC','SYRIAN ARAB REPUBLIC','SYR');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (642,'SYRIAN ARAB REPUBLIC','SYRIAN ARAB REPUBLIC','760');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (643,'TAIWAN, PROVINCE OF CHINA','TAIWAN, PROVINCE OF CHINA','TW');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (644,'TAIWAN, PROVINCE OF CHINA','TAIWAN, PROVINCE OF CHINA','TWN');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (645,'TAIWAN, PROVINCE OF CHINA','TAIWAN, PROVINCE OF CHINA','158');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (646,'TAJIKISTAN','TAJIKISTAN','TJ');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (647,'TAJIKISTAN','TAJIKISTAN','TJK');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (648,'TAJIKISTAN','TAJIKISTAN','762');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (649,'TANZANIA, UNITED REPUBLIC OF','TANZANIA, UNITED REPUBLIC OF','TZ');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (650,'TANZANIA, UNITED REPUBLIC OF','TANZANIA, UNITED REPUBLIC OF','TZA');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (651,'TANZANIA, UNITED REPUBLIC OF','TANZANIA, UNITED REPUBLIC OF','834');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (652,'THAILAND','THAILAND','TH');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (653,'THAILAND','THAILAND','THA');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (654,'THAILAND','THAILAND','764');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (655,'TIMOR-LESTE','TIMOR-LESTE','TL');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (656,'TIMOR-LESTE','TIMOR-LESTE','TLS');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (657,'TIMOR-LESTE','TIMOR-LESTE','626');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (658,'TOGO','TOGO','TG');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (659,'TOGO','TOGO','TGO');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (660,'TOGO','TOGO','768');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (661,'TOKELAU','TOKELAU','TK');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (662,'TOKELAU','TOKELAU','TKL');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (663,'TOKELAU','TOKELAU','772');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (664,'TONGA','TONGA','TO');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (665,'TONGA','TONGA','TON');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (666,'TONGA','TONGA','776');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (667,'TRINIDAD AND TOBAGO','TRINIDAD AND TOBAGO','TT');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (668,'TRINIDAD AND TOBAGO','TRINIDAD AND TOBAGO','TTO');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (669,'TRINIDAD AND TOBAGO','TRINIDAD AND TOBAGO','780');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (670,'TUNISIA','TUNISIA','TN');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (671,'TUNISIA','TUNISIA','TUN');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (672,'TUNISIA','TUNISIA','788');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (673,'TURKEY','TURKEY','TR');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (674,'TURKEY','TURKEY','TUR');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (675,'TURKEY','TURKEY','792');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (676,'TURKMENISTAN','TURKMENISTAN','TM');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (677,'TURKMENISTAN','TURKMENISTAN','TKM');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (678,'TURKMENISTAN','TURKMENISTAN','795');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (679,'TURKS AND CAICOS ISLANDS','TURKS AND CAICOS ISLANDS','TC');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (680,'TURKS AND CAICOS ISLANDS','TURKS AND CAICOS ISLANDS','TCA');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (681,'TURKS AND CAICOS ISLANDS','TURKS AND CAICOS ISLANDS','796');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (682,'TUVALU','TUVALU','TV');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (683,'TUVALU','TUVALU','TUV');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (684,'TUVALU','TUVALU','798');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (685,'UGANDA','UGANDA','UG');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (686,'UGANDA','UGANDA','UGA');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (687,'UGANDA','UGANDA','800');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (688,'UKRAINE','UKRAINE','UA');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (689,'UKRAINE','UKRAINE','UKR');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (690,'UKRAINE','UKRAINE','804');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (691,'UNITED ARAB EMIRATES','UNITED ARAB EMIRATES','AE');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (692,'UNITED ARAB EMIRATES','UNITED ARAB EMIRATES','ARE');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (693,'UNITED ARAB EMIRATES','UNITED ARAB EMIRATES','784');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (694,'UNITED KINGDOM','UNITED KINGDOM','GB');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (695,'UNITED KINGDOM','UNITED KINGDOM','GBR');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (696,'UNITED KINGDOM','UNITED KINGDOM','826');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (697,'UNITED STATES','UNITED STATES','US');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (698,'UNITED STATES','UNITED STATES','USA');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (699,'UNITED STATES','UNITED STATES','840');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (700,'UNITED STATES MINOR OUTLYING ISLANDS','UNITED STATES MINOR OUTLYING ISLANDS','UM');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (701,'UNITED STATES MINOR OUTLYING ISLANDS','UNITED STATES MINOR OUTLYING ISLANDS','UMI');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (702,'UNITED STATES MINOR OUTLYING ISLANDS','UNITED STATES MINOR OUTLYING ISLANDS','581');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (703,'URUGUAY','URUGUAY','UY');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (704,'URUGUAY','URUGUAY','URY');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (705,'URUGUAY','URUGUAY','858');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (706,'UZBEKISTAN','UZBEKISTAN','UZ');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (707,'UZBEKISTAN','UZBEKISTAN','UZB');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (708,'UZBEKISTAN','UZBEKISTAN','860');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (709,'VANUATU','VANUATU','VU');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (710,'VANUATU','VANUATU','VUT');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (711,'VANUATU','VANUATU','548');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (712,'VATICAN CITY STATE (HOLY SEE)','VATICAN CITY STATE (HOLY SEE)','VA');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (713,'VATICAN CITY STATE (HOLY SEE)','VATICAN CITY STATE (HOLY SEE)','VAT');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (714,'VATICAN CITY STATE (HOLY SEE)','VATICAN CITY STATE (HOLY SEE)','336');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (715,'VENEZUELA, BOLIVARIAN REPUBLIC OF','VENEZUELA, BOLIVARIAN REPUBLIC OF','VE');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (716,'VENEZUELA, BOLIVARIAN REPUBLIC OF','VENEZUELA, BOLIVARIAN REPUBLIC OF','VEN');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (717,'VENEZUELA, BOLIVARIAN REPUBLIC OF','VENEZUELA, BOLIVARIAN REPUBLIC OF','862');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (718,'VIET NAM','VIET NAM','VN');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (719,'VIET NAM','VIET NAM','VNM');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (720,'VIET NAM','VIET NAM','704');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (721,'VIRGIN ISLANDS (BRITISH)','VIRGIN ISLANDS (BRITISH)','VG');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (722,'VIRGIN ISLANDS (BRITISH)','VIRGIN ISLANDS (BRITISH)','VGB');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (723,'VIRGIN ISLANDS (BRITISH)','VIRGIN ISLANDS (BRITISH)','092');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (724,'VIRGIN ISLANDS (U.S.)','VIRGIN ISLANDS (U.S.)','VI');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (725,'VIRGIN ISLANDS (U.S.)','VIRGIN ISLANDS (U.S.)','VIR');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (726,'VIRGIN ISLANDS (U.S.)','VIRGIN ISLANDS (U.S.)','850');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (727,'WALLIS AND FUTUNA','WALLIS AND FUTUNA','WF');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (728,'WALLIS AND FUTUNA','WALLIS AND FUTUNA','WLF');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (729,'WALLIS AND FUTUNA','WALLIS AND FUTUNA','876');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (730,'WESTERN SAHARA','WESTERN SAHARA','EH');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (731,'WESTERN SAHARA','WESTERN SAHARA','ESH');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (732,'WESTERN SAHARA','WESTERN SAHARA','732');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (733,'YEMEN','YEMEN','YE');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (734,'YEMEN','YEMEN','YEM');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (735,'YEMEN','YEMEN','887');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (736,'YUGOSLAVIA','YUGOSLAVIA','YU');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (737,'YUGOSLAVIA','YUGOSLAVIA','YUG');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (738,'YUGOSLAVIA','YUGOSLAVIA','891');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (739,'ZAMBIA','ZAMBIA','ZM');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (740,'ZAMBIA','ZAMBIA','ZMB');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (741,'ZAMBIA','ZAMBIA','894');

INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (742,'ZIMBABWE','ZIMBABWE','ZW');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (743,'ZIMBABWE','ZIMBABWE','ZWE');
INSERT INTO nationality (nationality_cd, nationality_name, nationality_description, nationality_code) VALUES (744,'ZIMBABWE','ZIMBABWE','716');

-- Structure for table person (OID = 34518):
CREATE TABLE person (
    person_id BIGINT NOT NULL,

    given_name VARCHAR(64),
    middle_name VARCHAR(64),
    family_name VARCHAR(64),
    family_name2 VARCHAR(64),
    prefix VARCHAR(50),
    suffix VARCHAR(50),
    name_type_cd INTEGER,

    date_of_birth DATE,
    birth_place VARCHAR(255),
    multiple_birth_ind VARCHAR(1),
    birth_order INTEGER,
    mothers_maiden_name VARCHAR(50),
    ssn VARCHAR(30),

    gender_cd INTEGER,
    ethnic_group_cd INTEGER,
    race_cd INTEGER,
    nationality_cd INTEGER,
    language_cd INTEGER,
    religion_cd INTEGER,
    marital_status_code VARCHAR(1),
    degree VARCHAR(50),

    email VARCHAR(255),
    address1 VARCHAR(64),
    address2 VARCHAR(64),
    city VARCHAR(64),
    state VARCHAR(64),
    postal_code VARCHAR(30),
    country VARCHAR(64),
    country_code VARCHAR(16),
    address_type_cd INTEGER,
    phone_country_code VARCHAR(32),
    phone_area_code VARCHAR(32),
    phone_number VARCHAR(64),
    phone_ext VARCHAR(30),

    date_created TIMESTAMP without TIME ZONE NOT NULL,
    creator_id bigint NOT NULL,
    date_changed TIMESTAMP without TIME ZONE,
    changed_by_id bigint,
    date_voided TIMESTAMP without TIME ZONE,
    voided_by_id bigint,
    death_ind VARCHAR(1),
    death_time DATE,

    custom1 VARCHAR(255),
    custom2 VARCHAR(255),
    custom3 VARCHAR(255),
    custom4 VARCHAR(255),
    custom5 VARCHAR(255)
) WITHOUT OIDS;

-- Structure for table person_identifier (OID = 34526):
CREATE TABLE person_identifier (
    person_identifier_id INTEGER NOT NULL,
    date_created TIMESTAMP without TIME ZONE NOT NULL,
    date_voided TIMESTAMP without TIME ZONE,
    identifier VARCHAR(255) NOT NULL,
    person_id INTEGER NOT NULL,
    creator_id bigint NOT NULL,
    identifier_domain_id INTEGER NOT NULL,
    voided_by_id bigint
) WITHOUT OIDS;

-- Structure for table person_link (OID = 34531):
CREATE TABLE person_link (
    person_link_id INTEGER NOT NULL,
    date_created TIMESTAMP without TIME ZONE NOT NULL,
    weight DOUBLE PRECISION NOT NULL,
    rh_person_id INTEGER NOT NULL,
    creator_id bigint NOT NULL,
    lh_person_id INTEGER NOT NULL
) WITHOUT OIDS;

-- Structure for table race (OID = 34536):
CREATE TABLE race (
    race_cd INTEGER NOT NULL,
    race_name VARCHAR(64) NOT NULL,
    race_description VARCHAR(255),
    race_code VARCHAR(64) NOT NULL
) WITHOUT OIDS;

INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (1,'American Indian or Alaska Native','American Indian or Alaska Native','1002-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (2,'American Indian','American Indian','1004-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (3,'Abenaki','Abenaki','1006-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (4,'Algonquian','Algonquian','1008-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (5,'Apache','Apache','1010-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (6,'Chiricahua','Chiricahua','1011-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (7,'Fort Sill Apache','Fort Sill Apache','1012-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (8,'Jicarilla Apache','Jicarilla Apache','1013-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (9,'Lipan Apache','Lipan Apache','1014-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (10,'Mescalero Apache','Mescalero Apache','1015-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (11,'Oklahoma Apache','Oklahoma Apache','1016-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (12,'Payson Apache','Payson Apache','1017-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (13,'San Carlos Apache ','San Carlos Apache ','1018-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (14,'White Mountain Apache','White Mountain Apache','1019-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (15,'Arapaho','Arapaho','1021-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (16,'Northern Arapaho','Northern Arapaho','1022-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (17,'Southern Arapaho ','Southern Arapaho ','1023-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (18,'Wind River Arapaho','Wind River Arapaho','1024-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (19,'Arikara','Arikara','1026-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (20,'Assiniboine','Assiniboine','1028-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (21,'Assiniboine Sioux','Assiniboine Sioux','1030-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (22,'Fort Peck Assiniboine Sioux','Fort Peck Assiniboine Sioux','1031-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (23,'Bannock','Bannock','1033-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (24,'Blackfeet','Blackfeet','1035-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (25,'Brotherton','Brotherton','1037-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (26,'Burt Lake Band','Burt Lake Band','1039-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (27,'Caddo','Caddo','1041-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (28,'Oklahoma Cado','Oklahoma Cado','1042-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (29,'Cahuilla','Cahuilla','1044-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (30,'Agua Caliente Cahuilla','Agua Caliente Cahuilla','1045-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (31,'Augustine','Augustine','1046-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (32,'Cabazon','Cabazon','1047-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (33,'Los Coyotes','Los Coyotes','1048-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (34,'Morongo','Morongo','1049-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (35,'Santa Rosa Cahuilla','Santa Rosa Cahuilla','1050-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (36,'Torres-Martinez','Torres-Martinez','1051-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (37,'California Tribes','California Tribes','1053-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (38,'Cahto','Cahto','1054-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (39,'Chimariko','Chimariko','1055-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (40,'Coast Miwok','Coast Miwok','1056-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (41,'Digger','Digger','1057-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (42,'Kawaiisu','Kawaiisu','1058-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (43,'Kern River','Kern River','1059-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (44,'Mattole','Mattole','1060-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (45,'Red Wood','Red Wood','1061-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (46,'Santa Rosa','Santa Rosa','1062-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (47,'Takelma','Takelma','1063-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (48,'Wappo','Wappo','1064-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (49,'Yana','Yana','1065-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (50,'Yuki','Yuki','1066-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (51,'Canadian and Latin American Indian','Canadian and Latin American Indian','1068-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (52,'Canadian Indian','Canadian Indian','1069-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (53,'Central American Indian','Central American Indian','1070-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (54,'French American Indian','French American Indian','1071-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (55,'Mexican American Indian','Mexican American Indian','1072-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (56,'South American Indian','South American Indian','1073-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (57,'Spanish American Indian','Spanish American Indian','1074-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (58,'Catawba','Catawba','1076-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (59,'Cayuse','Cayuse','1078-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (60,'Chehalis','Chehalis','1080-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (61,'Chemakuan','Chemakuan','1082-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (62,'Hoh','Hoh','1083-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (63,'Quileute','Quileute','1084-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (64,'Chemehuevi','Chemehuevi','1086-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (65,'Cherokee','Cherokee','1088-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (66,'Cherokee Alabama','Cherokee Alabama','1089-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (67,'Cherokees of Northeast Alabama','Cherokees of Northeast Alabama','1090-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (68,'Cherokees of Southeast Alabama','Cherokees of Southeast Alabama','1091-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (69,'Eastern Cherokee','Eastern Cherokee','1092-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (70,'Echota Cherokee','Echota Cherokee','1093-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (71,'Etowah Cherokee','Etowah Cherokee','1094-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (72,'Northern Cherokee ','Northern Cherokee ','1095-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (73,'Tuscola','Tuscola','1096-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (74,'United Keetowah Band of Cherokee','United Keetowah Band of Cherokee','1097-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (75,'Western Cherokee','Western Cherokee','1098-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (76,'Cherokee Shawnee','Cherokee Shawnee','1100-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (77,'Cheyenne','Cheyenne','1102-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (78,'Northern Cheyenne ','Northern Cheyenne ','1103-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (79,'Southern Cheyenne','Southern Cheyenne','1104-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (80,'Cheyenne-Arapaho','Cheyenne-Arapaho','1106-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (81,'Chickahominy','Chickahominy','1108-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (82,'Eastern Chickahominy','Eastern Chickahominy','1109-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (83,'Western Chickahominy','Western Chickahominy','1110-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (84,'Chickasaw','Chickasaw','1112-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (85,'Chinook','Chinook','1114-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (86,'Clatsop','Clatsop','1115-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (87,'Columbia River Chinook ','Columbia River Chinook ','1116-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (88,'Kathlamet','Kathlamet','1117-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (89,'Upper Chinook','Upper Chinook','1118-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (90,'Wakiakum Chinook','Wakiakum Chinook','1119-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (91,'Willapa Chinook','Willapa Chinook','1120-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (92,'Wishram','Wishram','1121-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (93,'Chippewa','Chippewa','1123-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (94,'Bad River','Bad River','1124-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (95,'Bay Mills Chippewa','Bay Mills Chippewa','1125-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (96,'Bois Forte','Bois Forte','1126-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (97,'Burt Lake Chippewa','Burt Lake Chippewa','1127-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (98,'Fond du Lac','Fond du Lac','1128-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (99,'Grand Portage','Grand Portage','1129-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (100,'Grand Traverse Band of Ottawa/Chippewa','Grand Traverse Band of Ottawa/Chippewa','1130-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (101,'Keweenaw','Keweenaw','1131-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (102,'Lac Courte Oreilles','Lac Courte Oreilles','1132-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (103,'Lac du Flambeau','Lac du Flambeau','1133-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (104,'Lac Vieux Desert Chippewa','Lac Vieux Desert Chippewa','1134-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (105,'Lake Superior','Lake Superior','1135-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (106,'Leech Lake','Leech Lake','1136-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (107,'Little Shell Chippewa','Little Shell Chippewa','1137-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (108,'Mille Lacs','Mille Lacs','1138-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (109,'Minnesota Chippewa','Minnesota Chippewa','1139-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (110,'Ontonagon','Ontonagon','1140-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (111,'Red Cliff Chippewa','Red Cliff Chippewa','1141-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (112,'Red Lake Chippewa','Red Lake Chippewa','1142-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (113,'Saginaw Chippewa','Saginaw Chippewa','1143-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (114,'St. Croix Chippewa','St. Croix Chippewa','1144-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (115,'Sault Ste. Marie Chippewa','Sault Ste. Marie Chippewa','1145-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (116,'Sokoagon Chippewa ','Sokoagon Chippewa ','1146-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (117,'Turtle Mountain','Turtle Mountain','1147-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (118,'White Earth','White Earth','1148-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (119,'Chippewa Cree','Chippewa Cree','1150-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (120,'Rocky Boy''s Chippewa Cree','Rocky Boy''s Chippewa Cree','1151-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (121,'Chitimacha','Chitimacha','1153-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (122,'Choctaw','Choctaw','1155-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (123,'Clifton Choctaw','Clifton Choctaw','1156-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (124,'Jena Choctaw','Jena Choctaw','1157-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (125,'Mississippi Choctaw','Mississippi Choctaw','1158-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (126,'Mowa Band of Choctaw','Mowa Band of Choctaw','1159-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (127,'Oklahoma Choctaw','Oklahoma Choctaw','1160-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (128,'Chumash','Chumash','1162-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (129,'Santa Ynez','Santa Ynez','1163-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (130,'Clear Lake','Clear Lake','1165-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (131,'Coeur D''Alene','Coeur D''Alene','1167-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (132,'Coharie','Coharie','1169-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (133,'Colorado River','Colorado River','1171-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (134,'Colville','Colville','1173-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (135,'Comanche','Comanche','1175-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (136,'Oklahoma Comanche','Oklahoma Comanche','1176-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (137,'Coos, Lower Umpqua, Siuslaw','Coos, Lower Umpqua, Siuslaw','1178-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (138,'Coos','Coos','1180-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (139,'Coquilles','Coquilles','1182-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (140,'Costanoan','Costanoan','1184-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (141,'Coushatta','Coushatta','1186-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (142,'Alabama Coushatta','Alabama Coushatta','1187-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (143,'Cowlitz','Cowlitz','1189-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (144,'Cree','Cree','1191-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (145,'Creek','Creek','1193-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (146,'Alabama Creek','Alabama Creek','1194-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (147,'Alabama Quassarte','Alabama Quassarte','1195-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (148,'Eastern Creek','Eastern Creek','1196-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (149,'Eastern Muscogee','Eastern Muscogee','1197-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (150,'Kialegee','Kialegee','1198-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (151,'Lower Muscogee','Lower Muscogee','1199-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (152,'Machis Lower Creek Indian','Machis Lower Creek Indian','1200-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (153,'Poarch Band','Poarch Band','1201-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (154,'Principal Creek Indian Nation','Principal Creek Indian Nation','1202-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (155,'Star Clan of Muscogee Creeks','Star Clan of Muscogee Creeks','1203-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (156,'Thlopthlocco','Thlopthlocco','1204-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (157,'Tuckabachee','Tuckabachee','1205-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (158,'Croatan','Croatan','1207-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (159,'Crow','Crow','1209-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (160,'Cupeno','Cupeno','1211-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (161,'Agua Caliente','Agua Caliente','1212-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (162,'Delaware','Delaware','1214-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (163,'Eastern Delaware','Eastern Delaware','1215-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (164,'Lenni-Lenape','Lenni-Lenape','1216-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (165,'Munsee','Munsee','1217-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (166,'Oklahoma Delaware','Oklahoma Delaware','1218-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (167,'Rampough Mountain','Rampough Mountain','1219-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (168,'Sand Hill','Sand Hill','1220-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (169,'Campo','Campo','1223-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (170,'Capitan Grande','Capitan Grande','1224-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (171,'Cuyapaipe','Cuyapaipe','1225-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (172,'La Posta','La Posta','1226-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (173,'Manzanita','Manzanita','1227-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (174,'Mesa Grande','Mesa Grande','1228-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (175,'San Pasqual','San Pasqual','1229-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (176,'Santa Ysabel','Santa Ysabel','1230-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (177,'Sycuan','Sycuan','1231-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (178,'Eastern Tribes','Eastern Tribes','1233-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (179,'Biloxi','Biloxi','1235-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (180,'Georgetown','Georgetown','1236-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (181,'Moor','Moor','1237-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (182,'Nansemond','Nansemond','1238-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (183,'Natchez','Natchez','1239-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (184,'Nausu Waiwash','Nausu Waiwash','1240-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (185,'Nipmuc','Nipmuc','1241-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (186,'Paugussett','Paugussett','1242-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (187,'Pocomoke Acohonock','Pocomoke Acohonock','1243-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (188,'Southeastern Indians ','Southeastern Indians ','1244-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (189,'Susquehanock','Susquehanock','1245-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (190,'Tunica Biloxi','Tunica Biloxi','1246-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (191,'Waccamaw-Siousan','Waccamaw-Siousan','1247-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (192,'Wicomico','Wicomico','1248-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (193,'Esselen','Esselen','1250-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (194,'Fort Belknap','Fort Belknap','1252-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (195,'Fort Berthold ','Fort Berthold ','1254-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (196,'Fort Mcdowell','Fort Mcdowell','1256-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (197,'Fort Hall','Fort Hall','1258-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (198,'Gabrieleno','Gabrieleno','1260-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (199,'Grand Ronde','Grand Ronde','1262-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (200,'Gros Ventres','Gros Ventres','1264-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (201,'Atsina','Atsina','1265-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (202,'Haliwa','Haliwa','1267-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (203,'Hidatsa','Hidatsa','1269-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (204,'Hoopa','Hoopa','1271-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (205,'Trinity','Trinity','1272-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (206,'Whilkut','Whilkut','1273-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (207,'Hoopa Extension','Hoopa Extension','1275-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (208,'Houma','Houma','1277-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (209,'Inaja-Cosmit','Inaja-Cosmit','1279-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (210,'Iowa','Iowa','1281-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (211,'Iowa of Kansas-Nebraska','Iowa of Kansas-Nebraska','1282-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (212,'Iowa of Oklahoma','Iowa of Oklahoma','1283-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (213,'Iroquois','Iroquois','1285-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (214,'Cayuga','Cayuga','1286-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (215,'Mohawk','Mohawk','1287-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (216,'Oneida','Oneida','1288-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (217,'Onondaga','Onondaga','1289-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (218,'Seneca','Seneca','1290-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (219,'Seneca Nation','Seneca Nation','1291-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (220,'Seneca-Cayuga','Seneca-Cayuga','1292-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (221,'Tonawanda Seneca','Tonawanda Seneca','1293-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (222,'Tuscarora','Tuscarora','1294-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (223,'Wyandotte','Wyandotte','1295-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (224,'Juaneno','Juaneno','1297-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (225,'Kalispel','Kalispel','1299-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (226,'Karuk','Karuk','1301-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (227,'Kaw','Kaw','1303-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (228,'Kickapoo','Kickapoo','1305-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (229,'Oklahoma Kickapoo','Oklahoma Kickapoo','1306-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (230,'Texas Kickapoo','Texas Kickapoo','1307-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (231,'Kiowa','Kiowa','1309-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (232,'Oklahoma Kiowa','Oklahoma Kiowa','1310-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (233,'Klallam','Klallam','1312-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (234,'Jamestown','Jamestown','1313-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (235,'Lower Elwha','Lower Elwha','1314-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (236,'Port Gamble Klallam','Port Gamble Klallam','1315-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (237,'Klamath','Klamath','1317-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (238,'Konkow','Konkow','1319-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (239,'Kootenai','Kootenai','1321-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (240,'Lassik','Lassik','1323-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (241,'Long Island','Long Island','1325-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (242,'Matinecock','Matinecock','1326-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (243,'Montauk','Montauk','1327-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (244,'Poospatuck','Poospatuck','1328-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (245,'Setauket','Setauket','1329-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (246,'Luiseno','Luiseno','1331-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (247,'La Jolla','La Jolla','1332-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (248,'Pala','Pala','1333-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (249,'Pauma','Pauma','1334-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (250,'Pechanga','Pechanga','1335-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (251,'Soboba','Soboba','1336-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (252,'Twenty-Nine Palms','Twenty-Nine Palms','1337-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (253,'Temecula','Temecula','1338-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (254,'Lumbee','Lumbee','1340-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (255,'Lummi','Lummi','1342-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (256,'Maidu','Maidu','1344-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (257,'Mountain Maidu','Mountain Maidu','1345-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (258,'Nishinam','Nishinam','1346-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (259,'Makah','Makah','1348-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (260,'Maliseet','Maliseet','1350-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (261,'Mandan','Mandan','1352-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (262,'Mattaponi','Mattaponi','1354-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (263,'Menominee','Menominee','1356-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (264,'Miami','Miami','1358-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (265,'Illinois Miami','Illinois Miami','1359-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (266,'Indiana Miami','Indiana Miami','1360-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (267,'Oklahoma Miami','Oklahoma Miami','1361-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (268,'Miccosukee','Miccosukee','1363-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (269,'Micmac','Micmac','1365-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (270,'Aroostook','Aroostook','1366-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (271,'Mission Indians','Mission Indians','1368-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (272,'Miwok','Miwok','1370-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (273,'Modoc','Modoc','1372-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (274,'Mohegan','Mohegan','1374-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (275,'Mono','Mono','1376-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (276,'Nanticoke','Nanticoke','1378-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (277,'Narragansett','Narragansett','1380-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (278,'Navajo','Navajo','1382-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (279,'Alamo Navajo','Alamo Navajo','1383-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (280,'Canoncito Navajo','Canoncito Navajo','1384-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (281,'Ramah Navajo','Ramah Navajo','1385-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (282,'Nez Perce','Nez Perce','1387-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (283,'Nomalaki','Nomalaki','1389-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (284,'Northwest Tribes','Northwest Tribes','1391-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (285,'Alsea','Alsea','1392-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (286,'Celilo','Celilo','1393-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (287,'Columbia','Columbia','1394-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (288,'Kalapuya','Kalapuya','1395-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (289,'Molala','Molala','1396-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (290,'Talakamish','Talakamish','1397-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (291,'Tenino','Tenino','1398-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (292,'Tillamook','Tillamook','1399-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (293,'Wenatchee','Wenatchee','1400-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (294,'Yahooskin','Yahooskin','1401-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (295,'Omaha','Omaha','1403-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (296,'Oregon Athabaskan','Oregon Athabaskan','1405-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (297,'Osage','Osage','1407-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (298,'Otoe-Missouria','Otoe-Missouria','1409-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (299,'Ottawa','Ottawa','1411-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (300,'Burt Lake Ottawa','Burt Lake Ottawa','1412-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (301,'Michigan Ottawa','Michigan Ottawa','1413-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (302,'Oklahoma Ottawa','Oklahoma Ottawa','1414-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (303,'Paiute','Paiute','1416-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (304,'Bishop','Bishop','1417-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (305,'Bridgeport','Bridgeport','1418-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (306,'Burns Paiute','Burns Paiute','1419-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (307,'Cedarville','Cedarville','1420-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (308,'Fort Bidwell','Fort Bidwell','1421-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (309,'Fort Independence','Fort Independence','1422-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (310,'Kaibab','Kaibab','1423-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (311,'Las Vegas','Las Vegas','1424-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (312,'Lone Pine','Lone Pine','1425-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (313,'Lovelock','Lovelock','1426-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (314,'Malheur Paiute','Malheur Paiute','1427-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (315,'Moapa','Moapa','1428-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (316,'Northern Paiute','Northern Paiute','1429-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (317,'Owens Valley','Owens Valley','1430-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (318,'Pyramid Lake','Pyramid Lake','1431-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (319,'San Juan Southern Paiute ','San Juan Southern Paiute ','1432-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (320,'Southern Paiute ','Southern Paiute ','1433-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (321,'Summit Lake','Summit Lake','1434-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (322,'Utu Utu Gwaitu Paiute','Utu Utu Gwaitu Paiute','1435-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (323,'Walker River','Walker River','1436-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (324,'Yerington Paiute','Yerington Paiute','1437-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (325,'Pamunkey','Pamunkey','1439-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (326,'Passamaquoddy','Passamaquoddy','1441-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (327,'Indian Township','Indian Township','1442-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (328,'Pleasant Point Passamaquoddy','Pleasant Point Passamaquoddy','1443-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (329,'Pawnee','Pawnee','1445-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (330,'Oklahoma Pawnee','Oklahoma Pawnee','1446-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (331,'Penobscot','Penobscot','1448-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (332,'Peoria','Peoria','1450-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (333,'Oklahoma Peoria','Oklahoma Peoria','1451-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (334,'Pequot','Pequot','1453-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (335,'Marshantucket Pequot','Marshantucket Pequot','1454-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (336,'Pima','Pima','1456-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (337,'Gila River Pima-Maricopa','Gila River Pima-Maricopa','1457-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (338,'Salt River Pima-Maricopa','Salt River Pima-Maricopa','1458-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (339,'Piscataway','Piscataway','1460-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (340,'Pit River','Pit River','1462-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (341,'Pomo','Pomo','1464-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (342,'Central Pomo','Central Pomo','1465-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (343,'Dry Creek','Dry Creek','1466-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (344,'Eastern Pomo','Eastern Pomo','1467-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (345,'Kashia','Kashia','1468-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (346,'Northern Pomo','Northern Pomo','1469-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (347,'Scotts Valley','Scotts Valley','1470-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (348,'Stonyford','Stonyford','1471-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (349,'Sulphur Bank','Sulphur Bank','1472-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (350,'Ponca','Ponca','1474-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (351,'Nebraska Ponca','Nebraska Ponca','1475-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (352,'Oklahoma Ponca','Oklahoma Ponca','1476-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (353,'Potawatomi','Potawatomi','1478-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (354,'Citizen Band Potawatomi','Citizen Band Potawatomi','1479-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (355,'Forest County','Forest County','1480-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (356,'Hannahville','Hannahville','1481-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (357,'Huron Potawatomi','Huron Potawatomi','1482-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (358,'Pokagon Potawatomi','Pokagon Potawatomi','1483-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (359,'Prairie Band','Prairie Band','1484-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (360,'Wisconsin Potawatomi','Wisconsin Potawatomi','1485-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (361,'Powhatan','Powhatan','1487-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (362,'Pueblo','Pueblo','1489-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (363,'Acoma','Acoma','1490-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (364,'Arizona Tewa','Arizona Tewa','1491-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (365,'Cochiti','Cochiti','1492-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (366,'Hopi','Hopi','1493-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (367,'Isleta','Isleta','1494-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (368,'Jemez','Jemez','1495-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (369,'Keres','Keres','1496-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (370,'Laguna','Laguna','1497-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (371,'Nambe','Nambe','1498-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (372,'Picuris','Picuris','1499-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (373,'Piro','Piro','1500-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (374,'Pojoaque','Pojoaque','1501-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (375,'San Felipe','San Felipe','1502-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (376,'San Ildefonso','San Ildefonso','1503-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (377,'San Juan Pueblo','San Juan Pueblo','1504-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (378,'San Juan De','San Juan De','1505-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (379,'San Juan','San Juan','1506-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (380,'Sandia','Sandia','1507-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (381,'Santa Ana','Santa Ana','1508-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (382,'Santa Clara','Santa Clara','1509-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (383,'Santo Domingo','Santo Domingo','1510-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (384,'Taos','Taos','1511-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (385,'Tesuque','Tesuque','1512-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (386,'Tewa','Tewa','1513-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (387,'Tigua','Tigua','1514-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (388,'Zia','Zia','1515-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (389,'Zuni','Zuni','1516-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (390,'Puget Sound Salish','Puget Sound Salish','1518-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (391,'Duwamish','Duwamish','1519-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (392,'Kikiallus','Kikiallus','1520-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (393,'Lower Skagit','Lower Skagit','1521-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (394,'Muckleshoot','Muckleshoot','1522-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (395,'Nisqually','Nisqually','1523-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (396,'Nooksack','Nooksack','1524-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (397,'Port Madison','Port Madison','1525-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (398,'Puyallup','Puyallup','1526-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (399,'Samish','Samish','1527-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (400,'Sauk-Suiattle','Sauk-Suiattle','1528-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (401,'Skokomish','Skokomish','1529-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (402,'Skykomish','Skykomish','1530-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (403,'Snohomish','Snohomish','1531-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (404,'Snoqualmie','Snoqualmie','1532-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (405,'Squaxin Island','Squaxin Island','1533-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (406,'Steilacoom','Steilacoom','1534-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (407,'Stillaguamish','Stillaguamish','1535-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (408,'Suquamish','Suquamish','1536-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (409,'Swinomish','Swinomish','1537-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (410,'Tulalip','Tulalip','1538-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (411,'Upper Skagit','Upper Skagit','1539-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (412,'Quapaw','Quapaw','1541-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (413,'Quinault','Quinault','1543-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (414,'Rappahannock','Rappahannock','1545-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (415,'Reno-Sparks','Reno-Sparks','1547-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (416,'Round Valley','Round Valley','1549-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (417,'Sac and Fox','Sac and Fox','1551-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (418,'Iowa Sac and Fox','Iowa Sac and Fox','1552-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (419,'Missouri Sac and Fox','Missouri Sac and Fox','1553-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (420,'Oklahoma Sac and Fox','Oklahoma Sac and Fox','1554-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (421,'Salinan','Salinan','1556-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (422,'Salish','Salish','1558-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (423,'Salish and Kootenai','Salish and Kootenai','1560-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (424,'Schaghticoke','Schaghticoke','1562-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (425,'Scott Valley','Scott Valley','1564-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (426,'Seminole','Seminole','1566-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (427,'Big Cypress','Big Cypress','1567-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (428,'Brighton','Brighton','1568-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (429,'Florida Seminole','Florida Seminole','1569-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (430,'Hollywood Seminole','Hollywood Seminole','1570-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (431,'Oklahoma Seminole','Oklahoma Seminole','1571-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (432,'Serrano','Serrano','1573-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (433,'San Manual','San Manual','1574-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (434,'Shasta','Shasta','1576-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (435,'Shawnee','Shawnee','1578-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (436,'Absentee Shawnee','Absentee Shawnee','1579-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (437,'Eastern Shawnee','Eastern Shawnee','1580-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (438,'Shinnecock','Shinnecock','1582-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (439,'Shoalwater Bay','Shoalwater Bay','1584-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (440,'Shoshone','Shoshone','1586-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (441,'Battle Mountain','Battle Mountain','1587-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (442,'Duckwater','Duckwater','1588-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (443,'Elko','Elko','1589-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (444,'Ely','Ely','1590-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (445,'Goshute','Goshute','1591-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (446,'Panamint','Panamint','1592-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (447,'Ruby Valley','Ruby Valley','1593-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (448,'Skull Valley','Skull Valley','1594-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (449,'South Fork Shoshone','South Fork Shoshone','1595-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (450,'Te-Moak Western Shoshone','Te-Moak Western Shoshone','1596-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (451,'Timbi-Sha Shoshone','Timbi-Sha Shoshone','1597-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (452,'Washakie','Washakie','1598-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (453,'Wind River Shoshone','Wind River Shoshone','1599-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (454,'Yomba','Yomba','1600-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (455,'Shoshone Paiute','Shoshone Paiute','1602-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (456,'Duck Valley','Duck Valley','1603-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (457,'Fallon','Fallon','1604-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (458,'Fort McDermitt','Fort McDermitt','1605-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (459,'Siletz','Siletz','1607-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (460,'Sioux','Sioux','1609-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (461,'Blackfoot Sioux ','Blackfoot Sioux ','1610-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (462,'Brule Sioux','Brule Sioux','1611-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (463,'Cheyenne River Sioux','Cheyenne River Sioux','1612-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (464,'Crow Creek Sioux','Crow Creek Sioux','1613-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (465,'Dakota Sioux','Dakota Sioux','1614-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (466,'Flandreau Santee','Flandreau Santee','1615-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (467,'Fort Peck','Fort Peck','1616-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (468,'Lake Traverse Sioux','Lake Traverse Sioux','1617-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (469,'Lower Brule Sioux','Lower Brule Sioux','1618-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (470,'Lower Sioux','Lower Sioux','1619-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (471,'Mdewakanton Sioux','Mdewakanton Sioux','1620-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (472,'Miniconjou','Miniconjou','1621-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (473,'Oglala Sioux','Oglala Sioux','1622-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (474,'Pine Ridge Sioux','Pine Ridge Sioux','1623-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (475,'Pipestone Sioux','Pipestone Sioux','1624-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (476,'Prairie Island Sioux','Prairie Island Sioux','1625-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (477,'Prior Lake Sioux','Prior Lake Sioux','1626-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (478,'Rosebud Sioux','Rosebud Sioux','1627-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (479,'Sans Arc Sioux','Sans Arc Sioux','1628-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (480,'Santee Sioux','Santee Sioux','1629-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (481,'Sisseton-Wahpeton','Sisseton-Wahpeton','1630-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (482,'Sisseton Sioux','Sisseton Sioux','1631-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (483,'Spirit Lake Sioux','Spirit Lake Sioux','1632-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (484,'Standing Rock Sioux','Standing Rock Sioux','1633-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (485,'Teton Sioux','Teton Sioux','1634-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (486,'Two Kettle Sioux','Two Kettle Sioux','1635-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (487,'Upper Sioux','Upper Sioux','1636-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (488,'Wahpekute Sioux','Wahpekute Sioux','1637-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (489,'Wahpeton Sioux','Wahpeton Sioux','1638-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (490,'Wazhaza Sioux','Wazhaza Sioux','1639-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (491,'Yankton Sioux','Yankton Sioux','1640-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (492,'Yanktonai Sioux','Yanktonai Sioux','1641-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (493,'Siuslaw','Siuslaw','1643-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (494,'Spokane','Spokane','1645-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (495,'Stewart','Stewart','1647-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (496,'Stockbridge','Stockbridge','1649-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (497,'Susanville','Susanville','1651-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (498,'Tohono O''Odham','Tohono O''Odham','1653-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (499,'Ak-Chin','Ak-Chin','1654-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (500,'Gila Bend','Gila Bend','1655-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (501,'San Xavier','San Xavier','1656-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (502,'Sells','Sells','1657-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (503,'Tolowa','Tolowa','1659-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (504,'Tonkawa','Tonkawa','1661-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (505,'Tygh','Tygh','1663-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (506,'Umatilla','Umatilla','1665-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (507,'Umpqua','Umpqua','1667-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (508,'Cow Creek Umpqua','Cow Creek Umpqua','1668-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (509,'Ute','Ute','1670-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (510,'Allen Canyon','Allen Canyon','1671-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (511,'Uintah Ute','Uintah Ute','1672-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (512,'Ute Mountain Ute','Ute Mountain Ute','1673-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (513,'Wailaki','Wailaki','1675-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (514,'Walla-Walla','Walla-Walla','1677-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (515,'Wampanoag','Wampanoag','1679-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (516,'Gay Head Wampanoag ','Gay Head Wampanoag ','1680-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (517,'Mashpee Wampanoag','Mashpee Wampanoag','1681-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (518,'Warm Springs','Warm Springs','1683-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (519,'Wascopum','Wascopum','1685-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (520,'Washoe','Washoe','1687-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (521,'Alpine','Alpine','1688-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (522,'Carson','Carson','1689-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (523,'Dresslerville','Dresslerville','1690-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (524,'Wichita','Wichita','1692-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (525,'Wind River','Wind River','1694-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (526,'Winnebago','Winnebago','1696-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (527,'Ho-chunk','Ho-chunk','1697-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (528,'Nebraska Winnebago','Nebraska Winnebago','1698-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (529,'Winnemucca','Winnemucca','1700-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (530,'Wintun','Wintun','1702-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (531,'Wiyot','Wiyot','1704-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (532,'Table Bluff ','Table Bluff ','1705-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (533,'Yakama','Yakama','1707-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (534,'Yakama Cowlitz','Yakama Cowlitz','1709-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (535,'Yaqui','Yaqui','1711-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (536,'Barrio Libre','Barrio Libre','1712-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (537,'Pascua Yaqui','Pascua Yaqui','1713-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (538,'Yavapai Apache','Yavapai Apache','1715-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (539,'Yokuts','Yokuts','1717-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (540,'Chukchansi','Chukchansi','1718-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (541,'Tachi','Tachi','1719-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (542,'Tule River','Tule River','1720-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (543,'Yuchi','Yuchi','1722-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (544,'Yuman','Yuman','1724-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (545,'Cocopah','Cocopah','1725-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (546,'Havasupai','Havasupai','1726-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (547,'Hualapai','Hualapai','1727-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (548,'Maricopa','Maricopa','1728-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (549,'Mohave','Mohave','1729-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (550,'Quechan','Quechan','1730-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (551,'Yavapai','Yavapai','1731-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (552,'Yurok','Yurok','1732-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (553,'Coast Yurok','Coast Yurok','1733-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (554,'Alaska Native','Alaska Native','1735-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (555,'Alaska Indian  ','Alaska Indian  ','1737-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (556,'Alaskan Athabascan ','Alaskan Athabascan ','1739-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (557,'Ahtna','Ahtna','1740-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (558,'Alatna ','Alatna ','1741-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (559,'Alexander','Alexander','1742-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (560,'Allakaket','Allakaket','1743-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (561,'Alanvik','Alanvik','1744-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (562,'Anvik','Anvik','1745-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (563,'Arctic','Arctic','1746-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (564,'Beaver','Beaver','1747-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (565,'Birch Creek','Birch Creek','1748-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (566,'Cantwell','Cantwell','1749-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (567,'Chalkyitsik','Chalkyitsik','1750-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (568,'Chickaloon','Chickaloon','1751-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (569,'Chistochina','Chistochina','1752-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (570,'Chitina','Chitina','1753-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (571,'Circle','Circle','1754-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (572,'Cook Inlet','Cook Inlet','1755-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (573,'Copper Center','Copper Center','1756-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (574,'Copper River','Copper River','1757-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (575,'Dot Lake','Dot Lake','1758-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (576,'Doyon','Doyon','1759-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (577,'Eagle','Eagle','1760-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (578,'Eklutna','Eklutna','1761-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (579,'Evansville','Evansville','1762-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (580,'Fort Yukon','Fort Yukon','1763-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (581,'Gakona','Gakona','1764-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (582,'Galena','Galena','1765-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (583,'Grayling','Grayling','1766-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (584,'Gulkana','Gulkana','1767-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (585,'Healy Lake','Healy Lake','1768-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (586,'Holy Cross','Holy Cross','1769-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (587,'Hughes','Hughes','1770-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (588,'Huslia','Huslia','1771-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (589,'Iliamna','Iliamna','1772-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (590,'Kaltag','Kaltag','1773-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (591,'Kluti Kaah','Kluti Kaah','1774-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (592,'Knik','Knik','1775-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (593,'Koyukuk','Koyukuk','1776-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (594,'Lake Minchumina','Lake Minchumina','1777-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (595,'Lime','Lime','1778-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (596,'Mcgrath','Mcgrath','1779-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (597,'Manley Hot Springs','Manley Hot Springs','1780-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (598,'Mentasta Lake','Mentasta Lake','1781-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (599,'Minto','Minto','1782-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (600,'Nenana','Nenana','1783-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (601,'Nikolai','Nikolai','1784-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (602,'Ninilchik','Ninilchik','1785-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (603,'Nondalton','Nondalton','1786-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (604,'Northway','Northway','1787-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (605,'Nulato','Nulato','1788-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (606,'Pedro Bay','Pedro Bay','1789-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (607,'Rampart','Rampart','1790-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (608,'Ruby','Ruby','1791-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (609,'Salamatof','Salamatof','1792-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (610,'Seldovia','Seldovia','1793-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (611,'Slana','Slana','1794-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (612,'Shageluk','Shageluk','1795-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (613,'Stevens','Stevens','1796-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (614,'Stony River','Stony River','1797-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (615,'Takotna','Takotna','1798-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (616,'Tanacross','Tanacross','1799-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (617,'Tanaina','Tanaina','1800-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (618,'Tanana','Tanana','1801-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (619,'Tanana Chiefs','Tanana Chiefs','1802-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (620,'Tazlina','Tazlina','1803-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (621,'Telida','Telida','1804-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (622,'Tetlin','Tetlin','1805-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (623,'Tok','Tok','1806-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (624,'Tyonek','Tyonek','1807-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (625,'Venetie','Venetie','1808-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (626,'Wiseman','Wiseman','1809-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (627,'Southeast Alaska','Southeast Alaska','1811-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (628,'Tlingit-Haida','Tlingit-Haida','1813-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (629,'Angoon','Angoon','1814-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (630,'Central Council of Tlingit and Haida Tribes','Central Council of Tlingit and Haida Tribes','1815-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (631,'Chilkat','Chilkat','1816-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (632,'Chilkoot','Chilkoot','1817-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (633,'Craig','Craig','1818-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (634,'Douglas','Douglas','1819-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (635,'Haida','Haida','1820-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (636,'Hoonah','Hoonah','1821-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (637,'Hydaburg','Hydaburg','1822-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (638,'Kake','Kake','1823-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (639,'Kasaan','Kasaan','1824-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (640,'Kenaitze','Kenaitze','1825-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (641,'Ketchikan','Ketchikan','1826-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (642,'Klawock','Klawock','1827-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (643,'Pelican','Pelican','1828-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (644,'Petersburg','Petersburg','1829-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (645,'Saxman','Saxman','1830-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (646,'Sitka','Sitka','1831-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (647,'Tenakee Springs','Tenakee Springs','1832-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (648,'Tlingit','Tlingit','1833-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (649,'Wrangell','Wrangell','1834-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (650,'Yakutat','Yakutat','1835-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (651,'Tsimshian','Tsimshian','1837-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (652,'Metlakatla','Metlakatla','1838-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (653,'Eskimo','Eskimo','1840-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (654,'Greenland Eskimo','Greenland Eskimo','1842-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (655,'Inupiat Eskimo','Inupiat Eskimo','1844-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (656,'Ambler','Ambler','1845-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (657,'Anaktuvuk','Anaktuvuk','1846-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (658,'Anaktuvuk Pass','Anaktuvuk Pass','1847-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (659,'Arctic Slope Inupiat','Arctic Slope Inupiat','1848-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (660,'Arctic Slope Corporation','Arctic Slope Corporation','1849-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (661,'Atqasuk','Atqasuk','1850-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (662,'Barrow','Barrow','1851-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (663,'Bering Straits Inupiat','Bering Straits Inupiat','1852-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (664,'Brevig Mission','Brevig Mission','1853-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (665,'Buckland','Buckland','1854-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (666,'Chinik','Chinik','1855-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (667,'Council','Council','1856-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (668,'Deering','Deering','1857-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (669,'Elim','Elim','1858-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (670,'Golovin','Golovin','1859-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (671,'Inalik Diomede','Inalik Diomede','1860-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (672,'Inupiaq ','Inupiaq ','1861-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (673,'Kaktovik','Kaktovik','1862-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (674,'Kawerak','Kawerak','1863-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (675,'Kiana','Kiana','1864-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (676,'Kivalina','Kivalina','1865-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (677,'Kobuk','Kobuk','1866-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (678,'Kotzebue','Kotzebue','1867-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (679,'Koyuk','Koyuk','1868-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (680,'Kwiguk','Kwiguk','1869-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (681,'Mauneluk Inupiat','Mauneluk Inupiat','1870-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (682,'Nana Inupiat','Nana Inupiat','1871-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (683,'Noatak','Noatak','1872-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (684,'Nome','Nome','1873-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (685,'Noorvik','Noorvik','1874-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (686,'Nuiqsut','Nuiqsut','1875-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (687,'Point Hope','Point Hope','1876-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (688,'Point Lay','Point Lay','1877-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (689,'Selawik','Selawik','1878-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (690,'Shaktoolik','Shaktoolik','1879-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (691,'Shishmaref','Shishmaref','1880-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (692,'Shungnak','Shungnak','1881-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (693,'Solomon','Solomon','1882-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (694,'Teller','Teller','1883-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (695,'Unalakleet','Unalakleet','1884-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (696,'Wainwright','Wainwright','1885-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (697,'Wales','Wales','1886-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (698,'White Mountain','White Mountain','1887-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (699,'White Mountain Inupiat','White Mountain Inupiat','1888-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (700,'Mary''s Igloo','Mary''s Igloo','1889-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (701,'Siberian Eskimo','Siberian Eskimo','1891-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (702,'Gambell','Gambell','1892-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (703,'Savoonga','Savoonga','1893-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (704,'Siberian Yupik','Siberian Yupik','1894-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (705,'Yupik Eskimo','Yupik Eskimo','1896-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (706,'Akiachak','Akiachak','1897-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (707,'Akiak','Akiak','1898-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (708,'Alakanuk','Alakanuk','1899-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (709,'Aleknagik','Aleknagik','1900-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (710,'Andreafsky','Andreafsky','1901-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (711,'Aniak','Aniak','1902-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (712,'Atmautluak','Atmautluak','1903-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (713,'Bethel','Bethel','1904-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (714,'Bill Moore''s Slough','Bill Moore''s Slough','1905-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (715,'Bristol Bay Yupik','Bristol Bay Yupik','1906-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (716,'Calista Yupik','Calista Yupik','1907-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (717,'Chefornak','Chefornak','1908-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (718,'Chevak','Chevak','1909-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (719,'Chuathbaluk','Chuathbaluk','1910-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (720,'Clark''s Point','Clark''s Point','1911-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (721,'Crooked Creek','Crooked Creek','1912-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (722,'Dillingham','Dillingham','1913-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (723,'Eek','Eek','1914-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (724,'Ekuk','Ekuk','1915-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (725,'Ekwok','Ekwok','1916-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (726,'Emmonak','Emmonak','1917-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (727,'Goodnews Bay','Goodnews Bay','1918-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (728,'Hooper Bay','Hooper Bay','1919-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (729,'Iqurmuit (Russian Mission)','Iqurmuit (Russian Mission)','1920-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (730,'Kalskag','Kalskag','1921-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (731,'Kasigluk','Kasigluk','1922-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (732,'Kipnuk','Kipnuk','1923-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (733,'Koliganek','Koliganek','1924-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (734,'Kongiganak','Kongiganak','1925-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (735,'Kotlik','Kotlik','1926-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (736,'Kwethluk','Kwethluk','1927-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (737,'Kwigillingok','Kwigillingok','1928-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (738,'Levelock','Levelock','1929-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (739,'Lower Kalskag','Lower Kalskag','1930-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (740,'Manokotak','Manokotak','1931-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (741,'Marshall','Marshall','1932-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (742,'Mekoryuk','Mekoryuk','1933-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (743,'Mountain Village','Mountain Village','1934-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (744,'Naknek','Naknek','1935-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (745,'Napaumute','Napaumute','1936-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (746,'Napakiak','Napakiak','1937-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (747,'Napaskiak','Napaskiak','1938-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (748,'Newhalen','Newhalen','1939-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (749,'New Stuyahok','New Stuyahok','1940-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (750,'Newtok','Newtok','1941-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (751,'Nightmute','Nightmute','1942-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (752,'Nunapitchukv','Nunapitchukv','1943-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (753,'Oscarville','Oscarville','1944-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (754,'Pilot Station','Pilot Station','1945-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (755,'Pitkas Point','Pitkas Point','1946-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (756,'Platinum','Platinum','1947-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (757,'Portage Creek','Portage Creek','1948-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (758,'Quinhagak','Quinhagak','1949-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (759,'Red Devil','Red Devil','1950-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (760,'St. Michael','St. Michael','1951-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (761,'Scammon Bay','Scammon Bay','1952-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (762,'Sheldon''s Point','Sheldon''s Point','1953-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (763,'Sleetmute','Sleetmute','1954-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (764,'Stebbins','Stebbins','1955-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (765,'Togiak','Togiak','1956-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (766,'Toksook','Toksook','1957-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (767,'Tulukskak','Tulukskak','1958-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (768,'Tuntutuliak','Tuntutuliak','1959-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (769,'Tununak','Tununak','1960-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (770,'Twin Hills','Twin Hills','1961-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (771,'Georgetown','Georgetown','1962-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (772,'St. Mary''s','St. Mary''s','1963-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (773,'Umkumiate','Umkumiate','1964-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (774,'Aleut','Aleut','1966-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (775,'Alutiiq Aleut','Alutiiq Aleut','1968-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (776,'Tatitlek','Tatitlek','1969-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (777,'Ugashik','Ugashik','1970-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (778,'Bristol Bay Aleut','Bristol Bay Aleut','1972-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (779,'Chignik','Chignik','1973-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (780,'Chignik Lake','Chignik Lake','1974-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (781,'Egegik','Egegik','1975-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (782,'Igiugig','Igiugig','1976-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (783,'Ivanof Bay','Ivanof Bay','1977-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (784,'King Salmon','King Salmon','1978-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (785,'Kokhanok','Kokhanok','1979-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (786,'Perryville','Perryville','1980-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (787,'Pilot Point','Pilot Point','1981-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (788,'Port Heiden','Port Heiden','1982-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (789,'Chugach Aleut','Chugach Aleut','1984-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (790,'Chenega','Chenega','1985-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (791,'Chugach Corporation ','Chugach Corporation ','1986-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (792,'English Bay','English Bay','1987-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (793,'Port Graham','Port Graham','1988-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (794,'Eyak','Eyak','1990-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (795,'Koniag Aleut','Koniag Aleut','1992-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (796,'Akhiok','Akhiok','1993-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (797,'Agdaagux','Agdaagux','1994-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (798,'Karluk','Karluk','1995-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (799,'Kodiak','Kodiak','1996-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (800,'Larsen Bay','Larsen Bay','1997-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (801,'Old Harbor ','Old Harbor ','1998-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (802,'Ouzinkie','Ouzinkie','1999-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (803,'Port Lions','Port Lions','2000-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (804,'Sugpiaq','Sugpiaq','2002-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (805,'Suqpigaq','Suqpigaq','2004-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (806,'Unangan Aleut','Unangan Aleut','2006-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (807,'Akutan','Akutan','2007-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (808,'Aleut Corporation','Aleut Corporation','2008-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (809,'Aleutian','Aleutian','2009-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (810,'Aleutian Islander','Aleutian Islander','2010-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (811,'Atka','Atka','2011-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (812,'Belkofski','Belkofski','2012-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (813,'Chignik Lagoon','Chignik Lagoon','2013-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (814,'King Cove','King Cove','2014-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (815,'False Pass','False Pass','2015-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (816,'Nelson Lagoon','Nelson Lagoon','2016-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (817,'Nikolski','Nikolski','2017-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (818,'Pauloff Harbor','Pauloff Harbor','2018-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (819,'Qagan Toyagungin','Qagan Toyagungin','2019-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (820,'Qawalangin','Qawalangin','2020-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (821,'St. George','St. George','2021-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (822,'St. Paul','St. Paul','2022-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (823,'Sand Point','Sand Point','2023-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (824,'South Naknek','South Naknek','2024-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (825,'Unalaska','Unalaska','2025-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (826,'Unga','Unga','2026-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (827,'Asian','Asian','2028-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (828,'Asian Indian','Asian Indian','2029-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (829,'Bangladeshi','Bangladeshi','2030-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (830,'Bhutanese','Bhutanese','2031-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (831,'Burmese','Burmese','2032-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (832,'Cambodian','Cambodian','2033-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (833,'Chinese','Chinese','2034-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (834,'Taiwanese','Taiwanese','2035-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (835,'Filipino','Filipino','2036-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (836,'Hmong','Hmong','2037-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (837,'Indonesian','Indonesian','2038-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (838,'Japanese','Japanese','2039-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (839,'Korean','Korean','2040-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (840,'Laotian','Laotian','2041-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (841,'Malaysian','Malaysian','2042-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (842,'Okinawan','Okinawan','2043-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (843,'Pakistani','Pakistani','2044-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (844,'Sri Lankan','Sri Lankan','2045-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (845,'Thai','Thai','2046-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (846,'Vietnamese','Vietnamese','2047-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (847,'Iwo Jiman ','Iwo Jiman ','2048-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (848,'Maldivian','Maldivian','2049-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (849,'Nepalese','Nepalese','2050-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (850,'Singaporean','Singaporean','2051-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (851,'Madagascar','Madagascar','2052-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (852,'Black or African American','Black or African American','2054-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (853,'Black','Black','2056-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (854,'African American','African American','2058-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (855,'African','African','2060-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (856,'Botswanan','Botswanan','2061-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (857,'Ethiopian','Ethiopian','2062-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (858,'Liberian','Liberian','2063-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (859,'Namibian','Namibian','2064-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (860,'Nigerian','Nigerian','2065-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (861,'Zairean','Zairean','2066-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (862,'Bahamian','Bahamian','2067-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (863,'Barbadian','Barbadian','2068-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (864,'Dominican','Dominican','2069-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (865,'Dominica Islander','Dominica Islander','2070-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (866,'Haitian','Haitian','2071-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (867,'Jamaican','Jamaican','2072-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (868,'Tobagoan','Tobagoan','2073-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (869,'Trinidadian','Trinidadian','2074-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (870,'West Indian','West Indian','2075-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (871,'Native Hawaiian or Other Pacific Islander','Native Hawaiian or Other Pacific Islander','2076-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (872,'Polynesian','Polynesian','2078-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (873,'Native Hawaiian ','Native Hawaiian ','2079-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (874,'Samoan ','Samoan ','2080-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (875,'Tahitian','Tahitian','2081-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (876,'Tongan','Tongan','2082-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (877,'Tokelauan','Tokelauan','2083-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (878,'Micronesian','Micronesian','2085-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (879,'Guamanian or Chamorro','Guamanian or Chamorro','2086-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (880,'Guamanian','Guamanian','2087-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (881,'Chamorro','Chamorro','2088-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (882,'Mariana Islander','Mariana Islander','2089-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (883,'Marshallese','Marshallese','2090-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (884,'Palauan','Palauan','2091-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (885,'Carolinian','Carolinian','2092-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (886,'Kosraean','Kosraean','2093-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (887,'Pohnpeian','Pohnpeian','2094-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (888,'Saipanese','Saipanese','2095-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (889,'Kiribati','Kiribati','2096-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (890,'Chuukese','Chuukese','2097-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (891,'Yapese','Yapese','2098-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (892,'Melanesian','Melanesian','2100-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (893,'Fijian','Fijian','2101-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (894,'Papua New Guinean','Papua New Guinean','2102-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (895,'Solomon Islander','Solomon Islander','2103-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (896,'New Hebrides','New Hebrides','2104-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (897,'Other Pacific Islander','Other Pacific Islander','2500-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (898,'White','White','2106-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (899,'European','European','2108-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (900,'Armenian','Armenian','2109-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (901,'English','English','2110-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (902,'French','French','2111-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (903,'German','German','2112-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (904,'Irish','Irish','2113-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (905,'Italian','Italian','2114-7');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (906,'Polish','Polish','2115-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (907,'Scottish','Scottish','2116-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (908,'Middle Eastern or North African','Middle Eastern or North African','2118-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (909,'Assyrian','Assyrian','2119-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (910,'Egyptian','Egyptian','2120-4');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (911,'Iranian','Iranian','2121-2');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (912,'Iraqi','Iraqi','2122-0');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (913,'Lebanese','Lebanese','2123-8');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (914,'Palestinian','Palestinian','2124-6');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (915,'Syrian','Syrian','2125-3');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (916,'Afghanistani','Afghanistani','2126-1');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (917,'Israeili','Israeili','2127-9');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (918,'Arab','Arab','2129-5');
INSERT INTO race (race_cd, race_name, race_description, race_code) VALUES (919,'Other Race','Other Race','2131-1');

-- Structure for table religion (OID = 34541):
CREATE TABLE religion (
    religion_cd INTEGER NOT NULL,
    religion_name VARCHAR(64) NOT NULL,
    religion_description VARCHAR(255),
    religion_code VARCHAR(64) NOT NULL
) WITHOUT OIDS;

-- Structure for table user_session (OID = 34556):
CREATE TABLE user_session (
    session_id INTEGER NOT NULL,
    date_created TIMESTAMP without TIME ZONE NOT NULL,
    session_key VARCHAR(255),
    user_id bigint NOT NULL
) WITHOUT OIDS;

-- Structure for table audit_event (OID = 59451):
CREATE TABLE audit_event (
    audit_event_id INTEGER NOT NULL,
    date_created TIMESTAMP without TIME ZONE NOT NULL,
    audit_event_type_cd INTEGER NOT NULL,
    audit_event_description VARCHAR(255),
    ref_person_id bigint,
    alt_ref_person_id bigint,
    creator_id INTEGER NOT NULL
) WITHOUT OIDS;

-- Structure for table audit_event_type (OID = 59461):
CREATE TABLE audit_event_type (
    audit_event_type_cd INTEGER NOT NULL,
    audit_event_type_name VARCHAR(64) NOT NULL,
    audit_event_type_description VARCHAR(255),
    audit_event_type_code VARCHAR(64) NOT NULL
) WITHOUT OIDS;

INSERT INTO audit_event_type (audit_event_type_cd, audit_event_type_name, audit_event_type_description, audit_event_type_code) VALUES (1, 'Add Person', 'Add new person record', 'ADD');
INSERT INTO audit_event_type (audit_event_type_cd, audit_event_type_name, audit_event_type_description, audit_event_type_code) VALUES (2, 'Delete Person', 'Delete a person record', 'DEL');
INSERT INTO audit_event_type (audit_event_type_cd, audit_event_type_name, audit_event_type_description, audit_event_type_code) VALUES (3, 'Import Person', 'Import a person record', 'IMP');
INSERT INTO audit_event_type (audit_event_type_cd, audit_event_type_name, audit_event_type_description, audit_event_type_code) VALUES (4, 'Merge Person', 'Merge a person record', 'MRG');
INSERT INTO audit_event_type (audit_event_type_cd, audit_event_type_name, audit_event_type_description, audit_event_type_code) VALUES (5, 'Update Person', 'Update a person record', 'UPD');
INSERT INTO audit_event_type (audit_event_type_cd, audit_event_type_name, audit_event_type_description, audit_event_type_code) VALUES (6, 'Add Identifier Domain Attribute', 'Add a new identifier domain attribute', 'AIA');
INSERT INTO audit_event_type (audit_event_type_cd, audit_event_type_name, audit_event_type_description, audit_event_type_code) VALUES (7, 'Obtain Identifier Domain', 'Obtain a new unique identifier domain of a given type', 'OID');
INSERT INTO audit_event_type (audit_event_type_cd, audit_event_type_name, audit_event_type_description, audit_event_type_code) VALUES (8, 'Delete Identifier Domain Attribute', 'Delete an identifier domain attribute', 'DID');
INSERT INTO audit_event_type (audit_event_type_cd, audit_event_type_name, audit_event_type_description, audit_event_type_code) VALUES (9, 'Update Identifier Domain Attribute', 'Update an identifier domain attribute', 'UID');
INSERT INTO audit_event_type (audit_event_type_cd, audit_event_type_name, audit_event_type_description, audit_event_type_code) VALUES (10, 'Unmerge Person', 'Unmerge a person record from another one', 'UMG');

-- Structure for table person_link_review (OID = 59489):
CREATE TABLE person_link_review (
    person_link_review_id INTEGER NOT NULL,
    rh_person_id INTEGER NOT NULL,
    lh_person_id INTEGER NOT NULL,
    date_created TIMESTAMP without TIME ZONE NOT NULL,
    weight DOUBLE PRECISION,
    creator_id INTEGER NOT NULL,
    reviewer_id INTEGER,
    date_reviewed INTEGER
) WITHOUT OIDS;


-- Structure for table user_file:
CREATE TABLE user_file (
  user_file_id  INTEGER NOT NULL,
  user_id       INTEGER NOT NULL,
  "name"        VARCHAR(255) NOT NULL,
  filename      VARCHAR(255) NOT NULL,
  imported_ind  CHAR NOT NULL DEFAULT 'N'::bpchar,
  date_created  TIMESTAMP WITHOUT TIME ZONE NOT NULL
) WITHOUT OIDS;

-- Definition for sequence audit_event_seq
CREATE SEQUENCE audit_event_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
  
-- Definition for sequence hibernate_sequence (OID = 34671):
CREATE SEQUENCE hibernate_sequence
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;

-- Definition for sequence identifier_domain_seq (OID = 34673):
CREATE SEQUENCE identifier_domain_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
    
-- Definition for sequence identifier_domain_attribute_seq
CREATE SEQUENCE identifier_domain_attribute_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;

-- Definition for sequence person_identifier_seq (OID = 34675):
CREATE SEQUENCE person_identifier_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;

-- Definition for sequence person_link_seq (OID = 34677):
CREATE SEQUENCE person_link_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;

CREATE SEQUENCE person_link_review_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;

-- Definition for sequence person_seq (OID = 34679):
CREATE SEQUENCE person_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;

-- Definition for sequence user_file_seq:
CREATE SEQUENCE user_file_seq
    START WITH 10000
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;
    
-- Definition for sequence user_session_seq (OID = 34681):
CREATE SEQUENCE user_session_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;

-- Definition for sequence identifier_seq:
CREATE SEQUENCE identifier_seq
    START WITH 10000
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;

INSERT INTO identifier_domain (identifier_domain_id, universal_identifier, universal_identifier_type_code, namespace_identifier, date_created, creator_id) VALUES (10, '2.16.840.1.113883.4.1', 'SSN', '2.16.840.1.113883.4.1', '6/17/2009 11:45:54 AM', 1);
INSERT INTO identifier_domain (identifier_domain_id, universal_identifier, universal_identifier_type_code, namespace_identifier, date_created, creator_id) VALUES (11, '2.16.840.1.113883.4.2', 'TIN', '2.16.840.1.113883.4.2', '6/17/2009 11:45:54 AM', 1);
INSERT INTO identifier_domain (identifier_domain_id, universal_identifier, universal_identifier_type_code, namespace_identifier, date_created, creator_id) VALUES (12, '2.16.840.1.113883.4.3.40', 'OklahomaDLN', '2.16.840.1.113883.4.3.40', '6/17/2009 11:45:54 AM', 1);
INSERT INTO identifier_domain (identifier_domain_id, universal_identifier, universal_identifier_type_code, namespace_identifier, date_created, creator_id) VALUES (13, '2.16.840.1.113883.4.3.49', 'UtahDLN', '2.16.840.1.113883.4.3.49', '6/17/2009 11:45:54 AM', 1);
INSERT INTO identifier_domain (identifier_domain_id, universal_identifier, universal_identifier_type_code, namespace_identifier, date_created, creator_id) VALUES (14, '2.16.840.1.113883.4.3.38', 'NorthDakotaDLN', '2.16.840.1.113883.4.3.38', '6/17/2009 11:45:54 AM', 1);
INSERT INTO identifier_domain (identifier_domain_id, universal_identifier, universal_identifier_type_code, namespace_identifier, date_created, creator_id) VALUES (15, '2.16.840.1.113883.4.3.37', 'NorthCarolinaDLN', '2.16.840.1.113883.4.3.37', '6/17/2009 11:45:54 AM', 1);
INSERT INTO identifier_domain (identifier_domain_id, universal_identifier, universal_identifier_type_code, namespace_identifier, date_created, creator_id) VALUES (16, '2.16.840.1.113883.4.3.36', 'NewYorkDLN', '2.16.840.1.113883.4.3.36', '6/17/2009 11:45:54 AM', 1);
INSERT INTO identifier_domain (identifier_domain_id, universal_identifier, universal_identifier_type_code, namespace_identifier, date_created, creator_id) VALUES (17, '2.16.840.1.113883.4.3.35', 'NewMexicoDLN', '2.16.840.1.113883.4.3.35', '6/17/2009 11:45:54 AM', 1);
INSERT INTO identifier_domain (identifier_domain_id, universal_identifier, universal_identifier_type_code, namespace_identifier, date_created, creator_id) VALUES (18, '2.16.840.1.113883.4.3.34', 'NewJerseyDLN', '2.16.840.1.113883.4.3.34', '6/17/2009 11:45:54 AM', 1);
INSERT INTO identifier_domain (identifier_domain_id, universal_identifier, universal_identifier_type_code, namespace_identifier, date_created, creator_id) VALUES (19, '2.16.840.1.113883.4.3.48', 'TexasDLN', '2.16.840.1.113883.4.3.48', '6/17/2009 11:45:54 AM', 1);
INSERT INTO identifier_domain (identifier_domain_id, universal_identifier, universal_identifier_type_code, namespace_identifier, date_created, creator_id) VALUES (20, '2.16.840.1.113883.4.3.32', 'NevadaDLN', '2.16.840.1.113883.4.3.32', '6/17/2009 11:45:54 AM', 1);
INSERT INTO identifier_domain (identifier_domain_id, universal_identifier, universal_identifier_type_code, namespace_identifier, date_created, creator_id) VALUES (21, '2.16.840.1.113883.4.3.39', 'OhioDLN', '2.16.840.1.113883.4.3.39', '6/17/2009 11:45:54 AM', 1);
INSERT INTO identifier_domain (identifier_domain_id, universal_identifier, universal_identifier_type_code, namespace_identifier, date_created, creator_id) VALUES (22, '2.16.840.1.113883.4.3.15', 'HawaiiDLN', '2.16.840.1.113883.4.3.15', '6/17/2009 11:45:54 AM', 1);
INSERT INTO identifier_domain (identifier_domain_id, universal_identifier, universal_identifier_type_code, namespace_identifier, date_created, creator_id) VALUES (23, '2.16.840.1.113883.4.3.51', 'VirginiaDLN', '2.16.840.1.113883.4.3.51', '6/17/2009 11:45:54 AM', 1);
INSERT INTO identifier_domain (identifier_domain_id, universal_identifier, universal_identifier_type_code, namespace_identifier, date_created, creator_id) VALUES (24, '2.16.840.1.113883.4.3.53', 'WashingtonDLN', '2.16.840.1.113883.4.3.53', '6/17/2009 11:45:54 AM', 1);
INSERT INTO identifier_domain (identifier_domain_id, universal_identifier, universal_identifier_type_code, namespace_identifier, date_created, creator_id) VALUES (25, '2.16.840.1.113883.4.3.54', 'WestVirginiaDLN', '2.16.840.1.113883.4.3.54', '6/17/2009 11:45:54 AM', 1);
INSERT INTO identifier_domain (identifier_domain_id, universal_identifier, universal_identifier_type_code, namespace_identifier, date_created, creator_id) VALUES (26, '2.16.840.1.113883.4.3.55', 'WisconsinDLN', '2.16.840.1.113883.4.3.55', '6/17/2009 11:45:54 AM', 1);
INSERT INTO identifier_domain (identifier_domain_id, universal_identifier, universal_identifier_type_code, namespace_identifier, date_created, creator_id) VALUES (27, '2.16.840.1.113883.4.3.56', 'WyomingDLN', '2.16.840.1.113883.4.3.56', '6/17/2009 11:45:54 AM', 1);
INSERT INTO identifier_domain (identifier_domain_id, universal_identifier, universal_identifier_type_code, namespace_identifier, date_created, creator_id) VALUES (28, '2.16.840.1.113883.4.3.42', 'PennsylvaniaDLN', '2.16.840.1.113883.4.3.42', '6/17/2009 11:45:54 AM', 1);
INSERT INTO identifier_domain (identifier_domain_id, universal_identifier, universal_identifier_type_code, namespace_identifier, date_created, creator_id) VALUES (29, '2.16.840.1.113883.4.3.47', 'TennesseeDLN', '2.16.840.1.113883.4.3.47', '6/17/2009 11:45:54 AM', 1);
INSERT INTO identifier_domain (identifier_domain_id, universal_identifier, universal_identifier_type_code, namespace_identifier, date_created, creator_id) VALUES (30, '2.16.840.1.113883.4.3.41', 'OregonDLN', '2.16.840.1.113883.4.3.41', '6/17/2009 11:45:54 AM', 1);
INSERT INTO identifier_domain (identifier_domain_id, universal_identifier, universal_identifier_type_code, namespace_identifier, date_created, creator_id) VALUES (31, '2.16.840.1.113883.4.3.46', 'SouthDakotaDLN', '2.16.840.1.113883.4.3.46', '6/17/2009 11:45:54 AM', 1);
INSERT INTO identifier_domain (identifier_domain_id, universal_identifier, universal_identifier_type_code, namespace_identifier, date_created, creator_id) VALUES (32, '2.16.840.1.113883.4.3.45', 'SouthCarolinaDLN', '2.16.840.1.113883.4.3.45', '6/17/2009 11:45:54 AM', 1);
INSERT INTO identifier_domain (identifier_domain_id, universal_identifier, universal_identifier_type_code, namespace_identifier, date_created, creator_id) VALUES (33, '2.16.840.1.113883.4.3.31', 'NebraskaDLN', '2.16.840.1.113883.4.3.31', '6/17/2009 11:45:54 AM', 1);
INSERT INTO identifier_domain (identifier_domain_id, universal_identifier, universal_identifier_type_code, namespace_identifier, date_created, creator_id) VALUES (34, '2.16.840.1.113883.4.3.50', 'VermontDLN', '2.16.840.1.113883.4.3.50', '6/17/2009 11:45:54 AM', 1);
INSERT INTO identifier_domain (identifier_domain_id, universal_identifier, universal_identifier_type_code, namespace_identifier, date_created, creator_id) VALUES (35, '2.16.840.1.113883.4.3.30', 'MontanaDLN', '2.16.840.1.113883.4.3.30', '6/17/2009 11:45:54 AM', 1);
INSERT INTO identifier_domain (identifier_domain_id, universal_identifier, universal_identifier_type_code, namespace_identifier, date_created, creator_id) VALUES (36, '2.16.840.1.113883.4.3.44', 'RhodeIslandDLN', '2.16.840.1.113883.4.3.44', '6/17/2009 11:45:54 AM', 1);
INSERT INTO identifier_domain (identifier_domain_id, universal_identifier, universal_identifier_type_code, namespace_identifier, date_created, creator_id) VALUES (37, '2.16.840.1.113883.4.3.16', 'IdahoDLN', '2.16.840.1.113883.4.3.16', '6/17/2009 11:45:54 AM', 1);
INSERT INTO identifier_domain (identifier_domain_id, universal_identifier, universal_identifier_type_code, namespace_identifier, date_created, creator_id) VALUES (38, '2.16.840.1.113883.4.3.5', 'ArkansasDLN', '2.16.840.1.113883.4.3.5', '6/17/2009 11:45:54 AM', 1);
INSERT INTO identifier_domain (identifier_domain_id, universal_identifier, universal_identifier_type_code, namespace_identifier, date_created, creator_id) VALUES (39, '2.16.840.1.113883.4.3.9', 'ConnecticutDLN', '2.16.840.1.113883.4.3.9', '6/17/2009 11:45:54 AM', 1);
INSERT INTO identifier_domain (identifier_domain_id, universal_identifier, universal_identifier_type_code, namespace_identifier, date_created, creator_id) VALUES (40, '2.16.840.1.113883.4.3.8', 'ColoradoDLN', '2.16.840.1.113883.4.3.8', '6/17/2009 11:45:54 AM', 1);
INSERT INTO identifier_domain (identifier_domain_id, universal_identifier, universal_identifier_type_code, namespace_identifier, date_created, creator_id) VALUES (41, '2.16.840.1.113883.4.3.6', 'CaliforniaDLN', '2.16.840.1.113883.4.3.6', '6/17/2009 11:45:54 AM', 1);
INSERT INTO identifier_domain (identifier_domain_id, universal_identifier, universal_identifier_type_code, namespace_identifier, date_created, creator_id) VALUES (42, '2.16.840.1.113883.4.3.33', 'NewHampshireDLN', '2.16.840.1.113883.4.3.33', '6/17/2009 11:45:54 AM', 1);
INSERT INTO identifier_domain (identifier_domain_id, universal_identifier, universal_identifier_type_code, namespace_identifier, date_created, creator_id) VALUES (43, '2.16.840.1.113883.4.3.11', 'DCDLN', '2.16.840.1.113883.4.3.11', '6/17/2009 11:45:54 AM', 1);
INSERT INTO identifier_domain (identifier_domain_id, universal_identifier, universal_identifier_type_code, namespace_identifier, date_created, creator_id) VALUES (44, '2.16.840.1.113883.4.3.10', 'DelawareDLN', '2.16.840.1.113883.4.3.10', '6/17/2009 11:45:54 AM', 1);
INSERT INTO identifier_domain (identifier_domain_id, universal_identifier, universal_identifier_type_code, namespace_identifier, date_created, creator_id) VALUES (45, '2.16.840.1.113883.4.3.12', 'FloridaDLN', '2.16.840.1.113883.4.3.12', '6/17/2009 11:45:54 AM', 1);
INSERT INTO identifier_domain (identifier_domain_id, universal_identifier, universal_identifier_type_code, namespace_identifier, date_created, creator_id) VALUES (46, '2.16.840.1.113883.4.3.4', 'ArizonaDLN', '2.16.840.1.113883.4.3.4', '6/17/2009 11:45:54 AM', 1);
INSERT INTO identifier_domain (identifier_domain_id, universal_identifier, universal_identifier_type_code, namespace_identifier, date_created, creator_id) VALUES (47, '2.16.840.1.113883.4.3.2', 'AlaskaDLN', '2.16.840.1.113883.4.3.2', '6/17/2009 11:45:54 AM', 1);
INSERT INTO identifier_domain (identifier_domain_id, universal_identifier, universal_identifier_type_code, namespace_identifier, date_created, creator_id) VALUES (48, '2.16.840.1.113883.4.3.1', 'AlabamaDLN', '2.16.840.1.113883.4.3.1', '6/17/2009 11:45:54 AM', 1);
INSERT INTO identifier_domain (identifier_domain_id, universal_identifier, universal_identifier_type_code, namespace_identifier, date_created, creator_id) VALUES (49, '2.16.840.1.113883.4.3', 'USDLN', '2.16.840.1.113883.4.3', '6/17/2009 11:45:54 AM', 1);
COMMIT;

-- Definition for index identifier_domain_namespace_identifier_key (OID = 34683):
CREATE UNIQUE INDEX identifier_domain_namespace_identifier_key ON identifier_domain USING btree (namespace_identifier);

-- Definition for index identifier_domain_universal_identifier (OID = 34684):
CREATE UNIQUE INDEX identifier_domain_universal_identifier ON identifier_domain USING btree (universal_identifier, universal_identifier_type_code);

-- Definition for index idx_audit_event_ref_person (OID = 59456):
CREATE INDEX idx_audit_event_ref_person ON audit_event USING btree (ref_person_id);

-- Definition for index idx_audit_event_type_code (OID = 59468):
CREATE INDEX idx_audit_event_type_code ON audit_event_type USING btree (audit_event_type_code);

-- Definition for index idx_identifier_domain_id
CREATE INDEX idx_identifier_domain_id ON PUBLIC.identifier_domain_attribute USING btree (identifier_domain_id);

-- Definition for index person_identifier_person_id (OID = 34685):
CREATE INDEX person_identifier_person_id ON person_identifier USING btree (person_id);

-- Definition for index person_identifier_identifier_domain (OID = 34686):
CREATE INDEX person_identifier_identifier_domain ON person_identifier USING btree (identifier_domain_id);

-- Definition for index person_link_rh_person (OID = 34712):
CREATE INDEX person_link_rh_person ON person_link USING btree (rh_person_id);

-- Definition for index person_link_lh_person (OID = 34713):
CREATE INDEX person_link_lh_person ON person_link USING btree (lh_person_id);

-- Definition for index role_name (OID = 34729):
CREATE UNIQUE INDEX role_name ON role USING btree (NAME);

-- Definition for index user_session_session_key (OID = 34740):
CREATE UNIQUE INDEX user_session_session_key ON user_session USING btree (session_key);

-- Definition for index user_session_date_created (OID = 34741):
CREATE INDEX user_session_date_created ON user_session USING btree (date_created);

-- Definition for index address_type_pkey (OID = 34477):
ALTER TABLE ONLY address_type
    ADD CONSTRAINT address_type_pkey PRIMARY KEY (address_type_cd);

-- Definition for index app_user_pkey (OID = 34482):
ALTER TABLE ONLY app_user
    ADD CONSTRAINT app_user_pkey PRIMARY KEY (id);

-- Definition for index app_user_username_key (OID = 34484):
ALTER TABLE ONLY app_user
    ADD CONSTRAINT app_user_username_key UNIQUE (username);

-- Definition for index app_user_email_key (OID = 34486):
ALTER TABLE ONLY app_user
    ADD CONSTRAINT app_user_email_key UNIQUE (email);

-- Definition for index ethnic_group_pkey (OID = 34491):
ALTER TABLE ONLY ethnic_group
    ADD CONSTRAINT ethnic_group_pkey PRIMARY KEY (ethnic_group_cd);

-- Definition for index gender_pkey (OID = 34496):
ALTER TABLE ONLY gender
    ADD CONSTRAINT gender_pkey PRIMARY KEY (gender_cd);

-- Definition for index identifier_domain_pkey (OID = 34501):
ALTER TABLE ONLY identifier_domain
    ADD CONSTRAINT identifier_domain_pkey PRIMARY KEY (identifier_domain_id);

-- Definition for index identifier_domain_attribute_pkey
ALTER TABLE ONLY identifier_domain_attribute
    ADD CONSTRAINT identifier_domain_attribute_pkey PRIMARY KEY (identifier_domain_attribute_id);

-- Definition for index language_pkey (OID = 34506):
ALTER TABLE ONLY "language"
    ADD CONSTRAINT language_pkey PRIMARY KEY (language_cd);

-- Definition for index name_type_pkey (OID = 34511):
ALTER TABLE ONLY name_type
    ADD CONSTRAINT name_type_pkey PRIMARY KEY (name_type_cd);

-- Definition for index nationality_pkey (OID = 34516):
ALTER TABLE ONLY nationality
    ADD CONSTRAINT nationality_pkey PRIMARY KEY (nationality_cd);

-- Definition for index person_pkey (OID = 34524):
ALTER TABLE ONLY person
    ADD CONSTRAINT person_pkey PRIMARY KEY (person_id);

-- Definition for index person_identifier_pkey (OID = 34529):
ALTER TABLE ONLY person_identifier
    ADD CONSTRAINT person_identifier_pkey PRIMARY KEY (person_identifier_id);

-- Definition for index person_link_pkey (OID = 34534):
ALTER TABLE ONLY person_link
    ADD CONSTRAINT person_link_pkey PRIMARY KEY (person_link_id);

-- Definition for index race_pkey (OID = 34539):
ALTER TABLE ONLY race
    ADD CONSTRAINT race_pkey PRIMARY KEY (race_cd);

-- Definition for index religion_pkey (OID = 34544):
ALTER TABLE ONLY religion
    ADD CONSTRAINT religion_pkey PRIMARY KEY (religion_cd);

-- Definition for index role_pkey (OID = 34549):
ALTER TABLE ONLY role
    ADD CONSTRAINT role_pkey PRIMARY KEY (id);

-- Definition for index user_role_pkey (OID = 34554):
ALTER TABLE ONLY user_role
    ADD CONSTRAINT user_role_pkey PRIMARY KEY (user_id, role_id);

-- Definition for index user_session_pkey (OID = 34559):
ALTER TABLE ONLY user_session
    ADD CONSTRAINT user_session_pkey PRIMARY KEY (session_id);

-- Definition for index fk87a8451ade850683 (OID = 34561):
ALTER TABLE ONLY identifier_domain
    ADD CONSTRAINT fk87a8451ade850683 FOREIGN KEY (creator_id) REFERENCES app_user(id);

-- Definition for foreign key constraint on identifier_domain_id on table identifier_domain_attribute
ALTER TABLE ONLY identifier_domain_attribute
    ADD CONSTRAINT fk_identifier_domain_id FOREIGN KEY (identifier_domain_id) REFERENCES identifier_domain(identifier_domain_id) ON DELETE CASCADE ON UPDATE NO ACTION;

-- Definition for index fk_identifier_domain (OID = 34692):
ALTER TABLE ONLY person_identifier
    ADD CONSTRAINT fk_identifier_domain FOREIGN KEY (identifier_domain_id) REFERENCES identifier_domain(identifier_domain_id);

-- Definition for index fk_voided_by_app_user (OID = 34697):
ALTER TABLE ONLY person_identifier
    ADD CONSTRAINT fk_voided_by_app_user FOREIGN KEY (voided_by_id) REFERENCES app_user(id);

-- Definition for index fk_created_by_app_user (OID = 34702):
ALTER TABLE ONLY person_identifier
    ADD CONSTRAINT fk_created_by_app_user FOREIGN KEY (creator_id) REFERENCES app_user(id);

-- Definition for index fk_person (OID = 34707):
ALTER TABLE ONLY person_identifier
    ADD CONSTRAINT fk_person FOREIGN KEY (person_id) REFERENCES person(person_id);

-- Definition for index fk_person_lh (OID = 34714):
ALTER TABLE ONLY person_link
    ADD CONSTRAINT fk_person_lh FOREIGN KEY (lh_person_id) REFERENCES person(person_id);

-- Definition for index fk_person_rh (OID = 34719):
ALTER TABLE ONLY person_link
    ADD CONSTRAINT fk_person_rh FOREIGN KEY (rh_person_id) REFERENCES person(person_id);

-- Definition for index fk_created_by_app_user (OID = 34724):
ALTER TABLE ONLY person_link
    ADD CONSTRAINT fk_created_by_app_user FOREIGN KEY (creator_id) REFERENCES app_user(id);

-- Definition for index fk_user_role_user (OID = 34730):
ALTER TABLE ONLY user_role
    ADD CONSTRAINT fk_user_role_user FOREIGN KEY (user_id) REFERENCES app_user(id);

-- Definition for index fk_user_role_role (OID = 34735):
ALTER TABLE ONLY user_role
    ADD CONSTRAINT fk_user_role_role FOREIGN KEY (role_id) REFERENCES role(id);

-- Definition for index fk_user_session_user (OID = 34742):
ALTER TABLE ONLY user_session
    ADD CONSTRAINT fk_user_session_user FOREIGN KEY (user_id) REFERENCES app_user(id);

-- Definition for index fk_voided_by_app_user (OID = 34747):
ALTER TABLE ONLY person
    ADD CONSTRAINT fk_voided_by_app_user FOREIGN KEY (voided_by_id) REFERENCES app_user(id);

-- Definition for index fk_address_type (OID = 34752):
ALTER TABLE ONLY person
    ADD CONSTRAINT fk_address_type FOREIGN KEY (address_type_cd) REFERENCES address_type(address_type_cd);

-- Definition for index fk_nationality (OID = 34757):
ALTER TABLE ONLY person
    ADD CONSTRAINT fk_nationality FOREIGN KEY (nationality_cd) REFERENCES nationality(nationality_cd);

-- Definition for index fk_language (OID = 34762):
ALTER TABLE ONLY person
    ADD CONSTRAINT fk_language FOREIGN KEY (language_cd) REFERENCES LANGUAGE(language_cd);

-- Definition for index fk_changed_by_app_user (OID = 34767):
ALTER TABLE ONLY person
    ADD CONSTRAINT fk_changed_by_app_user FOREIGN KEY (changed_by_id) REFERENCES app_user(id);

-- Definition for index fk_ethnic_group (OID = 34772):
ALTER TABLE ONLY person
    ADD CONSTRAINT fk_ethnic_group FOREIGN KEY (ethnic_group_cd) REFERENCES ethnic_group(ethnic_group_cd);

-- Definition for index fk_religion (OID = 34777):
ALTER TABLE ONLY person
    ADD CONSTRAINT fk_religion FOREIGN KEY (religion_cd) REFERENCES religion(religion_cd);

-- Definition for index fk_gender (OID = 34782):
ALTER TABLE ONLY person
    ADD CONSTRAINT fk_gender FOREIGN KEY (gender_cd) REFERENCES gender(gender_cd);

-- Definition for index fk_race (OID = 34787):
ALTER TABLE ONLY person
    ADD CONSTRAINT fk_race FOREIGN KEY (race_cd) REFERENCES race(race_cd);

-- Definition for index fk_created_by_app_user (OID = 34792):
ALTER TABLE ONLY person
    ADD CONSTRAINT fk_created_by_app_user FOREIGN KEY (creator_id) REFERENCES app_user(id);

-- Definition for index fk_name_type (OID = 34797):
ALTER TABLE ONLY person
    ADD CONSTRAINT fk_name_type FOREIGN KEY (name_type_cd) REFERENCES name_type(name_type_cd);


-- Definition for index audit_event_pkey (OID = 59454):
ALTER TABLE ONLY audit_event
    ADD CONSTRAINT audit_event_pkey PRIMARY KEY (audit_event_id);

-- Definition for index audit_event_type_pkey (OID = 59464):
ALTER TABLE ONLY audit_event_type
    ADD CONSTRAINT audit_event_type_pkey PRIMARY KEY (audit_event_type_cd);

-- Definition for index idx_audit_event_type_name (OID = 59466):
ALTER TABLE ONLY audit_event_type
    ADD CONSTRAINT idx_audit_event_type_name UNIQUE (audit_event_type_name);

-- Definition for index fk_audit_event_type_cd (OID = 59469):
ALTER TABLE ONLY audit_event
    ADD CONSTRAINT fk_audit_event_type_cd FOREIGN KEY (audit_event_type_cd) REFERENCES audit_event_type(audit_event_type_cd);

-- Definition for index fk_ref_person_id (OID = 59474):
ALTER TABLE ONLY audit_event
    ADD CONSTRAINT fk_ref_person_id FOREIGN KEY (ref_person_id) REFERENCES person(person_id);

-- Definition for index fk_alt_ref_person_id (OID = 59479):
ALTER TABLE ONLY audit_event
    ADD CONSTRAINT fk_alt_ref_person_id FOREIGN KEY (alt_ref_person_id) REFERENCES person(person_id);

-- Definition for index fk_creator_id (OID = 59484):
ALTER TABLE ONLY audit_event
    ADD CONSTRAINT fk_creator_id FOREIGN KEY (creator_id) REFERENCES app_user(id);

-- Definition for index person_link_review_pkey (OID = 59492):
ALTER TABLE ONLY person_link_review
    ADD CONSTRAINT person_link_review_pkey PRIMARY KEY (person_link_review_id);

-- Definition for index fk_rh_person_id (OID = 59494):
ALTER TABLE ONLY person_link_review
    ADD CONSTRAINT fk_rh_person_id FOREIGN KEY (rh_person_id) REFERENCES person(person_id);

-- Definition for index fk_lh_person_id (OID = 59499):
ALTER TABLE ONLY person_link_review
    ADD CONSTRAINT fk_lh_person_id FOREIGN KEY (lh_person_id) REFERENCES person(person_id);

-- Definition for index fk_creator_id (OID = 59504):
ALTER TABLE ONLY person_link_review
    ADD CONSTRAINT fk_creator_id FOREIGN KEY (creator_id) REFERENCES app_user(id);

-- Definition for index fk_reviewer_id (OID = 59509):
ALTER TABLE ONLY person_link_review
    ADD CONSTRAINT fk_reviewer_id FOREIGN KEY (reviewer_id) REFERENCES app_user(id);

ALTER TABLE ONLY user_file
    ADD CONSTRAINT user_files_pkey PRIMARY KEY (user_file_id);

ALTER TABLE ONLY user_file
    ADD CONSTRAINT fk_user_file_user FOREIGN KEY (user_id) REFERENCES app_user(id);

COMMENT ON SCHEMA PUBLIC IS 'standard public schema';
COMMENT ON COLUMN audit_event.ref_person_id IS 'The field refers to a person record that is associated in some way with the audit event. For example in the case of a person record update audit event this field will refer to the person record that was updated.';
COMMENT ON COLUMN audit_event.alt_ref_person_id IS 'The audit event may refer to a second person that is associated with the event in some way. For example a link audit event would refer to the second person record that was linked.';
