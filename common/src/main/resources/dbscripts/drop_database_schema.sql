------------------------------------------
--Host     : localhost
--Database : openempi



SET SESSION AUTHORIZATION 'openempi';
SET search_path = PUBLIC, pg_catalog;

ALTER TABLE ONLY PUBLIC.person DROP CONSTRAINT fk_name_type;
ALTER TABLE ONLY PUBLIC.person DROP CONSTRAINT fk_created_by_app_user;
ALTER TABLE ONLY PUBLIC.person DROP CONSTRAINT fk_race;
ALTER TABLE ONLY PUBLIC.person DROP CONSTRAINT fk_gender;
ALTER TABLE ONLY PUBLIC.person DROP CONSTRAINT fk_religion;
ALTER TABLE ONLY PUBLIC.person DROP CONSTRAINT fk_ethnic_group;
ALTER TABLE ONLY PUBLIC.person DROP CONSTRAINT fk_changed_by_app_user;
ALTER TABLE ONLY PUBLIC.person DROP CONSTRAINT fk_language;
ALTER TABLE ONLY PUBLIC.person DROP CONSTRAINT fk_nationality;
ALTER TABLE ONLY PUBLIC.person DROP CONSTRAINT fk_address_type;
ALTER TABLE ONLY PUBLIC.person DROP CONSTRAINT fk_voided_by_app_user;
ALTER TABLE ONLY PUBLIC.user_session DROP CONSTRAINT fk_user_session_user;
ALTER TABLE ONLY PUBLIC.user_role DROP CONSTRAINT fk_user_role_role;
ALTER TABLE ONLY PUBLIC.user_role DROP CONSTRAINT fk_user_role_user;
ALTER TABLE ONLY PUBLIC.person_link DROP CONSTRAINT fk_created_by_app_user;
ALTER TABLE ONLY PUBLIC.person_link DROP CONSTRAINT fk_person_rh;
ALTER TABLE ONLY PUBLIC.person_link DROP CONSTRAINT fk_person_lh;
ALTER TABLE ONLY PUBLIC.person_identifier DROP CONSTRAINT fk_person;
ALTER TABLE ONLY PUBLIC.person_identifier DROP CONSTRAINT fk_created_by_app_user;
ALTER TABLE ONLY PUBLIC.person_identifier DROP CONSTRAINT fk_voided_by_app_user;
ALTER TABLE ONLY PUBLIC.person_identifier DROP CONSTRAINT fk_identifier_domain;
ALTER TABLE ONLY PUBLIC.identifier_domain DROP CONSTRAINT fk87a8451ade850683;
ALTER TABLE ONLY PUBLIC.identifier_domain_attribute DROP CONSTRAINT fk_identifier_domain_id;
ALTER TABLE ONLY PUBLIC.user_session DROP CONSTRAINT user_session_pkey;
ALTER TABLE ONLY PUBLIC.user_role DROP CONSTRAINT user_role_pkey;
ALTER TABLE ONLY PUBLIC."role" DROP CONSTRAINT role_pkey;
ALTER TABLE ONLY PUBLIC.religion DROP CONSTRAINT religion_pkey;
ALTER TABLE ONLY PUBLIC.race DROP CONSTRAINT race_pkey;
ALTER TABLE ONLY PUBLIC.person_link DROP CONSTRAINT person_link_pkey;
ALTER TABLE ONLY PUBLIC.person_identifier DROP CONSTRAINT person_identifier_pkey;
ALTER TABLE ONLY PUBLIC.person DROP CONSTRAINT person_pkey CASCADE;
ALTER TABLE ONLY PUBLIC.nationality DROP CONSTRAINT nationality_pkey;
ALTER TABLE ONLY PUBLIC.name_type DROP CONSTRAINT name_type_pkey;
ALTER TABLE ONLY PUBLIC."language" DROP CONSTRAINT language_pkey;
ALTER TABLE ONLY PUBLIC.identifier_domain DROP CONSTRAINT identifier_domain_pkey;
ALTER TABLE ONLY PUBLIC.identifier_domain_attribute DROP CONSTRAINT identifier_domain_attribute_pkey;
ALTER TABLE ONLY PUBLIC.gender DROP CONSTRAINT gender_pkey;
ALTER TABLE ONLY PUBLIC.ethnic_group DROP CONSTRAINT ethnic_group_pkey;
ALTER TABLE ONLY PUBLIC.app_user DROP CONSTRAINT app_user_email_key;
ALTER TABLE ONLY PUBLIC.app_user DROP CONSTRAINT app_user_username_key;
ALTER TABLE ONLY PUBLIC.app_user DROP CONSTRAINT app_user_pkey CASCADE;
ALTER TABLE ONLY PUBLIC.address_type DROP CONSTRAINT address_type_pkey;
ALTER TABLE ONLY PUBLIC.audit_event DROP CONSTRAINT fk_audit_event_type_cd;
ALTER TABLE ONLY PUBLIC.audit_event_type DROP CONSTRAINT idx_audit_event_type_name;
ALTER TABLE ONLY PUBLIC.audit_event_type DROP CONSTRAINT audit_event_type_pkey;
ALTER TABLE ONLY PUBLIC.audit_event DROP CONSTRAINT audit_event_pkey;


DROP INDEX PUBLIC.user_session_date_created;
DROP INDEX PUBLIC.user_session_session_key;
DROP INDEX PUBLIC.role_name;
DROP INDEX PUBLIC.person_link_lh_person;
DROP INDEX PUBLIC.person_link_rh_person;
DROP INDEX PUBLIC.person_identifier_identifier_domain;
DROP INDEX PUBLIC.person_identifier_person_id;
DROP INDEX PUBLIC.identifier_domain_universal_identifier;
DROP INDEX PUBLIC.identifier_domain_namespace_identifier_key;
DROP INDEX PUBLIC.idx_audit_event_type_code;
DROP INDEX PUBLIC.idx_audit_event_ref_person;

DROP SEQUENCE PUBLIC.user_session_seq;
DROP SEQUENCE PUBLIC.person_seq;
DROP SEQUENCE PUBLIC.person_link_seq;
DROP SEQUENCE PUBLIC.person_identifier_seq;
DROP SEQUENCE PUBLIC.identifier_domain_seq;
DROP SEQUENCE PUBLIC.identifier_domain_attribute_seq;
DROP SEQUENCE PUBLIC.identifier_seq;
DROP SEQUENCE PUBLIC.hibernate_sequence;
DROP SEQUENCE PUBLIC.audit_event_seq;
DROP SEQUENCE PUBLIC.person_link_review_seq;
DROP SEQUENCE PUBLIC.user_file_seq;

DROP TABLE PUBLIC.user_session;
DROP TABLE PUBLIC.user_role;
DROP TABLE PUBLIC."role";
DROP TABLE PUBLIC.religion;
DROP TABLE PUBLIC.race;
DROP TABLE PUBLIC.person_link_review;
DROP TABLE PUBLIC.audit_event;
DROP TABLE PUBLIC.audit_event_type;
DROP TABLE PUBLIC.person_link;
DROP TABLE PUBLIC.person_identifier;
DROP TABLE PUBLIC.person;
DROP TABLE PUBLIC.nationality;
DROP TABLE PUBLIC.name_type;
DROP TABLE PUBLIC."language";
DROP TABLE PUBLIC.identifier_domain;
DROP TABLE PUBLIC.identifier_domain_attribute;
DROP TABLE PUBLIC.gender;
DROP TABLE PUBLIC.ethnic_group;
DROP TABLE PUBLIC.user_file;
DROP TABLE PUBLIC.app_user;
DROP TABLE PUBLIC.address_type;
DROP TABLE PUBLIC.phone_type;

