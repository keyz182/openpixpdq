/**
 *  Copyright (c) 2009-2011 Misys Open Source Solutions (MOSS) and others
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *  implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 *  Contributors:
 *    Misys Open Source Solutions - initial API and implementation
 *    -
 */

package org.openhealthtools.openpixpdq.integrationtests.v2;

import ca.uhn.hl7v2.app.Initiator;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.v231.segment.MSA;
import ca.uhn.hl7v2.model.v25.message.RSP_K21;
import ca.uhn.hl7v2.model.v25.segment.PID;
import ca.uhn.hl7v2.model.v25.segment.QAK;
import ca.uhn.hl7v2.parser.PipeParser;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Test additional PIX Manager functionality.
 *
 * @author Wenzhi Li
 * @version 1.0, Jan 22, 2009
 */
public class PixMoreTest extends AbstractPixPdqTestCase {

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test PIX Feed a Patient with Null Sex.
     */
    @Test
    public void testPixFeedNoSex() {
        try {
            //Step 1: PIX Feed one patient in HIMSS2005
            String msg = "MSH|^~\\&|MESA_ADT|DOMAIN1_ADMITTING|EHR_MISYS|MISYS|200310011100||ADT^A04^ADT_A01|PixMore101|P|2.3.1||||||||\r" +
                    "EVN||200310011100||||200310011043\r" +
                    "PID|||PIXMore101^^^HIMSS2005&1.3.6.1.4.1.21367.2005.1.1&ISO^PI||WATERS^SUSAN||19580224|||WH|18 PINETREE^^WEBSTER^MO^63119|||||||10501-101|||||||||||||||||||||\r" +
                    "PV1||O||||||||||||||||||||||||||||||||||||||||||||||||||";
            PipeParser pipeParser = new PipeParser();
            Message adt = pipeParser.parse(msg);
            Initiator initiator = pixConnection.getInitiator();
            Message response = initiator.sendAndReceive(adt);
            String responseString = pipeParser.encode(response);
            System.out.println("Received response:\n" + responseString);
            MSA msa = (MSA) response.get("MSA");
            assertEquals("AA", msa.getAcknowledgementCode().getValue());
            assertEquals("PixMore101", msa.getMessageControlID().getValue());

        } catch (Exception e) {
            e.printStackTrace();
            fail("Fail to test PIX Feed with NULL Sex");
        }
    }

    /**
     * Test PIX Feed a Patient with Null Sex.
     */
    @Test
    public void testPatientNameWithNameTypeCode() {
        try {
            //Step 1: PIX Feed one patient in HIMSS2005 on a patient with name type code
            String msg = "MSH|^~\\&|MESA_ADT|DOMAIN1_ADMITTING|EHR_MISYS|MISYS|200310011100||ADT^A04^ADT_A01|PixMore102|P|2.3.1||||||||\r" +
                    "EVN||200310011100||||200310011043\r" +
                    "PID|||PIXMore102^^^HIMSS2005&1.3.6.1.4.1.21367.2005.1.1&ISO^PI||DEPINTO^DAVID^^^^^L|MCGEEX^^^^^^M|19760228|||WH|18 PINETREE^^WEBSTER^MO^63119|||||||10501-101|||||||||||||||||||||\r" +
                    "PV1||O||||||||||||||||||||||||||||||||||||||||||||||||||";
            PipeParser pipeParser = new PipeParser();
            Message adt = pipeParser.parse(msg);
            Initiator initiator = pixConnection.getInitiator();
            Message response = initiator.sendAndReceive(adt);
            String responseString = pipeParser.encode(response);
            System.out.println("Received response:\n" + responseString);
            MSA msa = (MSA) response.get("MSA");
            assertEquals("AA", msa.getAcknowledgementCode().getValue());
            assertEquals("PixMore102", msa.getMessageControlID().getValue());


            //Step 2. PDQ on the patient name should return name type code back
            msg = "MSH|^~\\&|MESA_PD_CONSUMER|MESA_DEPARTMENT|EHR_MISYS|MISYS|||QBP^Q22|11311110|P|2.5||||||||\r" +
                    "QPD|IHE PDQ Query|QRY11311110|@PID.5.1^DEPINTO~@PID.5.2^DAVID|||||^^^HIMSS2005&1.3.6.1.4.1.21367.2005.1.1&ISO\r" +
                    "RCP|I|10^RD|||||";

            Message query = pipeParser.parse(msg);

            initiator = pdqConnection.getInitiator();
            response = initiator.sendAndReceive(query);
            responseString = pipeParser.encode(response);
            //System.out.println("Received response:\n" + responseString);
            ca.uhn.hl7v2.model.v25.segment.MSA msaQuery = (ca.uhn.hl7v2.model.v25.segment.MSA) response.get("MSA");
            assertEquals("AA", msaQuery.getAcknowledgmentCode().getValue());
            QAK qak = (QAK) response.get("QAK");
            assertEquals("OK", qak.getQueryResponseStatus().getValue());
            PID pid = ((RSP_K21) response).getQUERY_RESPONSE().getPID();
            assertEquals("DEPINTO", pid.getPatientName(0).getFamilyName().getSurname().getValue());
            assertEquals("DAVID", pid.getPatientName(0).getGivenName().getValue());
            assertEquals("L", pid.getPatientName(0).getNameTypeCode().getValue());
        } catch (Exception e) {
            e.printStackTrace();
            fail("Fail to testPatientNameWithNameTypeCode");
        }
    }

}
