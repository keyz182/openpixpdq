/**
 *  Copyright (c) 2009-2010 Misys Open Source Solutions (MOSS) and others
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *  implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 *  Contributors:
 *    Misys Open Source Solutions - initial API and implementation
 *    -
 */
package org.openhealthtools.openpixpdq.integrationtests.v3;

import org.apache.axiom.om.OMElement;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * This is the NIST ITI-47-Query-Case2-Data-Found-Single-Domain test.
 * <p/>
 * Pre-condition:
 * <ul>
 * <li>1. Your PDQ Supplier is configured to send and receive demographics in a
 * single domain: 2.16.840.1.113883.3.72.5.9.1 (NIST2010)</li>
 * <li>2. The following patients have been loaded into the PDQ Supplier (via
 * Load PDQ Supplier Data) in the domain given in pre-condition (1) above:
 * DAVID GREGORYX with ID GD-951
 * JOYCE GROSSX with ID HJ-361
 * DANNY GREGORYX with ID GD-698
 * KELLY GREGORYX with ID GK-891
 * JANE BRACYX with ID BJ-516</li>
 * </ul>
 * <p/>
 * <p/>
 * Description:
 * Test case ITI-47-Query-Case2-Data-Found-Single-Domain addresses PDQ Query
 * Case 2. You need to have loaded the set of patients indicated in the pre-conditions
 * into your system before running this test. The purpose of the test is to
 * confirm that your PDQ Supplier supports the multiple query parameters.
 * <p/>
 * The following queries are tested:
 * 1. Query for the patient ID
 * 2. Query for the patient name
 * 3. Query for the patient name and the date of birth
 * 4. Query for the patient name and the administrative sex
 * 5. Query for the address (street, city, state and zip code)
 *
 * @author <a href="mailto:wenzhi.li@misys.com">Wenzhi Li</a>
 */
public class ITI47QueryCase2DataFoundSingleDomain extends AbstractPixPdqV3Test {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testITI47QueryCase2DataFoundSingleDomain() throws Exception {

        // Step 1: The NIST PDQ Consumer sends a query message (Patient Registry
        //Find Candidates Query : PRPA_IN201305UV02) to ask for the demographics
        //of all patient in domain 2.16.840.1.113883.3.72.5.9.1 (NIST2010) with
        //a patient ID HJ-361. Your PDQ Supplier shall answer correctly to the
        //query with the demographics of patient JOYCE HINOJOXS in domain
        //2.16.840.1.113883.3.72.5.9.1 (NIST2010).
        OMElement response = sendPdqQuery("iti_47_query_case2_data_found_single_domain_1.xml");
        assertNotNull(response);
        assertEquals("AA", getAcknowledgmentTypeCode(response));
        //Make sure the pdq query find the patient whose id is SM-942 in domain NIST2010
        assertEquals("2.16.840.1.113883.3.72.5.9.1", getPatientIdRoot(response));
        assertEquals("HJ-361", getPatientIdExtension(response));
        assertEquals("HINOJOXS", getPatientLastName(response));
        assertEquals("JOYCE", getPatientFirstName(response));

        //Step 2: The NIST PDQ Consumer sends a query message (PRPA_IN201305UV02)
        //to ask for the demographics of all patient in domain
        //2.16.840.1.113883.3.72.5.9.1 (NIST2010) with a patient name DAVID GREGORYX.
        //Your PDQ Supplier shall answer correctly to the query with the demographics
        //of patient DAVID GREGORYX in domain 2.16.840.1.113883.3.72.5.9.1 (NIST2010).
        response = sendPdqQuery("iti_47_query_case2_data_found_single_domain_2.xml");
        assertNotNull(response);
        assertEquals("AA", getAcknowledgmentTypeCode(response));
        assertEquals("2.16.840.1.113883.3.72.5.9.1", getPatientIdRoot(response));
        assertEquals("GD-951", getPatientIdExtension(response));
        assertEquals("GREGORYX", getPatientLastName(response));
        assertEquals("DAVID", getPatientFirstName(response));

        //Step 3: The NIST PDQ Consumer sends a query message (PRPA_IN201305UV02) to
        //ask for the demographics of all patient in domain 2.16.840.1.113883.3.72.5.9.1
        //(NIST2010) with a patient name GREGORYX and DOB 19291015. Your PDQ Supplier
        //shall answer correctly to the query with the demographics of patient DANNY
        //GREGORYX in domain 2.16.840.1.113883.3.72.5.9.1 (NIST2010).
        response = sendPdqQuery("iti_47_query_case2_data_found_single_domain_3.xml");
        assertNotNull(response);
        assertEquals("AA", getAcknowledgmentTypeCode(response));
        assertEquals("2.16.840.1.113883.3.72.5.9.1", getPatientIdRoot(response));
        assertEquals("GD-698", getPatientIdExtension(response));
        assertEquals("GREGORYX", getPatientLastName(response));
        assertEquals("DANNY", getPatientFirstName(response));
        assertEquals("19291015", getPatientBirthTime(response));

        //Step 4: The NIST PDQ Consumer sends a query message (PRPA_IN201305UV02) to
        //ask for the demographics of all patient in domain 2.16.840.1.113883.3.72.5.9.1
        //(NIST2010) with a patient name GREGORYX and feminine sex. Your PDQ Supplier
        //shall answer correctly to the query with the demographics of patient KELLY
        //GREGORYX in domain 2.16.840.1.113883.3.72.5.9.1 (NIST2010).
        response = sendPdqQuery("iti_47_query_case2_data_found_single_domain_4.xml");
        assertNotNull(response);
        assertEquals("AA", getAcknowledgmentTypeCode(response));
        assertEquals("2.16.840.1.113883.3.72.5.9.1", getPatientIdRoot(response));
        assertEquals("GREGORYX", getPatientLastName(response));
        assertEquals("KELLY", getPatientFirstName(response));
        assertEquals("F", getPatientSex(response));

        //Step 5: The NIST PDQ Consumer sends a query message (PRPA_IN201305UV02) to
        //ask for the demographics of all patient in domain 2.16.840.1.113883.3.72.5.9.1
        //(NIST2010) with a patient living at 1905 Romrog Way, ROCK SPRINGS, WY, 82901
        //Your PDQ Supplier shall answer correctly to the query with the demographics
        //of patient JANE BRACYX in domain 2.16.840.1.113883.3.72.5.9.1 (NIST2010).
        response = sendPdqQuery("iti_47_query_case2_data_found_single_domain_5.xml");
        assertNotNull(response);
        assertEquals("AA", getAcknowledgmentTypeCode(response));
        assertEquals("2.16.840.1.113883.3.72.5.9.1", getPatientIdRoot(response));
        assertEquals("BRACYX", getPatientLastName(response));
        assertEquals("JANE", getPatientFirstName(response));
        assertEquals("ROCK SPRINGS", getPatientCity(response));
        assertEquals("1905 Romrog Way", getPatientStreet(response));

    }

}
