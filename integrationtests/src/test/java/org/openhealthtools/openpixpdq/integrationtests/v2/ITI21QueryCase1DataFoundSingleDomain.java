/**
 *  Copyright (c) 2009-2011 Misys Open Source Solutions (MOSS) and others
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *  implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 *  Contributors:
 *    Misys Open Source Solutions - initial API and implementation
 *    -
 */

package org.openhealthtools.openpixpdq.integrationtests.v2;

import ca.uhn.hl7v2.app.Initiator;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.v25.message.RSP_K21;
import ca.uhn.hl7v2.model.v25.segment.MSA;
import ca.uhn.hl7v2.model.v25.segment.PID;
import ca.uhn.hl7v2.model.v25.segment.QAK;
import ca.uhn.hl7v2.parser.PipeParser;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * This is the NIST ITI-21-Query-Case1-Data-Found-Single-Domain test.
 * Be sure to run ITI21PDQSupplierLoadPatientData to load the test patients before
 * running this test case.
 * <p/>
 * Pre-condition:
 * <ul>
 * <li>1. The PDQ Supplier is configured to send and receive demographics in a
 * single domain: NIST2010&2.16.840.1.113883.3.72.5.9.1&ISO</li>
 * <li>2. The following patients have been loaded into the PDQ Supplier (via
 * Load PDQ Supplier Data) in the domain given in pre-condition (1) above:
 * <p/>
 * JERMAINO NXEAL with ID JN-018
 * BARBARA LXEIGHTON with ID BL-180
 * SHAQUILLO NXEAL with ID SN-032
 * RUTH NXEAL with ID RN-480
 * DAVID VASXQUEZ with ID DV-301
 * KATHRINE MARXCHANT with ID KM-375</li>
 * </ul>
 * <p/>
 * Description:
 * Test case ITI-21-Query-Case1-Data-Found-Single-Domain addresses PDQ Query
 * Case 1. You need to have loaded the set of patients indicated in the
 * pre-conditions into your system before running this test. The purpose
 * of the test is to confirm that your PDQ Supplier supports the multiple
 * query parameters.
 * <p/>
 * The following queries are tested:
 * <p/>
 * 1. Query for the patient ID
 * 2. Query for the patient name
 * 3. Query for the patient name and the date of birth
 * 4. Query for the patient name and the administrative sex
 * 5. Query for the address (street, city, state and zip code)
 * 6. Query for the patient account number
 *
 * @author <a href="mailto:wenzhi.li@misys.com">Wenzhi Li</a>
 */
public class ITI21QueryCase1DataFoundSingleDomain extends AbstractPixPdqTestCase {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testITI21QueryCase1DataFoundSingleDomain() throws Exception {

        // Step 1: The NIST PDQ Consumer sends a query message (QBP^Q22) to
        //ask for the demographics of all patient in domain
        //NIST2010&2.16.840.1.113883.3.72.5.9.1&ISO with a patient ID BL-180.
        //Your PDQ Supplier shall answer correctly to the query with the
        //demographics of patient BARBARA LXEIGHTON in domain
        //NIST2010&2.16.840.1.113883.3.72.5.9.1&ISO.

        // PDQ Request Message:
        String msg = "MSH|^~\\&|NIST_Hydra_PDQ_Consumer^^|NIST^^|OpenPIXPDQ^^|MOSS^^|20110109105019||QBP^Q22^QBP_Q21|NIST-20110109105019|T|2.5\r" +
                "QPD|IHE PDQ Query|QRY12481848949|@PID.3.1^BL-180\r" +
                "RCP|I";
        PipeParser pipeParser = new PipeParser();
        Message pdq = pipeParser.parse(msg);

        Initiator initiator = pdqConnection.getInitiator();
        Message response = initiator.sendAndReceive(pdq);
        String responseString = pipeParser.encode(response);
        //System.out.println("Received response:\n" + responseString);
        MSA msa = (MSA) response.get("MSA");
        assertEquals("AA", msa.getAcknowledgmentCode().getValue());
        QAK qak = (QAK) response.get("QAK");
        assertEquals("OK", qak.getQueryResponseStatus().getValue());
        PID pid = ((RSP_K21) response).getQUERY_RESPONSE().getPID();
        assertEquals("LXEIGHTON", pid.getPatientName(0).getFamilyName().getSurname().getValue());
        assertEquals("BARBARA", pid.getPatientName(0).getGivenName().getValue());
        assertEquals("L", pid.getPatientName(0).getNameTypeCode().getValue());
        assertEquals("BL-180", pid.getPatientIdentifierList(0).getIDNumber().getValue());
        assertEquals("NIST2010", pid.getPatientIdentifierList(0).getAssigningAuthority().getNamespaceID().getValue());
        assertEquals("2.16.840.1.113883.3.72.5.9.1", pid.getPatientIdentifierList(0).getAssigningAuthority().getUniversalID().getValue());
        assertEquals("BRYANTX", pid.getMotherSMaidenName(0).getFamilyName().getSurname().getValue());
        //Not working at this moment
        //assertEquals("L",       pid.getMotherSMaidenName(0).getNameTypeCode().getValue());
        assertEquals("19460717", pid.getDateTimeOfBirth().getTime().getValue());
        assertEquals("F", pid.getAdministrativeSex().getValue());
        assertEquals("2180484", pid.getPatientAccountNumber().getIDNumber().getValue());

        //Step 2: The NIST PDQ Consumer sends a query message (QBP^Q22) to ask for
        //the demographics of all patient in domain
        //NIST2010&2.16.840.1.113883.3.72.5.9.1&ISO with a patient name JERMAINO
        //NXEAL. Your PDQ Supplier shall answer correctly to the query with the
        //demographics of patient JERMAINO NXEAL in domain
        //NIST2010&2.16.840.1.113883.3.72.5.9.1&ISO.
        msg = "MSH|^~\\&|NIST_Hydra_PDQ_Consumer^^|NIST^^|OpenPIXPDQ^^|MOSS^^|20110109105609||QBP^Q22^QBP_Q21|NIST-20110109105609|D|2.5\r" +
                "QPD|IHE PDQ Query|QRY11849105448994|@PID.5.1.1^NXEAL~@PID.5.2^JERMAINO\r" +
                "RCP|I";
        pdq = pipeParser.parse(msg);

        initiator = pdqConnection.getInitiator();
        response = initiator.sendAndReceive(pdq);
        responseString = pipeParser.encode(response);
        //System.out.println("Received response:\n" + responseString);
        msa = (MSA) response.get("MSA");
        assertEquals("AA", msa.getAcknowledgmentCode().getValue());
        qak = (QAK) response.get("QAK");
        assertEquals("OK", qak.getQueryResponseStatus().getValue());
        pid = ((RSP_K21) response).getQUERY_RESPONSE().getPID();
        assertEquals("NXEAL", pid.getPatientName(0).getFamilyName().getSurname().getValue());
        assertEquals("JERMAINO", pid.getPatientName(0).getGivenName().getValue());
        assertEquals("L", pid.getPatientName(0).getNameTypeCode().getValue());
        assertEquals("JN-018", pid.getPatientIdentifierList(0).getIDNumber().getValue());
        assertEquals("NIST2010", pid.getPatientIdentifierList(0).getAssigningAuthority().getNamespaceID().getValue());
        assertEquals("2.16.840.1.113883.3.72.5.9.1", pid.getPatientIdentifierList(0).getAssigningAuthority().getUniversalID().getValue());
        assertEquals("HAWKINSX", pid.getMotherSMaidenName(0).getFamilyName().getSurname().getValue());
        //Not working at this moment
        //assertEquals("L",       pid.getMotherSMaidenName(0).getNameTypeCode().getValue());
        assertEquals("19781013", pid.getDateTimeOfBirth().getTime().getValue());
        assertEquals("M", pid.getAdministrativeSex().getValue());
        assertEquals("2590014", pid.getPatientAccountNumber().getIDNumber().getValue());

        //Step 3: The NIST PDQ Consumer sends a query message (QBP^Q22) to ask for
        //the demographics of all patient in domain
        //NIST2010&2.16.840.1.113883.3.72.5.9.1&ISO with a patient name NXEAL and
        //DOB 19720306. Your PDQ Supplier shall answer correctly to the query with
        //the demographics of patient SHAQUILLO NXEAL in domain
        //NIST2010&2.16.840.1.113883.3.72.5.9.1&ISO.
        msg = "MSH|^~\\&|NIST_Hydra_PDQ_Consumer^^|NIST^^|OpenPIXPDQ^^|MOSS^^|20110109105819||QBP^Q22^QBP_Q21|NIST-20110109105819|T|2.5\r" +
                "QPD|IHE PDQ Query|QRY11011048808|@PID.5.1.1^NXEAL~@PID.7.1^19720306\r" +
                "RCP|I";
        pdq = pipeParser.parse(msg);

        initiator = pdqConnection.getInitiator();
        response = initiator.sendAndReceive(pdq);
        responseString = pipeParser.encode(response);
        //System.out.println("Received response:\n" + responseString);
        msa = (MSA) response.get("MSA");
        assertEquals("AA", msa.getAcknowledgmentCode().getValue());
        qak = (QAK) response.get("QAK");
        assertEquals("OK", qak.getQueryResponseStatus().getValue());
        pid = ((RSP_K21) response).getQUERY_RESPONSE().getPID();
        assertEquals("NXEAL", pid.getPatientName(0).getFamilyName().getSurname().getValue());
        assertEquals("SHAQUILLO", pid.getPatientName(0).getGivenName().getValue());
        assertEquals("L", pid.getPatientName(0).getNameTypeCode().getValue());
        assertEquals("SN-032", pid.getPatientIdentifierList(0).getIDNumber().getValue());
        assertEquals("NIST2010", pid.getPatientIdentifierList(0).getAssigningAuthority().getNamespaceID().getValue());
        assertEquals("2.16.840.1.113883.3.72.5.9.1", pid.getPatientIdentifierList(0).getAssigningAuthority().getUniversalID().getValue());
        assertEquals("HAWKINSX", pid.getMotherSMaidenName(0).getFamilyName().getSurname().getValue());
        //Not working at this moment
        //assertEquals("L",       pid.getMotherSMaidenName(0).getNameTypeCode().getValue());
        assertEquals("19720306", pid.getDateTimeOfBirth().getTime().getValue());
        assertEquals("M", pid.getAdministrativeSex().getValue());
        assertEquals("2184999", pid.getPatientAccountNumber().getIDNumber().getValue());

        //Step 4: The NIST PDQ Consumer sends a query message (QBP^Q22) to ask for
        //the demographics of all patient in domain
        //NIST2010&2.16.840.1.113883.3.72.5.9.1&ISO with a patient name NXEAL and
        //feminine sex. Your PDQ Supplier shall answer correctly to the query with
        //the demographics of patient RUTH NXEAL in domain
        //NIST2010&2.16.840.1.113883.3.72.5.9.1&ISO.
        msg = "MSH|^~\\&|NIST_Hydra_PDQ_Consumer^^|NIST^^|OpenPIXPDQ^^|MOSS^^|20110109110033||QBP^Q22^QBP_Q21|NIST-20110109110033|T|2.5\r" +
                "QPD|IHE PDQ Query|QRY12400480880077|@PID.5.1.1^NXEAL~@PID.8^F|||||^^^NIST2010\r" +
                "RCP|I";
        pdq = pipeParser.parse(msg);

        initiator = pdqConnection.getInitiator();
        response = initiator.sendAndReceive(pdq);
        responseString = pipeParser.encode(response);
        //System.out.println("Received response:\n" + responseString);
        msa = (MSA) response.get("MSA");
        assertEquals("AA", msa.getAcknowledgmentCode().getValue());
        qak = (QAK) response.get("QAK");
        assertEquals("OK", qak.getQueryResponseStatus().getValue());
        pid = ((RSP_K21) response).getQUERY_RESPONSE().getPID();
        assertEquals("NXEAL", pid.getPatientName(0).getFamilyName().getSurname().getValue());
        assertEquals("RUTH", pid.getPatientName(0).getGivenName().getValue());
        assertEquals("L", pid.getPatientName(0).getNameTypeCode().getValue());
        assertEquals("RN-480", pid.getPatientIdentifierList(0).getIDNumber().getValue());
        assertEquals("NIST2010", pid.getPatientIdentifierList(0).getAssigningAuthority().getNamespaceID().getValue());
        assertEquals("2.16.840.1.113883.3.72.5.9.1", pid.getPatientIdentifierList(0).getAssigningAuthority().getUniversalID().getValue());
        assertEquals("HAWKINSX", pid.getMotherSMaidenName(0).getFamilyName().getSurname().getValue());
        //Not working at this moment
        //assertEquals("L",       pid.getMotherSMaidenName(0).getNameTypeCode().getValue());
        assertEquals("19361120", pid.getDateTimeOfBirth().getTime().getValue());
        assertEquals("F", pid.getAdministrativeSex().getValue());
        assertEquals("1990844", pid.getPatientAccountNumber().getIDNumber().getValue());

        //Step 5: The NIST PDQ Consumer sends a query message (QBP^Q22) to ask for
        //the demographics of all patient in domain
        //NIST2010&2.16.840.1.113883.3.72.5.9.1&ISO with a patient living at 4670
        //Raver Croft Drive HAMPTON, TN 37658. Your PDQ Supplier shall answer
        //correctly to the query with the demographics of patient KATHRINE
        //MARXCHANT in domain NIST2010&2.16.840.1.113883.3.72.5.9.1&ISO.
        msg = "MSH|^~\\&|NIST_Hydra_PDQ_Consumer^^|NIST^^|OpenPIXPDQ^^|MOSS^^|20110109110304||QBP^Q22^QBP_Q21|NIST-20110109110304|P|2.5\r" +
                "QPD|IHE PDQ Query|QRY12489878110556|@PID.11.1.1^4670 Raver Croft Drive~@PID.11.3^HAMPTON~@PID.11.4^TN~@PID.11.5^37658\r" +
                "RCP|I";
        pdq = pipeParser.parse(msg);

        initiator = pdqConnection.getInitiator();
        response = initiator.sendAndReceive(pdq);
        responseString = pipeParser.encode(response);
        //System.out.println("Received response:\n" + responseString);
        msa = (MSA) response.get("MSA");
        assertEquals("AA", msa.getAcknowledgmentCode().getValue());
        qak = (QAK) response.get("QAK");
        assertEquals("OK", qak.getQueryResponseStatus().getValue());
        pid = ((RSP_K21) response).getQUERY_RESPONSE().getPID();
        assertEquals("MARXCHANT", pid.getPatientName(0).getFamilyName().getSurname().getValue());
        assertEquals("KATHRINE", pid.getPatientName(0).getGivenName().getValue());
        assertEquals("L", pid.getPatientName(0).getNameTypeCode().getValue());
        assertEquals("KM-375", pid.getPatientIdentifierList(0).getIDNumber().getValue());
        assertEquals("NIST2010", pid.getPatientIdentifierList(0).getAssigningAuthority().getNamespaceID().getValue());
        assertEquals("2.16.840.1.113883.3.72.5.9.1", pid.getPatientIdentifierList(0).getAssigningAuthority().getUniversalID().getValue());
        assertEquals("MCDOWELLX", pid.getMotherSMaidenName(0).getFamilyName().getSurname().getValue());
        //Not working at this moment
        //assertEquals("L",       pid.getMotherSMaidenName(0).getNameTypeCode().getValue());
        assertEquals("19421105", pid.getDateTimeOfBirth().getTime().getValue());
        assertEquals("F", pid.getAdministrativeSex().getValue());
        assertEquals("1840084", pid.getPatientAccountNumber().getIDNumber().getValue());

        //Step 6: The NIST PDQ Consumer sends a query message (QBP^Q22) to ask for
        //the demographics of all patient in domain
        //NIST2010&2.16.840.1.113883.3.72.5.9.1&ISO with a patient account number
        //ID 0007240. Your PDQ Supplier shall answer correctly to the query with
        //the demographics of patient DAVID VASXQUEZ in domain
        //NIST2010&2.16.840.1.113883.3.72.5.9.1&ISO.
        msg = "MSH|^~\\&|NIST_Hydra_PDQ_Consumer^^|NIST^^|OpenPIXPDQ^^|MOSS^^|20110109110451||QBP^Q22^QBP_Q21|NIST-20110109110451|P|2.5\r" +
                "QPD|IHE PDQ Query|QRY581897987987897|@PID.18.1^1007240\r" +
                "RCP|I";
        pdq = pipeParser.parse(msg);

        initiator = pdqConnection.getInitiator();
        response = initiator.sendAndReceive(pdq);
        responseString = pipeParser.encode(response);
        //System.out.println("Received response:\n" + responseString);
        msa = (MSA) response.get("MSA");
        assertEquals("AA", msa.getAcknowledgmentCode().getValue());
        qak = (QAK) response.get("QAK");
        assertEquals("OK", qak.getQueryResponseStatus().getValue());
        pid = ((RSP_K21) response).getQUERY_RESPONSE().getPID();
        assertEquals("VASXQUEZ", pid.getPatientName(0).getFamilyName().getSurname().getValue());
        assertEquals("DAVID", pid.getPatientName(0).getGivenName().getValue());
        assertEquals("L", pid.getPatientName(0).getNameTypeCode().getValue());
        assertEquals("DV-301", pid.getPatientIdentifierList(0).getIDNumber().getValue());
        assertEquals("NIST2010", pid.getPatientIdentifierList(0).getAssigningAuthority().getNamespaceID().getValue());
        assertEquals("2.16.840.1.113883.3.72.5.9.1", pid.getPatientIdentifierList(0).getAssigningAuthority().getUniversalID().getValue());
        assertEquals("WILSONX", pid.getMotherSMaidenName(0).getFamilyName().getSurname().getValue());
        //Not working at this moment
        //assertEquals("L",       pid.getMotherSMaidenName(0).getNameTypeCode().getValue());
        assertEquals("19670305", pid.getDateTimeOfBirth().getTime().getValue());
        assertEquals("M", pid.getAdministrativeSex().getValue());
        assertEquals("1007240", pid.getPatientAccountNumber().getIDNumber().getValue());

    }

}

