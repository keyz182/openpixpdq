/**
 *  Copyright (c) 2009-2010 Misys Open Source Solutions (MOSS) and others
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *  implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 *  Contributors:
 *    Misys Open Source Solutions - initial API and implementation
 *    -
 */
package org.openhealthtools.openpixpdq.integrationtests.v3;

import org.apache.axiom.om.OMElement;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * This is the NIST ITI-45-Query-Case6 test.
 * <p/>
 * Pre-condition: The PIX Manager is configured to handle these domains:
 * <ul>
 * <li>2.16.840.1.113883.3.72.5.9.1 (NIST2010)</li>
 * <li>2.16.840.1.113883.3.72.5.9.2 (NIST2010-2)</li>
 * </ul>
 * <p/>
 * Description:
 * Test case ITI-45-Query-Case6 covers the PIXv3 Query Case 6. One patient
 * (FERNANDEZ) is registered twice with different ids in the same domain NIST2010.
 * These two patients are not linked because they are in the same domain. This
 * patient is also registered in another domain NIST2010-2. Three registration
 * messages are sent to a Cross Reference Manager. A PIX Query is sent to find
 * corresponding ids in domain NIST2010 for the patient ALBERT FERNANDEZ with ID
 * FA-957-003 in domain NIST2010-2. Patient FERNANDEZ should be found in domain
 * NIST2010. Your PIX Manager shall answer correctly to the query returning one of
 * identifiers or both in domain NIST2010.
 *
 * @author <a href="mailto:wenzhi.li@misys.com">Wenzhi Li</a>
 */
public class ITI45QueryCase6 extends AbstractPixPdqV3Test {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testITI45QueryCase6() throws Exception {
        //Step 1: The NIST PIX Source sends a registration message (PRPA_IN201301UV02)
        //to register patient ALBERT FERNANDEZ in domain 2.16.840.1.113883.3.72.5.9.1
        //(NIST2010). Patient ID is FA-957-001. Your PIX Manager shall register the
        //patient and send a correct acknowledgement (MCCI_IN000002UV01) message back.
        OMElement response = sendPixCreate("iti_45_query_case6_1.xml");
        assertNotNull(response);
        assertEquals("CA", getAcknowledgmentTypeCode(response));

        //Step 2: The NIST PIX Source sends a registration message (PRPA_IN201301UV02)
        //to register patient ALBERT FERNANDEZ in domain 2.16.840.1.113883.3.72.5.9.1
        //(NIST2010). Patient ID is FA-957-002. Your PIX Manager shall register the
        //patient and send a correct acknowledgement (MCCI_IN000002UV01) message back.
        response = sendPixCreate("iti_45_query_case6_2.xml");
        assertNotNull(response);
        assertEquals("CA", getAcknowledgmentTypeCode(response));

        //Step 3: The NIST PIX Source sends a registration message (PRPA_IN201301UV02)
        //to register patient ALBERT FERNANDEZ in domain 2.16.840.1.113883.3.72.5.9.2
        //(NIST2010-2). Patient ID is FA-957-003. Your PIX Manager shall register the
        //patient and send a correct acknowledgement (MCCI_IN000002UV01) message back.
        response = sendPixCreate("iti_45_query_case6_3.xml");
        assertNotNull(response);
        assertEquals("CA", getAcknowledgmentTypeCode(response));

        //Step 4: The NIST PIX Consumer sends a query message (PRPA_IN201309UV02) to
        //ask for ALBERT FERNANDEZ''s ID in domain 2.16.840.1.113883.3.72.5.9.1
        //(NIST2010) using his id FA-957-003 in domain 2.16.840.1.113883.3.72.5.9.2
        //(NIST2010-2). Your PIX Manager shall answer correctly to the query with
        //ALBERT FERNANDEZ''s IDs FA-957-001 or FA-957-002 or both in domain
        //2.16.840.1.113883.3.72.5.9.1 (NIST2010).
        response = sendPixQuery("iti_45_query_case6_4.xml");
        assertNotNull(response);
        assertEquals("AA", getAcknowledgmentTypeCode(response));
        assertEquals("OK", getQueryAckCode(response));
        evalPatientId(response, "2.16.840.1.113883.3.72.5.9.1", "FA-957-001");
        evalPatientId(response, "2.16.840.1.113883.3.72.5.9.1", "FA-957-002");
    }

}
