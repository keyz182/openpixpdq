/**
 *  Copyright (c) 2009-2010 Misys Open Source Solutions (MOSS) and others
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *  implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 *  Contributors:
 *    Misys Open Source Solutions - initial API and implementation
 *    -
 */
package org.openhealthtools.openpixpdq.integrationtests.v3;

import org.apache.axiom.om.OMElement;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * This is the NIST ITI-45-Query-Case1-Data-Found test.
 * <p/>
 * Pre-condition: The PIX Manager is configured to handle these domains:
 * <ul>
 * <li>2.16.840.1.113883.3.72.5.9.1 (NIST2010)</li>
 * <li>2.16.840.1.113883.3.72.5.9.2 (NIST2010-2)</li>
 * <li>2.16.840.1.113883.3.72.5.9.3 (NIST2010-3)</li>
 * </ul>
 * <p/>
 * Description: Test case ITI-45-Query-Case1-Data-Found covers the PIXv3 Query
 * Case 1. One patient (REGALADO) is registered in three different domains.
 * Three registration messages are sent toyour PIX Manager. A PIX Query is sent
 * to resolve a reference to REGALADO in a specific domain (NIST2010-2). Patient
 * REGALADO should be found. Another PIX Query is sent to resolve a reference to
 * REGALADO in two different domains (NIST2010-2 and NIST2010-3). Patient
 * REGALADO should be found in those two domains. Another PIX Query is sent to
 * resolve a reference to REGALADO in all domains. Patient REGALADO should be
 * found in two domains (NIST2010-2 and NIST2010-3).
 *
 * @author <a href="mailto:wenzhi.li@misys.com">Wenzhi Li</a>
 */
public class ITI45QueryCase1DataFound extends AbstractPixPdqV3Test {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testITI45QueryCase1DataFound() throws Exception {

        // Step 1: The NIST PIX Source sends a registration message
        // (PRPA_IN201301UV02) to register patient STEPHEN REGALADO in domain
        // 2.16.840.1.113883.3.72.5.9.1 (NIST2010). Patient ID is RS-491. Your
        // PIX Manager shall register the patient and send a correct
        // acknowledgement message back.
        OMElement response = sendPixCreate("iti_45_query_case1_data_found_1.xml");
        assertNotNull(response);
        assertEquals("CA", getAcknowledgmentTypeCode(response));

        // Step 2: The NIST PIX Source sends a registration message
        // (PRPA_IN201301UV02) to register patient STEPHEN REGALADO in domain
        // 2.16.840.1.113883.3.72.5.9.2 (NIST2010-2). Patient ID is
        // SREGALADO-18542. Your PIX Manager shall register the patient and send
        // a correct acknowledgement message back.
        response = sendPixCreate("iti_45_query_case1_data_found_2.xml");
        assertNotNull(response);
        assertEquals("CA", getAcknowledgmentTypeCode(response));

        // Step 3: The NIST PIX Source sends a registration message
        // (PRPA_IN201301UV02) to register patient STEPHEN REGALADO in domain
        // 2.16.840.1.113883.3.72.5.9.3 (NIST2010-3). Patient ID is
        // RS-48410-5918. Your PIX Manager shall register the patient and send a
        // correct acknowledgement message back.
        response = sendPixCreate("iti_45_query_case1_data_found_3.xml");
        assertNotNull(response);
        assertEquals("CA", getAcknowledgmentTypeCode(response));

        // Step 4: The NIST PIX Consumer sends a query message
        // (PRPA_IN201309UV02) to ask for STEPHEN REGALADO 's ID in domain
        // 2.16.840.1.113883.3.72.5.9.1 (NIST2010-2) using his id RS-491 in
        // domain2.16.840.1.113883.3.72.5.9.1 (NIST2010). Your PIX Manager shall
        // answer correctly to the query with STEPHEN REGALADO 's ID
        // SREGALADO-18542 in domain 2.16.840.1.113883.3.72.5.9.1 (NIST2010-2).
        response = sendPixQuery("iti_45_query_case1_data_found_4.xml");
        assertNotNull(response);
        assertEquals("AA", getAcknowledgmentTypeCode(response));
        //Make sure the pix query find the id SREGALADO-18542 in domain NIST2010-2
        evalPatientId(response, "2.16.840.1.113883.3.72.5.9.2", "SREGALADO-18542");
        assertEquals("2.16.840.1.113883.3.72.5.9.2", getPatientIdRoot(response));
        assertEquals("SREGALADO-18542", getPatientIdExtension(response));

        // Step 5: The NIST PIX Consumer sends a query message
        // (PRPA_IN201309UV02) to ask for STEPHEN REGALADO 's ID in domain
        // 2.16.840.1.113883.3.72.5.9.2 (NIST2010-2) and
        // 2.16.840.1.113883.3.72.5.9.3 (NIST2010-3) using his id RS-491 in
        // domain 2.16.840.1.113883.3.72.5.9.1 (NIST2010). Your PIX Manager
        // shall answer correctly to the query with STEPHEN REGALADO 's ID
        // SREGALADO-18542 and RS-48410-5918 in domain
        // 2.16.840.1.113883.3.72.5.9.2 (NIST2010-2) and
        // 2.16.840.1.113883.3.72.5.9.3 (NIST2010-3).
        response = sendPixQuery("iti_45_query_case1_data_found_5.xml");
        assertNotNull(response);
        assertEquals("AA", getAcknowledgmentTypeCode(response));
        //Make sure the pix query find the id in domain NIST2010-2
        evalPatientId(response, "2.16.840.1.113883.3.72.5.9.2", "SREGALADO-18542");
        evalPatientId(response, "2.16.840.1.113883.3.72.5.9.3", "RS-48410-5918");

    }

}
