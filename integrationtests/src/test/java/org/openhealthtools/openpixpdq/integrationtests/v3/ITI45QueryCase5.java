/**
 *  Copyright (c) 2009-2010 Misys Open Source Solutions (MOSS) and others
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *  implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 *  Contributors:
 *    Misys Open Source Solutions - initial API and implementation
 *    -
 */
package org.openhealthtools.openpixpdq.integrationtests.v3;

import org.apache.axiom.om.OMElement;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * This is the NIST ITI-45-Query-Case5 test.
 * <p/>
 * Pre-condition: The PIX Manager is configured to handle these domains:
 * <ul>
 * <li>2.16.840.1.113883.3.72.5.9.1 (NIST2010)</li>
 * <li>2.16.840.1.113883.3.72.5.9.2 (NIST2010-2)</li>
 * </ul>
 * <p/>
 * Description:
 * Test case ITI-45-Query-Case5 covers the PIXv3 Query Case 5. One patient
 * (JOEX) is registered in two different domains. Two registration messages
 * are sent to a Cross Reference Manager. A PIX Query is sent to find
 * corresponding ids in domain UNKNOWNDOMAIN for the patient SCOTT JOEX with
 * ID JS-879 in domain NIST2010. The PIX Manager does not recognize the
 * requested domain UNKNOWNDOMAIN. The same query is sent but the requested
 * domains are NIST2010-2 and UNKNOWNDOMAIN. In both cases the PIX Manager
 * shoud return a response with the specific error.
 *
 * @author <a href="mailto:wenzhi.li@misys.com">Wenzhi Li</a>
 */
public class ITI45QueryCase5 extends AbstractPixPdqV3Test {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testITI45QueryCase5() throws Exception {
        //Step 1: The NIST PIX Source sends a registration message
        //(PRPA_IN201301UV02) to register patient SCOTT JOEX in
        //domain 2.16.840.1.113883.3.72.5.9.1(NIST2010). Patient ID is
        //JS-879. Your PIX Manager shall register the patient and send
        //a correct ACK message back.
        OMElement response = sendPixCreate("iti_45_query_case5_1.xml");
        assertNotNull(response);
        assertEquals("CA", getAcknowledgmentTypeCode(response));

        //Step 2: The NIST PIX Source sends a registration message
        //(PRPA_IN201301UV02) to register patient SCOTT JOEX in domain
        //2.16.840.1.113883.3.72.5.9.2 (NIST2010-2). Patient ID is
        //SJOEX001884. Your PIX Manager shall register the patient and
        //send a correct ACK message back.
        response = sendPixCreate("iti_45_query_case5_2.xml");
        assertNotNull(response);
        assertEquals("CA", getAcknowledgmentTypeCode(response));

        //Step 3: The NIST PIX Consumer sends a query message
        //(PRPA_IN201309UV02) to ask for SCOTT JOEX's ID in
        //domain 2.16.840.1.113883.3.72.5.9.99 (UNKNOWNDOMAIN) using
        //his id JS-879 in domain 2.16.840.1.113883.3.72.5.9.1
        //(NIST2010). Your PIX Manager shall send a query response
        //back indicating that an error occured.
        response = sendPixQuery("iti_45_query_case5_3.xml");
        assertNotNull(response);
        assertEquals("AE", getAcknowledgmentTypeCode(response));
        assertEquals("AE", getQueryAckCode(response));

        //Step 4: The NIST PIX Consumer sends a query message
        //(PRPA_IN201309UV02) to ask for SCOTT JOEX's ID in domain
        //2.16.840.1.113883.3.72.5.9.2 (NIST2010-2) and
        //2.16.840.1.113883.3.72.5.9.99 (UNKNOWNDOMAIN) using
        //his id JS-879 in domain 2.16.840.1.113883.3.72.5.9.1
        //(NIST2010). Your PIX Manager shall send a query response
        //back indicating that an error occured.
        response = sendPixQuery("iti_45_query_case5_4.xml");
        assertNotNull(response);
        assertEquals("AE", getAcknowledgmentTypeCode(response));
        assertEquals("AE", getQueryAckCode(response));
    }

}
