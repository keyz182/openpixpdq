/**
 *  Copyright (c) 2009-2010 Misys Open Source Solutions (MOSS) and others
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *  implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 *  Contributors:
 *    Misys Open Source Solutions - initial API and implementation
 *    -
 */
package org.openhealthtools.openpixpdq.integrationtests.v3;

import org.apache.axiom.om.OMElement;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * This is the NIST ITI-47-Query-Case2-Data-Found-Exact-Match-Single-Domain test.
 * <p/>
 * Pre-condition:
 * <ul>
 * <li>1. Your PDQ Supplier is configured to send and receive demographics
 * in a single domain: 2.16.840.1.113883.3.72.5.9.1 (NIST2010)</li>
 * <li>2. The following patient has been loaded into the PDQ Supplier
 * (via Load PDQ Supplier Data) in the domain given in pre-condition (1) above:</li>
 * </ul>
 * MARK STAMXM with ID SM-942
 * <p/>
 * Description:
 * Test case ITI-47-Query-Case2-Data-Found-Exact-Match-Single-Domain covers the PDQv3
 * Query Case 2. You need to have loaded the set of patients indicated in the
 * pre-conditions into your system before running this test. The purpose of the test
 * is to confirm that your PDQ Supplier supports the multiple query parameters.
 * <p/>
 * The following query is tested:
 * 1. Query on the patient first and last name
 *
 * @author <a href="mailto:wenzhi.li@misys.com">Wenzhi Li</a>
 */
public class ITI47QueryCase2DataFoundExactMatchSingleDomain extends AbstractPixPdqV3Test {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testITI47QueryCase1DataFoundExactMatchSingleDomain() throws Exception {

        //Step 1: The NIST PDQ Consumer sends a query message (Patient Registry Find
        //Candidates Query : PRPA_IN201305UV02) to ask for the demographics of all
        //patients in domain 2.16.840.1.113883.3.72.5.9.1 (NIST2010) with a patient
        //name MARK STAMXM . OtherIDs.scopingOrganization.id is populated to request
        //patient ids in a specific domain. Your PDQ Supplier shall answer correctly
        //to the query with the demographics of patient MARK STAMXM in domain
        //2.16.840.1.113883.3.72.5.9.1 (NIST2010).

        OMElement response = sendPdqQuery("iti_47_query_case2_data_found_exact_match_single_domain.xml");
        assertNotNull(response);
        assertEquals("AA", getAcknowledgmentTypeCode(response));
        //Make sure the pdq query find the patient whose id is SM-942 in domain NIST2010
        assertEquals("2.16.840.1.113883.3.72.5.9.1", getPatientIdRoot(response));
        assertEquals("SM-942", getPatientIdExtension(response));
        assertEquals("STAMXM", getPatientLastName(response));
        assertEquals("MARK", getPatientFirstName(response));
    }

}
