/**
 *  Copyright (c) 2009-2011 Misys Open Source Solutions (MOSS) and others
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *  implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 *  Contributors:
 *    Misys Open Source Solutions - initial API and implementation
 *    -
 */

package org.openhealthtools.openpixpdq.integrationtests.v2;

import ca.uhn.hl7v2.app.Initiator;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.v25.segment.ERR;
import ca.uhn.hl7v2.model.v25.segment.MSA;
import ca.uhn.hl7v2.model.v25.segment.QAK;
import ca.uhn.hl7v2.parser.PipeParser;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * This is the NIST ITI-21-Query-Case3 test.
 * Be sure to run ITI21PDQSupplierLoadPatientData to load the test patients before
 * running this test case.
 * <p/>
 * Pre-condition:
 * The PDQ Supplier is configured to send and receive demographics in a single
 * domain: NIST2010&2.16.840.1.113883.3.72.5.9.1&ISO
 * <p/>
 * Description:
 * Test case ITI-21-Query-Case3 covers the PDQ Query Case 3. The NIST PDQ Consumer
 * will query your PDQ Supplier using an unknown domain in field QPD-8 (What
 * domains returned).
 *
 * @author <a href="mailto:wenzhi.li@misys.com">Wenzhi Li</a>
 */
public class ITI21QueryCase3 extends AbstractPixPdqTestCase {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testITI21QueryCase1DataFoundExactMatchSingleDomain() throws Exception {

        // Step 1 : The NIST PDQ Consumer sends a query message (QBP^Q22) to ask for
        //the demographics of all patients with a patient name NXEAL using
        //UNKNOWNDOMAIN&2.16.840.1.113883.3.72.5.9.99&ISO in QPD-8. Your PDQ
        //Supplier shall send an ERR segment for the domain that was not recognized.

        // PDQ Request Message:
        String msg = "MSH|^~\\&|NIST_Hydra_PDQ_Consumer^^|NIST^^|OpenPIXPDQ^^|MOSS^^|20110109113525||QBP^Q22^QBP_Q21|NIST-20110109113525|D|2.5\r" +
                "QPD|IHE PIX Query|QRY121548499947|@PID.5.1.1^NXEAL|||||^^^UNKNOWNDOMAIN\r" +
                "RCP|I";

        PipeParser pipeParser = new PipeParser();
        Message pdq = pipeParser.parse(msg);

        Initiator initiator = pdqConnection.getInitiator();
        Message response = initiator.sendAndReceive(pdq);
        String responseString = pipeParser.encode(response);
        //System.out.println("Received response:\n" + responseString);
        MSA msa = (MSA) response.get("MSA");
        assertEquals("AE", msa.getAcknowledgmentCode().getValue());
        QAK qak = (QAK) response.get("QAK");
        assertEquals("AE", qak.getQueryResponseStatus().getValue());
        ERR err = (ERR) response.get("ERR");
        assertEquals("QPD", err.getErrorLocation(0).getSegmentID().getValue());
        assertEquals("8", err.getErrorLocation(0).getFieldPosition().getValue());
        assertEquals("204", err.getHL7ErrorCode().getIdentifier().getValue());
    }

}

