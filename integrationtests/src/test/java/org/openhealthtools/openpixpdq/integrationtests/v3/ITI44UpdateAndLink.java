/**
 *  Copyright (c) 2009-2010 Misys Open Source Solutions (MOSS) and others
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *  implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 *  Contributors:
 *    Misys Open Source Solutions - initial API and implementation
 *    -
 */
package org.openhealthtools.openpixpdq.integrationtests.v3;


import org.apache.axiom.om.OMElement;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * This is the NIST ITI-44-Update-And-Link test.
 * <p/>
 * Pre-condition:
 * <ul>
 * <li>1. Ensure that your database is cleared of the patient names and
 * identifiers in the specified domains used in this test case.
 * </li>
 * <li>2. Your PIX Manager needs to be configured to support the following
 * assigning authority domains:
 * 2.16.840.1.113883.3.72.5.9.1 (NIST2010)
 * 2.16.840.1.113883.3.72.5.9.2 (NIST2010-2)
 * </li>
 * </ul>
 * <p/>
 * Description:
 * Test case ITI-44-Update-and-Link covers PIX Patient Feed, the update
 * (Patient Registry Record Revised : PRPA_IN201302UV02) message, and PIX
 * queries. Patient LEDESMA CAROL (ID=LD-165-001) is registered in domain
 * NIST2010 with "correct" demographics. The same patient is then registered
 * in domain NIST2010-2 with incorrect demographics; the name is spelled
 * wrong (LIDIMAE KAROLLE; ID=LD-165-002) and the DOB is wrong. The
 * demographics are sufficiently different that a Cross Reference Manager
 * should not link these two records. A query is made to confirm that the
 * records are not linked.
 * <p/>
 * A patient update message is sent for the patient in domain NIST2010-2 that
 * should synchronize the demographics as found in NIST2010. That is, patient
 * LIDIMAE is updated to LEDESMA with the DOB also corrected. The Cross
 * Reference Manager should now link the two records. A query is made to
 * confirm that the records are now linked.
 *
 * @author <a href="mailto:wenzhi.li@misys.com">Wenzhi Li</a>
 */
public class ITI44UpdateAndLink extends AbstractPixPdqV3Test {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testITI44UpdateAndLink() throws Exception {

        //Step 1: The NIST PIX Source sends a registration message
        //(PRPA_IN201301UV02) to register patient LEDESMA CAROL in domain NIST2010.
        //Patient ID is LD-165-001 Your PIX Manager shall register the patient and
        //send a correct acknowledgement message back.
        OMElement response = sendPixCreate("iti_44_update_and_link_1.xml");
        assertNotNull(response);
        assertEquals("CA", getAcknowledgmentTypeCode(response));

        //Step 2: The NIST PIX Source sends a registration message (PRPA_IN201301UV02)
        //to register patient LIDIMAE KAROLLE in domain NIST2010-2. Patient ID is
        //LD-165-002 Your PIX Manager shall register the patient and send a correct
        //acknowledgement message back.
        response = sendPixCreate("iti_44_update_and_link_2.xml");
        assertNotNull(response);
        assertEquals("CA", getAcknowledgmentTypeCode(response));

        //Step 3: The NIST PIX Consumer sends a query message (PRPA_IN201309UV02)
        //to ask for LIDIMAE KAROLLE's ID in domain NIST2010 using her id LD-165-002
        //in domain NIST2010-2. The query should not find patient with id LD-165-001
        //in domain NIST2010.
        response = sendPixQuery("iti_44_update_and_link_3.xml");
        assertNotNull(response);
        assertEquals("AA", getAcknowledgmentTypeCode(response));
        assertEquals("NF", getQueryAckCode(response));

        //Step 4: Update Patient LIDIMAE KAROLLE to LEDESMA CAROL The DOB is
        //also updated. The patients should now be linked.
        response = sendPixUpdate("iti_44_update_and_link_4.xml");
        assertNotNull(response);
        assertEquals("CA", getAcknowledgmentTypeCode(response));

        //Step 5: The NIST PIX Consumer sends a query message (PRPA_IN201309UV02) to
        //ask for LEDESMA CAROL's ID in domain NIST2010 using her id LD-165-002 in
        //domain NIST2010-2. Your PIX Manager shall answer correctly to the query
        //with LEDESMA CAROL's ID LD-165-001 in domain NIST2010.
        response = sendPixQuery("iti_44_update_and_link_5.xml");
        assertNotNull(response);
        assertEquals("AA", getAcknowledgmentTypeCode(response));
        //Make sure the pix query find the id LD-165-001 in domain NIST2010
        assertEquals("2.16.840.1.113883.3.72.5.9.1", getPatientIdRoot(response));
        assertEquals("LD-165-001", getPatientIdExtension(response));
    }

}
