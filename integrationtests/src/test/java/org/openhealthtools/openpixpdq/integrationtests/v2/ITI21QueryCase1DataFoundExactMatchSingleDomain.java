/**
 *  Copyright (c) 2009-2011 Misys Open Source Solutions (MOSS) and others
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *  implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 *  Contributors:
 *    Misys Open Source Solutions - initial API and implementation
 *    -
 */

package org.openhealthtools.openpixpdq.integrationtests.v2;

import ca.uhn.hl7v2.app.Initiator;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.v25.message.RSP_K21;
import ca.uhn.hl7v2.model.v25.segment.MSA;
import ca.uhn.hl7v2.model.v25.segment.PID;
import ca.uhn.hl7v2.model.v25.segment.QAK;
import ca.uhn.hl7v2.parser.PipeParser;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * This is the NIST ITI-21-Query-Case1-Data-Found-Exact-Match-Single-Domain test.
 * Be sure to run ITI21PDQSupplierLoadPatientData to load the test patients before
 * running this test case.
 * <p/>
 * <p/>
 * Pre-condition:
 * <ul>
 * <li>1. The PDQ Supplier is configured to send and receive demographics in a
 * single domain: NIST2010&2.16.840.1.113883.3.72.5.9.1&ISO</li>
 * <li>2. The following patient has been loaded into the PDQ Supplier (via Load
 * PDQ Supplier Data) in the domain given in pre-condition (1) above:
 * GERALD BXRACK with ID GB-481</li>
 * </ul>
 * <p/>
 * Description:
 * Test case ITI-21-Query-Case1-Data-Found-Exact-Match-Single-Domain covers the
 * PDQ Query Case 1. You need to have loaded the set of patients indicated in
 * the pre-conditions into your system before running this test. The purpose of
 * the test is to confirm that your PDQ Supplier supports the multiple query
 * parameters.
 * <p/>
 * The following query is tested:
 * <p/>
 * 1. Query on the patient first and last name
 *
 * @author <a href="mailto:wenzhi.li@misys.com">Wenzhi Li</a>
 */
public class ITI21QueryCase1DataFoundExactMatchSingleDomain extends AbstractPixPdqTestCase {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testITI21QueryCase1DataFoundExactMatchSingleDomain() throws Exception {

        // Step 1: The NIST PDQ Consumer sends a query message (QBP^Q22) to
        //ask for the demographics of all patients in domain
        //NIST2010&2.16.840.1.113883.3.72.5.9.1&ISO with a patient name GERALD BXRACK.
        //Your PDQ Supplier shall answer correctly to the query with the demographics
        //of patient GERALD BXRACK in domain NIST2010&2.16.840.1.113883.3.72.5.9.1&ISO.

        // PDQ Request Message:
        String msg = "MSH|^~\\&|NIST_Hydra_PDQ_Consumer^^|NIST^^|OpenPIXPDQ^^|MOSS^^|20110109095944||QBP^Q22^QBP_Q21|NIST-20110109095944|T|2.5\r" +
                "QPD|IHE PDQ Query|QRY1184848949494|@PID.5.1.1^BXRACK~@PID.5.2^GERALD\r" +
                "RCP|I";

        PipeParser pipeParser = new PipeParser();
        Message pdq = pipeParser.parse(msg);

        Initiator initiator = pdqConnection.getInitiator();
        Message response = initiator.sendAndReceive(pdq);
        String responseString = pipeParser.encode(response);
        //System.out.println("Received response:\n" + responseString);
        MSA msa = (MSA) response.get("MSA");
        assertEquals("AA", msa.getAcknowledgmentCode().getValue());
        QAK qak = (QAK) response.get("QAK");
        assertEquals("OK", qak.getQueryResponseStatus().getValue());
        PID pid = ((RSP_K21) response).getQUERY_RESPONSE().getPID();
        assertEquals("BXRACK", pid.getPatientName(0).getFamilyName().getSurname().getValue());
        assertEquals("GERALD", pid.getPatientName(0).getGivenName().getValue());
        assertEquals("L", pid.getPatientName(0).getNameTypeCode().getValue());
        assertEquals("GB-481", pid.getPatientIdentifierList(0).getIDNumber().getValue());
        assertEquals("NIST2010", pid.getPatientIdentifierList(0).getAssigningAuthority().getNamespaceID().getValue());
        assertEquals("2.16.840.1.113883.3.72.5.9.1", pid.getPatientIdentifierList(0).getAssigningAuthority().getUniversalID().getValue());
        assertEquals("MCGEEX", pid.getMotherSMaidenName(0).getFamilyName().getSurname().getValue());
        //Not working at this moment
        //assertEquals("L",       pid.getMotherSMaidenName(0).getNameTypeCode().getValue());
        assertEquals("19800130", pid.getDateTimeOfBirth().getTime().getValue());
        assertEquals("M", pid.getAdministrativeSex().getValue());
        assertEquals("5819980", pid.getPatientAccountNumber().getIDNumber().getValue());

    }

}

