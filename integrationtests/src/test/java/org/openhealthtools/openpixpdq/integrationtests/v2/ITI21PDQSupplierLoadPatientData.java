/**
 *  Copyright (c) 2009-2011 Misys Open Source Solutions (MOSS) and others
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *  implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 *  Contributors:
 *    Misys Open Source Solutions - initial API and implementation
 *    -
 */

package org.openhealthtools.openpixpdq.integrationtests.v2;

import ca.uhn.hl7v2.app.Initiator;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.v231.segment.MSA;
import ca.uhn.hl7v2.parser.PipeParser;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * This is the NIST ITI-21-PDQ-Supplier-Data test.
 * <p/>
 * Pre-condition:
 * The PDQ Supplier is configured to send and receive demographics in a single
 * domain: NIST2010&2.16.840.1.113883.3.72.5.9.1&ISO
 * <p/>
 * Description:
 * The test is to confirm that you have loaded a set of patients into your
 * PDQ supplier. You need to complete this step before you can run any of the
 * PDQ Supplier Test Cases. You have the option to load the data by sending a
 * set of ADT feed messages (this test) to your supplier or by loading the
 * data manually (See "Load PDQ Supplier Data with Feed Messages").
 * <p/>
 * This test will load the following patients into your PDQ Supplier:
 * JERMAINO NXEAL
 * BARBARA LXEIGHTON
 * SHAQUILLO NXEAL
 * RUTH NXEAL
 * DAVID VASXQUEZ
 * KATHRINE MARXCHANT
 * GERALD BXRACK
 * DEMI MOORXE
 * ROGER MOORXE
 * MICHAEL MOORXE
 *
 * @author <a href="mailto:wenzhi.li@misys.com">Wenzhi Li</a>
 */
public class ITI21PDQSupplierLoadPatientData extends AbstractPixPdqTestCase {

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
    }


    /**
     * Loads 10 patients
     */
    @Test
    public void testITI21PDQSupplierLoadPatientData() throws Exception {
        //1. Register Patient NXEAL^JERMAINO with ID JN-018 in domain NIST2010.
        String msg = "MSH|^~\\&|NIST_Pearl_PIX_Source^^|NIST^^|OpenPIXPDQ^^|MOSS^^|20110109030202||ADT^A04^ADT_A01|NIST-20110109030202|1|2.3.1\r" +
                "EVN||200310011100||||200310011043\r" +
                "PID|||JN-018^^^NIST2010&2.16.840.1.113883.3.72.5.9.1&ISO||NXEAL^JERMAINO^^^^^L|HAWKINSX^^^^^^L|19781013|M|||3715 Red Hawk Road^^LONGWOOD^FL^32750||^PRN^PH^^^321^2169372|||||2590014|210-78-0118\r" +
                "PV1||O";
        PipeParser pipeParser = new PipeParser();
        Message adt = pipeParser.parse(msg);
        Initiator initiator = pixConnection.getInitiator();
        Message response = initiator.sendAndReceive(adt);
        String responseString = pipeParser.encode(response);
        MSA msa = (MSA) response.get("MSA");
        assertEquals("AA", msa.getAcknowledgementCode().getValue());
        System.out.println("Received response:\n" + responseString);

        //2. Register Patient LXEIGHTON^BARBARA with ID BL-180 in domain NIST2010.
        msg = "MSH|^~\\&|NIST_Pearl_PIX_Source^^|NIST^^|OpenPIXPDQ^^|MOSS^^|20110109031330||ADT^A04^ADT_A01|NIST-20110109031330|1|2.3.1\r" +
                "EVN||20101020\r" +
                "PID|||BL-180^^^NIST2010&2.16.840.1.113883.3.72.5.9.1&ISO||LXEIGHTON^BARBARA^^^^^L|BRYANTX^^^^^^L|19460717|F|||2674 Farland Avenue^^SAN ANTONIO^TX^78205||^PRN^PH^^^830^4876570|||||2180484|321-48-9511\r" +
                "PV1||O";
        adt = pipeParser.parse(msg);
        response = initiator.sendAndReceive(adt);
        responseString = pipeParser.encode(response);
        msa = (MSA) response.get("MSA");
        assertEquals("AA", msa.getAcknowledgementCode().getValue());
        System.out.println("Received response:\n" + responseString);

        //3. Register Patient NXEAL^SHAQUILLO with ID SN-032 in domain NIST2010.
        msg = "MSH|^~\\&|NIST_Pearl_PIX_Source^^|NIST^^|OpenPIXPDQ^^|MOSS^^|20110109093000||ADT^A04^ADT_A01|NIST-20110109093000|A|2.3.1\r" +
                "EVN||20101020\r" +
                "PID|||SN-032^^^NIST2010&2.16.840.1.113883.3.72.5.9.1&ISO||NXEAL^SHAQUILLO^^^^^L|HAWKINSX^^^^^^L|19720306|M|||2169 Warner Street^^BOSTON^MA^02101||^PRN^PH^^^401^1549812|||||2184999|598-99-5899\r" +
                "PV1||O";
        adt = pipeParser.parse(msg);
        response = initiator.sendAndReceive(adt);
        responseString = pipeParser.encode(response);
        msa = (MSA) response.get("MSA");
        assertEquals("AA", msa.getAcknowledgementCode().getValue());
        System.out.println("Received response:\n" + responseString);

        //4. Register Patient NXEAL^RUTH with ID RN-480 in domain NIST2010.
        msg = "MSH|^~\\&|NIST_Pearl_PIX_Source^^|NIST^^|OpenPIXPDQ^^|MOSS^^|20110109093156||ADT^A04^ADT_A01|NIST-20110109093156|A|2.3.1\r" +
                "EVN||20101020\r" +
                "PID|||RN-480^^^NIST2010&2.16.840.1.113883.3.72.5.9.1&ISO||NXEAL^RUTH^^^^^L|HAWKINSX^^^^^^L|19361120|F|||2222 Raver Croft Drive^^HAMPTON^TN^37658||^PRN^PH^^^435^9485847|||||1990844|588-97-4411\r" +
                "PV1||O";
        adt = pipeParser.parse(msg);
        response = initiator.sendAndReceive(adt);
        responseString = pipeParser.encode(response);
        msa = (MSA) response.get("MSA");
        assertEquals("AA", msa.getAcknowledgementCode().getValue());
        System.out.println("Received response:\n" + responseString);

        //5. Register Patient VASXQUEZ^DAVID with ID DV-301 in domain NIST2010.
        msg = "MSH|^~\\&|NIST_Pearl_PIX_Source^^|NIST^^|OpenPIXPDQ^^|MOSS^^|20110109093338||ADT^A04^ADT_A01|NIST-20110109093338|A|2.3.1\r" +
                "EVN||20101020\r" +
                "PID|||DV-301^^^NIST2010&2.16.840.1.113883.3.72.5.9.1&ISO||VASXQUEZ^DAVID^^^^^L|WILSONX^^^^^^L|19670305|M|||1589 Brentwood Drive^^HUTTO^TX^78634||^PRN^PH^^^512^7592000|||||1007240|148-81-8414\r" +
                "PV1||O";
        adt = pipeParser.parse(msg);
        response = initiator.sendAndReceive(adt);
        responseString = pipeParser.encode(response);
        msa = (MSA) response.get("MSA");
        assertEquals("AA", msa.getAcknowledgementCode().getValue());
        System.out.println("Received response:\n" + responseString);

        //6. Register Patient MARXCHANT^KATHRINE with ID KM-375 in domain NIST2010.
        msg = "MSH|^~\\&|NIST_Pearl_PIX_Source^^|NIST^^|OpenPIXPDQ^^|MOSS^^|20110109093459||ADT^A04^ADT_A01|NIST-20110109093459|1|2.3.1\r" +
                "EVN||20101020\r" +
                "PID|||KM-375^^^NIST2010&2.16.840.1.113883.3.72.5.9.1&ISO||MARXCHANT^KATHRINE^^^^^L|MCDOWELLX^^^^^^L|19421105|F|||4670 Raver Croft Drive^^HAMPTON^TN^37658||^PRN^PH^^^423^7259587|||||1840084|633-00-0104\r" +
                "PV1||O";
        adt = pipeParser.parse(msg);
        response = initiator.sendAndReceive(adt);
        responseString = pipeParser.encode(response);
        msa = (MSA) response.get("MSA");
        assertEquals("AA", msa.getAcknowledgementCode().getValue());
        System.out.println("Received response:\n" + responseString);

        //7. Register Patient BXRACK^GERALD with ID GB-481 in domain NIST2010.
        msg = "MSH|^~\\&|NIST_Pearl_PIX_Source^^|NIST^^|OpenPIXPDQ^^|MOSS^^|20110109093611||ADT^A04^ADT_A01|NIST-20110109093611|1|2.3.1\r" +
                "EVN||20101020\r" +
                "PID|||GB-481^^^NIST2010&2.16.840.1.113883.3.72.5.9.1&ISO||BXRACK^GERALD^^^^^L|MCGEEX^^^^^^L|19800130|M|||3005 Tori Lane^^SALT LAKE CITY^UT^84104||^PRN^PH^^^801^6054813|||||5819980|647-80-8974\r" +
                "PV1||O";
        adt = pipeParser.parse(msg);
        response = initiator.sendAndReceive(adt);
        responseString = pipeParser.encode(response);
        msa = (MSA) response.get("MSA");
        assertEquals("AA", msa.getAcknowledgementCode().getValue());
        System.out.println("Received response:\n" + responseString);

        //8. Register Patient MOORXE^DEMI with ID DM-026 in domain NIST2010.
        msg = "MSH|^~\\&|NIST_Pearl_PIX_Source^^|NIST^^|OpenPIXPDQ^^|MOSS^^|20110109093805||ADT^A04^ADT_A01|NIST-20110109093805|1|2.3.1\r" +
                "EVN||20101020\r" +
                "PID|||DM-026^^^NIST2010&2.16.840.1.113883.3.72.5.9.1&ISO||MOORXE^DEMI^^^^^L|GUYNESX^^^^^^L|19621111|F|||3046 Pooh Bear Lane^^GREENVILLE^SC^29607||^PRN^PH^^^864^6209874|||||94819416|655-03-2186\r" +
                "PV1||O";
        adt = pipeParser.parse(msg);
        response = initiator.sendAndReceive(adt);
        responseString = pipeParser.encode(response);
        msa = (MSA) response.get("MSA");
        assertEquals("AA", msa.getAcknowledgementCode().getValue());
        System.out.println("Received response:\n" + responseString);

        //9. Register Patient MOORXE^ROGER with ID RM-489 in domain NIST2010.
        msg = "MSH|^~\\&|NIST_Pearl_PIX_Source^^|NIST^^|OpenPIXPDQ^^|MOSS^^|20110109093913||ADT^A04^ADT_A01|NIST-20110109093913|A|2.3.1\r" +
                "EVN||20101020\r" +
                "PID|||RM-489^^^NIST2010&2.16.840.1.113883.3.72.5.9.1&ISO||MOORXE^ROGER^^^^^L|PARKSX^^^^^^L|19271014|M|||4510 Park Street^^PITTSBURGH^CA^94565||^PRN^PH^^^925^1459742|||||1588908|604-41-5879\r" +
                "PV1||O";
        adt = pipeParser.parse(msg);
        response = initiator.sendAndReceive(adt);
        responseString = pipeParser.encode(response);
        msa = (MSA) response.get("MSA");
        assertEquals("AA", msa.getAcknowledgementCode().getValue());
        System.out.println("Received response:\n" + responseString);

        //10. Register Patient MOORXE^MICHAEL with ID MM-971 in domain NIST2010.
        msg = "MSH|^~\\&|NIST_Pearl_PIX_Source^^|NIST^^|OpenPIXPDQ^^|MOSS^^|20110109094021||ADT^A04^ADT_A01|NIST-20110109094021|1|2.3.1\r" +
                "EVN||20101020\r" +
                "PID|||MM-971^^^NIST2010&2.16.840.1.113883.3.72.5.9.1&ISO||MOORXE^MICHAEL^^^^^L|NEWMANX^^^^^^L|19540423|M|||4756 Stoney Lane^^IRVING^TX^75063||^PRN^PH^^^972^9154876|||||184940891|644-03-0711\r" +
                "PV1||O";
        adt = pipeParser.parse(msg);
        response = initiator.sendAndReceive(adt);
        responseString = pipeParser.encode(response);
        msa = (MSA) response.get("MSA");
        assertEquals("AA", msa.getAcknowledgementCode().getValue());
        System.out.println("Received response:\n" + responseString);

        //finally close the connection
        pixConnection.close();
    }

}
