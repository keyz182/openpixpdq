/**
 *  Copyright (c) 2009-2010 Misys Open Source Solutions (MOSS) and others
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *  implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 *  Contributors:
 *    Misys Open Source Solutions - initial API and implementation
 *    -
 */
package org.openhealthtools.openpixpdq.integrationtests.v3;

import org.apache.axiom.om.OMElement;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * This is the NIST ITI-45-Query-Case2-Data-Found test.
 * <p/>
 * Pre-condition: The PIX Manager is configured to handle these domains:
 * <ul>
 * <li>2.16.840.1.113883.3.72.5.9.1 (NIST2010)</li>
 * <li>2.16.840.1.113883.3.72.5.9.2 (NIST2010-2)</li>
 * <li>2.16.840.1.113883.3.72.5.9.3 (NIST2010-3)</li>
 * </ul>
 * <p/>
 * Description: Test case ITI-45-Query-Case2-Data-Found covers the PIXv3 Query
 * Case 2. One patient (SVOBODA) is registered in three different domains. Three
 * registration messages are sent to your PIX Manager. A PIX Query is sent to
 * resolve a reference to SVOBODA in all domains. Patient SVOBODA should be
 * found.
 *
 * @author <a href="mailto:wenzhi.li@misys.com">Wenzhi Li</a>
 */
public class ITI45QueryCase2DataFound extends AbstractPixPdqV3Test {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testITI45QueryCase2DataFound() throws Exception {

        // Step 1: The NIST PIX Source sends a registration message
        // (PRPA_IN201301UV02) to register patient PAUL SVOBODA in domain
        // 2.16.840.1.113883.3.72.5.9.1 (NIST2010). Patient ID is SP-940. Your
        // PIX Manager shall register the patient and send a correct
        // acknowledgement message back.
        OMElement response = sendPixCreate("iti_45_query_case2_data_found_1.xml");
        assertNotNull(response);
        assertEquals("CA", getAcknowledgmentTypeCode(response));

        // Step 2: The NIST PIX Source sends a registration message
        // (PRPA_IN201301UV02) to register patient PAUL SVOBODA in domain
        // 2.16.840.1.113883.3.72.5.9.2 (NIST2010-2). Patient ID is
        // PSVOBODA-18904. Your PIX Manager shall register the patient and send
        // a correct acknowledgement message back.
        response = sendPixCreate("iti_45_query_case2_data_found_2.xml");
        assertNotNull(response);
        assertEquals("CA", getAcknowledgmentTypeCode(response));

        // Step 3: The NIST PIX Source sends a registration message
        // (PRPA_IN201301UV02) to register patient PAUL SVOBODA in domain
        // 2.16.840.1.113883.3.72.5.9.3 (NIST2010-3). Patient ID is
        // SP-36694-84087. Your PIX Manager shall register the patient and send
        // a correct acknowledgement message back.
        response = sendPixCreate("iti_45_query_case2_data_found_3.xml");
        assertNotNull(response);
        assertEquals("CA", getAcknowledgmentTypeCode(response));

        // Step 4: The NIST PIX Consumer sends a query message
        // (PRPA_IN201309UV02) to ask for PAUL SVOBODA 's ID in all domains
        // using his id SP-940 in domain2.16.840.1.113883.3.72.5.9.1 (NIST2010).
        // Your PIX Manager shall answer correctly to the query with PAUL
        // SVOBODA 's ID PSVOBODA-18904 in domain NIST2010-2 and SP-36694-84087
        // in domain NIST2010-3.
        response = sendPixQuery("iti_45_query_case2_data_found_4.xml");
        assertNotNull(response);
        assertEquals("AA", getAcknowledgmentTypeCode(response));
        //Make sure the pix query find the id in domain NIST2010-2
        evalPatientId(response, "2.16.840.1.113883.3.72.5.9.2", "PSVOBODA-18904");
        evalPatientId(response, "2.16.840.1.113883.3.72.5.9.3", "SP-36694-84087");
    }

}
