/**
 *  Copyright (c) 2009-2010 Misys Open Source Solutions (MOSS) and others
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *  implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 *  Contributors:
 *    Misys Open Source Solutions - initial API and implementation
 *    -
 */

package org.openhealthtools.openpixpdq.integrationtests.v3;


import org.apache.axiom.om.OMElement;
import org.apache.axiom.soap.SOAP12Constants;
import org.apache.axis2.AxisFault;
import org.apache.axis2.Constants;
import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.context.ConfigurationContext;
import org.apache.axis2.context.ConfigurationContextFactory;
import org.apache.axis2.description.WSDL2Constants;
import org.apache.commons.io.IOUtils;
import org.apache.tools.ant.filters.StringInputStream;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.openhealthtools.openexchange.config.PropertyFacade;
import org.openhealthtools.openexchange.utils.AxiomUtil;
import org.openhealthtools.openexchange.utils.OMUtil;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.*;
import java.io.IOException;
import java.net.URL;

import static org.junit.Assert.assertEquals;

/**
 * The base class of all XDS TestCases.
 *
 * @author <a href="mailto:wenzhi.li@misys.com">Wenzhi Li</a>
 */
public abstract class AbstractPixPdqV3Test {
    protected static String pixManagerUrl;
    protected static String pdqSupplierUrl;

    /**
     * @throws java.lang.Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        PropertyFacade.loadProperties(new String[]{"test.properties"});
        pixManagerUrl = PropertyFacade.getString("pixManagerUrl");
        pdqSupplierUrl = PropertyFacade.getString("pdqSupplierUrl");

    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
    }

    public enum PIXType {
        CREATE("urn:hl7-org:v3:PRPA_IN201301UV02"),
        UPDATE("urn:hl7-org:v3:PRPA_IN201302UV02"),
        MERGE("urn:hl7-org:v3:PRPA_IN201304UV02"),
        QUERY("urn:hl7-org:v3:PRPA_IN201309UV02");

        private String action = null;

        private PIXType(String action) {
            this.action = action;
        }

        public String getAction() {
            return this.action;
        }
    }

    public enum PDQType {
        QUERY("urn:hl7-org:v3:PRPA_IN201305UV02"),
        CONTINUE("urn:hl7-org:v3:QUQI_IN000003UV01_Continue"),
        CANCEL("urn:hl7-org:v3:QUQI_IN000003UV01_Cancel");

        private String action = null;

        private PDQType(String action) {
            this.action = action;
        }

        public String getAction() {
            return this.action;
        }
    }

    protected ServiceClient getPixManagerServiceClient(PIXType type) throws AxisFault {
        ConfigurationContext configctx = getContext();
        ServiceClient sender = new ServiceClient(configctx, null);
        boolean enableMTOM = false;
        sender.setOptions(getOptions(type.getAction(), enableMTOM, pixManagerUrl));
        sender.engageModule("addressing");
        return sender;
    }

    protected ServiceClient getPdqSupplierServiceClient(PDQType type) throws AxisFault {
        ConfigurationContext configctx = getContext();
        ServiceClient sender = new ServiceClient(configctx, null);
        boolean enableMTOM = false;
        sender.setOptions(getOptions(type.getAction(), enableMTOM, pdqSupplierUrl));
        sender.engageModule("addressing");
        return sender;
    }

    private ConfigurationContext getContext() throws AxisFault {
        URL axis2repo = AbstractPixPdqV3Test.class.getResource("/axis2repository");
        URL axis2testxml = AbstractPixPdqV3Test.class.getResource("/axis2repository/axis2_test.xml");
        ConfigurationContext configctx = ConfigurationContextFactory
                .createConfigurationContextFromFileSystem(axis2repo.getPath(), axis2testxml.getPath());
        return configctx;
    }

    protected Options getOptions(String action, boolean enableMTOM, String url) {
        Options options = new Options();
        options.setAction(action);
        options.setProperty(WSDL2Constants.ATTRIBUTE_MUST_UNDERSTAND, "1");
        options.setTo(new EndpointReference(url));
        options.setTransportInProtocol(Constants.TRANSPORT_HTTP);
        options.setTimeOutInMilliSeconds(60000);
        if (enableMTOM)
            options.setProperty(Constants.Configuration.ENABLE_MTOM, Constants.VALUE_TRUE);
        else
            options.setProperty(Constants.Configuration.ENABLE_MTOM, Constants.VALUE_FALSE);
        //use SOAP12,
        options.setSoapVersionURI(SOAP12Constants.SOAP_ENVELOPE_NAMESPACE_URI);
        return options;
    }

    protected OMElement getRequest(String fileName) throws IOException, XMLStreamException {
        String message = IOUtils.toString(AbstractPixPdqV3Test.class
                .getResourceAsStream("/data/" + fileName));
        OMElement request = OMUtil.xmlStringToOM(message);
        return request;
    }

    protected OMElement sendPixCreate(String requestFile) throws Exception {
        OMElement request = getRequest(requestFile);
        return sendPixCreate(request);
    }

    protected OMElement sendPixUpdate(String requestFile) throws Exception {
        OMElement request = getRequest(requestFile);
        return sendPixUpdate(request);
    }

    protected OMElement sendPixMerge(String requestFile) throws Exception {
        OMElement request = getRequest(requestFile);
        return sendPixMerge(request);
    }

    protected OMElement sendPixQuery(String requestFile) throws Exception {
        OMElement request = getRequest(requestFile);
        return sendPixQuery(request);
    }

    protected OMElement sendPdqQuery(String requestFile) throws Exception {
        OMElement request = getRequest(requestFile);
        return sendPdqQuery(request);
    }

    protected OMElement sendPdqQueryContinue(String requestFile) throws Exception {
        OMElement request = getRequest(requestFile);
        return sendPdqQueryContinue(request);
    }

    protected OMElement sendPdqQueryCancel(String requestFile) throws Exception {
        OMElement request = getRequest(requestFile);
        return sendPdqQueryCancel(request);
    }

    private OMElement sendPixCreate(OMElement request) throws Exception {
        print(request, true);
        ServiceClient pixCreateSender = getPixManagerServiceClient(PIXType.CREATE);
        OMElement response = pixCreateSender.sendReceive(request);
        print(response, false);
        return response;
    }

    private OMElement sendPixUpdate(OMElement request) throws Exception {
        print(request, true);
        ServiceClient pixCreateSender = getPixManagerServiceClient(PIXType.UPDATE);
        OMElement response = pixCreateSender.sendReceive(request);
        print(response, false);
        return response;
    }

    private OMElement sendPixMerge(OMElement request) throws Exception {
        print(request, true);
        ServiceClient pixCreateSender = getPixManagerServiceClient(PIXType.MERGE);
        OMElement response = pixCreateSender.sendReceive(request);
        print(response, false);
        return response;
    }

    private OMElement sendPixQuery(OMElement request) throws Exception {
        print(request, true);
        ServiceClient pixQuerySender = getPixManagerServiceClient(PIXType.QUERY);
        OMElement response = pixQuerySender.sendReceive(request);
        print(response, false);
        return response;
    }

    private OMElement sendPdqQuery(OMElement request) throws Exception {
        print(request, true);
        ServiceClient pixQuerySender = getPdqSupplierServiceClient(PDQType.QUERY);
        OMElement response = pixQuerySender.sendReceive(request);
        print(response, false);
        return response;
    }

    private OMElement sendPdqQueryContinue(OMElement request) throws Exception {
        print(request, true);
        ServiceClient pixQuerySender = getPdqSupplierServiceClient(PDQType.CONTINUE);
        OMElement response = pixQuerySender.sendReceive(request);
        print(response, false);
        return response;
    }

    private OMElement sendPdqQueryCancel(OMElement request) throws Exception {
        print(request, true);
        ServiceClient pixQuerySender = getPdqSupplierServiceClient(PDQType.CANCEL);
        OMElement response = pixQuerySender.sendReceive(request);
        print(response, false);
        return response;
    }

    private void print(OMElement element, boolean isRequest) {
        if (!PropertyFacade.getBoolean("print.message"))
            return;

        if (isRequest) {
            String request = element.toString();
            System.out.println("Request:\n" + request);
        } else {
            System.out.println("Response:");
            StreamResult sr = new StreamResult(System.out);
            try {
                AxiomUtil.prettify(element, sr);
            } catch (Exception e) {
                String response = element.toString();
                System.out.println(response);
            }
        }
    }

    protected String getAcknowledgmentTypeCode(OMElement response)
            throws ParserConfigurationException, IOException, SAXException, XPathExpressionException {
        String path = "//acknowledgement/typeCode/@code";
        return getValue(response, path);
    }

    protected String getQueryAckCode(OMElement response)
            throws ParserConfigurationException, IOException, SAXException, XPathExpressionException {
        String path = "//queryAck/queryResponseCode/@code";
        return getValue(response, path);
    }

    protected String getTotalQuantity(OMElement response)
            throws ParserConfigurationException, IOException, SAXException, XPathExpressionException {
        String path = "//queryAck/resultTotalQuantity/@value";
        return getValue(response, path);
    }

    protected String getCurrentQuantity(OMElement response)
            throws ParserConfigurationException, IOException, SAXException, XPathExpressionException {
        String path = "//queryAck/resultCurrentQuantity/@value";
        return getValue(response, path);
    }

    protected String getRemainingQuantity(OMElement response)
            throws ParserConfigurationException, IOException, SAXException, XPathExpressionException {
        String path = "//queryAck/resultRemainingQuantity /@value";
        return getValue(response, path);
    }

    protected String getPatientIdRoot(OMElement response) throws XPathExpressionException,
            SAXException, IOException, ParserConfigurationException {
        String path = "//controlActProcess/subject/registrationEvent/subject1/patient/id/@root";
        return getValue(response, path);
    }

    protected String getPatientIdExtension(OMElement response) throws XPathExpressionException,
            SAXException, IOException, ParserConfigurationException {
        String path = "//controlActProcess/subject/registrationEvent/subject1/patient/id/@extension";
        return getValue(response, path);
    }

    protected String getPatientLastName(OMElement response) throws XPathExpressionException,
            SAXException, IOException, ParserConfigurationException {
        String path = "//controlActProcess/subject/registrationEvent/subject1/patient/patientPerson/name/family";
        return getValue(response, path);
    }

    protected String getPatientFirstName(OMElement response) throws XPathExpressionException,
            SAXException, IOException, ParserConfigurationException {
        String path = "//controlActProcess/subject/registrationEvent/subject1/patient/patientPerson/name/given";
        return getValue(response, path);
    }

    protected String getPatientBirthTime(OMElement response) throws XPathExpressionException,
            SAXException, IOException, ParserConfigurationException {
        String path = "//controlActProcess/subject/registrationEvent/subject1/patient/patientPerson/birthTime/@value";
        return getValue(response, path);
    }

    protected String getPatientSex(OMElement response) throws XPathExpressionException,
            SAXException, IOException, ParserConfigurationException {
        String path = "//controlActProcess/subject/registrationEvent/subject1/patient/patientPerson/administrativeGenderCode/@code";
        return getValue(response, path);
    }

    protected String getPatientStreet(OMElement response) throws XPathExpressionException,
            SAXException, IOException, ParserConfigurationException {
        String path = "//controlActProcess/subject/registrationEvent/subject1/patient/patientPerson/addr/streetAddressLine";
        return getValue(response, path);
    }

    protected String getPatientCity(OMElement response) throws XPathExpressionException,
            SAXException, IOException, ParserConfigurationException {
        String path = "//controlActProcess/subject/registrationEvent/subject1/patient/patientPerson/addr/city";
        return getValue(response, path);
    }

    protected void evalAcknowledgment(OMElement response, String ackCode)
            throws ParserConfigurationException, IOException, SAXException,
            XPathExpressionException {
        String path = "//acknowledgement/typeCode[@code='" + ackCode + "']";
        eval(response, path);
    }

    protected void evalQueryAck(OMElement response, String code)
            throws ParserConfigurationException, IOException, SAXException,
            XPathExpressionException {
        String path = "//queryAck/queryResponseCode[@code='" + code + "']";
        eval(response, path);
    }

    protected void evalPatientId(OMElement response, String pidRoot, String pidExtension)
            throws ParserConfigurationException, IOException, SAXException,
            XPathExpressionException {
        String path = "//controlActProcess/subject/registrationEvent/subject1/patient/id[@extension='"
                + pidExtension + "' and @root='" + pidRoot + "']";
        eval(response, path);
    }

    protected void eval(OMElement message, String path)
            throws ParserConfigurationException, IOException, SAXException, XPathExpressionException {
        DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
        domFactory.setNamespaceAware(false); // never forget this!
        DocumentBuilder builder = domFactory.newDocumentBuilder();
        Document doc = builder.parse(new StringInputStream(message.toString()));

        XPathFactory factory = XPathFactory.newInstance();
        XPath xpath = factory.newXPath();
        XPathExpression expr = xpath.compile(path);

        Object res = expr.evaluate(doc, XPathConstants.NODESET);
        NodeList nodes = (NodeList) res;
        assertEquals(1, nodes.getLength());
    }

    private String getValue(OMElement message, String path)
            throws ParserConfigurationException, IOException, SAXException, XPathExpressionException {
        DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
        domFactory.setNamespaceAware(false); // never forget this!
        DocumentBuilder builder = domFactory.newDocumentBuilder();
        Document doc = builder.parse(new StringInputStream(message.toString()));

        XPathFactory factory = XPathFactory.newInstance();
        XPath xpath = factory.newXPath();
        XPathExpression expr = xpath.compile(path);

        String value = (String) expr.evaluate(doc, XPathConstants.STRING);
        return value;
    }
}
