/**
 *  Copyright (c) 2009-2010 Misys Open Source Solutions (MOSS) and others
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *  implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 *  Contributors:
 *    Misys Open Source Solutions - initial API and implementation
 *    -
 */
package org.openhealthtools.openpixpdq.integrationtests.v3;


import org.apache.axiom.om.OMElement;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * This is the NIST ITI-44-Feed-Unknown-Domain test.
 * <p/>
 * Description:
 * The purpose of this test is to check that a PIX Manager does not
 * accept a feed message (Patient Registry Record Added : PRPA_IN201301UV02)
 * from an unknown domain. An unknown domain is a valid domain but it
 * is not recognized by your PIX Manager. The patient SARAH CROCHE with
 * ID SC4864 in the unknown domain 2.16.840.1.113883.3.72.5.9.99 (UNKNOWNDOMAIN).
 * Your PIX Manager is expected to return an acknowledgment (MCCI_IN000002UV01)
 * with the code CE or CR.
 *
 * @author <a href="mailto:wenzhi.li@misys.com">Wenzhi Li</a>
 */
public class ITI44FeedUnknownDomain extends AbstractPixPdqV3Test {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testITI44FeedUnknownDomain() throws Exception {

        OMElement response = sendPixCreate("iti_44_feed_unknown_domain.xml");

        assertNotNull(response);
        // assert acknowledgement to be false (CE)
        assertEquals("CE", getAcknowledgmentTypeCode(response));
    }

}
