/**
 *  Copyright (c) 2009-2010 Misys Open Source Solutions (MOSS) and others
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *  implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 *  Contributors:
 *    Misys Open Source Solutions - initial API and implementation
 *    -
 */
package org.openhealthtools.openpixpdq.integrationtests.v3;

import org.apache.axiom.om.OMElement;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * This is the NIST ITI-47-PDQ-Supplier-Data
 * <p/>
 * Pre-condition:
 * Your PDQ Supplier is configured to send and receive demographics in a
 * single domain: 2.16.840.1.113883.3.72.5.9.1 (NIST2010)
 * <p/>
 * Description:
 * Patients List	The test is to confirm that you have loaded a set of patients into your PDQ supplier. You need to complete this step before you can run any of the PDQ Supplier Test Cases. You have the option to load the data by sending a set of ADT feed messages (this test) to your supplier or by loading the data manually (See "Load PDQ Supplier Data with Feed Messages").
 * <p/>
 * This test will load the following patients into your PDQ Supplier:
 * KELLY GREGORYX
 * DAVID GREGORYX
 * DANNY GREGORYX
 * JOYCE HINOJOXS
 * JANE BRACYX
 * MARK STAMXM
 * LISA WILXLIS
 * VIRGINIA WILXLIS
 * PHILLIP WILXLIS
 *
 * @author <a href="mailto:wenzhi.li@misys.com">Wenzhi Li</a>
 */
public class ITI47PDQSupplierData extends AbstractPixPdqV3Test {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testITI47PDQSupplierData() throws Exception {

        // Step 1: Register Patient KELLY GREGORYX with ID GK-891 in domain NIST2010.
        OMElement response = sendPixCreate("iti_47_pdq_supplier_data_1.xml");
        assertNotNull(response);
        assertEquals("CA", getAcknowledgmentTypeCode(response));

        // Step 2: Register Patient DAVID GREGORYX with ID GD-951 in domain NIST2010.
        response = sendPixCreate("iti_47_pdq_supplier_data_2.xml");
        assertNotNull(response);
        assertEquals("CA", getAcknowledgmentTypeCode(response));

        // Step 3: Register Patient DANNY GREGORYX with ID GD-698 in domain NIST2010.
        response = sendPixCreate("iti_47_pdq_supplier_data_3.xml");
        assertNotNull(response);
        assertEquals("CA", getAcknowledgmentTypeCode(response));

        // Step 4: Register Patient JOYCE HINOJOXS with ID HJ-361 in domain NIST2010.
        response = sendPixCreate("iti_47_pdq_supplier_data_4.xml");
        assertNotNull(response);
        assertEquals("CA", getAcknowledgmentTypeCode(response));

        // Step 5: Register Patient JANE BRACYX with ID BJ-516 in domain NIST2010.
        response = sendPixCreate("iti_47_pdq_supplier_data_5.xml");
        assertNotNull(response);
        assertEquals("CA", getAcknowledgmentTypeCode(response));

        // Step 6: Register Patient MARK STAMXM with ID SM-942 in domain NIST2010.
        response = sendPixCreate("iti_47_pdq_supplier_data_6.xml");
        assertNotNull(response);
        assertEquals("CA", getAcknowledgmentTypeCode(response));

        // Step 7: Register Patient LISA WILXLIS with ID WL-251 in domain NIST2010.
        response = sendPixCreate("iti_47_pdq_supplier_data_7.xml");
        assertNotNull(response);
        assertEquals("CA", getAcknowledgmentTypeCode(response));

        // Step 8: Register Patient VIRGINIA WILXLIS with ID WV-941 in domain NIST2010.
        response = sendPixCreate("iti_47_pdq_supplier_data_8.xml");
        assertNotNull(response);
        assertEquals("CA", getAcknowledgmentTypeCode(response));

        // Step 9: Register Patient PHILLIP WILXLIS with ID WP-410 in domain NIST2010.
        response = sendPixCreate("iti_47_pdq_supplier_data_9.xml");
        assertNotNull(response);
        assertEquals("CA", getAcknowledgmentTypeCode(response));
    }

}
