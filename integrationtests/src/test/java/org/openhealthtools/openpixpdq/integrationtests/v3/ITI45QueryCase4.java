/**
 *  Copyright (c) 2009-2010 Misys Open Source Solutions (MOSS) and others
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *  implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 *  Contributors:
 *    Misys Open Source Solutions - initial API and implementation
 *    -
 */
package org.openhealthtools.openpixpdq.integrationtests.v3;

import org.apache.axiom.om.OMElement;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * This is the NIST ITI-45-Query-Case4 test.
 * <p/>
 * Pre-condition: The PIX Manager is configured to handle these domains:
 * <ul>
 * <li>2.16.840.1.113883.3.72.5.9.1 (NIST2010)</li>
 * <li>2.16.840.1.113883.3.72.5.9.2 (NIST2010-2)</li>
 * <li>2.16.840.1.113883.3.72.5.9.3 (NIST2010-3)</li>
 * </ul>
 * <p/>
 * Description:
 * Test case ITI-45-Query-Case4 covers the PIXv3 Query Case 4. A PIX Query
 * is sent to find corresponding ids in domain NIST2010-2 for the patient
 * HARVEY CARON with ID KXD5-48c-84848 in domain NIST2010. The PIX Manager
 * does not recognize the patient with ID KXD5-48c-84848 in domain NIST2010.
 * The same query is sent but the requested domains are NIST2010-2 and
 * NIST2010-3. Another one is sent but this time it is for all the domains.
 * In all cases your PIX Manager shoud return a response with the specific
 * error.
 *
 * @author <a href="mailto:wenzhi.li@misys.com">Wenzhi Li</a>
 */
public class ITI45QueryCase4 extends AbstractPixPdqV3Test {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testITI45QueryCase4() throws Exception {
        // Step 1: Step 1: The NIST PIX Consumer sends a query message
        //(Patient Registry Get Identifiers Query : PRPA_IN201309UV02) to
        //ask for HARVEY CARON's ID in domain 2.16.840.1.113883.3.72.5.9.2
        //(NIST2010-2) using his id KXD5-48c-84848 in domain
        //2.16.840.1.113883.3.72.5.9.1(NIST2010). Your PIX Manager shall
        //send a query response back indicating that an error occured.
        OMElement response = sendPixQuery("iti_45_query_case4_1.xml");
        assertNotNull(response);
        assertEquals("AE", getAcknowledgmentTypeCode(response));
        assertEquals("AE", getQueryAckCode(response));

        //Step 2: The NIST PIX Consumer sends a query message (PRPA_IN201309UV02)
        //to ask for HARVEY CARON's ID in domain 2.16.840.1.113883.3.72.5.9.2
        //(NIST2010-2) and 2.16.840.1.113883.3.72.5.9.3 (NIST2010-3) using his
        //id KXD5-48c-84848 in domain 2.16.840.1.113883.3.72.5.9.1(NIST2010).
        //Your PIX Manager shall send a query response back indicating that an
        //error occured.
        response = sendPixQuery("iti_45_query_case4_2.xml");
        assertNotNull(response);
        assertEquals("AE", getAcknowledgmentTypeCode(response));
        assertEquals("AE", getQueryAckCode(response));

        //Step 3: The NIST PIX Consumer sends a query message (PRPA_IN201309UV02)
        //to ask for HARVEY CARON's ID in all domains using his id KXD5-48c-84848
        //in domain 2.16.840.1.113883.3.72.5.9.1 (NIST2010). Your PIX Manager
        //shall send a query response back indicating that an error occured.
        response = sendPixQuery("iti_45_query_case4_3.xml");
        assertNotNull(response);
        assertEquals("AE", getAcknowledgmentTypeCode(response));
        assertEquals("AE", getQueryAckCode(response));

    }

}
