/**
 *  Copyright (c) 2009-2010 Misys Open Source Solutions (MOSS) and others
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *  implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 *  Contributors:
 *    Misys Open Source Solutions - initial API and implementation
 *    -
 */
package org.openhealthtools.openpixpdq.integrationtests.v3;

import org.apache.axiom.om.OMElement;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * This is the NIST ITI-44-Feed-Valid-Domain test.
 * <p/>
 * Pre-condition: The PIX Manager is configured to handle these domains:
 * 2.16.840.1.113883.3.72.5.9.1 (NIST2010) 2.16.840.1.113883.3.72.5.9.2
 * (NIST2010-2) 2.16.840.1.113883.3.72.5.9.3 (NIST2010-3)
 * <p/>
 * Description: The purpose of this test is to check that a PIX Manager accepts
 * a feed message (PRPA_IN201301UV02) that has a correctly specified assigning
 * authority domain for the patient"s identifier.
 * <p/>
 * Test Steps Description: Step 1: The patient WILMA JOYNER with ID JW-824-v3 in
 * domain NIST2010 is registered. Your PIX Manager is expected to return an
 * acknowledgment (MCCI_IN000002UV01) with the code CA.
 * <p/>
 * Step 2: The same patient WILMA JOYNER with ID JW26957-v3 in a different
 * domain NIST2010-2. Your PIX Manager is expected to return an acknowledgment
 * (MCCI_IN000002UV01) with the code CA.
 * <p/>
 * Step 3: A query is made with WILMA JOYNER with ID JW-824-v3 in domain
 * NIST2010 requesting his patient ID in domain NIST2010-2. Your PIX Manager is
 * expected to return a query response in which the patient WILMA JOYNER ID is
 * found in domain NIST2010-2.
 * <p/>
 * Step 4: The patient WILMA JOYNER with ID JW00598v3 in domain NIST2010-3. The
 * PIX Manager is expected to return an acknowledgment (MCCI_IN000002UV01) with
 * the code CA.
 * <p/>
 * Step 5: A query is made with WILMA JOYNER with ID JW-824-v3 in domain
 * NIST2010 requesting his patient ID in domain NIST2010-3. The PIX Manager is
 * expected to return a query response in which the patient WILMA JOYNER ID is
 * found in domain NIST2010-3.
 * <p/>
 * References ITI TF PIX/PDQV3 Supplement 3.44.4.1.3 and ITI TF PIX/PDQV3
 * Supplement 3.45.4.2
 *
 * @author <a href="mailto:wenzhi.li@misys.com">Wenzhi Li</a>
 */
public class ITI44FeedValidDomain extends AbstractPixPdqV3Test {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testITI44FeedValidDomain() throws Exception {

        // Step 1: Register patient WILMA JOYNER with ID JW-824-v3 in domain
        // NIST2010
        OMElement response = sendPixCreate("iti_44_feed_valid_domain_1.xml");
        assertNotNull(response);
        // assert acknowledgement to be CA
        assertEquals("CA", getAcknowledgmentTypeCode(response));

        // Step 2: Register the same patient WILMA JOYNER with ID JW26957-v3 in
        // domain NIST2010-2
        response = sendPixCreate("iti_44_feed_valid_domain_2.xml");
        assertNotNull(response);
        assertEquals("CA", getAcknowledgmentTypeCode(response));

        // Step 3: PIX Query WILMA JOYNER with ID JW-824-v3 in domain
        // NIST2010 requesting his patient ID in domain NIST2010-2
        response = sendPixQuery("iti_44_feed_valid_domain_3.xml");
        assertNotNull(response);
        assertEquals("AA", getAcknowledgmentTypeCode(response));
        //Make sure the pix query find the id JW26957-v3 in domain NIST2010-2
        assertEquals("2.16.840.1.113883.3.72.5.9.2", getPatientIdRoot(response));
        assertEquals("JW26957-v3", getPatientIdExtension(response));

        // Step 4: Register the same patient WILMA JOYNER with ID JW00598v3 in
        // domain NIST2010-3
        response = sendPixCreate("iti_44_feed_valid_domain_4.xml");
        assertNotNull(response);
        assertEquals("CA", getAcknowledgmentTypeCode(response));

        // Step 5: PIX Query WILMA JOYNER with ID JW-824-v3 in domain
        // NIST2010 requesting his patient ID in domain NIST2010-3
        response = sendPixQuery("iti_44_feed_valid_domain_5.xml");
        assertNotNull(response);
        assertEquals("AA", getAcknowledgmentTypeCode(response));
        //Make sure the pix query find the id JW00598v3 in domain NIST2010-3
        assertEquals("2.16.840.1.113883.3.72.5.9.3", getPatientIdRoot(response));
        assertEquals("JW00598v3", getPatientIdExtension(response));
    }

}
