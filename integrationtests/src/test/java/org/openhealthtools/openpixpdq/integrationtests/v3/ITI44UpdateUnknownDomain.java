/**
 *  Copyright (c) 2009-2010 Misys Open Source Solutions (MOSS) and others
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *  implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 *  Contributors:
 *    Misys Open Source Solutions - initial API and implementation
 *    -
 */
package org.openhealthtools.openpixpdq.integrationtests.v3;


import org.apache.axiom.om.OMElement;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * This is the NIST ITI-44-Update-Unknown-Domain test.
 * <p/>
 * Pre-Condition:
 * Your PIX Manager is configured to handle this domain:
 * 2.16.840.1.113883.3.72.5.9.1 (NIST2010)
 * <p/>
 * Description:
 * The purpose of this test is to check that a PIX Manager does not accept
 * an update message (Patient Registry Record Revised : PRPA_IN201302UV02)
 * that has an unknown domain. An unknown domain is a valid domain but it
 * is not recognized by the PIX Manager.
 * <p/>
 * The NIST PIX Source registers the patient COLEMAN ROBERT in a single
 * domain NIST2010. Then the NIST PIX Source updates the same patient in
 * an unknown domain. Your PIX Manager shall send an acknowledgment
 * (MCCI_IN000002UV01) back with the code CE or CR.
 *
 * @author <a href="mailto:wenzhi.li@misys.com">Wenzhi Li</a>
 */
public class ITI44UpdateUnknownDomain extends AbstractPixPdqV3Test {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testITI44UpdateUnknownDomain() throws Exception {

        //Step 1: Register patient COLEMAN ROBERT with the id CR-419 in domain NIST2010.
        OMElement response = sendPixCreate("iti_44_update_unknown_domain_1.xml");
        assertNotNull(response);
        assertEquals("CA", getAcknowledgmentTypeCode(response));

        //Step 2: Update COLEMAN ROBERT demographics in domain
        //2.16.840.1.113883.3.72.5.9.99 (UNKNOWNDOMAIN).
        response = sendPixUpdate("iti_44_update_unknown_domain_2.xml");
        assertNotNull(response);
        assertEquals("CE", getAcknowledgmentTypeCode(response));
    }

}
