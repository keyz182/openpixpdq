/**
 *  Copyright (c) 2009-2010 Misys Open Source Solutions (MOSS) and others
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *  implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 *  Contributors:
 *    Misys Open Source Solutions - initial API and implementation
 *    -
 */
package org.openhealthtools.openpixpdq.integrationtests.v3;

import org.apache.axiom.om.OMElement;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * This is the NIST ITI-47-Query-Continuation-Option test.
 * <p/>
 * Pre-condition:
 * <ul>
 * <li>1. The PDQ Supplier is configured to send and receive demographics in a single
 * domain: 2.16.840.1.113883.3.72.5.9.1 (NIST2010)</li>
 * <li>2. The following patients have been loaded into the PDQ Supplier (via Load
 * PDQ Supplier Data) in the domain given in pre-condition (1) above:LISA WILXLIS
 * with ID WL-251
 * <p/>
 * VIRGINIA WILXLIS with ID WV-941
 * PHILLIP WILXLIS with ID WP-410</li>
 * </ul>
 * <p/>
 * Description:
 * The purpose of this test is to test that your PDQ Supplier supports the HL7
 * Continuation Option You need to have loaded the set of patients indicated in
 * the pre-conditions into your system before running this test. Your PDQ Supplier
 * contains several members of the same family. The NIST PDQ Consumer queries for
 * all the patients with the name WILXLIS, restraining the response to one record.
 * The NIST PDQ Consumer will then request for additional increments and cancel the
 * query.
 * For all queries we expect a response back with the code AA/OK and the patient
 * demographics.
 *
 * @author <a href="mailto:wenzhi.li@misys.com">Wenzhi Li</a>
 */
public class ITI47QueryContinuationOption extends AbstractPixPdqV3Test {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testITI47QueryContinuationOption() throws Exception {

        // Step 1: The NIST PDQ Consumer sends a query message (Patient Registry Find
        //Candidates Query: PRPA_IN201305UV02) to ask for the demographics of all
        //patients with a patient name WILXLIS. The element initialQuantity contains
        //the value 1, which means your PDQ Supplier shall return one record per
        //response. Your PDQ Supplier shall answer correctly to the query with the
        //demographics of one of the WILXLIS patients in domain 2.16.840.1.113883.3.72.5.9.1
        //(NIST2010). The elements resultTotalQuantity, resultCurrentQuantity and
        //resultRemainingQuantity shall be correctly populated.

        OMElement response = sendPdqQuery("iti_47_query_continuation_option_1.xml");
        assertNotNull(response);
        assertEquals("AA", getAcknowledgmentTypeCode(response));
        assertEquals("OK", getQueryAckCode(response));
        assertEquals("3", getTotalQuantity(response));
        assertEquals("1", getCurrentQuantity(response));
        assertEquals("2", getRemainingQuantity(response));

        //Step 2: The NIST PDQ Consumer sends a query continuation message (General
        //Query Activate Query Continue : QUQI_IN000003UV01) to ask for the additional
        //records of the previous query. Your PDQ Supplier shall answer correctly to
        //the query with the demographics of one of the WILXLIS patients in domain
        //2.16.840.1.113883.3.72.5.9.1 (NIST2010). The elements resultTotalQuantity,
        //resultCurrentQuantity and resultRemainingQuantity shall be correctly populated.

        //Step 3: The NIST PDQ Consumer sends a query cancel message (General Query
        //Activate Query Continue : QUQI_IN000003UV01) to cancel the previous query.
        //Your PDQ Supplier shall answer correctly to the query cancel by responding
        //with an acknowledgement.
    }

}
