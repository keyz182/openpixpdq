/**
 *  Copyright (c) 2009-2010 Misys Open Source Solutions (MOSS) and others
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *  implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 *  Contributors:
 *    Misys Open Source Solutions - initial API and implementation
 *    -
 */
package org.openhealthtools.openpixpdq.integrationtests.v3;


import org.apache.axiom.om.OMElement;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * This is the NIST ITI-44-Merge-Patient test.
 * <p/>
 * Pre-condition:
 * <ul>
 * <li>1. Ensure that your database is cleared of the patient names and identifiers
 * in the specified domains used in this test case.
 * </li>
 * <li>2. Your PIX Manager needs to be configured to support the following
 * assigning authority domains:
 * 2.16.840.1.113883.3.72.5.9.1 (NIST2010)
 * 2.16.840.1.113883.3.72.5.9.2 (NIST2010-2)
 * </li>
 * </ul>
 * <p/>
 * Description:
 * The purpose is to check that your PIX Manager can merge two existing
 * patients when it receives a merge message (Patient Registry Duplicates
 * Resolved : PRPA_IN201304UV02).
 * <p/>
 * The patient BRIGGS CRYSTAL registers in domain NIST2010 with the id BC-659.
 * The same patient, with the same demographics registers in domain NIST2010-2
 * with the id BC-20002. Later, the patient FORD CRYSTAL registers in domain
 * NIST2010 with the id FC-303. FORD CRYSTAL and BRIGGS CRYSTAL are in fact
 * the same person: BRIGGS is a maiden name. Someone notices that BRIGGS and
 * FORD refer to the same person in domain NIST2010 and a merge message
 * (PRPA_IN201304UV02) is sent to correct that. BRIGGS is merged with FORD,
 * FORD is the remaining record. The domain NIST2010-2 has no direct knowledge
 * of the merge. Your PIX Manager should maintain the link for this patient
 * between domains NIST2010 and NIST2010-2. The NIST PIX Consumer sends a query
 * to request corresponding id for patient BRIGGS CRYSTAL (BC-20002). Your PIX
 * Manager shall return the remaining id for FORD (FC-303) in domain NIST2010.
 *
 * @author <a href="mailto:wenzhi.li@misys.com">Wenzhi Li</a>
 */
public class ITI44MergePatient extends AbstractPixPdqV3Test {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testITI44MergePatient() throws Exception {

        //Step 1: BRIGGS registers in domain NIST2010 with ID BC-659.
        OMElement response = sendPixCreate("iti_44_merge_patient_1.xml");
        assertNotNull(response);
        assertEquals("CA", getAcknowledgmentTypeCode(response));

        //Step 2: BRIGGS registers in domain NIST2010-2 with ID BC-20002
        response = sendPixCreate("iti_44_merge_patient_2.xml");
        assertNotNull(response);
        assertEquals("CA", getAcknowledgmentTypeCode(response));

        //Step 3: BRIGGS marries, takes on the name FORD and changes address at
        //the same time. A new registration message is sent in domain NIST2010
        //with ID FC-303 (because she forgot to tell the administrator of her
        //name change). This registration message is for FORD with a new address
        //but same DOB and other demographics.
        response = sendPixCreate("iti_44_merge_patient_3.xml");
        assertNotNull(response);
        assertEquals("CA", getAcknowledgmentTypeCode(response));

        //Step 4: Someone recognizes the mistake and merges the records BC-659
        //and FC-303 in domain NIST2010. FORD (FC-303) is the remaining record.
        //Your PIX Manager should maintain the link between BRIGGS in domain
        //NIST2010-2 and FORD in domain NIST2010.
        response = sendPixMerge("iti_44_merge_patient_4.xml");
        assertNotNull(response);
        assertEquals("CA", getAcknowledgmentTypeCode(response));

        //Step 5: The domain NIST2010-2 has no direct knowledge of the merge. A
        //query from domain NIST2010-2 is made with the ID for BRIGGS (BC-20002).
        //Your PIX manager shall return the remaining ID for FORD (FC-303) in
        //domain NIST2010.
        response = sendPixQuery("iti_44_merge_patient_5.xml");
        assertNotNull(response);
        assertEquals("AA", getAcknowledgmentTypeCode(response));
        //Make sure the pix query find the id FC-303 in domain NIST2010
        assertEquals("2.16.840.1.113883.3.72.5.9.1", getPatientIdRoot(response));
        assertEquals("FC-303", getPatientIdExtension(response));

    }

}
