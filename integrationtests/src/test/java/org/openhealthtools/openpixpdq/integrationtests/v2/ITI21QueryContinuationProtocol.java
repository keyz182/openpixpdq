/**
 *  Copyright (c) 2009-2011 Misys Open Source Solutions (MOSS) and others
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *  implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 *  Contributors:
 *    Misys Open Source Solutions - initial API and implementation
 *    -
 */

package org.openhealthtools.openpixpdq.integrationtests.v2;

import ca.uhn.hl7v2.app.Initiator;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.v25.message.RSP_K21;
import ca.uhn.hl7v2.model.v25.segment.DSC;
import ca.uhn.hl7v2.model.v25.segment.MSA;
import ca.uhn.hl7v2.model.v25.segment.PID;
import ca.uhn.hl7v2.model.v25.segment.QAK;
import ca.uhn.hl7v2.parser.PipeParser;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * This is the NIST ITI-21-Query-Continuation-Protocol test.
 * Be sure to run ITI21PDQSupplierLoadPatientData to load the test patients before
 * running this test case.
 * <p/>
 * Pre-condition:
 * <ul>
 * <li>1. The PDQ Supplier is configured to send and receive demographics in
 * a single domain: NIST2010&2.16.840.1.113883.3.72.5.9.1&ISO</li>
 * <li>2. The following patients have been loaded into the PDQ Supplier (via
 * Load PDQ Supplier Data) in the domain given in pre-condition (1) above:
 * DEMI MOORXE with ID DM-026
 * ROGER MOORXE with ID RM-489
 * MICHAEL MOORXE with ID MM-971</li>
 * </ul>
 * <p/>
 * Description:
 * The purpose of this test is to test that your PDQ Supplier supports the HL7
 * Continuation Protocol. You need to have loaded the set of patients indicated
 * in the pre-conditions into your system before running this test.
 * Your PDQ Supplier contains several members of the same family. The NIST PDQ
 * Consumer queries for all the patients with the name MOORXE, restraining the
 * response to one record. The NIST PDQ Consumer will then use the HL7
 * Continuation Protocol to request for additional increments and cancel the
 * query.
 * For all queries we expect a response back with the code AA/OK and the patient
 * demographics.
 *
 * @author <a href="mailto:wenzhi.li@misys.com">Wenzhi Li</a>
 */
public class ITI21QueryContinuationProtocol extends AbstractPixPdqTestCase {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testITI21QueryContinuationProtocol() throws Exception {

        //Step 1: The NIST PDQ Consumer sends a query message (QBP^Q22) to ask for
        //the demographics of all patients with a patient name MOORE. The field
        //RCP.2 contains the value 1^RD, which means your PDQ Supplier shall
        //return one record per response. Your PDQ Supplier shall answer correctly
        //to the query with the demographics of one of the MOORXE patients in
        //domain NIST2010&2.16.840.1.113883.3.72.5.9.1&ISO and populate field DSC.1

        // PDQ Request Message:
        String msg = "MSH|^~\\&|NIST_Hydra_PDQ_Consumer^^|NIST^^|OpenPIXPDQ^^|MOSS^^|20110109115433||QBP^Q22^QBP_Q21|NIST-20110109115433|T|2.5\r" +
                "QPD|IHE PIX Query|QRY1245648158646188|@PID.5.1.1^MOORXE\r" +
                "RCP|I|1^RD";
        PipeParser pipeParser = new PipeParser();
        Message pdq = pipeParser.parse(msg);

        Initiator initiator = pdqConnection.getInitiator();
        Message response = initiator.sendAndReceive(pdq);
        String responseString = pipeParser.encode(response);
        //System.out.println("Received response:\n" + responseString);
        MSA msa = (MSA) response.get("MSA");
        assertEquals("AA", msa.getAcknowledgmentCode().getValue());
        QAK qak = (QAK) response.get("QAK");
        assertEquals("OK", qak.getQueryResponseStatus().getValue());
        assertEquals("3", qak.getHitCount().getValue());
        assertEquals("1", qak.getThisPayload().getValue());
        assertEquals("2", qak.getHitsRemaining().getValue());
        PID pid = ((RSP_K21) response).getQUERY_RESPONSE().getPID();
        assertEquals("DM-026", pid.getPatientIdentifierList(0).getIDNumber().getValue());
        assertEquals(1, ((RSP_K21) response).getQUERY_RESPONSEReps());
        DSC dsc = (DSC) response.get("DSC");

        //Step 2: The NIST PDQ Consumer sends a query message (QBP^Q22) to ask for
        //the additional records of the previous query. Field DSC.1 echoes the
        //continuation pointer value sent previously by your PDQ Supplier. Your
        //PDQ Supplier shall answer correctly to the query with the demographics
        //of one of the MOORXE patients in domain NIST2010&2.16.840.1.113883.3.72.5.9.1&ISO
        //and populate field DSC.1
        msg = "MSH|^~\\&|NIST_Hydra_PDQ_Consumer^^|NIST^^|OpenPIXPDQ^^|MOSS^^|20110109115929||QBP^Q22^QBP_Q21|NIST-20110109115929|P|2.5\r" +
                "QPD|IHE PIX Query|QRY1245648158646188|@PID.5.1.1^MOORXE\r" +
                "RCP|I|1^RD\r" +
                "DSC|" + dsc.getContinuationPointer() + "|I";

        pdq = pipeParser.parse(msg);

        response = initiator.sendAndReceive(pdq);
        responseString = pipeParser.encode(response);
        //System.out.println("Received response:\n" + responseString);
        msa = (MSA) response.get("MSA");
        assertEquals("AA", msa.getAcknowledgmentCode().getValue());
        qak = (QAK) response.get("QAK");
        assertEquals("3", qak.getHitCount().getValue());
        assertEquals("1", qak.getThisPayload().getValue());
        assertEquals("1", qak.getHitsRemaining().getValue());
        assertEquals("OK", qak.getQueryResponseStatus().getValue());
        pid = ((RSP_K21) response).getQUERY_RESPONSE().getPID();
        assertEquals("RM-489", pid.getPatientIdentifierList(0).getIDNumber().getValue());
        assertEquals(1, ((RSP_K21) response).getQUERY_RESPONSEReps());
        dsc = (DSC) response.get("DSC");

        //Step 3: The NIST PDQ Consumer sends a query cancel message (QCN^J01) to
        //cancel the previous query.
        //Your PDQ Supplier shall answer correctly to the query cancel by responding
        //with an acknowledgement with the AA code.
        msg = "MSH|^~\\&|NIST_Hydra_PDQ_Consumer^^|NIST^^|OpenPIXPDQ^^|MOSS^^|20110109120607||QCN^J01^QCN_J01|NIST-20110109120607|D|2.5\r" +
                "QID|QRY1245648158646188|IHE PIX Query";
        pdq = pipeParser.parse(msg);

        response = initiator.sendAndReceive(pdq);
        responseString = pipeParser.encode(response);
        //System.out.println("Received response:\n" + responseString);
        msa = (MSA) response.get("MSA");
        assertEquals("AA", msa.getAcknowledgmentCode().getValue());
    }

}

