/**
 *  Copyright (c) 2009-2011 Misys Open Source Solutions (MOSS) and others
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *  implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 *  Contributors:
 *    Misys Open Source Solutions - initial API and implementation
 *    -
 */

package org.openhealthtools.openpixpdq.integrationtests.v2;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.app.Connection;
import ca.uhn.hl7v2.app.ConnectionHub;
import ca.uhn.hl7v2.llp.MinLowerLayerProtocol;
import ca.uhn.hl7v2.parser.PipeParser;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.openhealthtools.openexchange.config.PropertyFacade;

/**
 * The abstract class for all PIX/PDQ test cases. It set up
 * basic testing information such as server name and port.
 *
 * @author Wenzhi Li
 * @version 1.0, Jan 23, 2009
 */
public abstract class AbstractPixPdqTestCase {

    protected static Connection pixConnection = null;
    protected static Connection pdqConnection = null;

    /**
     * @throws java.lang.Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        PropertyFacade.loadProperties(new String[]{"test.properties"});
        createPIXConnection();
        createPDQConnection();
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        if (pixConnection != null) {
            //Find out why this is not working
            //pixConnection.close();
        }
        if (pdqConnection != null) {
            //Find out why this is not working
            //pdqConnection.close();
        }
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
    }

    protected static void createPIXConnection() throws HL7Exception {
        ConnectionHub connectionHub = ConnectionHub.getInstance();
        String host = PropertyFacade.getString("host");
        int pixPort = PropertyFacade.getInteger("pix.port");
        pixConnection = connectionHub.attach(host, pixPort, new PipeParser(), MinLowerLayerProtocol.class);
    }

    protected static void createPDQConnection() throws HL7Exception {
        ConnectionHub connectionHub = ConnectionHub.getInstance();
        String host = PropertyFacade.getString("host");
        int pdqPort = PropertyFacade.getInteger("pdq.port");
        pdqConnection = connectionHub.attach(host, pdqPort, new PipeParser(), MinLowerLayerProtocol.class);
    }
}
