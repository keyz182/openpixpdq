/**
 *  Copyright (c) 2009-2011 Misys Open Source Solutions (MOSS) and others
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *  implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 *  Contributors:
 *    Misys Open Source Solutions - initial API and implementation
 *    Moin Islam                  - v3 implementation
 */
package org.openhealthtools.openpixpdq.impl.v3;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.FeatureMapUtil;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.hl7.v3.*;
import org.hl7.v3.util.V3ResourceFactoryImpl;
import org.openhealthtools.openexchange.datamodel.Identifier;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

public class V3util {

    private static final Map<String, String> EMF_SAVE_OPTIONS = new HashMap<String, String>();

    static {
        EMF_SAVE_OPTIONS.put(XMLResource.OPTION_FORMATTED, Boolean.FALSE
                .toString());
    }

    public static Object transformDomToEmf(Element domElement,
                                           Resource.Factory resourceFactory, URI uri,
                                           boolean isValidationRelaxed) throws Exception {

        XMLResource resources = (XMLResource) resourceFactory
                .createResource(uri);
        if (isValidationRelaxed) {
            if (resources instanceof XMLResource) {
                resources.getDefaultLoadOptions()
                        .put(XMLResource.OPTION_RECORD_UNKNOWN_FEATURE,
                                Boolean.TRUE);
            }
        }

        resources.load(domElement, null);
        // Display what was loaded.
        resources.save(System.out, null);
        System.out.println();

        EList<?> list = resources.getContents();
        return list.get(0);
    }

    public static Element transformEmfToDom(EObject object,
                                            Resource.Factory resourceFactory, String uri) throws Exception {

        XMLResource resource = (XMLResource) resourceFactory
                .createResource(org.eclipse.emf.common.util.URI.createURI(uri));
        resource.getContents().add(object);
        // Display what was loaded.
        resource.save(System.out, null);
        return transformEmfToDom(resource);
    }

    /**
     * Transforms an EMF-based XML Resource instance into a DOM Document
     * Element.
     *
     * @param resource EMF-built XML Resource to transform
     * @return DOM Element representing the XML Resource
     * @throws Exception
     */
    public static Element transformEmfToDom(XMLResource resource)
            throws Exception {
        org.w3c.dom.Document domDocument = createDomDocument();
        // FIXME Fix for issue 121
        resource.save(domDocument, EMF_SAVE_OPTIONS, null);
        // resource.save(domDocument, EMF_SAVE_OPTIONS, EMF_DOM_HANDLER);
        return domDocument.getDocumentElement();
    }

    public static Document createDomDocument() throws Exception {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        DocumentBuilder builder = factory.newDocumentBuilder();

        return builder.newDocument();
    }

    public static TS1 createTS1(String time) {
        // create a TS1 element
        TS1 ts1 = V3Factory.eINSTANCE.createTS1();

        // set the current time
        ts1.setValue(time);

        return ts1;
    }

    public static TS1 createTS1CurrentTime() {
        // create a simple date format
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");

        return createTS1(dateFormat.format(new Date()));
    }

    public static PRPAMT201302UV02PatientStatusCode createPRPAMT201302UV02PatientStatusCode(String code) {
        PRPAMT201302UV02PatientStatusCode cs1 = V3Factory.eINSTANCE.createPRPAMT201302UV02PatientStatusCode();
        cs1.setCode(code);
        return cs1;
    }

    public static CS1 createCS1(String code) {
        CS1 cs1 = V3Factory.eINSTANCE.createCS1();
        cs1.setCode(code);
        return cs1;
    }

    public static CD createCD(String code, String CodeSystem) {
        CD cd = V3Factory.eINSTANCE.createCD();
        cd.setCode(code);
        if (null != CodeSystem) {
            cd.setCodeSystem(CodeSystem);
        }
        return cd;
    }

    public static II createII(String root, String extension, String namespace) {
        // create an ID to identify this message
        II idII = V3Factory.eINSTANCE.createII();
        // for now set these to a fixed root and extension
        if (null != root && "" != root)
            idII.setRoot(root);
        if (null != extension && "" != extension)
            idII.setExtension(extension);
        if (null != namespace && "" != namespace)
            idII.setAssigningAuthorityName(namespace);

        return idII;
    }

    public static PRPAMT201302UV02OtherIDsId createPRPAMT201302UV02OtherIDsId(String root, String extension, String namespace) {
        // create an ID to identify this message
        PRPAMT201302UV02OtherIDsId id = V3Factory.eINSTANCE.createPRPAMT201302UV02OtherIDsId();
        // for now set these to a fixed root and extension
        if (null != root && "" != root)
            id.setRoot(root);
        if (null != extension && "" != extension)
            id.setExtension(extension);
        if (null != namespace && "" != namespace)
            id.setAssigningAuthorityName(namespace);

        return id;
    }

    public static PRPAMT201302UV02PatientId createPRPAMT201302UV02PatientId(String root, String extension, String namespace) {
        // create an ID to identify this message
        PRPAMT201302UV02PatientId id = V3Factory.eINSTANCE.createPRPAMT201302UV02PatientId();
        // for now set these to a fixed root and extension
        if (null != root && "" != root)
            id.setRoot(root);
        if (null != extension && "" != extension)
            id.setExtension(extension);
        if (null != namespace && "" != namespace)
            id.setAssigningAuthorityName(namespace);

        return id;
    }

    public static MCCIMT000300UV01Receiver createMCCIMT000300UV01Receiver(
            String applicationOID, String facilityOID) {
        // create the receiver
        MCCIMT000300UV01Receiver receiver = V3Factory.eINSTANCE
                .createMCCIMT000300UV01Receiver();

        // create the receiver's communication function type
        CommunicationFunctionType receivercft = CommunicationFunctionType.RCV;

        // set the typecode of the receiver
        receiver.setTypeCode(receivercft);

        // now add the receiver device to the receiver
        receiver.setDevice(createMCCIMT000300UV01Device(applicationOID, facilityOID));

        // return the receiver
        return receiver;
    }

    public static MCCIMT000100UV01Receiver createMCCIMT000100UV01Receiver(
            String applicationOID, String facilityOID) {
        // create the receiver
        MCCIMT000100UV01Receiver receiver = V3Factory.eINSTANCE
                .createMCCIMT000100UV01Receiver();

        // create the receiver's communication function type
        CommunicationFunctionType receivercft = CommunicationFunctionType.RCV;

        // set the typecode of the receiver
        receiver.setTypeCode(receivercft);

        // now add the receiver device to the receiver
        receiver.setDevice(createMCCIMT000100UV01Device(applicationOID, facilityOID));

        // return the receiver
        return receiver;
    }

    public static MCCIMT000100UV01Sender createMCCIMT000100UV01Sender(
            String applicationOID, String facilityOID) {

        // create the sender
        MCCIMT000100UV01Sender sender = V3Factory.eINSTANCE
                .createMCCIMT000100UV01Sender();

        // create the sender's communication function type
        CommunicationFunctionType senderCFT = CommunicationFunctionType.SND;

        // set the typecode of the sender
        sender.setTypeCode(senderCFT);

        // now add the sender device to the sender
        sender.setDevice(createMCCIMT000100UV01Device(applicationOID, facilityOID));

        // return the sender
        return sender;

    }

    public static MCCIMT000300UV01Sender createMCCIMT000300UV01Sender(
            String applicationOID, String facilityOID) {

        // create the sender
        MCCIMT000300UV01Sender sender = V3Factory.eINSTANCE
                .createMCCIMT000300UV01Sender();

        // create the sender's communication function type
        CommunicationFunctionType senderCFT = CommunicationFunctionType.SND;

        // set the typecode of the sender
        sender.setTypeCode(senderCFT);

        // now add the sender device to the sender
        sender.setDevice(createMCCIMT000300UV01Device(applicationOID, facilityOID));

        // return the sender
        return sender;

    }


    public static MCCIMT000300UV01Device createMCCIMT000300UV01Device(
            String applicationOID, String facilityOID) {
        // create the sender device
        MCCIMT000300UV01Device device = V3Factory.eINSTANCE
                .createMCCIMT000300UV01Device();

        // Set the device class code
        device.setClassCode(EntityClassDevice.DEV);
        device.setDeterminerCode(EntityDeterminerMember2.INSTANCE);

        device.getId().add(createII(applicationOID, "", ""));

        if (facilityOID != null && facilityOID.length() > 0) {
            MCCIMT000300UV01Agent asAgent = V3Factory.eINSTANCE
                    .createMCCIMT000300UV01Agent();
            asAgent.setClassCode(RoleClassAgentMember1.AGNT);

            MCCIMT000300UV01Organization org = V3Factory.eINSTANCE
                    .createMCCIMT000300UV01Organization();
            org.setClassCode(EntityClassOrganizationMember1.ORG);
            org.setDeterminerCode(EntityDeterminerMember2.INSTANCE);
            org.getId().add(createII(facilityOID, "", ""));
            asAgent.setRepresentedOrganization(org);

            device.setAsAgent(asAgent);
        }

        // return the sender
        return device;
    }

    public static MCCIMT000200UV01Device createMCCIMT000200UV01Device(
            String applicationOID, String facilityOID) {
        // create the sender device
        MCCIMT000200UV01Device device = V3Factory.eINSTANCE
                .createMCCIMT000200UV01Device();

        // Set the device class code
        device.setClassCode(EntityClassDevice.DEV);
        device.setDeterminerCode(EntityDeterminerMember2.INSTANCE);

        device.getId().add(createII(applicationOID, "", ""));

        MCCIMT000200UV01Agent asAgent = V3Factory.eINSTANCE.createMCCIMT000200UV01Agent();
        asAgent.setClassCode(RoleClassAgentMember1.AGNT);

        MCCIMT000200UV01Organization org = V3Factory.eINSTANCE.createMCCIMT000200UV01Organization();
        org.setClassCode(EntityClassOrganizationMember1.ORG);
        org.setDeterminerCode(EntityDeterminerMember2.INSTANCE);
        org.getId().add(createII(facilityOID, "", ""));
        asAgent.setRepresentedOrganization(org);

        device.setAsAgent(asAgent);

        // return the sender
        return device;
    }

    public static MCCIMT000100UV01Device createMCCIMT000100UV01Device(
            String applicationOID, String facilityOID) {
        // create the sender device
        MCCIMT000100UV01Device device = V3Factory.eINSTANCE
                .createMCCIMT000100UV01Device();

        // Set the device class code
        device.setClassCode(EntityClassDevice.DEV);
        device.setDeterminerCode(EntityDeterminerMember2.INSTANCE);

        device.getId().add(createII(applicationOID, "", ""));

        MCCIMT000100UV01Agent asAgent = V3Factory.eINSTANCE.createMCCIMT000100UV01Agent();
        asAgent.setClassCode(RoleClassAgentMember1.AGNT);

        MCCIMT000100UV01Organization org = V3Factory.eINSTANCE.createMCCIMT000100UV01Organization();
        org.setClassCode(EntityClassOrganizationMember1.ORG);
        org.setDeterminerCode(EntityDeterminerMember2.INSTANCE);
        org.getId().add(createII(facilityOID, "", ""));
        asAgent.setRepresentedOrganization(org);

        device.setAsAgent(asAgent);

        // return the sender
        return device;
    }

    public static String getMixedValue(FeatureMap mixed) {
        String returnValue = "";
        // if we have a mixed
        if (mixed.size() > 0)
            returnValue = mixed.get(0).getValue().toString();
        return returnValue;
    }

    public static List<Enumerator> createEnumeratorList(Enumerator enumValue) {

        //  create a list to hold the "use" value
        List<Enumerator> enumList = new ArrayList<Enumerator>();

        // add the SRCH value;
        enumList.add(enumValue);

        return enumList;
    }

    public static CE createCE(String code, String codeSystem, String codeSystemName, String displayValue) {

        CE ce = V3Factory.eINSTANCE.createCE();
        if (null != code && "" != code)
            ce.setCode(code);
        if (null != codeSystem && "" != codeSystem)
            ce.setCodeSystem(codeSystem);
        if (null != codeSystemName && "" != codeSystemName)
            ce.setCodeSystemName(codeSystemName);
        if (null != displayValue && "" != displayValue)
            ce.setDisplayName(displayValue);

        return ce;
    }

    //TODO: need to revisit ProviderOrganization to get the correct data
    public static COCTMT150003UV03Organization createProviderOrganization(Identifier asigningAuthority) {
        COCTMT150003UV03Organization organization = V3Factory.eINSTANCE.createCOCTMT150003UV03Organization();
        organization.setClassCode(EntityClassOrganizationMember1.ORG);
        organization.setDeterminerCode(EntityDeterminerMember2.INSTANCE);

        //Add Id
        organization.getId().add(V3util.createII(asigningAuthority.getUniversalId(), "", ""));

        //add name
        if (null != asigningAuthority.getNamespaceId()) {
            ON name = V3Factory.eINSTANCE.createON();
            FeatureMapUtil.addText(name.getMixed(), asigningAuthority.getNamespaceId());
            organization.getName().add(name);
        }
        //Add contactParty
        COCTMT150003UV03ContactParty contactParty = V3Factory.eINSTANCE.createCOCTMT150003UV03ContactParty();
        contactParty.setClassCode(RoleClassContact.CON);
        organization.getContactParty().add(contactParty);

        return organization;
    }

    public static String emfToString(EObject v3Message) {
        Resource eResources = (new V3ResourceFactoryImpl().createResource(URI.createURI(org.hl7.v3.V3Package.eNS_URI)));
        eResources.getContents().add(v3Message);
        ByteArrayOutputStream bos = null;
        try {
            bos = new ByteArrayOutputStream();
            eResources.save(bos, null);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        } finally {
            if (bos != null) {
                try {
                    bos.close();
                } catch (IOException e) {
                    System.out.println(e.getMessage());
                }
            }
        }
        String xmlString = new String(bos.toByteArray());
        //System.out.println(xmlString);
        return xmlString;
    }


}
