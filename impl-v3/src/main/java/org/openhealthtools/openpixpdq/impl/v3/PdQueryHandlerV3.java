/**
 *  Copyright (c) 2009-2011 Misys Open Source Solutions (MOSS) and others
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *  implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 *  Contributors:
 *    Misys Open Source Solutions - initial API and implementation
 *    Moin Islam                  - v3 implementation
 */
package org.openhealthtools.openpixpdq.impl.v3;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hl7.v3.DocumentRoot;
import org.hl7.v3.PRPAIN201305UV02Type;
import org.hl7.v3.QUQIIN000003UV01Type;
import org.openhealthtools.openexchange.actorconfig.Configuration;
import org.openhealthtools.openexchange.actorconfig.IheConfigurationException;
import org.openhealthtools.openexchange.datamodel.Identifier;
import org.openhealthtools.openexchange.datamodel.MessageHeader;
import org.openhealthtools.openexchange.datamodel.Patient;
import org.openhealthtools.openexchange.datamodel.PatientIdentifier;
import org.openhealthtools.openexchange.utils.StringUtil;
import org.openhealthtools.openpixpdq.api.IPdSupplierAdapter;
import org.openhealthtools.openpixpdq.api.PdSupplierException;
import org.openhealthtools.openpixpdq.api.PdqQuery;
import org.openhealthtools.openpixpdq.api.PdqResult;
import org.openhealthtools.openpixpdq.common.AssigningAuthorityUtil;
import org.openhealthtools.openpixpdq.common.ContinuationPointer;
import org.openhealthtools.openpixpdq.common.PixPdqException;
import org.openhealthtools.openpixpdq.common.PixPdqFactory;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;


public class PdQueryHandlerV3 extends BaseHandlerV3 {
    private static Log log = LogFactory.getLog(PdQueryHandlerV3.class);
    /**
     * Used to store continuation pointer  <String(pointer), ContinuationPointer>
     */
    private static Hashtable<String, ContinuationPointer> dscMap = new Hashtable<String, ContinuationPointer>();

    /**
     * PDQ Adapter
     */
    private IPdSupplierAdapter pdqAdapter = PixPdqFactory.getPdSupplierAdapter();
    //private IPdSupplierAdapter pdqAdapter = null;

    /**
     * PD Supplier V3 Actor
     */
    private PdSupplierV3 actor;

    /**
     * PDQ V3 Handler
     */
    private static PdQueryHandlerV3 instance = null;

    /**
     * Singleton
     *
     * @return
     */
    synchronized static PdQueryHandlerV3 getInstance() {
        if (null == instance) {
            instance = new PdQueryHandlerV3();
        }
        return instance;
    }

    PdQueryHandlerV3() {
    }

    void registryPdSupplier(PdSupplierV3 actor) {
        this.actor = actor;
    }

    PdSupplierV3 getPdSupplier() {
        return this.actor;
    }

    DocumentRoot processMessage(DocumentRoot msgIn) throws PixPdqException {

        DocumentRoot retMessage = null;

        if (null != msgIn.getPRPAIN201305UV02()) { // Find Candidates Query
            retMessage = processQuery(msgIn);
        } else if (null != msgIn.getQUQIIN000003UV01()) { // Activate Query Continue
            retMessage = processQueryContinuation(msgIn);
        } else if (null != msgIn.getQUQIIN000003UV01Cancel()) { // Query Cancel
            retMessage = processQueryCancel(msgIn);
        } else {
            log.error("Message Type not recognized");
            throw new PixPdqException("Message Type not recoginized");
        }

        return retMessage;
    }

    private DocumentRoot processQuery(DocumentRoot msgIn) throws PixPdqException {
        PRPAIN201305UV02Type msgQuery = msgIn.getPRPAIN201305UV02();

        TransmissionWrapper hl7V3Header = new TransmissionWrapper(msgQuery);

        PdQueryParserV3 parser = new PdQueryParserV3(msgQuery, actor.getActorDescription());
        //PdQueryParserV3 parser = new PdQueryParserV3(msgQuery, null);

        PdQueryResponseV3 qryResponse = new PdQueryResponseV3(actor.getActorDescription());
        //PdQueryResponseV3 qryResponse = new PdQueryResponseV3(null);
        qryResponse.setReceiver(hl7V3Header.getSendingApplication(), hl7V3Header.getSendingFacility());
        qryResponse.setSender(getServerApplication(actor.getActorDescription()).getUniversalId(),
                getServerFacility(actor.getActorDescription()).getUniversalId());

        qryResponse.setQueryByParameter(parser.getQryByParam());

        List<ErrorDetail> errorList = new ArrayList<ErrorDetail>();

        // Validate incoming message
        validateMessage(msgIn, hl7V3Header, actor.getActorDescription(), errorList);

        if (errorList.size() > 0) {
            // TODO Find the proper error ack code
            qryResponse.setAck("AE", hl7V3Header.getMessageControlId());
            // set ack detail section of the ack message with list of errors
            qryResponse.addAckDetail(errorList);
            return qryResponse.getResponse();
        }
        List<Identifier> returnDomains = parser.getReturnDomain(errorList);
        if (returnDomains == null) {
            qryResponse.setAck("AE", hl7V3Header.getMessageControlId());
            qryResponse.addAckDetail(errorList);
            qryResponse.setQueryAck("AE", parser.getQryId().getRoot(),
                    parser.getQryId().getExtension(), 0, 0, 0);
            return qryResponse.getResponse();
        }

        PdqQuery query = parser.getQueryParams(errorList);
        if (errorList.size() > 0) {
            //TODO
        }
        //TODO Query name and tag?
        //query.setQueryName(qpd.getMessageQueryName().getText().getValue());
        //query.setQueryTag(qpd.getQueryTag().getValue());
        query.addReturnDomains(returnDomains);

        MessageHeader header = null;
        try {
            header = hl7V3Header.toMessageHeader();
        } catch (ParseException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        int recordRequestNumber = parser.getRecordRequestNumber();

        List<List<Patient>> finalPatients = new ArrayList<List<Patient>>();
        String newPointer = null;
        PdqResult pdqResult;
        int totalNumber = -1;
        int remainingNumber = -1;

        //This is the first time query, so get patients directly from EMPI.
        List<List<Patient>> allPatients = new ArrayList<List<Patient>>();

        try {
            pdqResult = pdqAdapter.findPatients(query, header);
        } catch (PdSupplierException e) {
            log.error(e.getMessage());
            throw new PixPdqException(e);
        }

        //TODO remove test code
        //MockPdqResult mck = new MockPdqResult();
        //pdqResult = mck.getPd();

        if (pdqResult == null || pdqResult.getPatients() == null) {
            log.error("Failed to get valid PDQ result.");
            throw new PixPdqException("Failed to get valid PDQ result.");
        }
        //Filter out patients by return domains.
        allPatients = getPatientList(pdqResult, returnDomains);
        totalNumber = allPatients.size();
        if (recordRequestNumber <= 0 || recordRequestNumber >= allPatients.size()) {
            finalPatients = allPatients;
            remainingNumber = 0;
        } else {
            finalPatients = getSubList(0, recordRequestNumber, allPatients);
            //add continuation pointer
            List<List<Patient>> remainingPatients = getSubList(recordRequestNumber, allPatients.size(), allPatients);
            String pointerControlId = parser.getQryId().getRoot() + ":" + parser.getQryId().getExtension();
            ContinuationPointer cp = new ContinuationPointer();
            cp.setPointer(pointerControlId);
            cp.setReturnDomain(returnDomains);
            cp.setPatients(remainingPatients);
            cp.setLastRequestTime(System.currentTimeMillis());
            cp.setTotalRecords(allPatients.size());
            //TODO is there any need for query Tag?
            //cp.setQueryTag(queryTag);
            synchronized (dscMap) {
                dscMap.put(pointerControlId, cp);
            }
            newPointer = pointerControlId;
            remainingNumber = remainingPatients.size();
        }
        if (finalPatients.size() == 0) {
            qryResponse.setQueryAck("NF", parser.getQryId().getRoot(), parser.getQryId().getExtension(), 0, 0, 0);
        } else {
            if (totalNumber == -1 || remainingNumber == -1) {
                qryResponse.setQueryAck("OK", parser.getQryId().getRoot(), parser.getQryId().getExtension(), 0, 0, 0);
            } else {
                qryResponse.setQueryAck("OK", parser.getQryId().getRoot(), parser.getQryId().getExtension(),
                        totalNumber, finalPatients.size(), remainingNumber);
            }
        }

        if (finalPatients.size() >= 1) {
            qryResponse.setPayload(finalPatients);
        }//end if found patient

        if (newPointer != null) {
            //TODO Populate DSC segment if appropriate
            //HL7v25.populateDSC(reply.getDSC(), newPointer);
        }
        qryResponse.setAck("AA", hl7V3Header.getMessageControlId());
        return qryResponse.getResponse();
    }

    private DocumentRoot processQueryContinuation(DocumentRoot msgIn) throws PixPdqException {
        QUQIIN000003UV01Type msgContinue = msgIn.getQUQIIN000003UV01();
        TransmissionWrapper hl7V3Header = new TransmissionWrapper(msgContinue);
        PdQueryParserV3 parser = new PdQueryParserV3(msgContinue, actor.getActorDescription());

        PdQueryResponseV3 qryResponse = new PdQueryResponseV3(actor.getActorDescription());
        qryResponse.setReceiver(hl7V3Header.getSendingApplication(), hl7V3Header.getSendingFacility());
        qryResponse.setSender(getServerApplication(actor.getActorDescription()).getUniversalId(),
                getServerFacility(actor.getActorDescription()).getUniversalId());
        qryResponse.setQueryByParameter(parser.getQryByParam());

        List<ErrorDetail> errorList = new ArrayList<ErrorDetail>();

        // Validate incoming message
        validateMessage(msgIn, hl7V3Header, actor.getActorDescription(), errorList);

        if (errorList.size() > 0) {
            // TODO Find the proper error ack code
            qryResponse.setAck("AE", hl7V3Header.getMessageControlId());
            // set ack detail section of the ack message with list of errors
            qryResponse.addAckDetail(errorList);
            return qryResponse.getResponse();
        }

        String pointer = parser.getContinuationPointer();
        int recordRequestNumber = parser.getRecordRequestNumber();

        List<List<Patient>> finalPatients = new ArrayList<List<Patient>>();
        String newPointer = null;
        PdqResult pdqResult;
        int totalNumber = -1;
        int remainingNumber = -1;
        if (isContinuationQueryByOpenPixPdq()) {
            if (StringUtil.goodString(pointer)) {
                //Get the patients from Cache Pointer
                if (!dscMap.containsKey(pointer)) {
                    ErrorDetail error = new ErrorDetail();
                    error.setCode("1");
                    error.setMessage("Unknown Continuation Pointer");
                    error.setLocation("//controlActProcess/queryContinuation/queryId");
                    errorList.add(error);
                    qryResponse.setAck("AE", hl7V3Header.getMessageControlId());
                    qryResponse.addAckDetail(errorList);
                    return qryResponse.getResponse();
                }
                ContinuationPointer cp = dscMap.get(pointer);
                totalNumber = cp.getTotalRecords();
                List<Identifier> returnDomains = cp.getReturnDomain();
                List<List<Patient>> allPatients = cp.getPatients();

                if (recordRequestNumber < 0 || recordRequestNumber >= allPatients.size()) {
                    finalPatients = allPatients;
                    //remove continuation pointer if no more patients available
                    synchronized (dscMap) {
                        dscMap.remove(pointer);
                    }
                    remainingNumber = 0;
                } else {
                    finalPatients = getSubList(0, recordRequestNumber, allPatients);
                    List<List<Patient>> remainingPatients = getSubList(recordRequestNumber, allPatients.size(), allPatients);
                    cp.setPatients(remainingPatients);
                    cp.setLastRequestTime(System.currentTimeMillis());
                    newPointer = pointer;
                    remainingNumber = remainingPatients.size();
                }
            } else {
                //TODO Otherwise Handle Continuation Query by EMPI                 /*
                pdqResult = getPdqResult(qpd, reply, hl7Header, outTerser, inTerser,
                        pointer, recordRequestNumber, true, returnDomains);
                if (pdqResult == null) return reply;
                finalPatients = getPatientList(pdqResult, returnDomains);
                newPointer = pdqResult.getContinuationPointer();

                //TODO: calculate totalNumber and remainingNumber
                */
            }
        }
        if (finalPatients.size() == 0) {
            qryResponse.setQueryAck("NF", parser.getQryId().getRoot(), parser.getQryId().getExtension(), 0, 0, 0);
        } else {
            if (totalNumber == -1 || remainingNumber == -1) {
                qryResponse.setQueryAck("OK", parser.getQryId().getRoot(), parser.getQryId().getExtension(), 0, 0, 0);
            } else {
                qryResponse.setQueryAck("OK", parser.getContinuationQryId().getRoot(),
                        parser.getContinuationQryId().getExtension(), totalNumber,
                        finalPatients.size(), remainingNumber);
            }
        }

        if (finalPatients.size() >= 1) {
            qryResponse.setPayload(finalPatients);
        }//end if found patient

        if (newPointer != null) {
            //TODO Populate DSC segment if appropriate
            //HL7v25.populateDSC(reply.getDSC(), newPointer);
        }
        qryResponse.setAck("AA", hl7V3Header.getMessageControlId());
        return qryResponse.getResponse();

    }

    private DocumentRoot processQueryCancel(DocumentRoot msgIn) throws PixPdqException {
        QUQIIN000003UV01Type msgCancel = msgIn.getQUQIIN000003UV01Cancel();
        TransmissionWrapper hl7V3Header = new TransmissionWrapper(msgCancel);
        PdQueryParserV3 parser = new PdQueryParserV3(msgCancel, actor.getActorDescription());

        //create v3 ack message
        MCCI_IN000002UV01Ack ack = new MCCI_IN000002UV01Ack(hl7V3Header.getMessageControlId(),
                getServerApplication(actor.getActorDescription()).getUniversalId(),
                getServerFacility(actor.getActorDescription()).getUniversalId(),
                hl7V3Header.getSendingApplication(), hl7V3Header.getSendingFacility());

        List<ErrorDetail> errorList = new ArrayList<ErrorDetail>();

        //Validate incoming message
        validateMessage(msgIn, hl7V3Header, actor.getActorDescription(), errorList);

        if (errorList.size() > 0) {
            // set ack code error AE
            ack.setAckType("AE");
            //set ack detail section of the ack message with list of errors
            ack.addAckDetail(errorList);
            return ack.getAck();
        }

        String pointerControlId = parser.getContinuationPointer();
        //TODO messageQueryName?
        //String messageQueryName= qid.getMessageQueryName().getIdentifier().getValue();
        if (isContinuationQueryByOpenPixPdq()) {
            long timeout = 600000; //defaults to 600000 millieseconds (10 minutes)
            try {
                timeout = Long.parseLong(Configuration.getPropertySetValue(actor.getActorDescription(),
                        "QueryProperties", "ContinuationPointerTimeout", false));
            } catch (IheConfigurationException e) {
                throw new PixPdqException(e);
            }

            List<String> removeList = new ArrayList<String>();
            synchronized (dscMap) {
                Set<String> keys = dscMap.keySet();
                for (String key : keys) {
                    ContinuationPointer cp = dscMap.get(key);
                    // Cancel the query matched by the pointer
                    if (cp == null || pointerControlId.equals(cp.getPointer())) {
                        //Remove the key with invalid value as well as
                        //the key whose query to be canceled
                        removeList.add(key);
                        continue;
                    }

                    //Also, clean up timed out entries. Set time out to be 10 minutes.
                    long lastTime = cp.getLastRequestTime();
                    if ((System.currentTimeMillis() - lastTime) > timeout)
                        removeList.add(key);
                }
                if (removeList.size() > 0) {
                    for (String key : removeList) {
                        if (dscMap.containsKey(key))
                            dscMap.remove(key);
                    }
                }
            }
        } else {
            try {
                //TODO queryTag ,messageQueryName?
                pdqAdapter.cancelQuery("queryTag", "messageQueryName");
            } catch (PdSupplierException e) {
                throw new PixPdqException(e);
            }
        }
        ack.setAckType("CA");
        return ack.getAck();

    }

    /*
     * Return the patient list for specified return domain otherwise it return
     * all patient list.
     *
     * @param pdqresult contains all matching patients
     * @param returnDomains the domains whose patient demographics to be
     *        returned to the PDQ consumer
     * @return List<List<Patient>> a list of filtered patient by return domains. The
     * first list for different logic patients, while the second list is for the same
     * logic patient in different domains.
     */
    private List<List<Patient>> getPatientList(PdqResult pdqResult, List<Identifier> returnDomains) {
        List<List<Patient>> finalPatients = new ArrayList<List<Patient>>();

        //pdqResult can never be null, otherwise exception would be thrown.
        List<List<Patient>> allPatients = pdqResult.getPatients();
        // List<List<Patient>> finalPatients = new ArrayList<List<Patient>>();
        if (returnDomains.size() == 0) {
            //If no return domain is specified, we consider all patients
            finalPatients = allPatients;
        } else {
            //Find a list of final patients that have ids in the return domain.
            for (List<Patient> lpatients : allPatients) {
                List<Patient> filteredPatients = new ArrayList<Patient>();
                for (Patient patient : lpatients) {
                    List<PatientIdentifier> pids = patient.getPatientIds();
                    for (PatientIdentifier pid : pids) {
                        Identifier authority = pid.getAssigningAuthority();
                        //authority might be partial (either namespaceId or universalId),
                        //so need to map to the one used in the configuration.
                        authority = AssigningAuthorityUtil.reconcileIdentifier(authority, actor.getActorDescription(), pdqAdapter);
                        if (returnDomains.contains(authority)) {
                            filteredPatients.add(patient);
                            break;
                        }
                    }
                }
                //We don't want an empty list of patient
                if (filteredPatients.size() > 0)
                    finalPatients.add(filteredPatients);
            }
        }
        return finalPatients;
    }

    /**
     * Gets the sub list from the parent list.
     *
     * @param start      the start index
     * @param end        the end index
     * @param parentList the parent list
     * @return a sublit with the given start and end indexes
     */
    private List<List<Patient>> getSubList(int start, int end, List<List<Patient>> parentList) {
        List<List<Patient>> returnList = new ArrayList<List<Patient>>();
        for (int j = start; j < end; j++) {
            List<Patient> list = parentList.get(j);
            returnList.add(list);
        }
        return returnList;
    }

    /*
     * Whether to handle Pagination (or Continuation Query) by OpenPIXPDQ or EMPI.
     *
     * @return <code>true</code> if continuation query is processed by OpenPIXPDQ;
     *         otherwise <code>false</code>. Defaults to true (namely, by OpenPIXPDQ).
     * @throw IheConfigureException if anything wrong with configuration
     */
    private boolean isContinuationQueryByOpenPixPdq() {
        boolean pagingByOpenPixPdq = true; //defaults to true(by OpenPIXPDQ).
        try {
            String pagination = Configuration.getPropertySetValue(actor.getActorDescription(),
                    "QueryProperties", "ContinuationQueryByOpenPIXPDQ", false);
            pagingByOpenPixPdq = Boolean.parseBoolean(pagination);
        } catch (IheConfigurationException e) {
            System.out.println(e);
        }
        return pagingByOpenPixPdq;
    }

}
