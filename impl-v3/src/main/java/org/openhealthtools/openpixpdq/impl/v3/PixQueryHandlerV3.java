/**
 *  Copyright (c) 2009-2011 Misys Open Source Solutions (MOSS) and others
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *  implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 *  Contributors:
 *    Misys Open Source Solutions - initial API and implementation
 *    Moin Islam                  - v3 implementation
 */
package org.openhealthtools.openpixpdq.impl.v3;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hl7.v3.DocumentRoot;
import org.hl7.v3.PRPAIN201309UV02Type;
import org.openhealthtools.openexchange.datamodel.Identifier;
import org.openhealthtools.openexchange.datamodel.MessageHeader;
import org.openhealthtools.openexchange.datamodel.PatientIdentifier;
import org.openhealthtools.openpixpdq.api.IPixManagerAdapter;
import org.openhealthtools.openpixpdq.api.PixManagerException;
import org.openhealthtools.openpixpdq.common.AssigningAuthorityUtil;
import org.openhealthtools.openpixpdq.common.PixPdqException;
import org.openhealthtools.openpixpdq.common.PixPdqFactory;

import java.text.ParseException;
import java.util.*;

class PixQueryHandlerV3 extends BaseHandlerV3 {
    private static Log log = LogFactory.getLog(PixQueryHandlerV3.class);

    /**
     * PIX Manager Adapter
     */
    private IPixManagerAdapter pixAdapter = PixPdqFactory.getPixManagerAdapter();

    /**
     * PIX Manager V3 Actor
     */
    private PixManagerV3 actor;

    /**
     * PIX Query V3 Handler
     */
    private static PixQueryHandlerV3 instance = null;

    /**
     * Singleton
     *
     * @return
     */
    synchronized static PixQueryHandlerV3 getInstance() {
        if (null == instance) {
            instance = new PixQueryHandlerV3();
        }
        return instance;
    }

    private PixQueryHandlerV3() {
    }

    void registryPixManager(PixManagerV3 actor) {
        this.actor = actor;
    }

    PixManagerV3 getPixManager() {
        return this.actor;
    }

    DocumentRoot processMessage(DocumentRoot msgIn) throws PixPdqException {

        DocumentRoot retMessage = null;

        if (null != msgIn.getPRPAIN201309UV02()) { // Get Identifiers Query
            retMessage = processQuery(msgIn);
        } else {
            log.error("Message Type not recognized");
            throw new PixPdqException("Message Type not recoginized");
        }

        return retMessage;
    }

    private DocumentRoot processQuery(DocumentRoot msgIn)
            throws PixPdqException {
        PRPAIN201309UV02Type msgQuery = msgIn.getPRPAIN201309UV02();

        TransmissionWrapper hl7V3Header = new TransmissionWrapper(msgQuery);

        PixQueryParserV3 parser = new PixQueryParserV3(msgQuery, actor.getActorDescription());

        QueryResponseV3 qryResponse = new QueryResponseV3();
        qryResponse.setReceiver(hl7V3Header.getSendingApplication(), hl7V3Header.getSendingFacility());
        qryResponse.setSender(getServerApplication(actor.getActorDescription()).getUniversalId(),
                getServerFacility(actor.getActorDescription()).getUniversalId());
        qryResponse.setQueryByParameter(parser.getQryByParam());

        List<ErrorDetail> errorList = new ArrayList<ErrorDetail>();

        // Validate incoming message
        validateMessage(msgIn, hl7V3Header, actor.getActorDescription(), errorList);

        if (errorList.size() > 0) {
            // TODO Find the proper error ack code
            qryResponse.setAck("AE", hl7V3Header.getMessageControlId());
            // set ack detail section of the ack message with list of errors
            qryResponse.addAckDetail(errorList);
            return qryResponse.getResponse();
        }

        PatientIdentifier requestPatientId = parser.getRequestPatientId();
        MessageHeader header = null;
        try {
            header = hl7V3Header.toMessageHeader();
        } catch (ParseException e) {
            throw new PixPdqException(e);
        }

        //Validate patient id domain (Case #4 in PIX specs request ID Domain is not valid)
        Identifier requestDomain = (Identifier) requestPatientId.getAssigningAuthority();

        boolean validDomain = AssigningAuthorityUtil.validateDomain(requestDomain,
                actor.getActorDescription(), PixPdqFactory.getPixManagerAdapter());

        if (!validDomain) {
            qryResponse.setAck("AE", hl7V3Header.getMessageControlId());
            qryResponse.setQueryAck("AE", parser.getQryId().getRoot(),
                    parser.getQryId().getExtension());

            ErrorDetail error = new ErrorDetail();
            error.setCode("204");
            error.setLocation("//controlActProcess/queryByParameter/parameterList/patientIdentifier/value/@root='" + requestDomain.getUniversalId() + "'");
            error.setMessage("Unknown Key Identifier - patient id request domain: " + requestDomain.getAuthorityNameString());
            qryResponse.addAckDetail(error);

            return qryResponse.getResponse();
        }

        // Validate ID itself
        try {
            boolean validPatient = pixAdapter.isValidPatient(requestPatientId, header);
            if (!validPatient) {
                qryResponse.setAck("AE", hl7V3Header.getMessageControlId());
                qryResponse.setQueryAck("AE", parser.getQryId().getRoot(),
                        parser.getQryId().getExtension());

                ErrorDetail error = new ErrorDetail();
                error.setCode("204");
                error.setLocation("//controlActProcess/queryByParameter/parameterList/patientIdentifier/value/@extension='" + requestPatientId.getId() + "'");
                error.setMessage("Unknown Key Indentifier - patient id: " + requestPatientId.getId() + " in domain " + requestDomain.getAuthorityNameString());
                qryResponse.addAckDetail(error);

                return qryResponse.getResponse();
            }
        } catch (PixManagerException e) {
            throw new PixPdqException(e);
        }

        // Validate return ID domains (Case #5 in PIX specs return ID
        // domain is valid or not)
        List<Identifier> returnDomains = parser.getReturnDomain(errorList);
        if (returnDomains == null) {
            // if return domains is null, then it means at least one return
            // domain is not recognized.
            // So just stop with a reply which contains an error message.

            qryResponse.setAck("AE", hl7V3Header.getMessageControlId());
            qryResponse.addAckDetail(errorList);
            qryResponse.setQueryAck("AE", parser.getQryId().getRoot(),
                    parser.getQryId().getExtension());
            return qryResponse.getResponse();
        }

        List<PatientIdentifier> ids = null;
        try {
            ids = pixAdapter.findPatientIds(requestPatientId, header);
        } catch (PixManagerException e) {
            throw new PixPdqException(e);
        }


        if (ids.size() >= 1) {
            List<PatientIdentifier> finalIds = null;
            if (returnDomains.size() == 0) {
                finalIds = ids;
            } else {
                finalIds = new ArrayList<PatientIdentifier>();
                for (PatientIdentifier pid : ids) {
                    // filter out patient id by return domains
                    if (returnDomains.contains(pid.getAssigningAuthority())) {
                        finalIds.add(pid);
                    }
                }
            }

            Map<Identifier, List<String>> map = new HashMap<Identifier, List<String>>();
            for (PatientIdentifier pid : finalIds) {
                // authority might be partial (either namespaceId or
                // universalId),
                // so need to map to the one used in the configuration.
                Identifier authority = AssigningAuthorityUtil
                        .reconcileIdentifier(pid.getAssigningAuthority(), actor
                                .getActorDescription(), pixAdapter);
                // In case of multiple IDs for a certain authority, we need to
                // group them together.
                List<PatientIdentifier> orderIds = new ArrayList<PatientIdentifier>();

                List<String> idList = map.get(authority);
                if (idList == null) {
                    idList = new ArrayList<String>();
                    map.put(authority, idList);
                }
                idList.add(pid.getId());
            }

            if (finalIds.size() == 0) {
                qryResponse.setQueryAck("NF", parser.getQryId().getRoot(),
                        parser.getQryId().getExtension());
            } else {
                qryResponse.setQueryAck("OK", parser.getQryId().getRoot(),
                        parser.getQryId().getExtension());

                List<PatientIdentifier> orderedIds = new ArrayList<PatientIdentifier>();
                Set<Identifier> authorities = map.keySet();
                for (Identifier authority : authorities) {
                    List<String> idNumbers = map.get(authority);
                    for (String id : idNumbers) {
                        orderedIds.add(new PatientIdentifier(id, authority));
                    }
                }
                qryResponse.setSubject(orderedIds);
            }
        } else {
            qryResponse.setQueryAck("NF", parser.getQryId().getRoot(), parser
                    .getQryId().getExtension());
        }

        // TODO: Audit Log
        // auditLog(ids, hl7Header, queryTag, msgIn);

        qryResponse.setAck("AA", hl7V3Header.getMessageControlId());
        return qryResponse.getResponse();
    }

}
