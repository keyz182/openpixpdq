/**
 *  Copyright (c) 2009-2011 Misys Open Source Solutions (MOSS) and others
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *  implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 *  Contributors:
 *    Misys Open Source Solutions - initial API and implementation
 *    Moin Islam                  - v3 implementation
 */
package org.openhealthtools.openpixpdq.impl.v3;

import org.apache.log4j.Logger;
import org.openhealthtools.openexchange.actorconfig.Configuration;
import org.openhealthtools.openexchange.actorconfig.IActorDescription;
import org.openhealthtools.openexchange.actorconfig.IheConfigurationException;
import org.openhealthtools.openexchange.actorconfig.net.IConnectionDescription;
import org.openhealthtools.openexchange.audit.IheAuditTrail;
import org.openhealthtools.openpixpdq.api.IPixManager;
import org.openhealthtools.openpixpdq.common.Constants;
import org.openhealthtools.openpixpdq.common.HL7Actor;

import java.util.Collection;

/**
 * This is the Patient Identifier Cross-referencing (PIX) Manager V3 actor,
 * the server side actor of the IHE PIX V3 profile. This actor accepts HL7 v3 messages
 * such as PRPA_IN201301UV02, PRPA_IN201302UV02 and PRPA_IN201304UV02 from a PIX Source,
 * and PRPA_IN201309UV02 from a PIX Consumer.
 * The transactions that this actor handles include PIX Feed, PIX Update, PIX Merge,
 * PIX Query and PIX Update Notification.
 *
 * @author <a href="mailto:wenzhi.li@misys.com">Wenzhi Li</a>
 * @see IPixManager
 */
public class PixManagerV3 extends HL7Actor implements IPixManager {
    /* Logger for problems during SOAP exchanges */
    private static Logger log = Logger.getLogger(PixManagerV3.class);

    /**
     * PIX Feed V3 Handler
     */
    private PixFeedHandlerV3 pixFeedHandler = null;

    /**
     * The XDS Registry Connection
     */
    private IConnectionDescription xdsRegistryConnection = null;
    /* The connections for PIX Consumers that subscribe to the PIX Update Notification*/
    private Collection<IConnectionDescription> pixConsumerConnections = null;

    /**
     * Creates a new PixManagerV3 that will talk to a PIX client over
     * the actor description supplied.
     *
     * @param actor      The actor description of this PIX manager
     * @param auditTrail The audit trail for this PIX Manager
     * @throws IheConfigurationException
     */
    public PixManagerV3(IActorDescription actor, IheAuditTrail auditTrail)
            throws IheConfigurationException {
        super(actor, auditTrail);
        this.xdsRegistryConnection = Configuration.getConnection(actor, Constants.XDS_REGISTRY, false);
        this.pixConsumerConnections = Configuration.getConnections(actor, Constants.PIX_CONSUMER, false);
    }

    @Override
    public void start() {
        //call the super one to initiate standard start process
        super.start();

        PixFeedHandlerV3 pixFeedHandler = PixPdqV3Factory.getPixFeedV3Handler();
        pixFeedHandler.registryPixManager(this);

        PixQueryHandlerV3 pixQueryHandler = PixPdqV3Factory.getPixQueryV3Handler();
        pixQueryHandler.registryPixManager(this);

        if (log.isInfoEnabled()) {
            log.info("Started PIX Manager V3: " + this.getName());
        }
    }

    @Override
    public void stop() {
        //call the super one to initiate standard stop process
        super.stop();

        if (log.isInfoEnabled()) {
            log.info("Stopped PIX Manager V3: " + this.getName());
        }
    }

    PixFeedHandlerV3 getPixFeedHandler() {
        return this.pixFeedHandler;
    }

    @Override
    public IConnectionDescription getXdsRegistryConnection() {
        return xdsRegistryConnection;
    }

    @Override
    public Collection<IConnectionDescription> getPixConsumerConnections() {
        return pixConsumerConnections;
    }

}

