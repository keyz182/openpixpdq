/**
 *  Copyright (c) 2009-2011 Misys Open Source Solutions (MOSS) and others
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *  implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 *  Contributors:
 *    Misys Open Source Solutions - initial API and implementation
 *    Moin Islam                  - v3 implementation
 */
package org.openhealthtools.openpixpdq.impl.v3;

import org.hl7.v3.*;
import org.openhealthtools.openexchange.actorconfig.IActorDescription;
import org.openhealthtools.openexchange.datamodel.Identifier;
import org.openhealthtools.openexchange.datamodel.PatientIdentifier;
import org.openhealthtools.openpixpdq.common.AssigningAuthorityUtil;
import org.openhealthtools.openpixpdq.common.PixPdqFactory;

import java.util.ArrayList;
import java.util.List;

public class PixQueryParserV3 {

    private PRPAMT201307UV02ParameterList parameterList = null;
    private PRPAMT201307UV02PatientIdentifier patientIdentifier = null;
    private PRPAMT201307UV02QueryByParameter qryByParam = null;
    private IActorDescription actor = null;

    public PixQueryParserV3(PRPAIN201309UV02Type msgQuery,
                            IActorDescription actorDescription) {

        PRPAIN201309UV02QUQIMT021001UV01ControlActProcess ctrlActPrs = msgQuery
                .getControlActProcess();

        qryByParam = ctrlActPrs.getQueryByParameter();

        if (null != qryByParam.getParameterList()) {
            parameterList = qryByParam.getParameterList();
        }

        patientIdentifier = (parameterList.getPatientIdentifier() != null) ? parameterList
                .getPatientIdentifier().get(0)
                : null;

        actor = actorDescription;
    }

    public List<Identifier> getReturnDomain(List<ErrorDetail> errorList) {
        List<Identifier> returnDomains = new ArrayList<Identifier>();
        List<PRPAMT201307UV02DataSource> dataSourceList;
        if (parameterList != null) {
            dataSourceList = parameterList.getDataSource();
            for (PRPAMT201307UV02DataSource dataSource : dataSourceList) {
                for (int i = 1; i <= dataSource.getValue().size(); i++)
                    for (II dataSourceValue : dataSource.getValue()) {
                        Identifier domain = new Identifier(null,
                                dataSourceValue.getRoot(), "ISO");

                        boolean validDomain = AssigningAuthorityUtil.validateDomain(domain,
                                actor, PixPdqFactory.getPixManagerAdapter());

                        // reconcile assigning authority
                        Identifier reconciledDomain = AssigningAuthorityUtil
                                .reconcileIdentifier(domain, actor, PixPdqFactory
                                        .getPixManagerAdapter());

                        if (validDomain) {
                            returnDomains.add(reconciledDomain);
                        } else {
                            ErrorDetail error = new ErrorDetail();
                            error.setCode("204");
                            error.setMessage("Unknow domain " + reconciledDomain.getAuthorityNameString());
                            error.setLocation("//controlActProcess/queryByParameter/parameterList/dataSource[position()="
                                    + i + "]/value/@root='" + dataSourceValue.getRoot() + "'");
                            errorList.add(error);
                            return null;
                        }
                    }
            }
        }
        return returnDomains;
    }

    public PatientIdentifier getRequestPatientId() {
        PatientIdentifier identifier = null;
        if (patientIdentifier != null
                && null != patientIdentifier.getValue().get(0)) {
            identifier = new PatientIdentifier();
            Identifier assignAuth = new Identifier(null, patientIdentifier
                    .getValue().get(0).getRoot(), "ISO");

            // reconcile assigning authority
            Identifier reconciledAssignAuth = AssigningAuthorityUtil
                    .reconcileIdentifier(assignAuth, actor, PixPdqFactory
                            .getPixManagerAdapter());

            identifier.setAssigningAuthority(reconciledAssignAuth);
            identifier.setId(patientIdentifier.getValue().get(0).getExtension());
        }

        return identifier;
    }

    public PRPAMT201307UV02QueryByParameter getQryByParam() {
        return qryByParam;

    }

    public II getQryId() {
        return getQryByParam().getQueryId();
    }
}
