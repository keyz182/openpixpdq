/**
 *  Copyright (c) 2009-2011 Misys Open Source Solutions (MOSS) and others
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *  implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 *  Contributors:
 *    Misys Open Source Solutions - initial API and implementation
 *    Moin Islam                  - v3 implementation
 */
package org.openhealthtools.openpixpdq.impl.v3;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.emf.ecore.util.FeatureMapUtil;
import org.hl7.v3.*;
import org.hl7.v3.util.V3ResourceFactoryImpl;
import org.openhealthtools.openexchange.actorconfig.Configuration;
import org.openhealthtools.openexchange.actorconfig.IheConfigurationException;
import org.openhealthtools.openexchange.actorconfig.net.IConnectionDescription;
import org.openhealthtools.openexchange.datamodel.*;
import org.openhealthtools.openexchange.utils.DateUtil;
import org.openhealthtools.openexchange.utils.StringUtil;
import org.openhealthtools.openpixpdq.common.Constants;
import org.w3c.dom.Element;

import java.util.List;
import java.util.UUID;

public class PIXUpdateMessageV3 {
    private static Log log = LogFactory.getLog(PIXUpdateMessageV3.class);

    private DocumentRoot pixUpdateMessage;
    private PRPAIN201302UV02Type rootElement;
    private PRPAIN201302UV02MFMIMT700701UV01ControlActProcess controlActProcess;


    public PIXUpdateMessageV3(IConnectionDescription connection, List<PatientIdentifier> pids, Patient patient) throws IheConfigurationException {
        //this.connection = connection;
        // The document root
        pixUpdateMessage = V3Factory.eINSTANCE.createDocumentRoot();
        rootElement = V3Factory.eINSTANCE.createPRPAIN201302UV02Type();
        rootElement.setITSVersion("XML_1.0");
        // create an id and set it
        rootElement.setId(V3util.createII(String.valueOf(UUID.randomUUID()), "", ""));
        // set current time
        rootElement.setCreationTime(V3util.createTS1CurrentTime());
        // set the interaction id for ACK
        rootElement.setInteractionId(V3util.createII("2.16.840.1.113883.1.6", "PRPA_IN201302UV02", ""));
        rootElement.setProcessingCode(V3util.createCS1("P"));
        rootElement.setProcessingModeCode(V3util.createCS1("T"));
        rootElement.setAcceptAckCode(V3util.createCS1("AL"));

        //set receiver
        setReceiver(Configuration.getIdentifier(connection, "ReceivingApplication", true).getUniversalId(),
                Configuration.getIdentifier(connection, "ReceivingFacility", true).getUniversalId());

        //set sender
        setSender(Configuration.getIdentifier(connection, "SendingApplication", true).getUniversalId(),
                Configuration.getIdentifier(connection, "SendingFacility", true).getUniversalId());

        controlActProcess = V3Factory.eINSTANCE.createPRPAIN201302UV02MFMIMT700701UV01ControlActProcess();
        controlActProcess.setCode(V3util.createCD("PRPA_TE201302UV02", null));
        controlActProcess.setClassCode(ActClassControlAct.CACT);
        controlActProcess.setMoodCode(XActMoodIntentEvent.EVN);

        // set the payload
        setPayload(pids, patient);
        rootElement.setControlActProcess(controlActProcess);
        pixUpdateMessage.setPRPAIN201302UV02(rootElement);
        log.info("create PIX UpdateMessage V3");

    }

    private void setReceiver(String recevierOID, String facilityOID) {
        rootElement.getReceiver().add(V3util.createMCCIMT000100UV01Receiver(recevierOID, facilityOID));
    }

    private void setSender(String senderOID, String facilityOID) {
        rootElement.setSender(V3util.createMCCIMT000100UV01Sender(senderOID, facilityOID));
    }

    private void setPayload(List<PatientIdentifier> pids, Patient patient) {

        PRPAIN201302UV02MFMIMT700701UV01Subject1 subject = V3Factory.eINSTANCE.createPRPAIN201302UV02MFMIMT700701UV01Subject1();
        subject.setTypeCode("SUBJ");
        controlActProcess.getSubject().add(subject);

        PRPAIN201302UV02MFMIMT700701UV01RegistrationEvent registrationEvent = V3Factory.eINSTANCE.createPRPAIN201302UV02MFMIMT700701UV01RegistrationEvent();
        registrationEvent.setClassCode(XActClassDocumentEntryAct.REG);
        registrationEvent.setMoodCode(XActMoodIntentEvent.EVN);
        registrationEvent.setStatusCode(V3util.createCS1("active"));

        PRPAIN201302UV02MFMIMT700701UV01Subject2 subject1 = V3Factory.eINSTANCE.createPRPAIN201302UV02MFMIMT700701UV01Subject2();
        subject1.setTypeCode(ParticipationTargetSubject.SBJ);


        PRPAMT201302UV02Patient patientClass = V3Factory.eINSTANCE.createPRPAMT201302UV02Patient();
        patientClass.setClassCode("PAT");
        for (PatientIdentifier patientId : pids) {
            String id = patientId.getId();
            Identifier assigningAuthority = patientId.getAssigningAuthority();
            //assigningAuthority = AssigningAuthorityUtil.reconcileIdentifier( assigningAuthority, connection.get, PixPdqFactory.getPdSupplierAdapter());
            patientClass.getId().add(V3util.createPRPAMT201302UV02PatientId(assigningAuthority.getUniversalId(), id, null));
        }
        //Set the status code to active
        patientClass.setStatusCode(V3util.createPRPAMT201302UV02PatientStatusCode("active"));

        //PRPAMT201302UV02PatientPatientPerson person = V3Factory.eINSTANCE.createPRPAMT201302UV02PatientPatientPerson();
        //TODO set null flavour
        //person.setNullFlavor(NullFlavor.NI);
        patientClass.setPatientPerson(setPatientPerson(patient));
        patientClass.setProviderOrganization(V3util.createProviderOrganization(pids.get(0).getAssigningAuthority()));

        subject1.setPatient(patientClass);

        registrationEvent.setSubject1(subject1);
        registrationEvent.setCustodian(createRegistrationCustodian(pids.get(0).getAssigningAuthority().getUniversalId(), pids.get(0).getAssigningAuthority().getAuthorityNameString()));
        subject.setRegistrationEvent(registrationEvent);

    }

    private PRPAMT201302UV02PatientPatientPerson setPatientPerson(Patient patient) {
        //PRPAMT201310UV02Person person =V3Factory.eINSTANCE.createPRPAMT201310UV02Person();
        PRPAMT201302UV02PatientPatientPerson person = V3Factory.eINSTANCE.createPRPAMT201302UV02PatientPatientPerson();
        person.setClassCode(EntityClassLivingSubjectMember1.PSN);
        person.setDeterminerCode(EntityDeterminerMember2.INSTANCE);

        PN name = V3Factory.eINSTANCE.createPN();
        if (null != patient.getPatientName().getLastName()) {
            EnFamily familyName = V3Factory.eINSTANCE.createEnFamily();
            FeatureMapUtil.addText(familyName.getMixed(), patient.getPatientName().getLastName());
            name.getFamily().add(familyName);

        }
        if (patient.getPatientName().getFirstName() != null) {
            EnGiven givenName = V3Factory.eINSTANCE.createEnGiven();
            FeatureMapUtil.addText(givenName.getMixed(), patient.getPatientName().getFirstName());
            name.getGiven().add(givenName);
        }
        // TODO second or other name
        person.getName().add(name);

        List<PhoneNumber> phones = patient.getPhoneNumbers();
        if (phones != null && phones.size() > 0) {
            System.out.println("Phone size:" + phones.size());
            for (PhoneNumber phone : phones) {
                boolean ok = false;
                String value = "tel:";
                if (StringUtil.goodString(phone.getCountryCode())) {
                    value += "+" + phone.getCountryCode() + "-";
                }
                if (StringUtil.goodString(phone.getAreaCode())) {
                    value += phone.getAreaCode() + "-";
                }
                if (StringUtil.goodString(phone.getNumber())) {
                    //replace white space with -
                    value += phone.getNumber().replace(" ", "-");
                    ok = true;
                }
                if (StringUtil.goodString(phone.getExtension())) {
                    value += ";ext=" + phone.getExtension();
                }
                if (ok) {
                    org.hl7.v3.TEL tel = V3Factory.eINSTANCE.createTEL();
                    tel.setValue(value);
                    if (phone.getType() == SharedEnums.PhoneType.WORK) {
                        tel.setUse(V3util.createEnumeratorList(WorkPlaceAddressUse.WP));
                    } else if (phone.getType() == SharedEnums.PhoneType.HOME) {
                        tel.setUse(V3util.createEnumeratorList(HomeAddressUse.H));
                    } else if (phone.getType() == SharedEnums.PhoneType.PRIMARY_HOME) {
                        tel.setUse(V3util.createEnumeratorList(HomeAddressUse.HP));
                    } else if (phone.getType() == SharedEnums.PhoneType.VACATION_HOME) {
                        tel.setUse(V3util.createEnumeratorList(HomeAddressUse.HV));
                    } else {
                        //TODO: handle other phone types and find a default phone type
                        //tel.setUse(V3util.createEnumeratorList(HomeAddressUse.H));
                    }
                    person.getTelecom().add(tel);
                }
            }
        }
        String gender = "U";
        if (patient.getAdministrativeSex() == SharedEnums.SexType.MALE) gender = "M";
        else if (patient.getAdministrativeSex() == SharedEnums.SexType.FEMALE) gender = "F";
        else if (patient.getAdministrativeSex() == SharedEnums.SexType.OTHER) gender = "O";
        //TODO check gender code
        person.setAdministrativeGenderCode(V3util.createCE(gender, null, null, null));

        List<Address> addresses = patient.getAddresses();
        if (addresses != null) {
            for (Address address : addresses) {
                person.getAddr().add(createAD(address));
            }
        }

        //Birth time
        if (patient.getBirthDateTime() != null) {
            TS1 birthTime = V3Factory.eINSTANCE.createTS1();
            String time = DateUtil.formatDateTime(patient.getBirthDateTime().getTime(), DateUtil.FORMAT_yyyyMMdd);
            birthTime.setValue(time);
            person.setBirthTime(birthTime);
        }

        //Set OtherIDs
        if (StringUtil.goodString(patient.getSsn())) {

            PRPAMT201302UV02OtherIDs otherIds = V3Factory.eINSTANCE.createPRPAMT201302UV02OtherIDs();
            otherIds.setClassCode("PAT");
            //Add ID
            otherIds.getId().add(V3util.createPRPAMT201302UV02OtherIDsId(Constants.SSN_OID, patient.getSsn(), null));
            //Ad Organization
            COCTMT150002UV01Organization org = V3Factory.eINSTANCE.createCOCTMT150002UV01Organization();
            org.setClassCode(EntityClassOrganizationMember1.ORG);
            org.setDeterminerCode(EntityDeterminerMember2.INSTANCE);
            org.getId().add(V3util.createII(Constants.SSN_OID, null, null));
            otherIds.setScopingOrganization(org);

            //Add OtherIDS
            person.getAsOtherIDs().add(otherIds);
        }

        if (patient.getMothersMaidenName() != null) {
            //Add personal relationship
            //TODO: Currently we don't have person relationship in the data model,
            // need to revisit to set the correct relationship values
            PRPAMT201302UV02PersonalRelationship relationship = V3Factory.eINSTANCE.createPRPAMT201302UV02PersonalRelationship();
            relationship.setClassCode("PRS");
            //set code
            CE code = V3Factory.eINSTANCE.createCE();
            code.setCode("MTH");
            code.setCodeSystem("2.16.840.1.113883.5.111");
            relationship.setCode(code);
            //relationship holder1
            COCTMT030007UVPerson cperson = V3Factory.eINSTANCE.createCOCTMT030007UVPerson();
            cperson.setClassCode(EntityClassLivingSubjectMember1.PSN);
            cperson.setDeterminerCode(XDeterminerInstanceKind.INSTANCE);

            //add name
            EN rname = V3Factory.eINSTANCE.createEN();
            EnFamily familyName = V3Factory.eINSTANCE.createEnFamily();
            FeatureMapUtil.addText(familyName.getMixed(), patient.getMothersMaidenName().getLastName());
            rname.getFamily().add(familyName);

            cperson.getName().add(rname);
            relationship.setRelationshipHolder1(cperson);
// Fix me 			
//			person.getPersonalRelationship().add(relationship);
        }
        return person;
    }

    private AD createAD(Address addr) {
        AD addressAD = V3Factory.eINSTANCE.createAD();

        if (addr.getAddLine1() != null) {
            AdxpStreetAddressLine streetAddress = V3Factory.eINSTANCE.createAdxpStreetAddressLine();
            FeatureMapUtil.addText(streetAddress.getMixed(), addr.getAddLine1());
            addressAD.getStreetAddressLine().add(streetAddress);
            if (addr.getAddLine2() != null) {
                FeatureMapUtil.addText(streetAddress.getMixed(), addr.getAddLine2());
                addressAD.getStreetAddressLine().add(streetAddress);
            }
        }
        if (addr.getAddCity() != null) {
            AdxpCity city = V3Factory.eINSTANCE.createAdxpCity();
            FeatureMapUtil.addText(city.getMixed(), addr.getAddCity());
            addressAD.getCity().add(city);
        }
        if (addr.getAddCountry() != null) {
            AdxpCounty county = V3Factory.eINSTANCE.createAdxpCounty();
            FeatureMapUtil.addText(county.getMixed(), addr.getAddCountry());
            addressAD.getCounty().add(county);
        }
        if (addr.getAddState() != null) {
            AdxpState state = V3Factory.eINSTANCE.createAdxpState();
            FeatureMapUtil.addText(state.getMixed(), addr.getAddState());
            addressAD.getState().add(state);
        }

        if (addr.getAddCountry() != null) {
            AdxpCountry country = V3Factory.eINSTANCE.createAdxpCountry();
            FeatureMapUtil.addText(country.getMixed(), addr.getAddCountry());
            addressAD.getCountry().add(country);
        }
        if (addr.getAddZip() != null) {
            AdxpPostalCode zipCode = V3Factory.eINSTANCE.createAdxpPostalCode();
            FeatureMapUtil.addText(zipCode.getMixed(), addr.getAddZip());
            addressAD.getPostalCode().add(zipCode);
        }
        return addressAD;
    }

    private MFMIMT700701UV01Custodian createRegistrationCustodian(String organizationOID, String organizationName) {
        log.info("Custodian orgOID:" + organizationOID + " org name:" + organizationName);
        MFMIMT700701UV01Custodian custodian = V3Factory.eINSTANCE.createMFMIMT700701UV01Custodian();
        custodian.setTypeCode(ParticipationParticipationMember8.CST);
        org.hl7.v3.COCTMT090003UV01AssignedEntity assignedEntity = V3Factory.eINSTANCE.createCOCTMT090003UV01AssignedEntity();
        custodian.setAssignedEntity(assignedEntity);
        assignedEntity.setClassCode(RoleClassAssignedEntityMember1.ASSIGNED);
        assignedEntity.getId().add(V3util.createII(organizationOID, "", ""));
        COCTMT090003UV01Organization assignedOrganization = V3Factory.eINSTANCE.createCOCTMT090003UV01Organization();
        assignedEntity.setAssignedOrganization(assignedOrganization);
        assignedOrganization.setClassCode(EntityClassOrganizationMember1.ORG);
        assignedOrganization.setDeterminerCode(EntityDeterminerMember2.INSTANCE);
        EN name = V3Factory.eINSTANCE.createEN();
        assignedOrganization.getName().add(name);
        FeatureMapUtil.addText(name.getMixed(), organizationName);
        return custodian;
    }

    public Element toDOM() {
        Element message = null;
        try {
            message = V3util.transformEmfToDom(pixUpdateMessage, new V3ResourceFactoryImpl(), org.hl7.v3.V3Package.eNS_URI);
            //message = V3util.transformEmfToDom(rootElement, new V3ResourceFactoryImpl(), org.hl7.v3.V3Package.eNS_URI);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return message;
    }

    public PRPAIN201302UV02Type getMsg() {
        return rootElement;
    }

    public DocumentRoot getDocRoot() {
        return pixUpdateMessage;
    }

}
