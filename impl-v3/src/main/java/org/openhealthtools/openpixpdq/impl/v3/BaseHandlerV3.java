/**
 *  Copyright (c) 2009-2011 Misys Open Source Solutions (MOSS) and others
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *  implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 *  Contributors:
 *    Misys Open Source Solutions - initial API and implementation
 *    Moin Islam                  - v3 implementation
 */
package org.openhealthtools.openpixpdq.impl.v3;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.hl7.v3.DocumentRoot;
import org.openhealthtools.openexchange.actorconfig.net.IBaseDescription;
import org.openhealthtools.openexchange.config.PropertyFacade;
import org.openhealthtools.openexchange.datamodel.Identifier;
import org.openhealthtools.openpixpdq.common.BaseHandler;
import org.openhealthtools.openpixpdq.common.Constants;
import org.openhealthtools.openpixpdq.common.PixPdqException;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class BaseHandlerV3 extends BaseHandler {

    /**
     * Validates the receiving application against the application
     * configured for this server.
     *
     * @param hl7V3Header the message header
     * @param description the actor or connection description where the server application is defined.
     * @param errorList   the error list holder
     * @throws PixPdqException if failed to validate
     */
    private void validateSendingReceivingApplication(TransmissionWrapper hl7V3Header,
                                                     IBaseDescription description, List<ErrorDetail> errorList) throws PixPdqException {

        if (!PropertyFacade.getBoolean(Constants.VALIDATE_RECEIVING_APPLICATION))
            return;

        //First must verify that sending and receiving application OIDs are not null
        if (null == hl7V3Header.getSendingApplication()) {
            ErrorDetail error = new ErrorDetail();
            error.setMessage("Missing Sending Application OID");
            error.setLocation("//sender/device/id/@root");
            errorList.add(error);
        }
        if (null == hl7V3Header.getReceivingApplication()) {
            ErrorDetail error = new ErrorDetail();
            error.setMessage("Missing Receiving Application OID");
            error.setLocation("//receiver/device/id/@root");
            errorList.add(error);
        }

        //Validate receiving application. Currently, we are not validating sending application
        Identifier ra = new Identifier(null, hl7V3Header.getReceivingApplication(), "ISO");

        Identifier expectedRa = getServerApplication(description);

        if (!expectedRa.equals(ra)) {
            ErrorDetail error = new ErrorDetail();
            error.setMessage("Unknow Receiving Application OID " +
                    hl7V3Header.getReceivingApplication());
            error.setLocation("//receiver/device/id/@root");
            errorList.add(error);
        }
    }

    /**
     * Validates the receiving facility against the facility
     * configured for this server.
     *
     * @param hl7V3Header the message header
     * @param description the actor or connection description where the server facility is defined.
     * @param errorList   the error list holder
     * @throws PixPdqException if failed to validate
     */
    private void validateSendingReceivingFacility(TransmissionWrapper hl7V3Header,
                                                  IBaseDescription description, List<ErrorDetail> errorList) throws PixPdqException {

        if (!PropertyFacade.getBoolean(Constants.VALIDATE_RECEIVING_FACILITY))
            return;

        //First must verify that sending and receiving facility OID is not null
        if (null == hl7V3Header.getSendingFacility()) {
            ErrorDetail error = new ErrorDetail();
            error.setMessage("Missing Sending Facility OID");
            error.setLocation("//sender/device/asAgent/representedOrganization/id/@root");
            errorList.add(error);
        }
        if (null == hl7V3Header.getReceivingFacility()) {
            ErrorDetail error = new ErrorDetail();
            error.setMessage("Missing Receiving Facility OID");
            error.setLocation("//receiver/device/asAgent/representedOrganization/id/@root");
            errorList.add(error);
        }

        //Validate receiving facility. Currently, we are not validating sending facility
        Identifier rf = new Identifier(null, hl7V3Header.getReceivingFacility(), "ISO");

        Identifier expectedRf = getServerFacility(description);

        if (!expectedRf.equals(rf)) {
            ErrorDetail error = new ErrorDetail();
            error.setMessage("Unknow Receiving Facility OID " +
                    hl7V3Header.getReceivingFacility());
            error.setLocation("//receiver/device/asAgent/representedOrganization/id/@root");
            errorList.add(error);
        }
    }

    protected void validateMessage(DocumentRoot v3Message, TransmissionWrapper hl7V3Header,
                                   IBaseDescription description, List<ErrorDetail> errorList) throws PixPdqException {
        //Validate Sending and Receiving Application
        validateSendingReceivingApplication(hl7V3Header, description, errorList);
        //Validate Sending and Receiving Facility
        validateSendingReceivingFacility(hl7V3Header, description, errorList);

        Diagnostic diagnostic = Diagnostician.INSTANCE.validate(v3Message);
        // schema level validation
        processDiagnostic(diagnostic, errorList);
        //TODO other validation
    }

    private void processDiagnostic(Diagnostic diagnostic, List<ErrorDetail> errorList) {
        Queue<Diagnostic> queue = new LinkedList<Diagnostic>();
        queue.add(diagnostic); // root
        // System.out.println("queue size:" + queue.size());
        while (!queue.isEmpty()) {
            Diagnostic d = queue.remove();
            handleDiagnostic(d, errorList);// visit
            for (Diagnostic childDiagnostic : d.getChildren()) { // process
                // successors
                queue.add(childDiagnostic);
            }
        }
    }

    private void handleDiagnostic(Diagnostic diagnostic, List<ErrorDetail> errorList) {
        // System.out.println("diag msg: " + diagnostic.getMessage());
        switch (diagnostic.getSeverity()) {
            case Diagnostic.ERROR:
                // System.out.println("ERROR: " + diagnostic.getMessage());
                ErrorDetail error = new ErrorDetail();
                //TODO set error code & error location
                error.setMessage(diagnostic.getMessage());
                errorList.add(error);
                break;
            case Diagnostic.WARNING:
                // System.out.println("WARNING: " + diagnostic.getMessage());
                break;
            case Diagnostic.INFO:
                // System.out.println("INFO: " + diagnostic.getMessage());
                break;
            case Diagnostic.OK:
                // System.out.println("OK: " + diagnostic.getMessage());
                break;
        }
    }

}
