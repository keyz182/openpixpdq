/**
 *  Copyright (c) 2009-2011 Misys Open Source Solutions (MOSS) and others
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *  implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 *  Contributors:
 *    Misys Open Source Solutions - initial API and implementation
 *    Moin Islam                  - v3 implementation
 */
package org.openhealthtools.openpixpdq.impl.v3;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.hl7.v3.DocumentRoot;
import org.hl7.v3.V3Factory;
import org.hl7.v3.V3Package;
import org.hl7.v3.util.V3ResourceFactoryImpl;
import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;

public class MainTest {
    public static void main(String[] args) {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        documentBuilderFactory.setNamespaceAware(true);
        DocumentBuilder documentBuilder;
        try {
            documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(new FileInputStream("resources/PatientAdd.xml"));
            DocumentRoot v3Message = V3Factory.eINSTANCE.createDocumentRoot();

            EPackage.Registry.INSTANCE.put(V3Package.eNS_URI, V3Package.eINSTANCE);

            v3Message = (DocumentRoot) V3util.transformDomToEmf(document.getDocumentElement(),
                    new V3ResourceFactoryImpl(), URI.createURI(org.hl7.v3.V3Package.eNS_URI), true);
            PixFeedHandlerV3 pix = PixPdqV3Factory.getPixFeedV3Handler();
            print(pix.processMessage(v3Message));

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    //print message
    private static void print(EObject v3Message) {
        Resource eResources = (new V3ResourceFactoryImpl().createResource(URI.createURI(org.hl7.v3.V3Package.eNS_URI)));
        eResources.getContents().add(v3Message);
        ByteArrayOutputStream bos = null;
        try {
            bos = new ByteArrayOutputStream();
            eResources.save(bos, null);
        } catch (IOException e) {
        } finally {
            if (bos != null) {
                try {
                    bos.close();
                } catch (IOException e) {
                }
            }
        }
        String xmlString = new String(bos.toByteArray());
        System.out.println(xmlString);
    }

}
