/**
 *  Copyright (c) 2009-2011 Misys Open Source Solutions (MOSS) and others
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *  implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 *  Contributors:
 *    Misys Open Source Solutions - initial API and implementation
 *    Moin Islam                  - v3 implementation
 */
package org.openhealthtools.openpixpdq.impl.v3;

import org.apache.log4j.Logger;
import org.apache.tools.ant.filters.StringInputStream;
import org.openhealthtools.openexchange.actorconfig.Configuration;
import org.openhealthtools.openexchange.actorconfig.net.IConnectionDescription;
import org.openhealthtools.openexchange.audit.ActiveParticipant;
import org.openhealthtools.openexchange.audit.ParticipantObject;
import org.openhealthtools.openexchange.datamodel.Identifier;
import org.openhealthtools.openexchange.datamodel.Patient;
import org.openhealthtools.openexchange.datamodel.PatientIdentifier;
import org.openhealthtools.openpixpdq.api.IMessageStoreLogger;
import org.openhealthtools.openpixpdq.api.IPixUpdateNotificationRequest;
import org.openhealthtools.openpixpdq.api.MessageStore;
import org.openhealthtools.openpixpdq.api.PixManagerException;
import org.openhealthtools.openpixpdq.common.PixPdqFactory;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;


public class PixUpdateNotificationRequestV3 implements IPixUpdateNotificationRequest {

    private static Logger log = Logger.getLogger(PixUpdateNotificationRequestV3.class);

    private PixManagerV3 actor = null;
    private List<PatientIdentifier> pids = null;
    private Patient patient;

    /**
     * Constructor
     *
     * @param actor the PIX Manager actor for this PIX Update Notification request
     * @param pids  a list of patient identifiers to be notified
     */
    PixUpdateNotificationRequestV3(PixManagerV3 actor, List<PatientIdentifier> pids, Patient patient) {
        super();
        this.actor = actor;
        this.pids = pids;
        this.patient = patient;
    }


    public void execute() {
        if (pids == null || pids.size() == 0)
            return;

        for (IConnectionDescription connection : actor.getPixConsumerConnections()) {
            boolean doNotNotify = Boolean.parseBoolean(connection.getProperty("DoNotNotify"));
            if (doNotNotify)
                continue;

            List<PatientIdentifier> idsToBeUpdated = new ArrayList<PatientIdentifier>();
            Set<Identifier> defaultDomains = Configuration.getAllDomains(actor.getActorDescription());
            Set<Identifier> domainsOfInterest = PixPdqFactory.getPixManagerAdapter().getDomainIdentifiers(defaultDomains);
            for (PatientIdentifier pid : pids) {
                if (domainsOfInterest.contains(pid.getAssigningAuthority())) {
                    idsToBeUpdated.add(pid);
                }
            }
            if (idsToBeUpdated.size() <= 0)
                continue;

            MessageStore store = null;

            try {
                PIXUpdateMessageV3 updateMessage = new PIXUpdateMessageV3(connection, idsToBeUpdated, patient);
                store = initMessageStore(updateMessage.toString(), actor.getStoreLogger(), false);

                //Populate message store header
                TransmissionWrapper header = new TransmissionWrapper(updateMessage.getMsg());
                header.populateMessageStore(store);

                log.info("Sending PIX Update Notification to " + connection.getDescription());

                HL7ChannelV3 channel = new HL7ChannelV3(connection);
                String reply = channel.sendMessage(updateMessage.toDOM());

                if (store != null) {
                    store.setInMessage(reply);
                }
                boolean ok = processPixUpdateNotificationResponse(reply, connection);

                if (ok) auditLog(header, idsToBeUpdated, connection);

            } catch (Exception e) {
                //Do not re-throw exception to let notification message continue to send to other PIX Consumers.
                String errorMsg = "Cannot send patient update notification to PIX Consumer: " + connection.getDescription();
                errorMsg += " Error Message:" + e.getMessage();
                log.error(errorMsg);
                if (store != null) {
                    store.setErrorMessage(errorMsg);
                }
            } finally {
                //Persist the message
                if (actor.getStoreLogger() != null && store != null) {
                    actor.getStoreLogger().saveLog(store);
                }
            }
        }
    }

    /**
     * Audits this PIX Update Notification message request.
     *
     * @param pids       the list of patient identifiers to notify the subscribed PIX Consumers.
     * @param connection the connection description
     */
    private void auditLog(TransmissionWrapper header, List<PatientIdentifier> pids, IConnectionDescription connection) {
        if (actor.getAuditTrail() == null) return;

        try {
            ActiveParticipant destination = new ActiveParticipant();
            Identifier receivingApplication = Configuration.getIdentifier(connection, "ReceivingApplication", true);
            Identifier receivingFacility = Configuration.getIdentifier(connection, "ReceivingFacility", true);
            destination.setUserId(receivingFacility.getAuthorityNameString() + "|" + receivingApplication.getAuthorityNameString());
            destination.setAccessPointId(Configuration.getPropertyValue(connection, "hostname", true));
            ParticipantObject patientObject = new ParticipantObject();
            patientObject.setId(pids);
            //TODO
            //patientObject.addDetail(new TypeValuePair("MSH-10", header.getMessageControlId()));
            actor.getAuditTrail().logPixUpdateNotification(destination, patientObject);
        } catch (Exception e) {
            log.error("Fail to audit log Pix Update Notification: " + e.getMessage());
        }
    }

    private boolean processPixUpdateNotificationResponse(String response, IConnectionDescription connection) throws XPathExpressionException,
            ParserConfigurationException, IOException, SAXException, PixManagerException {

        if (!getMessageType(response).equalsIgnoreCase("MCCI_IN000002UV01")) {
            //actor.logHL7MessageError(log, response, "Unexpected response from Patient Identity Consumer");
            throw new PixManagerException("Unexpected response from Patient Identity consumer \"" + connection.getDescription());
        }

        String status = getAcknowledgmentTypeCode(response);
        if ((status == null) || (!status.equalsIgnoreCase("AA") && !status.equalsIgnoreCase("CA"))) {
            // The server has rejected our request, or generated an error
            String code = getAcknowledgmentDetailCode(response);
            String text = getAcknowledgmentDetailText(response);
            String location = getAcknowledgmentDetailLocation(response);
            String error = null;
            if (code != null) error = "(" + code + ") ";
            if (text != null) error = error + " - " + text;
            if (location != null) error = " [" + location + "]";
            if (error == null) {
                //TODO?
            }

            if (error == null) error = "Unspecified error";

            error = "Error response from Patient Identity Consumer \"" + connection.getDescription() + "\": " + error;
            //actor.logHL7MessageError(log, message, error);
            throw new PixManagerException(error);

        }

        // Okay, we're good
        return true;

    }

    private MessageStore initMessageStore(String message, IMessageStoreLogger storeLogger, boolean isInbound) {
        if (storeLogger == null)
            return null;

        MessageStore ret = new MessageStore();
        if (message != null) {
            if (isInbound)
                ret.setInMessage(message);
            else
                ret.setOutMessage(message);
        }
        return ret;
    }

    private String getMessageType(String response)
            throws ParserConfigurationException, IOException, SAXException, XPathExpressionException {
        String path = "//MCCI_IN000002UV01/interactionId/@extension";
        return getValue(response, path);
    }

    private String getAcknowledgmentTypeCode(String response)
            throws ParserConfigurationException, IOException, SAXException, XPathExpressionException {
        String path = "//acknowledgement/typeCode/@code";
        return getValue(response, path);
    }

    private String getAcknowledgmentDetailCode(String response)
            throws ParserConfigurationException, IOException, SAXException, XPathExpressionException {
        String path = "//acknowledgement/acknowledgementDetail/code";
        return getValue(response, path);
    }

    private String getAcknowledgmentDetailText(String response)
            throws ParserConfigurationException, IOException, SAXException, XPathExpressionException {
        String path = "//acknowledgement/acknowledgementDetail/text";
        return getValue(response, path);
    }

    private String getAcknowledgmentDetailLocation(String response)
            throws ParserConfigurationException, IOException, SAXException, XPathExpressionException {
        String path = "//acknowledgement/acknowledgementDetail/location";
        return getValue(response, path);
    }

    private String getValue(String message, String path)
            throws ParserConfigurationException, IOException, SAXException, XPathExpressionException {
        DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
        domFactory.setNamespaceAware(false); // never forget this!
        DocumentBuilder builder = domFactory.newDocumentBuilder();
        Document doc = builder.parse(new StringInputStream(message));

        XPathFactory factory = XPathFactory.newInstance();
        XPath xpath = factory.newXPath();
        XPathExpression expr = xpath.compile(path);

        String value = (String) expr.evaluate(doc, XPathConstants.STRING);
        return value;
    }

}
