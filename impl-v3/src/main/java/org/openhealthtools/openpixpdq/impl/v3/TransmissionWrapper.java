/**
 *  Copyright (c) 2009-2011 Misys Open Source Solutions (MOSS) and others
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *  implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 *  Contributors:
 *    Misys Open Source Solutions - initial API and implementation
 *    Moin Islam                  - v3 implementation
 */
package org.openhealthtools.openpixpdq.impl.v3;

import org.hl7.v3.*;
import org.openhealthtools.openexchange.datamodel.Identifier;
import org.openhealthtools.openexchange.datamodel.MessageHeader;
import org.openhealthtools.openexchange.utils.DateUtil;
import org.openhealthtools.openexchange.utils.Pair;
import org.openhealthtools.openpixpdq.api.MessageStore;

import java.text.ParseException;


public class TransmissionWrapper {

    private String sendingApplicationOid;
    private String sendingFacilityOid;
    private String receivingApplicationOid;
    private String receivingFacilityOid;
    /**
     * Pair<String(root), String(extension)
     */
    private Pair messageControlId;
    private String messageCode;
    private String triggerEvent;
    private String messageStructure;
    private String messageDate;

    //TODO Refactor the code to reduce duplicate stuff

    public TransmissionWrapper(PRPAIN201302UV02Type updateMsg) {

        if (null != updateMsg.getId()) {
            messageControlId = new Pair(updateMsg.getId().getRoot(), updateMsg.getId().getExtension());
        }
        if (null != updateMsg.getSender().getDevice() && updateMsg.getSender().getDevice().getId().size() > 0) {

            sendingApplicationOid = updateMsg.getSender().getDevice().getId().get(0).getRoot();
        }
        if (null != updateMsg.getReceiver() && updateMsg.getReceiver().size() > 0
                && null != updateMsg.getReceiver().get(0).getDevice()
                && updateMsg.getReceiver().get(0).getDevice().getId().size() > 0) {

            receivingApplicationOid = updateMsg.getReceiver().get(0).getDevice().getId().get(0).getRoot();
        }
        if (null != updateMsg.getCreationTime()) {

            messageDate = updateMsg.getCreationTime().getValue();
        }
        if (null != updateMsg.getControlActProcess().getCode()) {

            triggerEvent = updateMsg.getControlActProcess().getCode().getCode().toString();
        }
        if (null != updateMsg.getInteractionId()) {

            messageCode = updateMsg.getInteractionId().getExtension();
        }
        if (null != updateMsg.getSender().getDevice().getAsAgent() && null != updateMsg.getSender().getDevice().getAsAgent().getRepresentedOrganization()
                && updateMsg.getSender().getDevice().getAsAgent().getRepresentedOrganization().getId().size() > 0) {

            sendingFacilityOid = updateMsg.getSender().getDevice().getAsAgent().getRepresentedOrganization().getId().get(0).getRoot();
        }

        if (null != updateMsg.getReceiver().get(0).getDevice().getAsAgent()) {

            receivingFacilityOid = updateMsg.getReceiver().get(0).getDevice().getAsAgent().getRepresentedOrganization().getId().get(0).getRoot();
        }
    }

    public TransmissionWrapper(PRPAIN201301UV02Type addMsg) {

        if (null != addMsg.getId()) {
            messageControlId = new Pair(addMsg.getId().getRoot(), addMsg.getId().getExtension());
        }
        if (null != addMsg.getSender().getDevice() && addMsg.getSender().getDevice().getId().size() > 0) {

            sendingApplicationOid = addMsg.getSender().getDevice().getId().get(0).getRoot();
        }
        if (null != addMsg.getReceiver() && addMsg.getReceiver().size() > 0
                && null != addMsg.getReceiver().get(0).getDevice()
                && addMsg.getReceiver().get(0).getDevice().getId().size() > 0) {

            receivingApplicationOid = addMsg.getReceiver().get(0).getDevice().getId().get(0).getRoot();
        }
        if (null != addMsg.getCreationTime()) {

            messageDate = addMsg.getCreationTime().getValue();
        }
        if (null != addMsg.getControlActProcess().getCode()) {

            triggerEvent = addMsg.getControlActProcess().getCode().getCode().toString();
        }
        if (null != addMsg.getInteractionId()) {

            messageCode = addMsg.getInteractionId().getExtension();
        }
        if (null != addMsg.getSender().getDevice().getAsAgent() && null != addMsg.getSender().getDevice().getAsAgent().getRepresentedOrganization()
                && addMsg.getSender().getDevice().getAsAgent().getRepresentedOrganization().getId().size() > 0) {

            sendingFacilityOid = addMsg.getSender().getDevice().getAsAgent().getRepresentedOrganization().getId().get(0).getRoot();
        }

        if (null != addMsg.getReceiver().get(0).getDevice().getAsAgent()) {

            receivingFacilityOid = addMsg.getReceiver().get(0).getDevice().getAsAgent().getRepresentedOrganization().getId().get(0).getRoot();
        }
    }

    public TransmissionWrapper(PRPAIN201304UV02Type mergeMsg) {

        if (null != mergeMsg.getId()) {
            messageControlId = new Pair(mergeMsg.getId().getRoot(), mergeMsg.getId().getExtension());
        }
        if (null != mergeMsg.getSender().getDevice() && mergeMsg.getSender().getDevice().getId().size() > 0) {

            sendingApplicationOid = mergeMsg.getSender().getDevice().getId().get(0).getRoot();
        }
        if (null != mergeMsg.getReceiver() && mergeMsg.getReceiver().size() > 0
                && null != mergeMsg.getReceiver().get(0).getDevice()
                && mergeMsg.getReceiver().get(0).getDevice().getId().size() > 0) {

            receivingApplicationOid = mergeMsg.getReceiver().get(0).getDevice().getId().get(0).getRoot();
        }
        if (null != mergeMsg.getCreationTime()) {

            messageDate = mergeMsg.getCreationTime().getValue();
        }
        if (null != mergeMsg.getControlActProcess().getCode()) {

            triggerEvent = mergeMsg.getControlActProcess().getCode().getCode().toString();
        }
        if (null != mergeMsg.getInteractionId()) {

            messageCode = mergeMsg.getInteractionId().getExtension();
        }
        if (null != mergeMsg.getSender().getDevice().getAsAgent() && null != mergeMsg.getSender().getDevice().getAsAgent().getRepresentedOrganization()
                && mergeMsg.getSender().getDevice().getAsAgent().getRepresentedOrganization().getId().size() > 0) {

            sendingFacilityOid = mergeMsg.getSender().getDevice().getAsAgent().getRepresentedOrganization().getId().get(0).getRoot();
        }

        if (null != mergeMsg.getReceiver().get(0).getDevice().getAsAgent()) {
            receivingFacilityOid = mergeMsg.getReceiver().get(0).getDevice().getAsAgent().getRepresentedOrganization().getId().get(0).getRoot();
        }

    }

    public TransmissionWrapper(PRPAIN201309UV02Type pixQryMsg) {

        if (null != pixQryMsg.getId()) {

            messageControlId = new Pair(pixQryMsg.getId().getRoot(), pixQryMsg.getId().getExtension());
        }
        if (null != pixQryMsg.getSender().getDevice() && pixQryMsg.getSender().getDevice().getId().size() > 0) {

            sendingApplicationOid = pixQryMsg.getSender().getDevice().getId().get(0).getRoot();
        }
        if (null != pixQryMsg.getReceiver() && pixQryMsg.getReceiver().size() > 0
                && null != pixQryMsg.getReceiver().get(0).getDevice()
                && pixQryMsg.getReceiver().get(0).getDevice().getId().size() > 0) {

            receivingApplicationOid = pixQryMsg.getReceiver().get(0).getDevice().getId().get(0).getRoot();
        }
        if (null != pixQryMsg.getCreationTime()) {

            messageDate = pixQryMsg.getCreationTime().getValue();
        }
        if (null != pixQryMsg.getControlActProcess().getCode()) {

            triggerEvent = pixQryMsg.getControlActProcess().getCode().getCode().toString();
        }
        if (null != pixQryMsg.getInteractionId()) {

            messageCode = pixQryMsg.getInteractionId().getExtension();
        }
        if (null != pixQryMsg.getSender().getDevice().getAsAgent() && null != pixQryMsg.getSender().getDevice().getAsAgent().getRepresentedOrganization()
                && pixQryMsg.getSender().getDevice().getAsAgent().getRepresentedOrganization().getId().size() > 0) {

            sendingFacilityOid = pixQryMsg.getSender().getDevice().getAsAgent().getRepresentedOrganization().getId().get(0).getRoot();
        }

        if (null != pixQryMsg.getReceiver().get(0).getDevice().getAsAgent()) {

            receivingFacilityOid = pixQryMsg.getReceiver().get(0).getDevice().getAsAgent().getRepresentedOrganization().getId().get(0).getRoot();
        }

    }

    public TransmissionWrapper(PRPAIN201305UV02Type pdQryMsg) {

        if (null != pdQryMsg.getId()) {

            messageControlId = new Pair(pdQryMsg.getId().getRoot(), pdQryMsg.getId().getExtension());
        }
        if (null != pdQryMsg.getSender().getDevice() && pdQryMsg.getSender().getDevice().getId().size() > 0) {

            sendingApplicationOid = pdQryMsg.getSender().getDevice().getId().get(0).getRoot();
        }
        if (null != pdQryMsg.getReceiver() && pdQryMsg.getReceiver().size() > 0
                && null != pdQryMsg.getReceiver().get(0).getDevice()
                && pdQryMsg.getReceiver().get(0).getDevice().getId().size() > 0) {

            receivingApplicationOid = pdQryMsg.getReceiver().get(0).getDevice().getId().get(0).getRoot();
        }
        if (null != pdQryMsg.getCreationTime()) {

            messageDate = pdQryMsg.getCreationTime().getValue();
        }
        if (null != pdQryMsg.getControlActProcess().getCode()) {

            triggerEvent = pdQryMsg.getControlActProcess().getCode().getCode().toString();
        }
        if (null != pdQryMsg.getInteractionId()) {

            messageCode = pdQryMsg.getInteractionId().getExtension();
        }
        if (null != pdQryMsg.getSender().getDevice().getAsAgent() && null != pdQryMsg.getSender().getDevice().getAsAgent().getRepresentedOrganization()
                && pdQryMsg.getSender().getDevice().getAsAgent().getRepresentedOrganization().getId().size() > 0) {

            sendingFacilityOid = pdQryMsg.getSender().getDevice().getAsAgent().getRepresentedOrganization().getId().get(0).getRoot();
        }

        if (null != pdQryMsg.getReceiver().get(0).getDevice().getAsAgent()) {

            receivingFacilityOid = pdQryMsg.getReceiver().get(0).getDevice().getAsAgent().getRepresentedOrganization().getId().get(0).getRoot();
        }

    }

    public TransmissionWrapper(QUQIIN000003UV01Type pdQryCancel) {

        if (null != pdQryCancel.getId()) {

            messageControlId = new Pair(pdQryCancel.getId().getRoot(), pdQryCancel.getId().getExtension());
        }
        if (null != pdQryCancel.getSender().getDevice() && pdQryCancel.getSender().getDevice().getId().size() > 0) {

            sendingApplicationOid = pdQryCancel.getSender().getDevice().getId().get(0).getRoot();
        }
        if (null != pdQryCancel.getReceiver() && pdQryCancel.getReceiver().size() > 0
                && null != pdQryCancel.getReceiver().get(0).getDevice()
                && pdQryCancel.getReceiver().get(0).getDevice().getId().size() > 0) {

            receivingApplicationOid = pdQryCancel.getReceiver().get(0).getDevice().getId().get(0).getRoot();
        }
        if (null != pdQryCancel.getCreationTime()) {

            messageDate = pdQryCancel.getCreationTime().getValue();
        }
        if (null != pdQryCancel.getControlActProcess().getCode()) {

            triggerEvent = pdQryCancel.getControlActProcess().getCode().getCode().toString();
        }
        if (null != pdQryCancel.getInteractionId()) {

            messageCode = pdQryCancel.getInteractionId().getExtension();
        }
        if (null != pdQryCancel.getSender().getDevice().getAsAgent() && null != pdQryCancel.getSender().getDevice().getAsAgent().getRepresentedOrganization()
                && pdQryCancel.getSender().getDevice().getAsAgent().getRepresentedOrganization().getId().size() > 0) {

            sendingFacilityOid = pdQryCancel.getSender().getDevice().getAsAgent().getRepresentedOrganization().getId().get(0).getRoot();
        }

        if (null != pdQryCancel.getReceiver().get(0).getDevice().getAsAgent()) {

            receivingFacilityOid = pdQryCancel.getReceiver().get(0).getDevice().getAsAgent().getRepresentedOrganization().getId().get(0).getRoot();
        }

    }

    /**
     * This method returns the sending Application Identifier
     *
     * @return the sending application of this message header
     */
    public String getSendingApplication() {
        return sendingApplicationOid;
    }

    /**
     * This method returns the sending Facility Identifier
     *
     * @return the sending facility of this message header
     */
    public String getSendingFacility() {
        return sendingFacilityOid;
    }

    /**
     * This method returns the Receiving Application Identifier
     *
     * @return the receiving application of this message header
     */
    public String getReceivingApplication() {
        return receivingApplicationOid;
    }

    /**
     * This method returns the Receiving Facility Identifier
     *
     * @return the receiving facility of this message header
     */
    public String getReceivingFacility() {
        return receivingFacilityOid;
    }

    /**
     * This method returns the MessageControl Id
     *
     * @return the message control id of this message header in
     *         Pair<String(root), String(extension).
     */
    public Pair getMessageControlId() {
        return messageControlId;
    }

    /**
     * This method returns the Message date
     *
     * @return the message date
     */

    public String getMessagedate() {
        return messageDate;
    }


    /**
     * Populates <code>MessageHeader</code> from this HL7Header.
     * This message should be invoked after this HL7Header
     * object is fully initialized.
     *
     * @return the same <code>MessageHeader</code> object with populated data
     * @throws ParseException
     */
    public MessageHeader toMessageHeader() throws ParseException {
        MessageHeader header = new MessageHeader();
        header.setSendingFacility(new Identifier(null, this.getSendingFacility(), "ISO"));
        header.setSendingApplication(new Identifier(null, this.getSendingApplication(), "ISO"));
        header.setReceivingFacility(new Identifier(null, this.getReceivingApplication(), "ISO"));
        header.setReceivingApplication(new Identifier(null, this.getReceivingFacility(), "ISO"));
        header.setMessageCode(this.getMessageCode());
        header.setTriggerEvent(this.getTriggerEvent());
        header.setMessgeDate(DateUtil.parseCalendar(messageDate, "yyyyMMddHHmmss"));
        return header;
    }

    /**
     * Populates <code>MessageStore</code> from this HL7Header.
     * This message should be invoked after this HL7Header
     * object is fully initialized.
     *
     * @param store the <code>MessageStore</code> to be populated
     * @return the same <code>MessageStore</code> object with populated data
     */
    public MessageStore populateMessageStore(MessageStore store) {
        if (store == null) return null;
        //TODO
        //store.setMessageId( this.getMessageControlId() );
        store.setSendingFacility(this.getSendingFacility() == null ? null : new Identifier(null, this.getSendingFacility(), "ISO").getAuthorityNameString());
        store.setSendingApplication(this.getSendingApplication() == null ? null : new Identifier(null, this.getSendingApplication(), "ISO").getAuthorityNameString());
        store.setReceivingFacility(this.getReceivingFacility() == null ? null : new Identifier(null, this.getReceivingFacility(), "ISO").getAuthorityNameString());
        store.setReceivingApplication(this.getReceivingApplication() == null ? null : new Identifier(null, this.getReceivingApplication(), "ISO").getAuthorityNameString());
        store.setMessageCode(this.getMessageCode());
        store.setTriggerEvent(this.getTriggerEvent());
        //store.setMessageStructure(this.getMessageStructure());
        store.setMessageDate(DateUtil.convertHL7Date(this.getMessagedate()));
        return store;
    }

    /**
     * Gets the message code
     *
     * @return the message code
     */
    public String getMessageCode() {
        return messageCode;
    }

    /**
     * Gets the trigger event
     *
     * @return the trigger event
     */
    public String getTriggerEvent() {
        return triggerEvent;
    }
}
