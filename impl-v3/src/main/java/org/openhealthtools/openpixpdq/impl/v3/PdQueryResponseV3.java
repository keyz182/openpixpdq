/**
 *  Copyright (c) 2009-2011 Misys Open Source Solutions (MOSS) and others
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *  implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 *  Contributors:
 *    Misys Open Source Solutions - initial API and implementation
 *    Moin Islam                  - v3 implementation
 */
package org.openhealthtools.openpixpdq.impl.v3;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.emf.ecore.util.FeatureMapUtil;
import org.hl7.v3.*;
import org.openhealthtools.openexchange.actorconfig.IActorDescription;
import org.openhealthtools.openexchange.datamodel.*;
import org.openhealthtools.openexchange.utils.DateUtil;
import org.openhealthtools.openexchange.utils.Pair;
import org.openhealthtools.openexchange.utils.StringUtil;
import org.openhealthtools.openpixpdq.common.AssigningAuthorityUtil;
import org.openhealthtools.openpixpdq.common.Constants;
import org.openhealthtools.openpixpdq.common.PixPdqFactory;

import java.math.BigInteger;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

public class PdQueryResponseV3 {
    private static Log log = LogFactory.getLog(PdQueryResponseV3.class);

    private DocumentRoot pdQueryResponseMessage;
    private PRPAIN201306UV02Type rootElement;
    private PRPAIN201306UV02MFMIMT700711UV01ControlActProcess controlActProcess;
    private MCCIMT000300UV01Acknowledgement ack;
    private IActorDescription actor = null;

    public PdQueryResponseV3(IActorDescription actorDescription) {
        // The document root
        pdQueryResponseMessage = V3Factory.eINSTANCE.createDocumentRoot();
        rootElement = V3Factory.eINSTANCE.createPRPAIN201306UV02Type();
        rootElement.setITSVersion("XML_1.0");
        // create an id and set it
        rootElement.setId(V3util.createII(String.valueOf(UUID.randomUUID()), "", ""));
        // set current time
        rootElement.setCreationTime(V3util.createTS1CurrentTime());
        // set the interaction id for ACK
        rootElement.setInteractionId(V3util.createII("2.16.840.1.113883.1.6", "PRPA_IN201306UV02", ""));
        rootElement.setProcessingCode(V3util.createCS1("P"));
        rootElement.setProcessingModeCode(V3util.createCS1("T"));
        rootElement.setAcceptAckCode(V3util.createCS1("NE"));

        controlActProcess = V3Factory.eINSTANCE.createPRPAIN201306UV02MFMIMT700711UV01ControlActProcess();
        controlActProcess.setCode(V3util.createCD("PRPA_TE201306UV02", null));
        controlActProcess.setClassCode(ActClassControlAct.CACT);
        controlActProcess.setMoodCode(XActMoodIntentEvent.EVN);

        rootElement.setControlActProcess(controlActProcess);
        pdQueryResponseMessage.setPRPAIN201306UV02(rootElement);

        actor = actorDescription;

    }

    public void setPayload(List<List<Patient>> finalPatients) {

        for (int patientIndex = 0; patientIndex < finalPatients.size(); patientIndex++) {
            List<Patient> patientRecord = finalPatients.get(patientIndex);
            //We grab the first patient descriptor as the patient demograhpics.
            Patient patient = patientRecord.get(0);
            PRPAIN201306UV02MFMIMT700711UV01Subject1 subject = getSubject();
            PRPAIN201306UV02MFMIMT700711UV01RegistrationEvent regEvn = getRegistrationEvent();
            regEvn.setCustodian(createRegistrationCustodian(patient.getPatientIds().get(0).getAssigningAuthority().getUniversalId(),
                    patient.getPatientIds().get(0).getAssigningAuthority().getAuthorityNameString()));

            PRPAMT201310UV02Patient patientClass = setPatient(patient);
            patientClass.getSubjectOf1().add(setQueryMatch(100));
            for (int i = 1; i < patientRecord.size(); i++) { //has to start with the second one, the first was handled above
                Patient pd = (Patient) patientRecord.get(i);
                List<PatientIdentifier> patientIds = pd.getPatientIds();
                for (PatientIdentifier patientId : patientIds) {
                    String id = patientId.getId();
                    Identifier assigningAuthority = patientId.getAssigningAuthority();
                    assigningAuthority = AssigningAuthorityUtil.reconcileIdentifier(assigningAuthority, actor, PixPdqFactory.getPdSupplierAdapter());
                    setPatientId(patientClass, id, assigningAuthority);
                }
            }

            PRPAIN201306UV02MFMIMT700711UV01Subject2 subjectOf1 = getSubjectOf1();
            subjectOf1.setPatient(patientClass);
            regEvn.setSubject1(subjectOf1);
            subject.setRegistrationEvent(regEvn);
        }

    }

    private PRPAIN201306UV02MFMIMT700711UV01Subject1 getSubject() {
        PRPAIN201306UV02MFMIMT700711UV01Subject1 subject = V3Factory.eINSTANCE.createPRPAIN201306UV02MFMIMT700711UV01Subject1();
        subject.setTypeCode("SUBJ");
        controlActProcess.getSubject().add(subject);

        return subject;
    }

    private PRPAIN201306UV02MFMIMT700711UV01RegistrationEvent getRegistrationEvent() {
        //Add registrationEvent
        PRPAIN201306UV02MFMIMT700711UV01RegistrationEvent registrationEvent = V3Factory.eINSTANCE.createPRPAIN201306UV02MFMIMT700711UV01RegistrationEvent();
        registrationEvent.setClassCode(XActClassDocumentEntryAct.REG);
        registrationEvent.setMoodCode(XActMoodIntentEvent.EVN);
        registrationEvent.setStatusCode(V3util.createCS1("active"));

        return registrationEvent;
    }

    private PRPAIN201306UV02MFMIMT700711UV01Subject2 getSubjectOf1() {
        PRPAIN201306UV02MFMIMT700711UV01Subject2 subject1 = V3Factory.eINSTANCE.createPRPAIN201306UV02MFMIMT700711UV01Subject2();
        subject1.setTypeCode(ParticipationTargetSubject.SBJ);
        return subject1;
    }

    private MFMIMT700711UV01Custodian createRegistrationCustodian(String organizationOID, String organizationName) {
        MFMIMT700711UV01Custodian custodian = V3Factory.eINSTANCE.createMFMIMT700711UV01Custodian();
        custodian.setTypeCode(ParticipationParticipationMember8.CST);
        org.hl7.v3.COCTMT090003UV01AssignedEntity assignedEntity = V3Factory.eINSTANCE.createCOCTMT090003UV01AssignedEntity();
        custodian.setAssignedEntity(assignedEntity);
        assignedEntity.setClassCode(RoleClassAssignedEntityMember1.ASSIGNED);
        assignedEntity.getId().add(V3util.createII(organizationOID, "", ""));
        COCTMT090003UV01Organization assignedOrganization = V3Factory.eINSTANCE.createCOCTMT090003UV01Organization();
        assignedEntity.setAssignedOrganization(assignedOrganization);
        assignedOrganization.setClassCode(EntityClassOrganizationMember1.ORG);
        assignedOrganization.setDeterminerCode(EntityDeterminerMember2.INSTANCE);
        EN name = V3Factory.eINSTANCE.createEN();
        assignedOrganization.getName().add(name);
        FeatureMapUtil.addText(name.getMixed(), organizationName);
        return custodian;
    }

    private void setPatientId(PRPAMT201310UV02Patient patient, String id, Identifier assigningAuthority) {
        patient.getId().add(V3util.createII(assigningAuthority.getUniversalId(), id, null));
    }

    private PRPAMT201310UV02Patient setPatient(Patient patient) {
        PRPAMT201310UV02Patient patientClass = V3Factory.eINSTANCE.createPRPAMT201310UV02Patient();
        patientClass.setClassCode("PAT");
        List<PatientIdentifier> patientIds = patient.getPatientIds();
        for (PatientIdentifier patientId : patientIds) {
            String id = patientId.getId();
            Identifier assigningAuthority = patientId.getAssigningAuthority();
            assigningAuthority = AssigningAuthorityUtil.reconcileIdentifier(assigningAuthority, actor, PixPdqFactory.getPdSupplierAdapter());
            patientClass.getId().add(V3util.createII(assigningAuthority.getUniversalId(), id, null));
        }

        //Set the status code to active
        CS1 status = V3Factory.eINSTANCE.createCS1();
        status.setCode("active");
        patientClass.setStatusCode(status);

        patientClass.setPatientPerson(setPatientPerson(patient));
        patientClass.setProviderOrganization(createProviderOrganization(patientIds.get(0).getAssigningAuthority()));

        return patientClass;
    }

    //TODO: need to revisit ProviderOrganization to get the correct data
    private COCTMT150003UV03Organization createProviderOrganization(Identifier asigningAuthority) {
        COCTMT150003UV03Organization organization = V3Factory.eINSTANCE.createCOCTMT150003UV03Organization();
        organization.setClassCode(EntityClassOrganizationMember1.ORG);
        organization.setDeterminerCode(EntityDeterminerMember2.INSTANCE);

        //Add Id
        organization.getId().add(V3util.createII(asigningAuthority.getUniversalId(), "", ""));

        //add name
        ON name = V3Factory.eINSTANCE.createON();
        FeatureMapUtil.addText(name.getMixed(), asigningAuthority.getNamespaceId());
        organization.getName().add(name);

        //Add contactParty
        COCTMT150003UV03ContactParty contactParty = V3Factory.eINSTANCE.createCOCTMT150003UV03ContactParty();
        contactParty.setClassCode(RoleClassContact.CON);
        organization.getContactParty().add(contactParty);

        return organization;
    }

    private PRPAMT201310UV02Subject setQueryMatch(int degreeMatch) {
        PRPAMT201310UV02Subject subjectOf1 = V3Factory.eINSTANCE.createPRPAMT201310UV02Subject();
        PRPAMT201310UV02QueryMatchObservation match = V3Factory.eINSTANCE.createPRPAMT201310UV02QueryMatchObservation();
        //TODO: use "COND" instead of CNOD.
        match.setClassCode(ActClassConditionMember1.COND);
        match.setMoodCode(XActMoodIntentEvent.EVN);
        match.setCode(V3util.createCD("IHE_PDQ", null));
        subjectOf1.setQueryMatchObservation(match);
        // TODO set degree macth
        INT1 value = V3Factory.eINSTANCE.createINT1();
        value.setValue(BigInteger.valueOf(degreeMatch));
        match.setValue(value);
        return subjectOf1;
    }

    public PRPAMT201310UV02Person setPatientPerson(Patient patient) {
        PRPAMT201310UV02Person person = V3Factory.eINSTANCE.createPRPAMT201310UV02Person();
        person.setClassCode(EntityClassLivingSubjectMember1.PSN);
        person.setDeterminerCode(EntityDeterminerMember2.INSTANCE);

        PN name = V3Factory.eINSTANCE.createPN();
        if (null != patient.getPatientName().getLastName()) {
            EnFamily familyName = V3Factory.eINSTANCE.createEnFamily();
            FeatureMapUtil.addText(familyName.getMixed(), patient.getPatientName().getLastName());
            name.getFamily().add(familyName);

        }
        if (patient.getPatientName().getFirstName() != null) {
            EnGiven givenName = V3Factory.eINSTANCE.createEnGiven();
            FeatureMapUtil.addText(givenName.getMixed(), patient.getPatientName().getFirstName());
            name.getGiven().add(givenName);
        }
        // TODO second or other name
        person.getName().add(name);

        List<PhoneNumber> phones = patient.getPhoneNumbers();
        if (phones != null && phones.size() > 0) {
            System.out.println("Phone size:" + phones.size());
            for (PhoneNumber phone : phones) {
                boolean ok = false;
                String value = "tel:";
                if (StringUtil.goodString(phone.getCountryCode())) {
                    value += "+" + phone.getCountryCode() + "-";
                }
                if (StringUtil.goodString(phone.getAreaCode())) {
                    value += phone.getAreaCode() + "-";
                }
                if (StringUtil.goodString(phone.getNumber())) {
                    //replace white space with -
                    value += phone.getNumber().replace(" ", "-");
                    ok = true;
                }
                if (StringUtil.goodString(phone.getExtension())) {
                    value += ";ext=" + phone.getExtension();
                }
                if (ok) {
                    org.hl7.v3.TEL tel = V3Factory.eINSTANCE.createTEL();
                    tel.setValue(value);
                    if (phone.getType() == SharedEnums.PhoneType.WORK) {
                        tel.setUse(V3util.createEnumeratorList(WorkPlaceAddressUse.WP));
                    } else if (phone.getType() == SharedEnums.PhoneType.HOME) {
                        tel.setUse(V3util.createEnumeratorList(HomeAddressUse.H));
                    } else if (phone.getType() == SharedEnums.PhoneType.PRIMARY_HOME) {
                        tel.setUse(V3util.createEnumeratorList(HomeAddressUse.HP));
                    } else if (phone.getType() == SharedEnums.PhoneType.VACATION_HOME) {
                        tel.setUse(V3util.createEnumeratorList(HomeAddressUse.HV));
                    } else {
                        //TODO: handle other phone types and find a default phone type
                        //tel.setUse(V3util.createEnumeratorList(HomeAddressUse.H));
                    }
                    person.getTelecom().add(tel);
                }
            }
        }
        String gender = "U";
        if (patient.getAdministrativeSex() == SharedEnums.SexType.MALE) gender = "M";
        else if (patient.getAdministrativeSex() == SharedEnums.SexType.FEMALE) gender = "F";
        else if (patient.getAdministrativeSex() == SharedEnums.SexType.OTHER) gender = "O";
        //TODO check gender code
        person.setAdministrativeGenderCode(V3util.createCE(gender, null, null, null));

        List<Address> addresses = patient.getAddresses();
        if (addresses != null) {
            for (Address address : addresses) {
                person.getAddr().add(createAD(address));
            }
        }

        //Birth time
        if (patient.getBirthDateTime() != null) {
            TS1 birthTime = V3Factory.eINSTANCE.createTS1();
            String time = DateUtil.formatDateTime(patient.getBirthDateTime().getTime(), DateUtil.FORMAT_yyyyMMdd);
            birthTime.setValue(time);
            person.setBirthTime(birthTime);
        }

        //Set OtherIDs
        if (StringUtil.goodString(patient.getSsn())) {

            PRPAMT201310UV02OtherIDs otherIds = V3Factory.eINSTANCE.createPRPAMT201310UV02OtherIDs();
            otherIds.setClassCode("PAT");
            //Add ID
            otherIds.getId().add(V3util.createII(Constants.SSN_OID, patient.getSsn(), null));
            //Ad Organization
            COCTMT150002UV01Organization org = V3Factory.eINSTANCE.createCOCTMT150002UV01Organization();
            org.setClassCode(EntityClassOrganizationMember1.ORG);
            org.setDeterminerCode(EntityDeterminerMember2.INSTANCE);
            org.getId().add(V3util.createII(Constants.SSN_OID, null, null));
            otherIds.setScopingOrganization(org);

            //Add OtherIDS
            person.getAsOtherIDs().add(otherIds);
        }

        if (patient.getMothersMaidenName() != null) {
            //Add personal relationship
            //TODO: Currently we don't have person relationship in the data model,
            // need to revisit to set the correct relationship values
            PRPAMT201310UV02PersonalRelationship relationship = V3Factory.eINSTANCE.createPRPAMT201310UV02PersonalRelationship();
            relationship.setClassCode("PRS");
            //set code
            CE code = V3Factory.eINSTANCE.createCE();
            code.setCode("MTH");
            code.setCodeSystem("2.16.840.1.113883.5.111");
            relationship.setCode(code);
            //relationship holder1
            COCTMT030007UVPerson cperson = V3Factory.eINSTANCE.createCOCTMT030007UVPerson();
            cperson.setClassCode(EntityClassLivingSubjectMember1.PSN);
            cperson.setDeterminerCode(XDeterminerInstanceKind.INSTANCE);

            //add name
            EN rname = V3Factory.eINSTANCE.createEN();
            EnFamily familyName = V3Factory.eINSTANCE.createEnFamily();
            FeatureMapUtil.addText(familyName.getMixed(), patient.getMothersMaidenName().getLastName());
            rname.getFamily().add(familyName);

            cperson.getName().add(rname);
            relationship.setRelationshipHolder1(cperson);
            person.getPersonalRelationship().add(relationship);
        }
        return person;
    }

    private AD createAD(Address addr) {
        AD addressAD = V3Factory.eINSTANCE.createAD();

        if (addr.getAddLine1() != null) {
            AdxpStreetAddressLine streetAddress = V3Factory.eINSTANCE.createAdxpStreetAddressLine();
            FeatureMapUtil.addText(streetAddress.getMixed(), addr.getAddLine1());
            addressAD.getStreetAddressLine().add(streetAddress);
            if (addr.getAddLine2() != null) {
                FeatureMapUtil.addText(streetAddress.getMixed(), addr.getAddLine2());
                addressAD.getStreetAddressLine().add(streetAddress);
            }
        }
        if (addr.getAddCity() != null) {
            AdxpCity city = V3Factory.eINSTANCE.createAdxpCity();
            FeatureMapUtil.addText(city.getMixed(), addr.getAddCity());
            addressAD.getCity().add(city);
        }
        if (addr.getAddCountry() != null) {
            AdxpCounty county = V3Factory.eINSTANCE.createAdxpCounty();
            FeatureMapUtil.addText(county.getMixed(), addr.getAddCountry());
            addressAD.getCounty().add(county);
        }
        if (addr.getAddState() != null) {
            AdxpState state = V3Factory.eINSTANCE.createAdxpState();
            FeatureMapUtil.addText(state.getMixed(), addr.getAddState());
            addressAD.getState().add(state);
        }

        if (addr.getAddCountry() != null) {
            AdxpCountry country = V3Factory.eINSTANCE.createAdxpCountry();
            FeatureMapUtil.addText(country.getMixed(), addr.getAddCountry());
            addressAD.getCountry().add(country);
        }
        if (addr.getAddZip() != null) {
            AdxpPostalCode zipCode = V3Factory.eINSTANCE.createAdxpPostalCode();
            FeatureMapUtil.addText(zipCode.getMixed(), addr.getAddZip());
            addressAD.getPostalCode().add(zipCode);
        }
        return addressAD;
    }

    public void setQueryAck(String responseCode, String qryIdRoot, String qryIdExt,
                            int totalQty, int currentQty, int remainingQty) {
        MFMIMT700711UV01QueryAck qryAck = V3Factory.eINSTANCE.createMFMIMT700711UV01QueryAck();
        qryAck.setQueryId(V3util.createII(qryIdRoot, qryIdExt, null));
        // OK = data found; NF = data not found
        qryAck.setQueryResponseCode(V3util.createCS1(responseCode));

        //Set the total number
        if (totalQty >= 0) {
            INT1 total = V3Factory.eINSTANCE.createINT1();
            total.setValue(BigInteger.valueOf(totalQty));
            qryAck.setResultTotalQuantity(total);
        }

        //Set the current number
        if (currentQty >= 0) {
            INT1 current = V3Factory.eINSTANCE.createINT1();
            current.setValue(BigInteger.valueOf(currentQty));
            qryAck.setResultCurrentQuantity(current);
        }

        //Set the remaining number
        if (remainingQty >= 0) {
            INT1 remaining = V3Factory.eINSTANCE.createINT1();
            remaining.setValue(BigInteger.valueOf(remainingQty));
            qryAck.setResultRemainingQuantity(remaining);
        }

        //INT1
        controlActProcess.setQueryAck(qryAck);
    }

    public void setReceiver(String recevierOID, String facilityOID) {
        rootElement.getReceiver().add(V3util.createMCCIMT000300UV01Receiver(recevierOID, facilityOID));
    }

    public void setQueryByParameter(PRPAMT201306UV02QueryByParameter qryByParam) {
        // set the QueryByParameter element from request message
        controlActProcess.setQueryByParameter(qryByParam);
    }

    public void setSender(String senderOID, String facilityOID) {
        rootElement.setSender(V3util.createMCCIMT000300UV01Sender(senderOID, facilityOID));
    }

    public void setAck(String ackTypeCode, Pair<String, String> OrgMsg) {
        ack = V3Factory.eINSTANCE.createMCCIMT000300UV01Acknowledgement();
        //set ack type code based on the cases defined in ITI doc, AA - success, AE - error
        ack.setTypeCode(V3util.createCS1(ackTypeCode));
        MCCIMT000300UV01TargetMessage trm = V3Factory.eINSTANCE.createMCCIMT000300UV01TargetMessage();
        trm.setId(V3util.createII(OrgMsg.first, OrgMsg.second, ""));
        ack.setTargetMessage(trm);
        rootElement.getAcknowledgement().add(0, ack);
    }

    public void addAckDetail(List<ErrorDetail> errorList) {
        for (Iterator<ErrorDetail> iter = errorList.iterator(); iter.hasNext(); ) {
            ErrorDetail error = iter.next();

            if (log.isDebugEnabled()) {
                log.debug("Ack error: " + error.toString());
            }

            MCCIMT000300UV01AcknowledgementDetail ackDetail = V3Factory.eINSTANCE.createMCCIMT000300UV01AcknowledgementDetail();
            ackDetail.setTypeCode(AcknowledgementDetailType.E);

            //Must have a meaningful message
            assert error.getMessage() != null;
            ED errorDesc = V3Factory.eINSTANCE.createED();
            FeatureMapUtil.addText(errorDesc.getMixed(), error.getMessage());
            ackDetail.setText(errorDesc);

            //Add error code
            if (StringUtil.goodString(error.getCode())) {
                CE ce = V3Factory.eINSTANCE.createCE();
                ce.setCode(error.getCode());
                ackDetail.setCode(ce);
            }

            //Add error location
            if (StringUtil.goodString(error.getLocation())) {
                ST1 location = V3Factory.eINSTANCE.createST1();
                FeatureMapUtil.addText(location.getMixed(), error.getLocation());
                ackDetail.getLocation().add(location);
            }

            ack.getAcknowledgementDetail().add(ackDetail);
        }
    }

    public DocumentRoot getResponse() {
        System.out.println("return pdq response");
        return pdQueryResponseMessage;
    }
}