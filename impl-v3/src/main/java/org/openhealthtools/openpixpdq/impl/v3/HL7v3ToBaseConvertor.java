/**
 *  Copyright (c) 2009-2011 Misys Open Source Solutions (MOSS) and others
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *  implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 *  Contributors:
 *    Misys Open Source Solutions - initial API and implementation
 *    Moin Islam                  - v3 implementation
 */
package org.openhealthtools.openpixpdq.impl.v3;

import com.globalmentor.itu.TelephoneNumber;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.hl7.v3.*;
import org.openhealthtools.openexchange.actorconfig.net.IBaseDescription;
import org.openhealthtools.openexchange.datamodel.*;
import org.openhealthtools.openexchange.utils.DateUtil;
import org.openhealthtools.openpixpdq.common.AssigningAuthorityUtil;
import org.openhealthtools.openpixpdq.common.Constants;
import org.openhealthtools.openpixpdq.common.PixPdqFactory;

import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

public class HL7v3ToBaseConvertor {

    private PRPAIN201301UV02Type addMsg;
    private PRPAIN201302UV02Type updateMsg;
    private PRPAIN201304UV02Type mergeMsg;
    private PRPAMT201301UV02Patient patient;
    private PRPAMT201302UV02Patient patient2;
    private PRPAMT201303UV02Patient patient3;
    private II replacementPersonId;

    /**
     * Actor/Connection description
     */
    private IBaseDescription description;

    public HL7v3ToBaseConvertor(DocumentRoot msgIn, IBaseDescription description) {
        if (null != msgIn.getPRPAIN201301UV02()) {
            addMsg = msgIn.getPRPAIN201301UV02();
            patient = ParsePatient(addMsg);
        } else if (null != msgIn.getPRPAIN201302UV02()) {
            updateMsg = msgIn.getPRPAIN201302UV02();
            patient2 = ParsePatient(updateMsg);
        } else if (null != msgIn.getPRPAIN201304UV02()) {
            mergeMsg = msgIn.getPRPAIN201304UV02();
            patient3 = ParsePatient(mergeMsg);
        }
        this.description = description;
    }

    private PRPAMT201301UV02Patient ParsePatient(PRPAIN201301UV02Type message) {
        PRPAMT201301UV02Patient patient = null;

        PRPAIN201301UV02MFMIMT700701UV01ControlActProcess controlActProcess = message
                .getControlActProcess();
        if (controlActProcess == null) {
            System.out.println("controlActProcess is null - no patient");
            return null;
        }

        List<PRPAIN201301UV02MFMIMT700701UV01Subject1> subjects = controlActProcess
                .getSubject();
        if ((subjects == null) || (subjects.size() == 0)) {
            System.out.println("subjects is blank/null - no patient");
            return null;
        }

        PRPAIN201301UV02MFMIMT700701UV01Subject1 subject = subjects.get(0);
        if (subject == null) {
            return null;
        }
        PRPAIN201301UV02MFMIMT700701UV01RegistrationEvent registrationevent = subject
                .getRegistrationEvent();
        if (registrationevent == null) {
            System.out.println("registrationevent is null - no patient");
            return null;
        }

        PRPAIN201301UV02MFMIMT700701UV01Subject2 subject1 = registrationevent
                .getSubject1();
        if (subject1 == null) {
            System.out.println("subject1 is null - no patient");
            return null;
        }

        patient = subject1.getPatient();
        if (patient == null) {
            System.out.println("patient is null - no patient");
            return null;
        }

        return patient;
    }

    private PRPAMT201302UV02Patient ParsePatient(PRPAIN201302UV02Type message) {
        PRPAMT201302UV02Patient patient = null;

        PRPAIN201302UV02MFMIMT700701UV01ControlActProcess controlActProcess = message
                .getControlActProcess();
        if (controlActProcess == null) {
            System.out.println("controlActProcess is null - no patient");
            return null;
        }

        List<PRPAIN201302UV02MFMIMT700701UV01Subject1> subjects = controlActProcess
                .getSubject();
        if ((subjects == null) || (subjects.size() == 0)) {
            System.out.println("subjects is blank/null - no patient");
            return null;
        }

        PRPAIN201302UV02MFMIMT700701UV01Subject1 subject = subjects.get(0);
        if (subject == null) {
            return null;
        }
        PRPAIN201302UV02MFMIMT700701UV01RegistrationEvent registrationevent = subject
                .getRegistrationEvent();
        if (registrationevent == null) {
            System.out.println("registrationevent is null - no patient");
            return null;
        }

        PRPAIN201302UV02MFMIMT700701UV01Subject2 subject1 = registrationevent
                .getSubject1();
        if (subject1 == null) {
            System.out.println("subject1 is null - no patient");
            return null;
        }

        patient = subject1.getPatient();
        if (patient == null) {
            System.out.println("patient is null - no patient");
            return null;
        }

        return patient;
    }

    private PRPAMT201303UV02Patient ParsePatient(PRPAIN201304UV02Type message) {
        PRPAMT201303UV02Patient patient = null;

        PRPAIN201304UV02MFMIMT700701UV01ControlActProcess controlActProcess = message
                .getControlActProcess();
        if (controlActProcess == null) {
            System.out.println("controlActProcess is null - no patient");
            return null;
        }

        List<PRPAIN201304UV02MFMIMT700701UV01Subject1> subjects = controlActProcess
                .getSubject();
        if ((subjects == null) || (subjects.size() == 0)) {
            System.out.println("subjects is blank/null - no patient");
            return null;
        }

        PRPAIN201304UV02MFMIMT700701UV01Subject1 subject = subjects.get(0);
        if (subject == null) {
            return null;
        }
        PRPAIN201304UV02MFMIMT700701UV01RegistrationEvent registrationevent = subject
                .getRegistrationEvent();
        if (registrationevent == null) {
            System.out.println("registrationevent is null - no patient");
            return null;
        }

        PRPAIN201304UV02MFMIMT700701UV01Subject2 subject1 = registrationevent
                .getSubject1();
        if (subject1 == null) {
            System.out.println("subject1 is null - no patient");
            return null;
        }

        replacementPersonId = registrationevent.getReplacementOf().get(0)
                .getPriorRegistration().getSubject1().getPriorRegisteredRole()
                .getId().get(0);

        patient = subject1.getPatient();
        if (patient == null) {
            System.out.println("patient is null - no patient");
            return null;
        }

        return patient;
    }

    public List<PatientIdentifier> getPatientIds() {
        List<PatientIdentifier> ret = null;
        if (null != patient) {
            return getAddPatientIds();
        } else if (null != patient2) {
            return getUpdatePatientIds();
        } else if (null != patient3) {
            return getMergePatientIds();
        }
        return new ArrayList<PatientIdentifier>();
    }

    private List<PatientIdentifier> getAddPatientIds() {
        List<PatientIdentifier> patientIds = new ArrayList<PatientIdentifier>();
        EList<II> pid = patient.getId();
        if (pid != null) {
            Iterator<II> iter = pid.iterator();
            while (iter.hasNext()) {
                II id = iter.next();
                PatientIdentifier identifier = new PatientIdentifier();
                Identifier assignAuth = new Identifier(null, id.getRoot(),
                        "ISO");

                // reconcile assigning authority
                Identifier reconciledAssignAuth = AssigningAuthorityUtil
                        .reconcileIdentifier(assignAuth, description,
                                PixPdqFactory.getPixManagerAdapter());

                identifier.setAssigningAuthority(reconciledAssignAuth);
                identifier.setId(id.getExtension());
                patientIds.add(identifier);
            }
        }
        // TODO other ids
        return patientIds;
    }

    private List<PatientIdentifier> getUpdatePatientIds() {
        List<PatientIdentifier> patientIds = new ArrayList<PatientIdentifier>();
        EList<PRPAMT201302UV02PatientId> pid = patient2.getId();
        if (pid != null) {
            Iterator<PRPAMT201302UV02PatientId> iter = pid.iterator();
            while (iter.hasNext()) {
                PRPAMT201302UV02PatientId id = iter.next();
                PatientIdentifier identifier = new PatientIdentifier();
                Identifier assignAuth = new Identifier(null, id.getRoot(),
                        "ISO");

                // reconcile assigning authority
                Identifier reconciledAssignAuth = AssigningAuthorityUtil
                        .reconcileIdentifier(assignAuth, description,
                                PixPdqFactory.getPixManagerAdapter());

                identifier.setAssigningAuthority(reconciledAssignAuth);
                identifier.setId(id.getExtension());
                patientIds.add(identifier);
            }
        }
        // TODO other ids
        return patientIds;
    }

    private List<PatientIdentifier> getMergePatientIds() {
        List<PatientIdentifier> patientIds = new ArrayList<PatientIdentifier>();
        EList<II> pid = patient3.getId();
        if (pid != null) {
            Iterator<II> iter = pid.iterator();
            while (iter.hasNext()) {
                II id = iter.next();
                PatientIdentifier identifier = new PatientIdentifier();
                Identifier assignAuth = new Identifier(null, id.getRoot(),
                        "ISO");

                // reconcile assigning authority
                Identifier reconciledAssignAuth = AssigningAuthorityUtil
                        .reconcileIdentifier(assignAuth, description,
                                PixPdqFactory.getPixManagerAdapter());

                identifier.setAssigningAuthority(reconciledAssignAuth);
                identifier.setId(id.getExtension());
                patientIds.add(identifier);
            }
        }
        // TODO other ids
        return patientIds;
    }

    public List<PatientIdentifier> getReplacementPatientId() {
        List<PatientIdentifier> patientIds = new ArrayList<PatientIdentifier>();

        PatientIdentifier identifier = new PatientIdentifier();
        Identifier assignAuth = new Identifier(null, replacementPersonId.getRoot(),
                "ISO");

        // reconcile assigning authority
        Identifier reconciledAssignAuth = AssigningAuthorityUtil
                .reconcileIdentifier(assignAuth, description,
                        PixPdqFactory.getPixManagerAdapter());

        identifier.setAssigningAuthority(reconciledAssignAuth);
        identifier.setId(replacementPersonId.getExtension());
        patientIds.add(identifier);

        return patientIds;
    }

    public PersonName getPatientName() {
        PersonName pName = new PersonName();
        List<PN> nameList = null;
        if (null != patient) {
            nameList = patient.getPatientPerson().getName();
        } else if (null != patient2) {
            nameList = patient2.getPatientPerson().getName();
        }
        if (nameList.size() > 0) {
            PN name = nameList.get(0);
            if (null != name.getUse() && name.getUse().size() > 0) {
                // TODO multi use name ?
                pName.setNameTypeCode(name.getUse().get(0).toString());
            }
            if (null != name.getSuffix() && name.getSuffix().size() > 0) {
                pName.setSuffix(getMixedValue(name.getSuffix().get(0).getMixed())); // patient name suffix
            }
            if (null != name.getPrefix() && name.getPrefix().size() > 0) {
                pName.setPrefix(getMixedValue(name.getPrefix().get(0).getMixed())); // patient name prefix
            }
            pName.setLastName(getMixedValue(name.getFamily().get(0).getMixed())); // patient last name
            List<EnGiven> givenNameList = name.getGiven();
            Iterator<EnGiven> iter = givenNameList.iterator();
            while (iter.hasNext()) {
                EnGiven given = iter.next();
                if (null != given.getQualifier()) {
                    // TODO what is the qualifier value for second name?
                    if (given.getQualifier().get(0).toString().equals("IN")) {
                        pName.setSecondName(getMixedValue(given.getMixed()));
                    }
                } else {
                    pName.setFirstName(getMixedValue(given.getMixed())); // patient
                    // name
                    // first
                }
            }
        }
        // TODO find possible match
        // pName.setNameRepresentationCode(pid.getPatientName(0).getNameRepresentationCode().getValue());
        // pName.setDegree(pid.getPatientName(0).getDegreeEgMD().getValue());

        return pName;
    }

    public PersonName getMotherMaidenName() {
        // TODO create generic method to parse name
        PersonName mName = new PersonName();
        if (null != patient) {
            EList<?> relationship = patient.getPatientPerson().getPersonalRelationship();
            if (relationship != null) {
                Iterator<?> iter = relationship.iterator();
                while (iter.hasNext()) {
                    PRPAMT201301UV02PersonalRelationship pr = (PRPAMT201301UV02PersonalRelationship) iter.next();
                    if (pr.getCode().getCode().equalsIgnoreCase("MTH")) {
                        EN en = pr.getRelationshipHolder1().getName().get(0);
                        mName = fromEN(en);
                    }
                }
            }
        } else if (null != patient2) {
            EList<?> relationship = patient2.getPatientPerson().getPersonalRelationship();
            if (relationship != null) {
                Iterator<?> iter = relationship.iterator();
                while (iter.hasNext()) {
                    PRPAMT201302UV02PersonalRelationship pr = (PRPAMT201302UV02PersonalRelationship) iter.next();
                    if (pr.getCode().getCode().equalsIgnoreCase("MTH")) {
                        EN en = pr.getRelationshipHolder1().getName().get(0);
                        mName = fromEN(en);
                    }
                }
            }
        }

        return mName;
    }

    private PersonName fromEN(EN en) {
        PersonName mName = new PersonName();
        if (null != en.getUse() && en.getUse().size() > 0) {
            // TODO multi use name ?
            mName.setNameTypeCode(en.getUse().get(0).toString());
        }
        mName.setLastName(getMixedValue(en.getFamily().get(0).getMixed())); // last name
        if (null != en.getSuffix() && en.getSuffix().size() > 0) {
            mName.setSuffix(getMixedValue(en.getSuffix().get(0).getMixed())); // name suffix
        }
        if (null != en.getPrefix() && en.getPrefix().size() > 0) {
            mName.setPrefix(getMixedValue(en.getPrefix().get(0).getMixed())); // name prefix
        }
        List<EnGiven> givenNameList = en.getGiven();
        Iterator<EnGiven> it = givenNameList.iterator();
        while (it.hasNext()) {
            EnGiven given = it.next();
            if (null != given.getQualifier()) {
                // TODO what is the qualifier value for second name?
                if (given.getQualifier().get(0).toString().equals("IN")) {
                    mName.setSecondName(getMixedValue(given.getMixed()));
                }
            } else {
                mName.setFirstName(getMixedValue(given.getMixed())); // patient
                // name
                // first
            }
        }
        return mName;
    }


    private String getMixedValue(FeatureMap mixed) {
        String returnValue = "";
        // if we have a mixed
        if (mixed.size() > 0)
            returnValue = mixed.get(0).getValue().toString();
        return returnValue;
    }

    public String getPrimaryLanguage() {
        if (null != patient
                && null != patient.getPatientPerson().getLanguageCommunication()
                && patient.getPatientPerson().getLanguageCommunication().size() > 0) {

            return patient.getPatientPerson().getLanguageCommunication().get(0)
                    .getLanguageCode().getCode();

        } else if (null != patient2
                && null != patient2.getPatientPerson().getLanguageCommunication()
                && patient2.getPatientPerson().getLanguageCommunication().size() > 0) {

            return patient2.getPatientPerson().getLanguageCommunication()
                    .get(0).getLanguageCode().getCode();
        }
        return null;
    }

    public List<Address> getAddressList() {
        List<Address> addressList = new ArrayList<Address>();
        EList<AD> addr = null;
        if (null != patient) {
            addr = patient.getPatientPerson().getAddr();
        } else if (null != patient2) {
            addr = patient2.getPatientPerson().getAddr();
        }
        if (addr != null) {
            Iterator<AD> iter = addr.iterator();
            while (iter.hasNext()) {
                AD ad = iter.next();
                Address address = new Address();
                if (null != ad.getCountry() && ad.getCountry().size() > 0)
                    address.setAddCountry(getMixedValue(ad.getCountry().get(0)
                            .getMixed()));
                if (null != ad.getCity() && ad.getCity().size() > 0)
                    address.setAddCity(getMixedValue(ad.getCity().get(0)
                            .getMixed()));
                if (null != ad.getStreetAddressLine()
                        && ad.getStreetAddressLine().size() > 0)
                    address.setAddLine1(getMixedValue(ad.getStreetAddressLine()
                            .get(0).getMixed()));
                if (null != ad.getCounty() && ad.getCounty().size() > 0)
                    address.setAddCounty(getMixedValue(ad.getCounty().get(0)
                            .getMixed()));
                if (null != ad.getState() && ad.getState().size() > 0)
                    address.setAddState(getMixedValue(ad.getState().get(0)
                            .getMixed()));
                if (null != ad.getPostalCode() && ad.getPostalCode().size() > 0)
                    address.setAddZip(getMixedValue(ad.getPostalCode().get(0)
                            .getMixed()));
                // TODO
                // address.setAddLine2(xad.getOtherDesignation().getValue());
                // address.setAddType(_mapAddressToBase(xad.getAddressType().getValue()));

                addressList.add(address);
            }
        }
        return addressList;
    }

    public List<PhoneNumber> getPhoneList() {
        List<PhoneNumber> phoneList = new ArrayList<PhoneNumber>();
        PhoneNumber telecom = null;
        List<TEL> telecomList = null;
        if (null != patient) {
            telecomList = patient.getPatientPerson().getTelecom();
        } else if (null != patient2) {
            telecomList = patient2.getPatientPerson().getTelecom();
        }
        if (telecomList.size() > 0) {
            Iterator<TEL> iter = telecomList.iterator();
            while (iter.hasNext()) {
                TEL tel = iter.next();
                telecom = new PhoneNumber();
                try {
                    // TODO check if multiple use code is permitted or not?
                    if (null != tel.getUse()) {
                        // TODO check v3 phone use vocabulary
                        String use = tel.getUse().get(0).toString();
                        SharedEnums.PhoneType phoneType = SharedEnums.PhoneType.hl7ValueOf(use);
                        telecom.setType(phoneType);
                    }
                    // RFC 3966
                    URI telURI = new URI(tel.getValue());
                    String uriScheme = telURI.getScheme();
                    // TODO check for fax type
                    if (uriScheme.equals("tel")) {
                        String number = telURI.getSchemeSpecificPart();
                        TelephoneNumber phone = new TelephoneNumber(number);
                        telecom.setCountryCode(phone.getCCString());
                        telecom.setAreaCode(phone.getNDCString());
                        telecom.setNumber(phone.getSNString());
                        // TODO: get extension
                        // if (phone.length() > 20) {
                        // //we have extension
                        // telecom.setExtension(phone.substring(20,phone.length()-1));
                        // }
                    }
                    if (uriScheme.equals("mailto")) {
                        telecom.setEmail(telURI.getSchemeSpecificPart());
                    }

                } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
                phoneList.add(telecom);
            }
        }
        return phoneList;
    }

    public Calendar getBirthDate() throws ParseException {
        String dob = null;
        if (null != patient
                && null != patient.getPatientPerson().getBirthTime().getValue()) {
            dob = patient.getPatientPerson().getBirthTime().getValue();
        } else if (null != patient2
                && null != patient2.getPatientPerson().getBirthTime().getValue()) {
            dob = patient2.getPatientPerson().getBirthTime().getValue();
        }
        return DateUtil.parseCalendar(dob, "yyyyMMdd");
    }

    public Calendar getDeathDate() throws ParseException {
        String dod = null;
        if (null != patient
                && null != patient.getPatientPerson().getDeceasedTime()) {
            dod = patient.getPatientPerson().getDeceasedTime().getValue();
        } else if (null != patient2
                && null != patient2.getPatientPerson().getDeceasedTime()) {
            dod = patient2.getPatientPerson().getDeceasedTime().getValue();
        }
        return DateUtil.parseCalendar(dod, "yyyyMMdd");
    }

    public SharedEnums.SexType getSexType() {
        String sex = null;
        if (null != patient
                && null != patient.getPatientPerson().getAdministrativeGenderCode()) {
            sex = patient.getPatientPerson().getAdministrativeGenderCode().getCode();
        } else if (null != patient2
                && null != patient2.getPatientPerson().getAdministrativeGenderCode()) {
            sex = patient2.getPatientPerson().getAdministrativeGenderCode().getCode();
        }
        if (sex == null)
            return null;
        return SharedEnums.SexType.getByString(sex);
    }

    public int getBirthOrder() {

        if (null != patient
                && null != patient.getPatientPerson()
                .getMultipleBirthOrderNumber()) {
            return patient.getPatientPerson().getMultipleBirthOrderNumber()
                    .getValue().intValue();
        } else if (null != patient2
                && null != patient2.getPatientPerson().getMultipleBirthOrderNumber()) {
            return patient2.getPatientPerson().getMultipleBirthOrderNumber()
                    .getValue().intValue();
        }

        return 0;
    }

    public String getBirthPlace() {
        // TODO In v3 birth place is a data type of AD but seems like v2 uses
        // just a string
        String place = null;        /*
         * if (null!= patient && null !=
		 * patient.getPatientPerson().getBirthPlace().getAddr().){ return
		 * patient
		 * .getPatientPerson().getMultipleBirthOrderNumber().getValue().intValue
		 * (); }
		 */
        return place;
    }

    public String getDeathIndicator() {
        boolean indicator = false;
        if (null != patient
                && null != patient.getPatientPerson().getDeceasedInd()) {
            indicator = patient.getPatientPerson().getDeceasedInd().isValue();
        } else if (null != patient2
                && null != patient2.getPatientPerson().getDeceasedInd()) {
            indicator = patient2.getPatientPerson().getDeceasedInd().isValue();
        }
        if (indicator) {
            return "T";
        }
        return "F";
    }

    public String getVipIndicator() {
        String vip = null;
        if (null != patient && null != patient.getVeryImportantPersonCode()) {
            vip = patient.getVeryImportantPersonCode().getCode();
        } else if (null != patient2 && null != patient2.getVeryImportantPersonCode()) {
            vip = patient2.getVeryImportantPersonCode().getCode();
        }
        return vip;
    }

    public String getRace() {
        String race = null;
        // TODO Multiple race code?
        if (null != patient
                && 0 < patient.getPatientPerson().getRaceCode().size()) {
            race = patient.getPatientPerson().getRaceCode().get(0).getCode();
        } else if (null != patient2
                && 0 < patient2.getPatientPerson().getRaceCode().size()) {
            race = patient2.getPatientPerson().getRaceCode().get(0).getCode();
        }

        return race;
    }

    /**
     * Example:
     * <asOtherIDs classCode="CIT">
     * <id root="2.16.840.1.113883.4.1" extension="999-89-3300"/>
     * <scopingOrganization classCode="ORG" determinerCode="INSTANCE">
     * <id root="2.16.840.1.113883.4.1"/>
     * </scopingOrganization>
     * </asOtherIDs>
     *
     * @return
     */
    public String getSsn() {
        //scopingOrganization/id/@root is the same as id/@root per IHE PIX/PDQ Specs.
        if (null != patient && !patient.getPatientPerson().getAsOtherIDs().isEmpty()) {
            for (PRPAMT201301UV02OtherIDs otherId : patient.getPatientPerson().getAsOtherIDs()) {
                COCTMT150002UV01Organization org = otherId.getScopingOrganization();
                for (II orgId : org.getId()) {
                    if (orgId.getRoot() != null && orgId.getRoot().equals(Constants.SSN_OID)) {
                        for (II id : otherId.getId()) {
                            //should be just one SSN id
                            if (!id.getRoot().equals(Constants.SSN_OID)) {
                                //TODO: send out warning message ?
                                return null;
                            }
                            return id.getExtension();
                        }
                    }
                }
            }
        } else if (null != patient2 && !patient2.getPatientPerson().getAsOtherIDs().isEmpty()) {
            for (PRPAMT201302UV02OtherIDs otherId : patient2.getPatientPerson().getAsOtherIDs()) {
                COCTMT150002UV01Organization org = otherId.getScopingOrganization();
                for (II orgId : org.getId()) {
                    if (orgId.getRoot() != null && orgId.getRoot().equals(Constants.SSN_OID)) {
                        for (II id : otherId.getId()) {
                            //should be just one SSN id
                            if (!id.getRoot().equals(Constants.SSN_OID)) {
                                //TODO: send out warning message ?
                                return null;
                            }
                            return id.getExtension();
                        }
                    }
                }
            }
        }
        return null;
    }

}
