/**
 *  Copyright (c) 2009-2011 Misys Open Source Solutions (MOSS) and others
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *  implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 *  Contributors:
 *    Misys Open Source Solutions - initial API and implementation
 *    Moin Islam                  - v3 implementation
 */
package org.openhealthtools.openpixpdq.impl.v3;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hl7.v3.DocumentRoot;
import org.hl7.v3.PRPAIN201301UV02Type;
import org.hl7.v3.PRPAIN201302UV02Type;
import org.hl7.v3.PRPAIN201304UV02Type;
import org.openhealthtools.openexchange.audit.ActiveParticipant;
import org.openhealthtools.openexchange.audit.AuditCodeMappings;
import org.openhealthtools.openexchange.audit.AuditCodeMappings.EventActionCode;
import org.openhealthtools.openexchange.audit.ParticipantObject;
import org.openhealthtools.openexchange.datamodel.Identifier;
import org.openhealthtools.openexchange.datamodel.MessageHeader;
import org.openhealthtools.openexchange.datamodel.Patient;
import org.openhealthtools.openexchange.datamodel.PatientIdentifier;
import org.openhealthtools.openpixpdq.api.IPixManagerAdapter;
import org.openhealthtools.openpixpdq.api.IPixUpdateNotificationRequest;
import org.openhealthtools.openpixpdq.api.PixManagerException;
import org.openhealthtools.openpixpdq.common.AssigningAuthorityUtil;
import org.openhealthtools.openpixpdq.common.PixPdqException;
import org.openhealthtools.openpixpdq.common.PixPdqFactory;
import org.openhealthtools.openpixpdq.common.PixUpdateNotifier;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;


class PixFeedHandlerV3 extends BaseHandlerV3 {

    private static Log log = LogFactory.getLog(PixFeedHandlerV3.class);
    private IPixManagerAdapter pixAdapter = PixPdqFactory.getPixManagerAdapter();

    private PixManagerV3 actor;

    private static PixFeedHandlerV3 instance = null;

    /**
     * Singleton
     *
     * @return
     */
    synchronized static PixFeedHandlerV3 getInstance() {
        if (null == instance) {
            instance = new PixFeedHandlerV3();
        }
        return instance;
    }

    private PixFeedHandlerV3() {
    }

    void registryPixManager(PixManagerV3 actor) {
        this.actor = actor;
    }

    PixManagerV3 getPixManager() {
        return this.actor;
    }

    DocumentRoot processMessage(DocumentRoot msgIn) throws PixPdqException {
        DocumentRoot retMessage = null;

        if (null != msgIn.getPRPAIN201301UV02()) { //Patient Record Added message
            retMessage = processCreate(msgIn);
        } else if (null != msgIn.getPRPAIN201302UV02()) { //Patient Record Revised message
            retMessage = processUpdate(msgIn);
        } else if (null != msgIn.getPRPAIN201304UV02()) { //Patient Duplicates Resolved message
            retMessage = processMerge(msgIn);
        } else {
            log.error("Message Type not recognized");
            throw new PixPdqException("Message Type not recoginized");
        }

        return retMessage;
    }

    private DocumentRoot processCreate(DocumentRoot msgIn) throws PixPdqException {
        PRPAIN201301UV02Type msgCreate = msgIn.getPRPAIN201301UV02();

        TransmissionWrapper hl7V3Header = new TransmissionWrapper(msgCreate);

        //create v3 ack message
        MCCI_IN000002UV01Ack ack = new MCCI_IN000002UV01Ack(hl7V3Header.getMessageControlId(),
                getServerApplication(actor.getActorDescription()).getUniversalId(),
                getServerFacility(actor.getActorDescription()).getUniversalId(),
                hl7V3Header.getSendingApplication(), hl7V3Header.getSendingFacility());

        List<ErrorDetail> errorList = new ArrayList<ErrorDetail>();

        //Validate incoming message
        validateMessage(msgIn, hl7V3Header, actor.getActorDescription(), errorList);

        if (errorList.size() > 0) {
            // set ack code error CE
            ack.setAckType("CE");
            //set ack detail section of the ack message with list of errors
            ack.addAckDetail(errorList);
            return ack.getAck();
        }

        MessageHeader header = null;
        Patient patient = null;
        try {
            header = hl7V3Header.toMessageHeader();
            patient = getPatient(msgIn);
        } catch (ParseException e) {
            throw new PixPdqException("Failed to parse patient " + e.getMessage(), e);
        }

        //validate the patient id is not empty
        validatePatientIdNotEmpty(patient, errorList);

        //validate the patient domains are valid
        validateDomain(patient, false, errorList);

        if (errorList.size() > 0) {
            return getErrorAck(ack, errorList);
        }

        //Invoke eMPI function
        try {
            List<PatientIdentifier> matching =
                    pixAdapter.createPatient(patient, header);

            if (matching != null && matching.size() > 0) {
                //Send PIX Update Notification
                IPixUpdateNotificationRequest request =
                        new PixUpdateNotificationRequestV3(actor, matching, patient);
                PixUpdateNotifier.getInstance().accept(request);

            }
        } catch (PixManagerException e) {
            throw new PixPdqException("Failed to create patient " + e.getMessage(), e);
        }

        //TODO Forward this PIX Feed (Merge) message to the XDS Registry
        //forwardToXdsRegistry(msgIn, patientId);

        //Finally, Audit Log PIX Feed Success
        auditLog(hl7V3Header, patient, AuditCodeMappings.EventActionCode.Create);

        ack.setAckType("CA");
        return ack.getAck();
    }

    private DocumentRoot processUpdate(DocumentRoot msgIn) throws PixPdqException {
        PRPAIN201302UV02Type msgUpdate = msgIn.getPRPAIN201302UV02();

        TransmissionWrapper hl7V3Header = new TransmissionWrapper(msgUpdate);

        //create v3 ack message
        MCCI_IN000002UV01Ack ack = new MCCI_IN000002UV01Ack(hl7V3Header.getMessageControlId(),
                getServerApplication(actor.getActorDescription()).getUniversalId(),
                getServerFacility(actor.getActorDescription()).getUniversalId(),
                hl7V3Header.getSendingApplication(), hl7V3Header.getSendingFacility());

        List<ErrorDetail> errorList = new ArrayList<ErrorDetail>();

        //Validate incoming message
        validateMessage(msgIn, hl7V3Header, actor.getActorDescription(), errorList);

        if (errorList.size() > 0) {
            return getErrorAck(ack, errorList);
        }

        MessageHeader header = null;
        Patient patient = null;
        try {
            header = hl7V3Header.toMessageHeader();
            patient = getPatient(msgIn);
        } catch (ParseException e) {
            throw new PixPdqException("Failed to parse patient " + e.getMessage(), e);
        }

        //validate the patient id is not empty
        validatePatientIdNotEmpty(patient, errorList);
        //validate the patient domains are valid
        validateDomain(patient, false, errorList);
        if (errorList.size() > 0) {
            return getErrorAck(ack, errorList);
        }

        //Validate patient id
        validatePatientId(patient, header, false, errorList);
        if (errorList.size() > 0) {
            return getErrorAck(ack, errorList);
        }

        //Invoke eMPI function
        try {
            List<List<PatientIdentifier>> matchingList = pixAdapter.updatePatient(patient, header);

            if (matchingList != null) {
                for (List<PatientIdentifier> matching : matchingList) {
                    //send PIX Update Notification to PIX consumers
                    IPixUpdateNotificationRequest request =
                            new PixUpdateNotificationRequestV3(actor, matching, patient);
                    PixUpdateNotifier.getInstance().accept(request);
                }
            }
        } catch (Exception e) {
            throw new PixPdqException("Failed to update patient " + e.getMessage(), e);
        }

        //TODO Forward this PIX Feed (Merge) message to the XDS Registry
        //forwardToXdsRegistry(msgIn, patientId);

        //Finally, Audit Log PIX Feed Success
        auditLog(hl7V3Header, patient, AuditCodeMappings.EventActionCode.Update);

        ack.setAckType("CA");
        return ack.getAck();
    }

    private DocumentRoot processMerge(DocumentRoot msgIn) throws PixPdqException {
        PRPAIN201304UV02Type msgMerge = msgIn.getPRPAIN201304UV02();

        TransmissionWrapper hl7V3Header = new TransmissionWrapper(msgMerge);

        //create v3 ack message
        MCCI_IN000002UV01Ack ack = new MCCI_IN000002UV01Ack(hl7V3Header.getMessageControlId(),
                getServerApplication(actor.getActorDescription()).getUniversalId(),
                getServerFacility(actor.getActorDescription()).getUniversalId(),
                hl7V3Header.getSendingApplication(), hl7V3Header.getSendingFacility());

        List<ErrorDetail> errorList = new ArrayList<ErrorDetail>();

        //Validate incoming message
        validateMessage(msgIn, hl7V3Header, actor.getActorDescription(), errorList);

        if (errorList.size() > 0) {
            return getErrorAck(ack, errorList);
        }

        MessageHeader header = null;
        Patient patient = null;
        Patient replacementPatient = null;
        try {
            header = hl7V3Header.toMessageHeader();
            patient = getActivePatient(msgIn);
            replacementPatient = getReplacementPatient(msgIn);
        } catch (ParseException e) {
            throw new PixPdqException("Failed to parse patients " + e.getMessage(), e);
        }

        //validate the both patients have the same domain
        validateSameDomain(patient, replacementPatient, errorList);
        //validate surviving patient domain
        validateDomain(patient, false, errorList);
        //validate subsumed patient domain
        validateDomain(replacementPatient, true, errorList);

        //return if error exists
        if (errorList.size() > 0) {
            return getErrorAck(ack, errorList);
        }

        //Validate patient id
        validatePatientId(patient, header, false, errorList);
        //Validate subsumed patient id
        validatePatientId(replacementPatient, header, true, errorList);

        if (errorList.size() > 0) {
            return getErrorAck(ack, errorList);
        }

        //Invoke eMPI function
        try {
            //Merge Patients
            List<List<PatientIdentifier>> matchingList =
                    pixAdapter.mergePatients(patient, replacementPatient, header);

            if (matchingList != null) {
                //send PIX Update Notification to PIX consumers
                for (List<PatientIdentifier> matching : matchingList) {
                    IPixUpdateNotificationRequest request =
                            new PixUpdateNotificationRequestV3(actor, matching, patient);
                    PixUpdateNotifier.getInstance().accept(request);
                }
            }
        } catch (Exception e) {
            throw new PixPdqException("Failed to merge patients " + e.getMessage(), e);
        }

        //TODO Forward this PIX Feed (Merge) message to the XDS Registry
        //forwardToXdsRegistry(msgIn, patientId);

        //Finally, Audit Log PIX Feed Success
        auditLog(hl7V3Header, patient, AuditCodeMappings.EventActionCode.Update);
        auditLog(hl7V3Header, replacementPatient, AuditCodeMappings.EventActionCode.Delete);

        ack.setAckType("CA");
        return ack.getAck();
    }

    private DocumentRoot getErrorAck(MCCI_IN000002UV01Ack ack, List<ErrorDetail> errorList) {
        // set ack code error CE
        ack.setAckType("CE");
        //set ack detail section of the ack message with list of errors
        ack.addAckDetail(errorList);
        return ack.getAck();
    }

    private boolean validateDomain(Patient patient, boolean isSubsumedPatient, List<ErrorDetail> errorList) {
        List<PatientIdentifier> patientIds = patient.getPatientIds();

        for (int i = 0; i < patientIds.size(); i++) {
            Identifier domain = patientIds.get(i).getAssigningAuthority();
            boolean domainOk = AssigningAuthorityUtil.validateDomain(domain, actor.getActorDescription(), pixAdapter);

            if (!domainOk) {
                ErrorDetail error = new ErrorDetail();
                error.setCode("204");
                if (isSubsumedPatient) {
                    error.setLocation("//controlActProcess/subject/registrationEvent/replacementOf/priorRegistration/subject1/priorRegisteredRole/id/@root='" + domain.getUniversalId() + "'");
                } else {
                    error.setLocation("//controlActProcess/subject/registrationEvent/subject1/patient/id/@root='" + domain.getUniversalId() + "'");
                }
                error.setMessage("Unknown Key Identifier - domain: " + domain.getAuthorityNameString());
                errorList.add(error);

                return false;
            }
        }

        return true;
    }

    private boolean validatePatientIdNotEmpty(Patient patient, List<ErrorDetail> errorList) {
        List<PatientIdentifier> pids = patient.getPatientIds();
        if (0 == pids.size()) {
            ErrorDetail error = new ErrorDetail();
            error.setCode("204");
            error.setLocation("//controlActProcess/subject/registrationEvent/subject1/patient/id");
            error.setMessage("Unknown Key Identifier - patient id is null");
            errorList.add(error);
            return false;
        }
        return true;
    }

    private boolean validateSameDomain(Patient survivingPatient, Patient subsumedPatient, List<ErrorDetail> errorList) {
        List<PatientIdentifier> survivingPatientIds = survivingPatient.getPatientIds();
        List<PatientIdentifier> subsumedPatientIds = subsumedPatient.getPatientIds();

        if (survivingPatientIds.size() == 0) {
            ErrorDetail error = new ErrorDetail();
            error.setMessage("No surviving patient is provided");
            error.setLocation("//controlActProcess/subject/registrationEvent/subject1/patient/id");
            errorList.add(error);
            return false;
        }
        if (survivingPatientIds.size() >= 2) {
            ErrorDetail error = new ErrorDetail();
            error.setMessage("Found multiple patient ids of surviving patient. Only one is needed");
            error.setLocation("//controlActProcess/subject/registrationEvent/subject1/patient/id");
            errorList.add(error);
            return false;
        }
        if (subsumedPatientIds.size() == 0) {
            ErrorDetail error = new ErrorDetail();
            error.setMessage("No subsumed patient id is provided");
            error.setLocation("//controlActProcess/subject/registrationEvent/replacementOf/priorRegistration/subject1/priorRegisteredRole/id");
            errorList.add(error);
            return false;
        }
        if (subsumedPatientIds.size() >= 2) {
            ErrorDetail error = new ErrorDetail();
            error.setMessage("Found multiple patient ids of subsumed patient. Only one is needed");
            error.setLocation("//controlActProcess/subject/registrationEvent/replacementOf/priorRegistration/subject1/priorRegisteredRole/id");
            errorList.add(error);
            return false;
        }

        //Make sure domains are the same for both patients to be merged
        if (!subsumedPatientIds.get(0).getAssigningAuthority().equals(survivingPatientIds.get(0).getAssigningAuthority())) {
            ErrorDetail error = new ErrorDetail();
            error.setMessage("The surviving patient and subsumed patient do not have the same patient domain.");
            error.setLocation("//controlActProcess/subject/registrationEvent/replacementOf/priorRegistration/subject1/priorRegisteredRole/id");
            errorList.add(error);
            return false;
        }
        return true;
    }


    private boolean validatePatientId(Patient patient, MessageHeader header, boolean isSubsumedPatient,
                                      List<ErrorDetail> errorList) throws PixPdqException {

        List<PatientIdentifier> patientIds = patient.getPatientIds();
        try {
            for (PatientIdentifier pid : patientIds) {
                boolean validPatient = pixAdapter.isValidPatient(pid, header);

                if (!validPatient) {
                    ErrorDetail error = new ErrorDetail();
                    error.setCode("204");
                    if (isSubsumedPatient) {
                        error.setLocation("//controlActProcess/subject/registrationEvent/replacementOf/priorRegistration/subject1/priorRegisteredRole/id/@extension='" + pid.getId() + "'");
                    } else {
                        error.setLocation("//controlActProcess/subject/registrationEvent/subject1/patient/id/@extension='" + pid.getId() + "'");
                    }
                    error.setMessage("Unknown Key Identifier - patient id: " + pid.getId() + " in domain " + pid.getAssigningAuthority().getAuthorityNameString());
                    errorList.add(error);

                    return false;
                }
            }
        } catch (PixManagerException e) {
            throw new PixPdqException(e);
        }
        return true;
    }

    private Patient getPatient(DocumentRoot msgIn) throws ParseException {
        HL7v3ToBaseConvertor convertor = null;
        convertor = new HL7v3ToBaseConvertor(msgIn, this.actor.getActorDescription());

        Patient patientDesc = new Patient();
        patientDesc.setPatientIds(convertor.getPatientIds());
        patientDesc.setPatientName(convertor.getPatientName());
        patientDesc.setAdministrativeSex(convertor.getSexType());
        patientDesc.setBirthDateTime(convertor.getBirthDate());
        patientDesc.setPrimaryLanguage(convertor.getPrimaryLanguage());
        patientDesc.setDeathDate(convertor.getDeathDate());
        patientDesc.setDeathIndicator(convertor.getDeathIndicator());
        patientDesc.setPhoneNumbers(convertor.getPhoneList());
        patientDesc.setAddresses(convertor.getAddressList());
        patientDesc.setMothersMaidenName(convertor.getMotherMaidenName());
        patientDesc.setBirthOrder(convertor.getBirthOrder());
        patientDesc.setVipIndicator(convertor.getVipIndicator());
        patientDesc.setRace(convertor.getRace());
        patientDesc.setBirthPlace(convertor.getBirthPlace());

        //patientDesc.setPatientAliases(convertor.getPatientAliases());

        //patientDesc.setMaritalStatus(convertor.getMaritalStatus());
        //patientDesc.setReligion(convertor.getReligion());
        //patientDesc.setPatientAccountNumber(convertor.getpatientAccountNumber());
        patientDesc.setSsn(convertor.getSsn());
        //patientDesc.setDriversLicense(convertor.getDriversLicense());
        //patientDesc.setMothersId(convertor.getMothersId());
        //patientDesc.setEthnicGroup(convertor.getEthnicGroup());


        //patientDesc.setCitizenship(convertor.getCitizenShip());
        //patientDesc.setVisits(convertor.getVisitList());

        //patientDesc.setNextOfKin(convertor.getNextOfKin());

        //System.out.println("Patient:" + patientDesc.getAddresses().get(0).getAddCity());
        //System.out.println("Patient:" + patientDesc.getAdministrativeSex());
        return patientDesc;
    }

    //Note for merge we are not suppose to update demographic information so just get active person id
    private Patient getActivePatient(DocumentRoot msgIn) {
        HL7v3ToBaseConvertor convertor = null;
        convertor = new HL7v3ToBaseConvertor(msgIn, this.actor.getActorDescription());

        Patient patientDesc = new Patient();
        patientDesc.setPatientIds(convertor.getPatientIds());

        return patientDesc;

    }

    private Patient getReplacementPatient(DocumentRoot msgIn) {
        HL7v3ToBaseConvertor convertor = null;
        convertor = new HL7v3ToBaseConvertor(msgIn, this.actor.getActorDescription());

        Patient patientDesc = new Patient();
        patientDesc.setPatientIds(convertor.getReplacementPatientId());

        return patientDesc;

    }

    /**
     * Audit Logging of PIX Feed message.
     *
     * @param hl7Header       the header message from the source application
     * @param patient         the patient to create, update or merged
     * @param eventActionCode the {@link EventActionCode}
     */
    private void auditLog(TransmissionWrapper hl7v3Header, Patient patient, AuditCodeMappings.EventActionCode eventActionCode) {
        if (actor.getAuditTrail() == null)
            return;

        String userId = hl7v3Header.getSendingFacility() + "|" +
                hl7v3Header.getSendingApplication();
        String messageId = (String) hl7v3Header.getMessageControlId().first + ':' + (String) hl7v3Header.getMessageControlId().second;
        //TODO: Get the ip address of the source application
        String sourceIp = "127.0.0.1";
        ActiveParticipant source = new ActiveParticipant(userId, messageId, sourceIp);

        ParticipantObject patientObj = new ParticipantObject(patient);
        //TODO
        //patientObj.setDetail(new Pair("MSH-10", hl7Header.getMessageControlId()));
        //patientObj.setDetail(hl7v3Header.getMessageControlId());
        actor.getAuditTrail().logPixFeed(source, patientObj, eventActionCode);
    }


}