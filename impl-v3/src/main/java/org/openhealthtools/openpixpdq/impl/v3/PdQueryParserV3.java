/**
 *  Copyright (c) 2009-2011 Misys Open Source Solutions (MOSS) and others
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *  implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 *  Contributors:
 *    Misys Open Source Solutions - initial API and implementation
 *    Moin Islam                  - v3 implementation
 */
package org.openhealthtools.openpixpdq.impl.v3;

import org.eclipse.emf.common.util.EList;
import org.hl7.v3.*;
import org.openhealthtools.openexchange.actorconfig.IActorDescription;
import org.openhealthtools.openexchange.datamodel.*;
import org.openhealthtools.openexchange.utils.DateUtil;
import org.openhealthtools.openpixpdq.api.PdqQuery;
import org.openhealthtools.openpixpdq.common.AssigningAuthorityUtil;
import org.openhealthtools.openpixpdq.common.PixPdqException;
import org.openhealthtools.openpixpdq.common.PixPdqFactory;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class PdQueryParserV3 {

    private PRPAMT201306UV02ParameterList parameterList = null;
    private PRPAMT201306UV02QueryByParameter qryByParam = null;
    private QUQIMT000001UV01QueryContinuation qryCancelOrContinuation = null;
    private IActorDescription actor = null;

    public PdQueryParserV3(PRPAIN201305UV02Type msgQuery, IActorDescription actorDescription) {
        PRPAIN201305UV02QUQIMT021001UV01ControlActProcess ctrlActPrs = msgQuery.getControlActProcess();

        qryByParam = ctrlActPrs.getQueryByParameter();
        if (null != qryByParam.getParameterList()) {
            parameterList = qryByParam.getParameterList();
        }
        actor = actorDescription;
    }

    public PdQueryParserV3(QUQIIN000003UV01Type msgQueryCancelOrContinue, IActorDescription actorDescription) {
        QUQIMT000001UV01ControlActProcess ctrlActPrs = msgQueryCancelOrContinue.getControlActProcess();

        qryCancelOrContinuation = ctrlActPrs.getQueryContinuation();
        actor = actorDescription;
    }

    public List<Identifier> getReturnDomain(List<ErrorDetail> errorList) {
        List<Identifier> returnDomains = new ArrayList<Identifier>();
        EList<PRPAMT201306UV02OtherIDsScopingOrganization> otherIdScopingOrgList;
        if (parameterList != null) {
            otherIdScopingOrgList = parameterList.getOtherIDsScopingOrganization();
            for (PRPAMT201306UV02OtherIDsScopingOrganization otherIdScopingOrg : otherIdScopingOrgList) {
                for (II otherIdScopingOrgValue : otherIdScopingOrg.getValue()) {
                    Identifier domain = new Identifier(null,
                            otherIdScopingOrgValue.getRoot(), "ISO");

                    boolean validDomain = AssigningAuthorityUtil.validateDomain(domain,
                            actor, PixPdqFactory.getPdSupplierAdapter());

                    // reconcile assigning authority
                    Identifier reconciledDomain = AssigningAuthorityUtil
                            .reconcileIdentifier(domain, actor, PixPdqFactory
                                    .getPixManagerAdapter());

                    if (validDomain) {
                        returnDomains.add(reconciledDomain);
                    } else {
                        ErrorDetail error = new ErrorDetail();
                        error.setCode("204");
                        error.setMessage("Unknow domain " + reconciledDomain.getAuthorityNameString());
                        error.setLocation("//controlActProcess/queryByParameter/parameterList/otherIDsScopingOrganization/value/@root='"
                                + otherIdScopingOrgValue.getRoot() + "'");
                        errorList.add(error);
                        return null;
                    }
                }
            }
        }
        return returnDomains;
    }

    public PdqQuery getQueryParams(List<ErrorDetail> errorList) throws PixPdqException {
        PdqQuery pdq = new PdqQuery();
        if (parameterList == null) {
            ErrorDetail error = new ErrorDetail();
            error.setCode("102");
            error.setMessage("Data type error");
            error.setLocation("//controlActProcess/queryByParameter/parameterList = null");
            errorList.add(error);
            return null;
        }
        Address address = null;
        PersonName personName = null;
        PatientIdentifier patientIdentifier = null;

        if (null != parameterList.getLivingSubjectId() && parameterList.getLivingSubjectId().size() > 0) {
            II id = parameterList.getLivingSubjectId().get(0).getValue().get(0);
            patientIdentifier = new PatientIdentifier();
            Identifier assignAuth = new Identifier(null, id.getRoot(), "ISO");
            Identifier reconciledAssignAuth = AssigningAuthorityUtil.reconcileIdentifier(assignAuth, actor,
                    PixPdqFactory.getPdSupplierAdapter());
            patientIdentifier.setAssigningAuthority(reconciledAssignAuth);
            patientIdentifier.setId(id.getExtension());

            pdq.setPatientIdentifier(patientIdentifier);
        }

        if (null != parameterList.getLivingSubjectAdministrativeGender() && parameterList.getLivingSubjectAdministrativeGender().size() > 0) {
            String gender = parameterList.getLivingSubjectAdministrativeGender().get(0).getValue().get(0).getCode();
            pdq.setSex(SharedEnums.SexType.getByString(gender));
        }

        if (null != parameterList.getLivingSubjectBirthTime() && parameterList.getLivingSubjectBirthTime().size() > 0) {
            //TODO A date or date range. This parameter can convey an exact moment (e.g., January 1, 1960 @ 03:00:00 EST),
            //an approximate date (e.g., January 1960), or even a range of dates (e.g., December 1, 1959 through March 31, 1960).

            String dob = parameterList.getLivingSubjectBirthTime().get(0).getValue().get(0).getValue();
            try {
                Calendar birthDate = DateUtil.parseCalendar(dob, DateUtil.FORMAT_yyyyMMdd);
                pdq.setBirthDate(birthDate);
            } catch (ParseException e) {
                throw new PixPdqException(e);
            }
        }
        //TODO test for potential null pointer exception
        if (null != parameterList.getLivingSubjectName() && parameterList.getLivingSubjectName().size() > 0) {
            personName = new PersonName();
            EList<EnFamily> lastNames = parameterList.getLivingSubjectName().get(0).getValue().get(0).getFamily();
            if (null != lastNames && lastNames.size() > 0) {
                String lastname = V3util.getMixedValue(lastNames.get(0).getMixed());
                personName.setLastName(lastname);
            }
            EList<EnGiven> firstNames = parameterList.getLivingSubjectName().get(0).getValue().get(0).getGiven();
            if (null != firstNames && firstNames.size() > 0) {
                String firstname = V3util.getMixedValue(firstNames.get(0).getMixed());
                personName.setFirstName(firstname);
            }
            pdq.setPersonName(personName);
        }

        if (null != parameterList.getPatientAddress() && parameterList.getPatientAddress().size() > 0) {
            address = new Address();
            EList<AdxpStreetAddressLine> addessLines = parameterList.getPatientAddress().get(0).getValue().get(0).getStreetAddressLine();
            if (null != addessLines && addessLines.size() > 0) {
                String addLine1 = V3util.getMixedValue(addessLines.get(0).getMixed());
                address.setAddLine1(addLine1);

                //Address Line2
                if (addessLines.size() > 1 && null != addessLines.get(1).getMixed()) {
                    String addLine2 = V3util.getMixedValue(addessLines.get(1).getMixed());
                    address.setAddLine2(addLine2);
                }
            }
            EList<AdxpCity> cities = parameterList.getPatientAddress().get(0).getValue().get(0).getCity();
            if (null != cities && cities.size() > 0) {
                String city = V3util.getMixedValue(cities.get(0).getMixed());
                address.setAddCity(city);
            }
            EList<AdxpState> states = parameterList.getPatientAddress().get(0).getValue().get(0).getState();
            if (null != states && states.size() > 0) {
                String state = V3util.getMixedValue(states.get(0).getMixed());
                address.setAddState(state);
            }
            EList<AdxpPostalCode> postalCodes = parameterList.getPatientAddress().get(0).getValue().get(0).getPostalCode();
            if (null != postalCodes && postalCodes.size() > 0) {
                String zipCode = V3util.getMixedValue(postalCodes.get(0).getMixed());
                address.setAddZip(zipCode);
            }
            EList<AdxpCounty> countys = parameterList.getPatientAddress().get(0).getValue().get(0).getCounty();
            if (null != countys && countys.size() > 0) {
                String county = V3util.getMixedValue(countys.get(0).getMixed());
                address.setAddCounty(county);
            }
            EList<AdxpCountry> countrys = parameterList.getPatientAddress().get(0).getValue().get(0).getCountry();
            if (null != countrys && countrys.size() > 0) {
                String country = V3util.getMixedValue(countrys.get(0).getMixed());
                address.setAddCountry(country);
            }
            pdq.setAddress(address);
        }

        return pdq;
    }

    public int getRecordRequestNumber() {
        int recordRequestNumber = -1;  //-1 indicates no limit
        if (null != qryByParam && null != qryByParam.getInitialQuantity()) {
            recordRequestNumber = qryByParam.getInitialQuantity().getValue().intValue();
        }
        if (null != qryCancelOrContinuation && null != qryCancelOrContinuation.getContinuationQuantity()) {
            recordRequestNumber = qryCancelOrContinuation.getContinuationQuantity().getValue().intValue();
        }
        return recordRequestNumber;
    }

    public PRPAMT201306UV02QueryByParameter getQryByParam() {
        return qryByParam;

    }

    public II getQryId() {
        return getQryByParam().getQueryId();
    }

    public II getQryCancelId() {
        return qryCancelOrContinuation.getQueryId();
    }

    public II getContinuationQryId() {
        return qryCancelOrContinuation.getQueryId();
    }

    public String getContinuationPointer() {
        String pointer = qryCancelOrContinuation.getQueryId().getRoot() + ":" + qryCancelOrContinuation.getQueryId().getExtension();
        return pointer;
    }

}
