/**
 *  Copyright (c) 2009-2011 Misys Open Source Solutions (MOSS) and others
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *  implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 *  Contributors:
 *    Misys Open Source Solutions - initial API and implementation
 *    Moin Islam                  - v3 implementation
 */
package org.openhealthtools.openpixpdq.impl.v3;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.emf.ecore.util.FeatureMapUtil;
import org.hl7.v3.*;
import org.openhealthtools.openexchange.datamodel.PatientIdentifier;
import org.openhealthtools.openexchange.utils.Pair;
import org.openhealthtools.openexchange.utils.StringUtil;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

public class QueryResponseV3 {
    private static Log log = LogFactory.getLog(QueryResponseV3.class);

    private DocumentRoot v3QueryResponseMessage;
    private PRPAIN201310UV02Type rootElement;
    private PRPAIN201310UV02MFMIMT700711UV01ControlActProcess controlActProcess;
    private MCCIMT000300UV01Acknowledgement ack;

    public QueryResponseV3() {
        // The document root
        v3QueryResponseMessage = V3Factory.eINSTANCE.createDocumentRoot();
        rootElement = V3Factory.eINSTANCE.createPRPAIN201310UV02Type();
        rootElement.setITSVersion("XML_1.0");
        // create an id and set it
        rootElement.setId(V3util.createII(String.valueOf(UUID.randomUUID()), "", ""));
        // set current time
        rootElement.setCreationTime(V3util.createTS1CurrentTime());
        // set the interaction id for ACK
        rootElement.setInteractionId(V3util.createII("2.16.840.1.113883.1.6", "PRPA_IN201310UV02", ""));
        rootElement.setProcessingCode(V3util.createCS1("P"));
        rootElement.setProcessingModeCode(V3util.createCS1("T"));
        rootElement.setAcceptAckCode(V3util.createCS1("NE"));

        //rootElement.
        controlActProcess = V3Factory.eINSTANCE.createPRPAIN201310UV02MFMIMT700711UV01ControlActProcess();
        controlActProcess.setCode(V3util.createCD("PRPA_TE201310UV02", null));
        controlActProcess.setClassCode(ActClassControlAct.CACT);
        controlActProcess.setMoodCode(XActMoodIntentEvent.EVN);

        rootElement.setControlActProcess(controlActProcess);
        v3QueryResponseMessage.setPRPAIN201310UV02(rootElement);
    }

    public void addAckDetail(ErrorDetail error) {
        List<ErrorDetail> errorList = new ArrayList<ErrorDetail>();
        errorList.add(error);
        addAckDetail(errorList);
    }

    public void addAckDetail(List<ErrorDetail> errorList) {
        for (Iterator<ErrorDetail> iter = errorList.iterator(); iter.hasNext(); ) {
            ErrorDetail error = iter.next();

            if (log.isDebugEnabled()) {
                log.debug("Ack error: " + error.toString());
            }

            MCCIMT000300UV01AcknowledgementDetail ackDetail = V3Factory.eINSTANCE.createMCCIMT000300UV01AcknowledgementDetail();
            ackDetail.setTypeCode(AcknowledgementDetailType.E);

            //Must have a meaningful message
            assert error.getMessage() != null;
            ED errorDesc = V3Factory.eINSTANCE.createED();
            FeatureMapUtil.addText(errorDesc.getMixed(), error.getMessage());
            ackDetail.setText(errorDesc);

            //Add error code
            if (StringUtil.goodString(error.getCode())) {
                CE ce = V3Factory.eINSTANCE.createCE();
                ce.setCode(error.getCode());
                ackDetail.setCode(ce);
            }

            //Add error location
            if (StringUtil.goodString(error.getLocation())) {
                ST1 location = V3Factory.eINSTANCE.createST1();
                FeatureMapUtil.addText(location.getMixed(), error.getLocation());
                ackDetail.getLocation().add(location);
            }

            ack.getAcknowledgementDetail().add(ackDetail);
        }
    }

    public void setAck(String ackTypeCode, Pair<String, String> OrgMsg) {
        ack = V3Factory.eINSTANCE.createMCCIMT000300UV01Acknowledgement();
        //set ack type code based on the cases defined in ITI doc, AA - success, AE - error
        ack.setTypeCode(V3util.createCS1(ackTypeCode));
        MCCIMT000300UV01TargetMessage trm = V3Factory.eINSTANCE.createMCCIMT000300UV01TargetMessage();
        trm.setId(V3util.createII(OrgMsg.first, OrgMsg.second, ""));
        ack.setTargetMessage(trm);
        rootElement.getAcknowledgement().add(0, ack);
    }

    public void setQueryAck(String ResponseCode, String qryIdRoot, String QryIdExt) {
        MFMIMT700711UV01QueryAck qryAck = V3Factory.eINSTANCE.createMFMIMT700711UV01QueryAck();
        qryAck.setQueryId(V3util.createII(qryIdRoot, QryIdExt, null));
        // OK = data found; NF = data not found
        qryAck.setQueryResponseCode(V3util.createCS1(ResponseCode));
        controlActProcess.setQueryAck(qryAck);
    }

    public void setQueryByParameter(PRPAMT201307UV02QueryByParameter qryByParam) {
        // set the QueryByParameter element from request message
        controlActProcess.setQueryByParameter(qryByParam);
    }

    public void setSender(String senderOID, String facilityOID) {
        rootElement.setSender(V3util.createMCCIMT000300UV01Sender(senderOID, facilityOID));
    }

    public void setSubject(List<PatientIdentifier> pids) {
        if (pids.size() == 0)
            return;

        //Add subject
        PRPAIN201310UV02MFMIMT700711UV01Subject1 subject = V3Factory.eINSTANCE.createPRPAIN201310UV02MFMIMT700711UV01Subject1();
        subject.setTypeCode("SUBJ");
        controlActProcess.getSubject().add(subject);

        //Add registrationEvent
        PRPAIN201310UV02MFMIMT700711UV01RegistrationEvent registrationEvent = V3Factory.eINSTANCE.createPRPAIN201310UV02MFMIMT700711UV01RegistrationEvent();
        registrationEvent.setClassCode(XActClassDocumentEntryAct.REG);
        registrationEvent.setMoodCode(XActMoodIntentEvent.EVN);

        //TODO set <id nullFlavor="NA"/>
        registrationEvent.setStatusCode(V3util.createCS1("active"));

        PRPAIN201310UV02MFMIMT700711UV01Subject2 subject1 = V3Factory.eINSTANCE.createPRPAIN201310UV02MFMIMT700711UV01Subject2();
        subject1.setTypeCode(ParticipationTargetSubject.SBJ);

        PRPAMT201304UV02Patient patient = V3Factory.eINSTANCE.createPRPAMT201304UV02Patient();
        patient.setClassCode("PAT");
        for (int i = 0; i < pids.size(); i++) {
            String idRoot = pids.get(i).getAssigningAuthority().getUniversalId();
            String idExt = pids.get(i).getId();
            patient.getId().add(V3util.createII(idRoot, idExt, null));
        }
        patient.setStatusCode(V3util.createCS1("active"));

        PRPAMT201304UV02Person person = V3Factory.eINSTANCE.createPRPAMT201304UV02Person();
        person.setClassCode(EntityClassLivingSubjectMember1.PSN);
        person.setDeterminerCode(EntityDeterminerMember2.INSTANCE);

        //Add null name
        PN name = V3Factory.eINSTANCE.createPN();
        name.setNullFlavor(NoInformationMember2.getByName("NA"));
        person.getName().add(name);

        patient.setPatientPerson(person);

        subject1.setPatient(patient);

        registrationEvent.setSubject1(subject1);

        MFMIMT700711UV01Custodian custodian = V3Factory.eINSTANCE.createMFMIMT700711UV01Custodian();
        custodian.setTypeCode(ParticipationParticipationMember8.CST);
        registrationEvent.setCustodian(custodian);

        COCTMT090003UV01AssignedEntity assignedEntity = V3Factory.eINSTANCE.createCOCTMT090003UV01AssignedEntity();
        custodian.setAssignedEntity(assignedEntity);
        assignedEntity.setClassCode(RoleClassAssignedEntityMember1.ASSIGNED);

        //TODO: get organizationOID
        String organizationOID = "1.2.3";
        assignedEntity.getId().add(V3util.createII(organizationOID, "", ""));
//		COCTMT090003UV01Organization assignedOrganization = V3Factory.eINSTANCE.createCOCTMT090003UV01Organization();
//		assignedEntity.setAssignedOrganization(assignedOrganization);		
//		assignedOrganization.setClassCode(EntityClassOrganizationMember1.ORG);  
//		assignedOrganization.setDeterminerCode(EntityDeterminerMember2.INSTANCE);
//		EN name = V3Factory.eINSTANCE.createEN();
//		assignedOrganization.getName().add(name);
//		FeatureMapUtil.addText(name.getMixed(), organizationName);

        subject.setRegistrationEvent(registrationEvent);

    }

    public void setReceiver(String recevierOID, String facilityOID) {
        rootElement.getReceiver().add(V3util.createMCCIMT000300UV01Receiver(recevierOID, facilityOID));
    }

    public DocumentRoot getResponse() {
        return v3QueryResponseMessage;
    }

}
