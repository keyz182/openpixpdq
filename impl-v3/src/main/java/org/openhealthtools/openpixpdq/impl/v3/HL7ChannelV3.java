/**
 *  Copyright (c) 2009-2011 Misys Open Source Solutions (MOSS) and others
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *  implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 *  Contributors:
 *    Misys Open Source Solutions - initial API and implementation
 *    Moin Islam                  - v3 implementation
 */
package org.openhealthtools.openpixpdq.impl.v3;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.soap.SOAP12Constants;
import org.apache.axis2.AxisFault;
import org.apache.axis2.Constants;
import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.description.WSDL2Constants;
import org.apache.log4j.Logger;
import org.openhealthtools.openexchange.actorconfig.net.ConnectionUtil;
import org.openhealthtools.openexchange.actorconfig.net.IConnectionDescription;
import org.w3c.dom.Element;

public class HL7ChannelV3 {

    /* Logger for problems during HL7 exchanges */
    private static final Logger log = Logger.getLogger(HL7ChannelV3.class);

    private static final String UPDATE_ACTION = "urn:hl7-org:v3:PRPA_IN201302UV02";

    /* The connection description for this server */
    private IConnectionDescription connection = null;

    private String consumerUrl;

    public HL7ChannelV3(IConnectionDescription connection) {
        this.connection = connection;
        consumerUrl = ConnectionUtil.getTransactionEndpoint(connection);
        //consumerUrl = "http://"+ connection.getHostname() +":" + Integer.toString(connection.getPort())+"/"+ connection.getUrlPath();
        log.info("PIX Update Notification consumer url: " + consumerUrl);
    }

    public String sendMessage(Element request) throws Exception {
        ServiceClient sender = getServiceClient(UPDATE_ACTION);
        //OMElement response = sender.sendReceive(AXIOMUtil.stringToOM(request));
        OMElement response = sender.sendReceive(org.apache.axis2.util.XMLUtils.toOM(request));
        return response.toString();
    }

    private ServiceClient getServiceClient(String action) throws AxisFault {
        ServiceClient sender = new ServiceClient();
        boolean enableMTOM = false;
        sender.setOptions(getOptions(action, enableMTOM, consumerUrl));
        sender.engageModule("addressing");
        return sender;
    }

    protected Options getOptions(String action, boolean enableMTOM, String url) {
        Options options = new Options();
        options.setAction(action);
        options.setProperty(WSDL2Constants.ATTRIBUTE_MUST_UNDERSTAND, "1");
        options.setTo(new EndpointReference(url));
        options.setTransportInProtocol(Constants.TRANSPORT_HTTP);
        options.setTimeOutInMilliSeconds(60000);
        if (enableMTOM)
            options.setProperty(Constants.Configuration.ENABLE_MTOM, Constants.VALUE_TRUE);
        else
            options.setProperty(Constants.Configuration.ENABLE_MTOM, Constants.VALUE_FALSE);
        //use SOAP12,
        options.setSoapVersionURI(SOAP12Constants.SOAP_ENVELOPE_NAMESPACE_URI);
        return options;
    }

}
