/**
 *  Copyright (c) 2009-2011 Misys Open Source Solutions (MOSS) and others
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *  implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 *  Contributors:
 *    Misys Open Source Solutions - initial API and implementation
 *    Moin Islam                  - v3 implementation
 */
package org.openhealthtools.openpixpdq.impl.v3;

import org.eclipse.emf.ecore.EPackage;
import org.hl7.v3.V3Package;

/**
 * The Factory to get PIXPDQ v3 objects.
 *
 * @author <a href="mailto:wenzhi.li@misys.com">Wenzhi Li</a>
 */
public class PixPdqV3Factory {

    static {
        EPackage.Registry.INSTANCE.put(V3Package.eNS_URI,
                V3Package.eINSTANCE);
    }

    static PixFeedHandlerV3 getPixFeedV3Handler() {
        return PixFeedHandlerV3.getInstance();
    }

    static PixQueryHandlerV3 getPixQueryV3Handler() {
        return PixQueryHandlerV3.getInstance();
    }

    static PdQueryHandlerV3 getPdQueryV3Handler() {
        return PdQueryHandlerV3.getInstance();
    }

}
