/**
 *  Copyright (c) 2009-2011 Misys Open Source Solutions (MOSS) and others
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *  implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 *  Contributors:
 *    Misys Open Source Solutions - initial API and implementation
 *    Moin Islam                  - v3 implementation
 */
package org.openhealthtools.openpixpdq.impl.v3;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMNamespace;
import org.apache.axiom.soap.SOAPEnvelope;
import org.apache.axiom.soap.SOAPFactory;
import org.apache.axis2.AxisFault;
import org.apache.axis2.context.MessageContext;
import org.apache.axis2.description.AxisOperation;
import org.apache.axis2.util.JavaUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class PDQSupplierServiceMessageReceiverInOut extends org.apache.axis2.receivers.AbstractInOutMessageReceiver {

    private static Log log = LogFactory.getLog(PDQSupplierServiceMessageReceiverInOut.class);

    @Override
    public void invokeBusinessLogic(MessageContext msgContext, MessageContext newMsgContext) throws org.apache.axis2.AxisFault {
        try {
            // get the implementation class for the Web Service
            Object obj = getTheImplementationObject(msgContext);
            PDQSupplierService supplier = (PDQSupplierService) obj;
            //Out Envelop
            SOAPEnvelope envelope = null;
            //Find the axisOperation that has been set by the Dispatch phase.
            AxisOperation op = msgContext.getOperationContext().getAxisOperation();
            if (op == null) {
                throw new AxisFault("Operation is not located, if this is doclit style the SOAP-ACTION should specified via the SOAP Action to use the RawXMLProvider");
            }
            java.lang.String methodName;
            if ((op.getName() != null) && ((methodName = JavaUtils.xmlNameToJavaIdentifier(op.getName().getLocalPart())) != null)) {
                if ("pDQSupplier_PRPA_IN201305UV02".equals(methodName)) {
                    OMElement PRPA_IN201306UV02 = null;
                    OMElement wrappedParam = (OMElement) fromOM(
                            msgContext.getEnvelope().getBody().getFirstElement(),
                            OMElement.class,
                            getEnvelopeNamespaces(msgContext.getEnvelope()));

                    PRPA_IN201306UV02 = supplier.PDQSupplier_PRPA_IN201305UV02(wrappedParam);
                    envelope = toEnvelope(getSOAPFactory(msgContext), PRPA_IN201306UV02, false);
                } else if ("pDQSupplier_QUQI_IN000003UV01_Continue".equals(methodName)) {
                    OMElement MCCI_IN000002UV015 = null;
                    OMElement wrappedParam = (OMElement) fromOM(
                            msgContext.getEnvelope().getBody().getFirstElement(),
                            OMElement.class,
                            getEnvelopeNamespaces(msgContext.getEnvelope()));

                    MCCI_IN000002UV015 = supplier.PDQSupplier_QUQI_IN000003UV01_Continue(wrappedParam);
                    envelope = toEnvelope(getSOAPFactory(msgContext), MCCI_IN000002UV015, false);
                } else if ("pDQSupplier_QUQI_IN000003UV01_Cancel".equals(methodName)) {
                    OMElement MCCI_IN000002UV019 = null;
                    OMElement wrappedParam = (OMElement) fromOM(
                            msgContext.getEnvelope().getBody().getFirstElement(),
                            OMElement.class,
                            getEnvelopeNamespaces(msgContext.getEnvelope()));

                    MCCI_IN000002UV019 = supplier.PDQSupplier_QUQI_IN000003UV01_Cancel(wrappedParam);
                    envelope = toEnvelope(getSOAPFactory(msgContext), MCCI_IN000002UV019, false);

                } else {
                    log.error("Requested method name :" + methodName + " not found");
                    throw new java.lang.RuntimeException("method not found");
                }


                newMsgContext.setEnvelope(envelope);
            }
        } catch (java.lang.Exception e) {
            throw org.apache.axis2.AxisFault.makeFault(e);
        }
    }

    private org.apache.axiom.soap.SOAPEnvelope toEnvelope(SOAPFactory factory, org.apache.axiom.om.OMElement param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {
        org.apache.axiom.soap.SOAPEnvelope envelope = factory.getDefaultEnvelope();
        envelope.getBody().addChild(param);
        return envelope;
    }

    private Map<String, String> getEnvelopeNamespaces(org.apache.axiom.soap.SOAPEnvelope env) {
        Map<String, String> returnMap = new HashMap<String, String>();
        Iterator<?> namespaceIterator = env.getAllDeclaredNamespaces();
        while (namespaceIterator.hasNext()) {
            OMNamespace ns = (OMNamespace) namespaceIterator.next();
            returnMap.put(ns.getPrefix(), ns.getNamespaceURI());
        }
        return returnMap;
    }

    private org.apache.axiom.om.OMElement fromOM(OMElement param, Class<?> type, Map<?, ?> extraNamespaces) throws AxisFault {
        return param;
    }


}
