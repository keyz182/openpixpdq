/**
 *  Copyright (c) 2009-2011 Misys Open Source Solutions (MOSS) and others
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *  implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 *  Contributors:
 *    Misys Open Source Solutions - initial API and implementation
 *    Moin Islam                  - v3 implementation
 */
package org.openhealthtools.openpixpdq.impl.v3;

import org.apache.log4j.Logger;
import org.openhealthtools.openexchange.actorconfig.IActorDescription;
import org.openhealthtools.openexchange.actorconfig.IheConfigurationException;
import org.openhealthtools.openexchange.audit.IheAuditTrail;
import org.openhealthtools.openpixpdq.api.IPdSupplier;
import org.openhealthtools.openpixpdq.common.HL7Actor;

/**
 * This is the Patient Demographics Supplier (PDS) V3 actor, the server side
 * actor of the IHE Patient Demographics Query (PDQ) V3 profile. This actor
 * accepts HL7 v3 messages such as PRPA_IN201305UV02 and QUQI_IN000003UV01
 * from a PDQ Consumer.
 *
 * @author Wenzhi Li
 * @version 1.0, Mar 27, 2007
 */
public class PdSupplierV3 extends HL7Actor implements IPdSupplier {
    /* Logger for problems */
    private static Logger log = Logger.getLogger(PdSupplierV3.class);

    /**
     * Creates a new PdSupplierV3 that will talk to a PDQ consumer over
     * the actor description supplied.
     *
     * @param actor The actor description of this PD Supplier
     * @throws IheConfigurationException
     */
    public PdSupplierV3(IActorDescription actorDescription, IheAuditTrail auditTrail) throws IheConfigurationException {
        super(actorDescription, auditTrail);
    }

    @Override
    public void start() {
        //call the super one to initiate standard start process
        super.start();

        PdQueryHandlerV3 pdQueryHandler = PixPdqV3Factory.getPdQueryV3Handler();
        pdQueryHandler.registryPdSupplier(this);

        if (log.isInfoEnabled()) {
            log.info("Started Patient Demographics Supplier V3: " + this.getName());
        }
    }

    @Override
    public void stop() {
        //call the super one to initiate standard stop process
        super.stop();

        if (log.isInfoEnabled()) {
            log.info("Stopped Patient Demographics Supplier V3: " + this.getName());
        }
    }

}
