/**
 *  Copyright (c) 2009-2011 Misys Open Source Solutions (MOSS) and others
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *  implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 *  Contributors:
 *    Misys Open Source Solutions - initial API and implementation
 *    Moin Islam                  - v3 implementation
 */
package org.openhealthtools.openpixpdq.impl.v3;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.emf.ecore.util.FeatureMapUtil;
import org.hl7.v3.*;
import org.openhealthtools.openexchange.utils.Pair;
import org.openhealthtools.openexchange.utils.StringUtil;

import java.text.SimpleDateFormat;
import java.util.*;

public class MCCI_IN000002UV01Ack {
    private static Log log = LogFactory.getLog(MCCI_IN000002UV01Ack.class);

    private DocumentRoot v3AckMessage = null;
    private MCCIIN000002UV01Type rootElement = null;
    MCCIMT000200UV01Acknowledgement ack;
    MCCIMT000200UV01AcknowledgementDetail ackDetail;

    public MCCI_IN000002UV01Ack(Pair<String, String> orgMsgID, String senderApplicationOID,
                                String senderFacilityOID, String receiverApplicationOID,
                                String receiverFacilityOID) {

        // The document root
        v3AckMessage = V3Factory.eINSTANCE.createDocumentRoot();
        rootElement = V3Factory.eINSTANCE.createMCCIIN000002UV01Type();
        rootElement.setITSVersion("XML_1.0");
        // create an id and set it
        rootElement.setId(createII(String.valueOf(UUID.randomUUID()), "", ""));
        // set current time
        rootElement.setCreationTime(createTS1CurrentTime());
        // set the interaction id for ACK
        rootElement.setInteractionId(createII("2.16.840.1.113883.1.6",
                "MCCI_IN000002UV01", ""));
        rootElement.setProcessingCode(createCS1("P"));
        rootElement.setProcessingModeCode(createCS1("R"));
        // The acceptAckCode SHALL be set to NE
        rootElement.setAcceptAckCode(createCS1("NE"));
        rootElement.setSender(createMCCIMT000200UV01Sender(
                senderApplicationOID, senderFacilityOID));
        rootElement.getReceiver().add(
                createMCCIMT000100UV01Receiver(receiverApplicationOID,
                        receiverFacilityOID));

        ack = V3Factory.eINSTANCE.createMCCIMT000200UV01Acknowledgement();
        ack.setTypeCode(createCS1("CA"));
        MCCIMT000200UV01TargetMessage trm = V3Factory.eINSTANCE
                .createMCCIMT000200UV01TargetMessage();
        trm.setId(createII(orgMsgID.first, orgMsgID.second, ""));
        ack.setTargetMessage(trm);

        rootElement.getAcknowledgement().add(0, ack);

        v3AckMessage.setMCCIIN000002UV01(rootElement);

    }

    private MCCIMT000200UV01Receiver createMCCIMT000100UV01Receiver(
            String applicationOID, String facilityOID) {
        // create the receiver
        MCCIMT000200UV01Receiver receiver = V3Factory.eINSTANCE
                .createMCCIMT000200UV01Receiver();

        // create the receiver's communication function type
        CommunicationFunctionType receivercft = CommunicationFunctionType.RCV;

        // set the typecode of the receiver
        receiver.setTypeCode(receivercft);

        // now add the receiver device to the receiver
        receiver.setDevice(createMCCIMT000100UV01Device(applicationOID,
                facilityOID));

        // return the receiver
        return receiver;
    }

    public DocumentRoot getAck() {
        return v3AckMessage;
    }

    public void setAckType(String ackCode) {
        ack.setTypeCode(createCS1(ackCode));
    }

    public void addAckDetail(ErrorDetail error) {
        List<ErrorDetail> errorList = new ArrayList<ErrorDetail>();
        errorList.add(error);
        addAckDetail(errorList);
    }

    public void addAckDetail(List<ErrorDetail> errorList) {
        for (Iterator<ErrorDetail> iter = errorList.iterator(); iter.hasNext(); ) {
            ErrorDetail error = iter.next();

            if (log.isDebugEnabled()) {
                log.debug("Ack error: " + error.toString());
            }

            MCCIMT000200UV01AcknowledgementDetail ackDetail = V3Factory.eINSTANCE.createMCCIMT000200UV01AcknowledgementDetail();
            ackDetail.setTypeCode(AcknowledgementDetailType.E);

            //Must have a meaningful message
            assert error.getMessage() != null;
            ED errorDesc = V3Factory.eINSTANCE.createED();
            FeatureMapUtil.addText(errorDesc.getMixed(), error.getMessage());
            ackDetail.setText(errorDesc);

            //Add error code
            if (StringUtil.goodString(error.getCode())) {
                CE ce = V3Factory.eINSTANCE.createCE();
                ce.setCode(error.getCode());
                ackDetail.setCode(ce);
            }

            //Add error location
            if (StringUtil.goodString(error.getLocation())) {
                ST1 location = V3Factory.eINSTANCE.createST1();
                FeatureMapUtil.addText(location.getMixed(), error.getLocation());
                ackDetail.getLocation().add(location);
            }

            ack.getAcknowledgementDetail().add(ackDetail);
        }
    }

    private II createII(String root, String extension, String namespace) {
        // create an ID to identify this message
        II idII = V3Factory.eINSTANCE.createII();
        // for now set these to a fixed root and extension
        if (null != root && "" != root)
            idII.setRoot(root);
        if (null != extension && "" != extension)
            idII.setExtension(extension);
        if (null != namespace && "" != namespace)
            idII.setAssigningAuthorityName(namespace);

        return idII;
    }

    private MCCIMT000200UV01Sender createMCCIMT000200UV01Sender(
            String applicationOID, String facilityOID) {
        // create the sender

        MCCIMT000200UV01Sender sender = V3Factory.eINSTANCE
                .createMCCIMT000200UV01Sender();

        // create the sender's communication function type
        CommunicationFunctionType senderCFT = CommunicationFunctionType.SND;

        // set the typecode of the sender
        sender.setTypeCode(senderCFT);

        // now add the sender device to the sender
        sender.setDevice(createMCCIMT000100UV01Device(applicationOID,
                facilityOID));

        // return the sender
        return sender;
    }

    private MCCIMT000200UV01Device createMCCIMT000100UV01Device(String applicationOID, String facilityOID) {
        // create the sender device
        MCCIMT000200UV01Device device = V3Factory.eINSTANCE.createMCCIMT000200UV01Device();

        // Set the device class code
        device.setClassCode(EntityClassDevice.DEV);
        device.setDeterminerCode(EntityDeterminerMember2.INSTANCE);

        device.getId().add(createII(applicationOID, "", ""));

        MCCIMT000200UV01Agent asAgent = V3Factory.eINSTANCE.createMCCIMT000200UV01Agent();
        asAgent.setClassCode(RoleClassAgentMember1.AGNT);

        MCCIMT000200UV01Organization org = V3Factory.eINSTANCE.createMCCIMT000200UV01Organization();
        org.setClassCode(EntityClassOrganizationMember1.ORG);
        org.setDeterminerCode(EntityDeterminerMember2.INSTANCE);
        org.getId().add(createII(facilityOID, "", ""));
        asAgent.setRepresentedOrganization(org);

        device.setAsAgent(asAgent);

        // return the sender
        return device;
    }

    private TS1 createTS1(String time) {
        // create a TS1 element
        TS1 ts1 = V3Factory.eINSTANCE.createTS1();

        // set the current time
        ts1.setValue(time);

        return ts1;
    }

    private TS1 createTS1CurrentTime() {
        // create a simple date format
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");

        return createTS1(dateFormat.format(new Date()));
    }

    private CS1 createCS1(String code) {
        CS1 cs1 = V3Factory.eINSTANCE.createCS1();
        cs1.setCode(code);
        return cs1;
    }    /*
     * private CE createCE(String code){ CE ce = V3Factory.eINSTANCE.createCE();
	 * ce.setCode(code); return ce; }
	 */

}
