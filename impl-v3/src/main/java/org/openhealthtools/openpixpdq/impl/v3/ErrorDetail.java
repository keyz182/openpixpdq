/**
 *  Copyright (c) 2009-2011 Misys Open Source Solutions (MOSS) and others
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *  implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 *  Contributors:
 *    Misys Open Source Solutions - initial API and implementation
 *    Moin Islam                  - v3 implementation
 */
package org.openhealthtools.openpixpdq.impl.v3;

/**
 * This is a data class representing HL7 v3 acknowledge detail including
 * error code, error message and error location etc.
 *
 * @author <a href="mailto:wenzhi.li@misys.com">Wenzhi Li</a>
 */
public class ErrorDetail {
    /**
     * The detail error message
     */
    private String message = null;
    /**
     * The error code
     */
    private String code = null;
    /**
     * The error location in XPath expression
     */
    private String location = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();

        if (code != null) {
            sb.append("Code:");
            sb.append(code);
            sb.append(" ");
        }
        if (message != null) {
            sb.append("Message:");
            sb.append(message);
            sb.append(" ");
        }
        if (location != null) {
            sb.append("Location:");
            sb.append(location);
        }
        return sb.toString();
    }
}
