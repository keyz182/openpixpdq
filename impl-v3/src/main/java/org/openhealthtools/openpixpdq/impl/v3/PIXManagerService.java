/**
 *  Copyright (c) 2009-2011 Misys Open Source Solutions (MOSS) and others
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *  implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 *  Contributors:
 *    Misys Open Source Solutions - initial API and implementation
 *    Moin Islam                  - v3 implementation
 */
package org.openhealthtools.openpixpdq.impl.v3;

import org.apache.axiom.om.OMElement;
import org.apache.axis2.AxisFault;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.emf.common.util.URI;
import org.hl7.v3.DocumentRoot;
import org.hl7.v3.util.V3ResourceFactoryImpl;
import org.openhealthtools.openpixpdq.common.PixPdqSysLogService;
import org.w3c.dom.Element;

/**
 * PIXManagerService java skeleton for the axisService
 */
public class PIXManagerService extends PixPdqSysLogService {
    private static Log log = LogFactory.getLog(PIXManagerService.class);

    public org.apache.axiom.om.OMElement PIXManager_PRPA_IN201309UV02(
            org.apache.axiom.om.OMElement PRPA_IN201309UV02) throws AxisFault {
        OMElement payload = null;
        Element domElement = null;
        DocumentRoot retMessage = null;
        Element retElement = null;
        try {
            beginTransaction(getRTransactionName(PRPA_IN201309UV02), PRPA_IN201309UV02);
            if (logMessage != null)
                logMessage.setTestMessage(getRTransactionName(PRPA_IN201309UV02));
            domElement = org.apache.axis2.util.XMLUtils
                    .toDOM(PRPA_IN201309UV02);
            DocumentRoot v3Message = (DocumentRoot) V3util.transformDomToEmf(
                    domElement, new V3ResourceFactoryImpl(), URI
                    .createURI(org.hl7.v3.V3Package.eNS_URI), true);
            PixQueryHandlerV3 pix = PixPdqV3Factory.getPixQueryV3Handler();
            retMessage = pix.processMessage(v3Message);

            retElement = V3util.transformEmfToDom(retMessage,
                    new V3ResourceFactoryImpl(), org.hl7.v3.V3Package.eNS_URI);
            payload = org.apache.axis2.util.XMLUtils.toOM(retElement);

            //log the message and stop transaction
            logResponse(payload);
            stopTransactionLog();
        } catch (Exception e) {
            log.error(e);
            endTransaction(PRPA_IN201309UV02, e, e.getMessage());
            throw AxisFault.makeFault(e);
        }

        return payload;
    }

    public org.apache.axiom.om.OMElement PIXManager_PRPA_IN201301UV02(
            org.apache.axiom.om.OMElement PRPA_IN201301UV02) throws AxisFault {
        OMElement payload = null;
        Element domElement = null;
        DocumentRoot retMessage = null;
        Element retElement = null;
        try {
            beginTransaction(getRTransactionName(PRPA_IN201301UV02), PRPA_IN201301UV02);
            if (logMessage != null)
                logMessage.setTestMessage(getRTransactionName(PRPA_IN201301UV02));

            domElement = org.apache.axis2.util.XMLUtils
                    .toDOM(PRPA_IN201301UV02);
            DocumentRoot v3Message = (DocumentRoot) V3util.transformDomToEmf(
                    domElement, new V3ResourceFactoryImpl(), URI
                    .createURI(org.hl7.v3.V3Package.eNS_URI), true);
            PixFeedHandlerV3 pix = PixPdqV3Factory.getPixFeedV3Handler();
            retMessage = pix.processMessage(v3Message);

            retElement = V3util.transformEmfToDom(retMessage,
                    new V3ResourceFactoryImpl(), org.hl7.v3.V3Package.eNS_URI);
            payload = org.apache.axis2.util.XMLUtils.toOM(retElement);

            //log the message and stop transaction
            logResponse(payload);
            stopTransactionLog();
        } catch (Exception e) {
            log.error(e);
            endTransaction(PRPA_IN201301UV02, e, e.getMessage());
            throw AxisFault.makeFault(e);
        }

        return payload;
    }

    public org.apache.axiom.om.OMElement PIXManager_PRPA_IN201302UV02(
            org.apache.axiom.om.OMElement pRPA_IN201302UV02) throws AxisFault {
        OMElement payload = null;
        Element domElement = null;
        DocumentRoot retMessage = null;
        Element retElement = null;
        try {
            beginTransaction(getRTransactionName(pRPA_IN201302UV02), pRPA_IN201302UV02);
            if (logMessage != null)
                logMessage.setTestMessage(getRTransactionName(pRPA_IN201302UV02));

            domElement = org.apache.axis2.util.XMLUtils
                    .toDOM(pRPA_IN201302UV02);
            DocumentRoot v3Message = (DocumentRoot) V3util.transformDomToEmf(
                    domElement, new V3ResourceFactoryImpl(), URI
                    .createURI(org.hl7.v3.V3Package.eNS_URI), true);
            PixFeedHandlerV3 pix = PixPdqV3Factory.getPixFeedV3Handler();
            retMessage = pix.processMessage(v3Message);

            retElement = V3util.transformEmfToDom(retMessage,
                    new V3ResourceFactoryImpl(), org.hl7.v3.V3Package.eNS_URI);
            payload = org.apache.axis2.util.XMLUtils.toOM(retElement);

            //log the message and stop transaction
            logResponse(payload);
            stopTransactionLog();
        } catch (Exception e) {
            log.error(e);
            endTransaction(pRPA_IN201302UV02, e, e.getMessage());
            throw AxisFault.makeFault(e);
        }

        return payload;
    }

    public org.apache.axiom.om.OMElement PIXManager_PRPA_IN201304UV02(
            org.apache.axiom.om.OMElement PRPA_IN201304UV02) throws AxisFault {
        OMElement payload = null;
        Element domElement = null;
        DocumentRoot retMessage = null;
        Element retElement = null;
        try {
            beginTransaction(getRTransactionName(PRPA_IN201304UV02), PRPA_IN201304UV02);
            if (logMessage != null)
                logMessage.setTestMessage(getRTransactionName(PRPA_IN201304UV02));

            domElement = org.apache.axis2.util.XMLUtils
                    .toDOM(PRPA_IN201304UV02);
            DocumentRoot v3Message = (DocumentRoot) V3util.transformDomToEmf(
                    domElement, new V3ResourceFactoryImpl(), URI
                    .createURI(org.hl7.v3.V3Package.eNS_URI), true);
            PixFeedHandlerV3 pix = PixPdqV3Factory.getPixFeedV3Handler();
            retMessage = pix.processMessage(v3Message);

            retElement = V3util.transformEmfToDom(retMessage,
                    new V3ResourceFactoryImpl(), org.hl7.v3.V3Package.eNS_URI);
            payload = org.apache.axis2.util.XMLUtils.toOM(retElement);

            //log the message and stop transaction
            logResponse(payload);
            stopTransactionLog();
        } catch (Exception e) {
            log.error(e);
            endTransaction(PRPA_IN201304UV02, e, e.getMessage());
            throw AxisFault.makeFault(e);
        }

        return payload;
    }

}
