/**
 *  Copyright (c) 2009-2011 Misys Open Source Solutions (MOSS) and others
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *  implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 *  Contributors:
 *    Misys Open Source Solutions - initial API and implementation
 *    Moin Islam                  - v3 implementation
 */
package org.openhealthtools.openpixpdq.impl.v3;

import org.apache.axiom.om.OMElement;
import org.apache.axis2.AxisFault;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.emf.common.util.URI;
import org.hl7.v3.DocumentRoot;
import org.hl7.v3.util.V3ResourceFactoryImpl;
import org.openhealthtools.openpixpdq.common.PixPdqSysLogService;
import org.w3c.dom.Element;

/**
 * PIXManagerService java skeleton for the axisService
 */

public class PDQSupplierService extends PixPdqSysLogService {
    private static Log log = LogFactory.getLog(PDQSupplierService.class);

    public org.apache.axiom.om.OMElement PDQSupplier_PRPA_IN201305UV02(
            org.apache.axiom.om.OMElement PRPA_IN201305UV02) throws AxisFault {
        OMElement payload = null;
        Element domElement = null;
        DocumentRoot retMessage = null;
        Element retElement = null;
        try {
            beginTransaction(getRTransactionName(PRPA_IN201305UV02), PRPA_IN201305UV02);
            if (logMessage != null)
                logMessage.setTestMessage(getRTransactionName(PRPA_IN201305UV02));

            domElement = org.apache.axis2.util.XMLUtils.toDOM(PRPA_IN201305UV02);
            DocumentRoot v3Message = (DocumentRoot) V3util.transformDomToEmf(
                    domElement, new V3ResourceFactoryImpl(), URI
                    .createURI(org.hl7.v3.V3Package.eNS_URI), true);
            PdQueryHandlerV3 pdq = PixPdqV3Factory.getPdQueryV3Handler();
            retMessage = pdq.processMessage(v3Message);

            retElement = V3util.transformEmfToDom(retMessage,
                    new V3ResourceFactoryImpl(), org.hl7.v3.V3Package.eNS_URI);
            payload = org.apache.axis2.util.XMLUtils.toOM(retElement);

            //log the message and stop transaction
            logResponse(payload);
            stopTransactionLog();
        } catch (Exception e) {
            log.error(e);
            endTransaction(PRPA_IN201305UV02, e, e.getMessage());
            throw AxisFault.makeFault(e);
        }

        return payload;
    }

    public org.apache.axiom.om.OMElement PDQSupplier_QUQI_IN000003UV01_Continue(
            org.apache.axiom.om.OMElement QUQI_IN000003UV01) throws AxisFault {
        OMElement payload = null;
        Element domElement = null;
        DocumentRoot retMessage = null;
        Element retElement = null;
        try {
            beginTransaction(getRTransactionName(QUQI_IN000003UV01), QUQI_IN000003UV01);
            if (logMessage != null)
                logMessage.setTestMessage(getRTransactionName(QUQI_IN000003UV01));

            domElement = org.apache.axis2.util.XMLUtils.toDOM(QUQI_IN000003UV01);
            DocumentRoot v3Message = (DocumentRoot) V3util.transformDomToEmf(
                    domElement, new V3ResourceFactoryImpl(), URI
                    .createURI(org.hl7.v3.V3Package.eNS_URI), true);
            PdQueryHandlerV3 pdq = PixPdqV3Factory.getPdQueryV3Handler();
            retMessage = pdq.processMessage(v3Message);

            retElement = V3util.transformEmfToDom(retMessage,
                    new V3ResourceFactoryImpl(), org.hl7.v3.V3Package.eNS_URI);
            payload = org.apache.axis2.util.XMLUtils.toOM(retElement);
            //log the message and stop transaction
            logResponse(payload);
            stopTransactionLog();
        } catch (Exception e) {
            log.error(e);
            endTransaction(QUQI_IN000003UV01, e, e.getMessage());
            throw AxisFault.makeFault(e);
        }

        return payload;
    }

    public org.apache.axiom.om.OMElement PDQSupplier_QUQI_IN000003UV01_Cancel(
            org.apache.axiom.om.OMElement QUQI_IN000003UV01_Cancel) throws AxisFault {
        OMElement payload = null;
        Element domElement = null;
        DocumentRoot retMessage = null;
        Element retElement = null;
        try {
            beginTransaction(getRTransactionName(QUQI_IN000003UV01_Cancel), QUQI_IN000003UV01_Cancel);
            if (logMessage != null)
                logMessage.setTestMessage(getRTransactionName(QUQI_IN000003UV01_Cancel));

            domElement = org.apache.axis2.util.XMLUtils.toDOM(QUQI_IN000003UV01_Cancel);
            DocumentRoot v3Message = (DocumentRoot) V3util.transformDomToEmf(
                    domElement, new V3ResourceFactoryImpl(), URI
                    .createURI(org.hl7.v3.V3Package.eNS_URI), true);
            PdQueryHandlerV3 pdq = PixPdqV3Factory.getPdQueryV3Handler();
            retMessage = pdq.processMessage(v3Message);

            retElement = V3util.transformEmfToDom(retMessage,
                    new V3ResourceFactoryImpl(), org.hl7.v3.V3Package.eNS_URI);
            payload = org.apache.axis2.util.XMLUtils.toOM(retElement);
            //log the message and stop transaction
            logResponse(payload);
            stopTransactionLog();
        } catch (Exception e) {
            log.error(e);
            endTransaction(QUQI_IN000003UV01_Cancel, e, e.getMessage());
            throw AxisFault.makeFault(e);
        }

        return payload;
    }

}
