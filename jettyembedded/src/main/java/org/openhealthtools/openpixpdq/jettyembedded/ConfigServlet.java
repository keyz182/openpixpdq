/**
 *  Copyright (c) 2009-2011 Misys Open Source Solutions (MOSS) and others
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *  implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 *  Contributors:
 *    Misys Open Source Solutions - initial API and implementation
 *    -
 */

package org.openhealthtools.openpixpdq.jettyembedded;

import com.misys.hieportal.sysmon.IJMXEventNotifier;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openhealthtools.openexchange.config.BootStrapProperties;
import org.openhealthtools.openexchange.config.ConfigurationException;
import org.openhealthtools.openexchange.config.PropertyFacade;
import org.openhealthtools.openpixpdq.common.PatientBroker;
import org.openhealthtools.openpixpdq.common.PixPdqFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

;

/**
 * The starting servlet.Main functionality is to destroy all Actors
 */
public class ConfigServlet extends HttpServlet {
    private static Log log = LogFactory.getLog(ConfigServlet.class);

    public ConfigServlet() {
    }

    /*
     * Destroys all Actors
     */
    public void destroy() {
        PatientBroker.getInstance().unregisterPixManagers(null);
        PatientBroker.getInstance().unregisterPdSuppliers(null);

    }

    public void init() throws ServletException {
        try {
            String[] propertyFiles = BootStrapProperties.getPropertyFiles(
                    new String[]{"openpixpdq.properties"});
            PropertyFacade.loadProperties(propertyFiles);
        } catch (ConfigurationException e) {
            log.error("Failed to load openpixpdq.properties", e);
        }


        PixPdqConfigurationLoader loader = PixPdqConfigurationLoader.getInstance();

        boolean eventEnabled = PropertyFacade.getBoolean("enable.event");
        if (eventEnabled) {
            IJMXEventNotifier eventBean = (IJMXEventNotifier) PixPdqFactory.getInstance().getBean("pixEvent");
            loader.setEventBean(eventBean);
        }

        loader.loadActorConfiguration();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) {
        doPost(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) {
        //do nothing
    }
}