
                             O p e n P I X P D Q
 

  What is it? 
  -----------
  
  OpenPIXPDQ a server side implementation of the Patient Identifier Cross-
  Reference (PIX) and Patient Demographic Query (PDQ) profiles specified by 
  IHE (IHE.net). The actors implemented are PIX Manager and PDQ Supplier.  


  Contents
  --------

    Included in this release are the following:

    README.txt 	                This file
    LICENSE.txt	                Software license
    NOTICE.txt	                Copyright and contribution Notice
    openpixpdq-1.2.jar          Stand alone openpixpdq binary executable file
    commons-logging.properties  common logging properties
    log4j.xml                   log4j logging properties
    jetty.properties            jetty container properties
    lib/                        All the libs needed for running the stand alone OpenPIXPDQ
    licenses/                   All the licenses file for the third part libraries distributed
    openempi/conf/              OpenEMPI 2.1.2.1 folder containing OpenEMP configurations
    openpixpdq-web/             Exploded OpenPIPXDQ webapp which can be run in a web container
    sql/                        Contains SQL files to create/update/drop the OpenEMPI database
    

  Requirements
  ------------

     JDK Version	
	 OpenPIXPDQ supports JDK 1.6 or higher.  Note that we have
  	 currently tested this implementation only with JDK 1.6.
    
     Database
         Either Postgresql or MySQL is needed. Our tested database is 
         Postgresql 8.3 and 8.4
                

  Installation and Configuration
  ------------------------------

  Installation and configuration guide is available on the OpenPIXPDQ Project 
  web site   on Open Health Tools (OHT) 
  <https://openpixpdq.projects.openhealthtools.org>.

  
  Documentation
  -------------

  Documentation is available on the OpenPIXPDQ Project web site
  on Open Health Tools (OHT) <https://openpixpdq.projects.openhealthtools.org>.

   
  The Latest Version
  ------------------

  Details of the latest version can be found on the OpenPIXPDQ Project web site 
  on Open Health Tools (OHT) <https://openpixpdq.projects.openhealthtools.org>.


  Problems
  ---------

  Our web page at https://openpixpdq.projects.openhealthtools.org has pointers 
  where you can post questions, report bugs or request features. You'll also 
  find information on how to subscribe to our dev list and forum.


  Licensing
  ---------

  This software is licensed under the terms you may find in the file 
  named "LICENSE.txt" in this directory.

 
  Release Notes:
  --------------
  Details of release notes can be found on the OpenPIXPDQ Project web site 
  on Open Health Tools (OHT) 
  <https://www.projects.openhealthtools.org/sf/go/page1171>.
          
 
  Thanks for using OpenPIXPDQ.

                                 Misys Open Source Solutions (MOSS) - Healthcare                                              
                                         <http://www.misysoss.com/>
